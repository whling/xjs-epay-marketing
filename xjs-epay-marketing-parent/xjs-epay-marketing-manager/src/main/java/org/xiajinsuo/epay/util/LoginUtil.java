package org.xiajinsuo.epay.util;

import io.bestpay.framework.controller.HttpRequestTemplate;
import io.bestpay.framework.resource.Resource;
import org.springframework.util.CollectionUtils;
import org.xiajinsuo.epay.security.Organization;
import org.xiajinsuo.epay.security.User;
import org.xiajinsuo.epay.vo.ResourceNode;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/10/10.
 */
public final class LoginUtil {
    private static final String LOGIN_USER = "LOGIN_USER";
    private static final String LOGIN_USER_ORG = "LOGIN_USER_ORG";
    /**
     * 菜单
     */
    private static final String LOGIN_USER_MENU = "LOGIN_USER_MENU";
    /**
     * 权限
     */
    private static final String LOGIN_USER_RESOURCE = "LOGIN_USER_RESOURCE";
    /**
     * 机构权限
     */
    private static final String LOGIN_USER_RESOURCE_ORG = "LOGIN_USER_RESOURCE_ORG";
    /**
     * 机构代码
     */
    private static final String LOGIN_USER_RESOURCE_ORG_STR = "LOGIN_USER_RESOURCE_ORG_STR";

    public static User getLoginUser(HttpServletRequest request) {
        return (User) request.getSession().getAttribute(LOGIN_USER);
    }

    public static void setLoginUser(HttpServletRequest request, User user) {
        request.getSession().setAttribute(HttpRequestTemplate.USER_ID_KEY, user.getId());
        request.getSession().setAttribute(LOGIN_USER, user);
    }


    public static List<ResourceNode> getMenu(HttpServletRequest request) {
        return (List<ResourceNode>) request.getSession().getAttribute(LOGIN_USER_MENU);
    }


    public static void initResource(HttpServletRequest request, List<Resource> resources, List<Resource> moduleResources) {
        request.getSession().setAttribute(LOGIN_USER_RESOURCE, resources);
        request.getSession().setAttribute(LOGIN_USER_MENU, TreeNodeUtils.buildMenu(resources, moduleResources));
    }

    public static String getRemoteHost(HttpServletRequest request) {
        String ip = request.getHeader("x-forwarded-for");
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
        return ip.equals("0:0:0:0:0:0:0:1") ? "127.0.0.1" : ip;
    }

    public static void initOrgResource(HttpServletRequest request, List<Organization> resources) {
        if (!CollectionUtils.isEmpty(resources)) {
            List<String> orgCodes = new ArrayList<>();
            resources.forEach(item -> orgCodes.add(item.getCode()));
            request.getSession().setAttribute(LOGIN_USER_RESOURCE_ORG_STR, orgCodes);
        }
        request.getSession().setAttribute(LOGIN_USER_RESOURCE_ORG, resources);
    }

    public static List<Organization> getOrgResource(HttpServletRequest request) {
        return (List<Organization>) request.getSession().getAttribute(LOGIN_USER_RESOURCE_ORG);
    }

    public static List<String> getOrgStr(HttpServletRequest request) {
        return (List<String>) request.getSession().getAttribute(LOGIN_USER_RESOURCE_ORG_STR);
    }

    public static void setLoginUserOrg(HttpServletRequest request, Organization organization) {
        request.getSession().setAttribute(LOGIN_USER_ORG, organization);
    }

    public static Organization getLoginUserOrg(HttpServletRequest request) {
        return (Organization) request.getSession().getAttribute(LOGIN_USER_ORG);
    }
}
