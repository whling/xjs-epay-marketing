package org.xiajinsuo.epay.util;

import io.bestpay.framework.base.SpecificPage;

import java.util.HashMap;
import java.util.Map;

/**
 * jQuery-EasyUI:DataGrid数据表格工具类
 *
 * @author tums
 */
public final class DataGridUtils {
    /**
     * Page转换为DataGrid数据格式json字符串
     *
     * @param page
     * @param <T>
     * @return
     */
    public static <T> Map<String, Object> format(SpecificPage<T> page) {
        Map<String, Object> map = new HashMap<String, Object>();
        if (page == null) {
            map.put("total", 0);
            map.put("rows", 0);
        } else {
            map.put("total", page.getTotal());
            map.put("rows", page.getContent());
        }
        return map;
    }
}
