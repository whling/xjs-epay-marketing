package org.xiajinsuo.epay.util;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by jhgj on 2017/8/7.
 */
public class DownloadApplyPersonServlet extends HttpServlet {

    /** 初始化日志引擎 * */
    private final Logger logger = LoggerFactory
            .getLogger(DownloadApplyPersonServlet.class);

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        doPost(request, response);
    }

    // 在doPost()方法中，当servlet收到浏览器发出的Post请求后，实现文件下载
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        logger.info("进入下载文件开始..........");
        String host="";//主机地址
        String port="";//主机端口
        String username="";//服务器用户名
        String password ="";//服务器密码
        String planPath ="";//文件所在服务器路径
        BufferedInputStream bis = null;
        BufferedOutputStream bos = null;
        OutputStream fos = null;

        String fileName = "KJ_CUST_KBYJ";//KJ_CUST_KBYJ20140326.txt
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        String currentDate =   formatter.format(new Date());
        String downloadFile = fileName + currentDate + ".zip";


        PrintWriter out=null;
        Test sftp = new Test(host, Integer.parseInt(port), username,
                password);
        try {
            sftp.connect();
            String filename="";
//	String[] strs=planUrl.split("/");
            String filePath=planPath;
//列出目录下的文件
            List<String> listFiles=sftp.listFiles(filePath);
            boolean isExists=listFiles.contains(downloadFile);
            if(isExists){
                sftp.cd(filePath);
                if(sftp.get(downloadFile)!=null){
                    bis = new BufferedInputStream(sftp.get(downloadFile));
                }
                filename=downloadFile;
                fos = response.getOutputStream();
                bos = new BufferedOutputStream(fos);
                response.setCharacterEncoding("UTF-8");
                response.setContentType("application/x-msdownload;charset=utf-8");
                final String agent = request.getHeader("User-Agent");
                String attachment = "attachment;fileName=";
                String outputFilename = null;

                if (agent.indexOf("Firefox") > 0) {
                    attachment = "attachment;fileName*=";
                    outputFilename = "=?UTF-8?B?" + (new String(Base64.encodeBase64(filename.getBytes("UTF-8")))) + "?=";;
                } else {
                    if (agent.indexOf("MSIE") != -1) {
                        outputFilename = new String(filename.getBytes("gbk"), "iso8859-1");
                    } else {
                        outputFilename = new String(filename.getBytes("UTF-8"), "iso8859-1");
                    }
                }
                response.setHeader("Content-Disposition", attachment + outputFilename);
                int bytesRead = 0;
//输入流进行先读，然后用输出流去写，下面用的是缓冲输入输出流
                byte[] buffer = new byte[8192];
                while ((bytesRead = bis.read(buffer)) != -1) {
                    bos.write(buffer,0,bytesRead);
                }
                bos.flush();
                logger.info("文件下载成功");
            }else{
                response.setCharacterEncoding("utf-8");
                response.setContentType("text/html; charset=UTF-8");
                out=response.getWriter();
                out.println("<html >" +
                        "<body>" +
                        "没有找到你要下载的文件" +
                        "</body>" +
                        "</html>");
            }
        } catch (Exception e) {
            response.setCharacterEncoding("utf-8");
            response.setContentType("text/html; charset=UTF-8");
            out=response.getWriter();
            out.println("<html >" +
                    "<body>" +
                    "没有找到你要下载的文件" +
                    "</body>" +
                    "</html>");
        }finally{
            try {
                sftp.disconnect();
                logger.info("SFTP连接已断开");
            } catch (Exception e) {
                e.printStackTrace();
            }

            if(out!=null){
                out.close();
            }
            logger.info("out已关闭");
            if(bis != null){
                bis.close();
            }
            logger.info("bis已关闭");
            if(bos != null){
                bos.close();
            }
            logger.info("bos已关闭");
        }
    }
}
