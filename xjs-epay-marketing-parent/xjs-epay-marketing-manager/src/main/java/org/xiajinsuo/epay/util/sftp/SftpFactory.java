package org.xiajinsuo.epay.util.sftp;

import com.jcraft.jsch.*;
import org.slf4j.LoggerFactory;

import java.util.Properties;

/**
 * Created by tums on 2017/8/7.
 */
public class SftpFactory {
    Session session = null;
    Channel channel = null;

    private final static SftpFactory sftpFactory = new SftpFactory();

    private SftpFactory() {
    }

    public static SftpFactory getInstance() {
        return sftpFactory;
    }

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(SftpFactory.class);

    public ChannelSftp getChannel(FtpConf ftpConf, int timeout) throws JSchException {
        String ftpHost = ftpConf.getSftpHost();
        int ftpPort = ftpConf.getSftpPort();
        String ftpUserName = ftpConf.getSftpUsername();
        String ftpPassword = ftpConf.getSftpPassword();

        JSch jsch = new JSch(); // 创建JSch对象
        session = jsch.getSession(ftpUserName, ftpHost, ftpPort); // 根据用户名，主机ip，端口获取一个Session对象
        LOGGER.debug("Session created.");
        if (ftpPassword != null) {
            session.setPassword(ftpPassword); // 设置密码
        }
        Properties config = new Properties();
        config.put("StrictHostKeyChecking", "no");
        session.setConfig(config); // 为Session对象设置properties
        session.setTimeout(timeout); // 设置timeout时间
        session.connect(); // 通过Session建立链接
        LOGGER.debug("Session connected.");
        LOGGER.debug("Opening Channel.");
        channel = session.openChannel("sftp"); // 打开SFTP通道
        channel.connect(); // 建立SFTP通道的连接
        LOGGER.debug("Connected successfully to ftpHost = " + ftpHost + ",as ftpUserName = " + ftpUserName
                + ", returning: " + channel);
        return (ChannelSftp) channel;
    }

    public void closeChannel() throws Exception {
        if (channel != null) {
            channel.disconnect();
        }
        if (session != null) {
            session.disconnect();
        }
    }

    public static void main(String[] args) throws JSchException {
        //String sftpHost, String sftpUsername, String sftpPassword, int sftpPort
        FtpConf ftpConf = new FtpConf("image.mypays.cn", "mypays", "Jn!@#5570707", 22);
        SftpFactory.getInstance().getChannel(ftpConf, 3000);
    }
}
