package org.xiajinsuo.epay.business.controller;

import io.bestpay.framework.base.*;
import io.bestpay.framework.controller.HttpRequestTemplate;
import io.bestpay.framework.request.UserHttpRequest;
import org.apache.commons.collections.map.HashedMap;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.xiajinsuo.epay.business.BenefitRule;
import org.xiajinsuo.epay.security.Organization;
import org.xiajinsuo.epay.util.DataGridUtils;
import org.xiajinsuo.epay.util.LoginUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.util.Map;

@SuppressWarnings("all")
/**  RequestBody Controller */
@org.apache.avro.specific.AvroGenerated
@Controller
@RequestMapping("/benefitRule")
public class BenefitRuleController extends io.bestpay.framework.controller.RequestBodyAbstractController<BenefitRule, String> {

    @javax.annotation.Resource
    private org.xiajinsuo.epay.business.protocol.rpc.BenefitRuleRpcProtocol benefitRuleRpcProtocol;

    @javax.annotation.Resource
    private org.xiajinsuo.epay.security.protocol.rpc.OrganizationRpcProtocol organizationRpcProtocol;

    protected org.xiajinsuo.epay.business.protocol.rpc.BenefitRuleRpcProtocol getProtocol() {
        return this.benefitRuleRpcProtocol;
    }

    /**
     * 分润规则列表
     *
     * @return
     * @throws CodeException
     */
    @RequestMapping("/index")
    public String index(Model model, HttpServletRequest request) throws CodeException {
        model.addAttribute("orgList", LoginUtil.getOrgResource(request));
        return "/benefitRule/index";
    }


    /**
     * 查询
     *
     * @param page
     * @param rows
     * @param trans_type
     * @return
     * @throws CodeException
     */
    @RequestMapping("/query")
    @ResponseBody
    public Map<String, Object> query(Long page,
                                     Long rows,
                                     String rate,
                                     Integer referee_cnt_start,
                                     Integer referee_cnt_end,
                                     String org_code,
                                     HttpServletRequest request
    ) throws CodeException, UnsupportedEncodingException {
        AbstractUserRequest abstractUserRequest = new UserHttpRequest();
        SpecificPage<BenefitRule> specificPage = benefitRuleRpcProtocol.query(abstractUserRequest, LoginUtil.getOrgStr(request), page, rows, org_code, rate, referee_cnt_start, referee_cnt_end);
        return DataGridUtils.format(specificPage);
    }

    /**
     * 创建
     *
     * @param parameter
     * @return
     */
    @RequestMapping("/create")
    @ResponseBody
    public Response<Boolean> create(String org_code,
                                    String rate,
                                    int referee_cnt_start,
                                    int referee_cnt_end) {
        return this.doRequest(new HttpRequestTemplate<Boolean>() {
            @Override
            public Boolean doRequest(UserRequest userRequest, HttpServletRequest req, HttpServletResponse resp) throws Exception {
                final BenefitRule e = newInstance();
                Map<String, Object> parameter = new HashedMap();
                parameter.put("org_code", org_code);
                Organization org = organizationRpcProtocol.getByCode(org_code);
                parameter.put("org_name", org.getName());
                parameter.put("rate", rate);
                parameter.put("referee_cnt_start", referee_cnt_start);
                parameter.put("referee_cnt_end", referee_cnt_end);
                parameter.put("memo", "cnt>=" + referee_cnt_start + " && cnt<" + referee_cnt_end);
                SpecificRecordUtils.parse(e, parameter);
                getProtocol().create(e, userRequest);
                return true;
            }

        });
    }

    /**
     * 更新
     *
     * @param parameter
     * @return
     */
    @RequestMapping("/update")
    @ResponseBody
    public Response<Boolean> update(String id,
                                    String org_code,
                                    String rate,
                                    int referee_cnt_start,
                                    int referee_cnt_end) {
        return this.doRequest(new HttpRequestTemplate<Boolean>() {
            @Override
            public Boolean doRequest(UserRequest userRequest, HttpServletRequest req, HttpServletResponse resp) throws Exception {
                final BenefitRule e = newInstance();
                Map<String, Object> parameter = new HashedMap();
                parameter.put("id", id);
                parameter.put("org_code", org_code);
                Organization org = organizationRpcProtocol.getByCode(org_code);
                parameter.put("org_name", org.getName());
                parameter.put("rate", rate);
                parameter.put("referee_cnt_start", referee_cnt_start);
                parameter.put("referee_cnt_end", referee_cnt_end);
                parameter.put("memo", "cnt>=" + referee_cnt_start + " && cnt<" + referee_cnt_end);
                SpecificRecordUtils.parse(e, parameter);
                getProtocol().update(e, userRequest);
                return true;
            }
        });
    }

    /**
     * 删除
     *
     * @param parameter
     * @return
     */
    @RequestMapping("/delete")
    @ResponseBody
    public Response<Boolean> delete(final String id) {
        return this.doRequest(new HttpRequestTemplate<Boolean>() {
            @Override
            public Boolean doRequest(UserRequest userRequest, HttpServletRequest req, HttpServletResponse resp) throws Exception {
                return getProtocol().delete(id, userRequest);
            }
        });
    }

}