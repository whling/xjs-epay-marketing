package org.xiajinsuo.epay.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Administrator on 2016/12/20.
 */
public class DateUtils {
    static final String FORMAT_DATETIME_DEFAULT = "yyyyMMddHHmmss";
    static final String FORMAT_DATE_DEFAULT = "yyyyMMdd";
    static final String FORMAT_MONTH_DEFAULT = "yyyyMM";

    public static String formatDateTime(Date date) {
        return new SimpleDateFormat(FORMAT_DATETIME_DEFAULT).format(date);
    }

    public static String formatDate(Date date) {
        return new SimpleDateFormat(FORMAT_DATE_DEFAULT).format(date);
    }

    /**
     * 查询日期+1,解决查询sql不支持小于等于的问题
     *
     * @param dateStr
     * @return
     */
    public static String formatQueryEndDate(String dateStr) throws ParseException {
        final SimpleDateFormat dateFormat = new SimpleDateFormat(FORMAT_DATE_DEFAULT);
        Date date = dateFormat.parse(dateStr);
        Date newDate = org.apache.commons.lang.time.DateUtils.addDays(date, 1);
        return dateFormat.format(newDate);
    }

    /**
     * 格式化时间字符串为（yyyyMMdd）格式
     * @return
     * @throws ParseException
     */
    public static String formatQueryDate(String dateStr) throws ParseException{
        final SimpleDateFormat dateFormat = new SimpleDateFormat(FORMAT_DATE_DEFAULT);
        Date date = dateFormat.parse(dateStr);
        return dateFormat.format(date);
    }
}
