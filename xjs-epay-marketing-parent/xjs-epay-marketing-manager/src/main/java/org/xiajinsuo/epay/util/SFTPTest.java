package org.xiajinsuo.epay.util;

import com.jcraft.jsch.ChannelSftp;

import java.io.File;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Created by jhgj on 2017/8/7.
 */
public class SFTPTest {
    public static void main(String[] args) throws Exception {

        Properties properties = new Properties();
        InputStream in = Thread.currentThread().getClass().getResourceAsStream("/database.properties");

        properties.load(in);

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, Integer.valueOf(properties.getProperty("indexday")));
        String yesteDay = new SimpleDateFormat("yyyyMMdd").format(calendar.getTime());
        SFTPTest test = new SFTPTest();


        Map<String, String> sftpDetails = new HashMap<String, String>();
        // 设置主机ip，端口，用户名，密码
        sftpDetails.put(SFTPConstants.SFTP_REQ_HOST, "image.mypays.cn");
        sftpDetails.put(SFTPConstants.SFTP_REQ_USERNAME, "mypays");
        sftpDetails.put(SFTPConstants.SFTP_REQ_PASSWORD, "Jn!@#5570707");
        sftpDetails.put(SFTPConstants.SFTP_REQ_PORT, "22");


        SFTPChannel channel = test.getSFTPChannel();
        ChannelSftp chSftp = channel.getChannel(sftpDetails, 60000);

        String pathINV = properties.getProperty("pathINV");
        String filePath = pathINV + yesteDay;

        String a = "/home/adb/JKLIFE/INVEST/";
        String dst = a + yesteDay; // 目标文件名
        int countDirectory = 0;//文件个数
        File folder = new File(filePath); // 自定义文件路径
        if (folder.exists() && folder.isDirectory()) {
            File files[] = folder.listFiles();
            for (File fileIndex : files) {
                countDirectory++;
                String src = fileIndex.toString();
                chSftp.put(src, dst, ChannelSftp.OVERWRITE); // 代码段2
                System.out.println(countDirectory);
                System.out.println("上传成功");
            }
        } else {
            System.out.println("文件不存在");
        }
        chSftp.quit();
        channel.closeChannel();
    }
    public SFTPChannel getSFTPChannel() {
        return new SFTPChannel();
    }
}
