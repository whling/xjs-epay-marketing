package org.xiajinsuo.epay.vo;

/**
 * Created by Administrator on 2017/3/16.
 */
public class TransStatistics {
    private String referee_family_name;
    private String agent_family_name;
    private String org_name;
    private String username;
    private String family_name;
    private String trans_type;
    private String sub_trans_type;
    private int total_cnt;
    private int total_amount;
    private int total_pay_amount;

    public int getTotal_cnt() {
        return total_cnt;
    }

    public void setTotal_cnt(int total_cnt) {
        this.total_cnt = total_cnt;
    }

    public int getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(int total_amount) {
        this.total_amount = total_amount;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFamily_name() {
        return family_name;
    }

    public void setFamily_name(String family_name) {
        this.family_name = family_name;
    }

    public String getReferee_family_name() {
        return referee_family_name;
    }

    public void setReferee_family_name(String referee_family_name) {
        this.referee_family_name = referee_family_name;
    }

    public String getTrans_type() {
        return trans_type;
    }

    public void setTrans_type(String trans_type) {
        this.trans_type = trans_type;
    }

    public TransStatistics(String org_name, String agent_family_name, String referee_family_name, String username, String family_name, String trans_type, String sub_trans_type, int total_cnt, int total_amount, int total_pay_amount) {
        this.org_name = org_name;
        this.agent_family_name = agent_family_name;
        this.referee_family_name = referee_family_name;
        this.username = username;
        this.family_name = family_name;
        this.trans_type = trans_type;
        this.sub_trans_type = sub_trans_type;
        this.total_cnt = total_cnt;
        this.total_amount = total_amount;
        this.total_pay_amount = total_pay_amount;
    }

    public int getTotal_pay_amount() {
        return total_pay_amount;
    }

    public void setTotal_pay_amount(int total_pay_amount) {
        this.total_pay_amount = total_pay_amount;
    }

    public String getAgent_family_name() {
        return agent_family_name;
    }

    public void setAgent_family_name(String agent_family_name) {
        this.agent_family_name = agent_family_name;
    }

    public String getOrg_name() {
        return org_name;
    }

    public void setOrg_name(String org_name) {
        this.org_name = org_name;
    }

    public String getSub_trans_type() {
        return sub_trans_type;
    }

    public void setSub_trans_type(String sub_trans_type) {
        this.sub_trans_type = sub_trans_type;
    }
}
