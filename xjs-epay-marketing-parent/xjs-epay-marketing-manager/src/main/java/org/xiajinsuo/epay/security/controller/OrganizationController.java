package org.xiajinsuo.epay.security.controller;

import io.bestpay.framework.base.*;
import io.bestpay.framework.controller.HttpRequestTemplate;
import io.bestpay.framework.request.UserHttpRequest;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.WebUtils;
import org.xiajinsuo.epay.business.Card;
import org.xiajinsuo.epay.security.Organization;
import org.xiajinsuo.epay.security.Sex;
import org.xiajinsuo.epay.security.User;
import org.xiajinsuo.epay.security.UserType;
import org.xiajinsuo.epay.util.*;
import org.xiajinsuo.epay.util.sftp.SftpUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.*;

@SuppressWarnings("all")
/**  RequestBody Controller */
@org.apache.avro.specific.AvroGenerated
@Controller
@RequestMapping("/organization")
public class OrganizationController extends io.bestpay.framework.controller.RequestBodyAbstractController<Organization, String> {

    @javax.annotation.Resource
    private org.xiajinsuo.epay.security.protocol.rpc.OrganizationRpcProtocol organizationRpcProtocol;
    @javax.annotation.Resource
    private org.xiajinsuo.epay.security.protocol.rpc.UserRpcProtocol userRpcProtocol;
    @javax.annotation.Resource
    private org.xiajinsuo.epay.business.protocol.rpc.CardRpcProtocol cardRpcProtocol;
    @javax.annotation.Resource
    private org.xiajinsuo.epay.security.protocol.rpc.PrivilegeRpcProtocol privilegeRpcProtocol;

    protected org.xiajinsuo.epay.security.protocol.rpc.OrganizationRpcProtocol getProtocol() {
        return this.organizationRpcProtocol;
    }

    /**
     * 机构列表
     *
     * @return
     * @throws CodeException
     */
    @RequestMapping("/index")
    public String index(Model model, HttpServletRequest request) throws CodeException {
        model.addAttribute("orgList", LoginUtil.getOrgResource(request));
        return "/organization/index";
    }


    /**
     * 查询
     *
     * @param page
     * @param rows
     * @param trans_type
     * @return
     * @throws CodeException
     */
    @RequestMapping("/query")
    @ResponseBody
    public Map<String, Object> query(Long page,
                                     Long rows,
                                     String name,
                                     String linkman,
                                     String type,
                                     Boolean enable,
                                     HttpServletRequest request) throws CodeException, UnsupportedEncodingException {
        AbstractUserRequest abstractUserRequest = new UserHttpRequest();
        Map<String, Object> queryParams = new HashMap<>();
        queryParams.put("name", name);
        queryParams.put("linkman", linkman);
        queryParams.put("type", type);
        queryParams.put("enable", enable);
        SpecificPage<Organization> specificPage = organizationRpcProtocol.query(abstractUserRequest, LoginUtil.getOrgStr(request), page, rows, queryParams);
        return DataGridUtils.format(specificPage);
    }

    /**
     * 创建
     *
     * @param parameter
     * @return
     */
    @RequestMapping("/create")
    @ResponseBody
    public Response<Boolean> create(HttpServletRequest request,  final @RequestPart(required = false) MultipartFile logo,
                                    final @RequestPart(required = false) MultipartFile logo_small,
                                    final @RequestPart(required = false) MultipartFile weixin_qrcode) {
        return this.doRequest(new HttpRequestTemplate<Boolean>() {
            @Override
            public Boolean doRequest(UserRequest userRequest, HttpServletRequest req, HttpServletResponse resp) throws Exception {
                Map<String, Object> parameter = WebUtils.getParametersStartingWith(request, "");
                String _linkman = (String) parameter.get("linkman");
                String logoUrl = SftpUtils.uploadMultipart(logo, "/logo");
                String logoSmallUrl = SftpUtils.uploadMultipart(logo_small, "/logo");
                final Organization e = newInstance();
                parameter.put("logo", logoUrl);
                parameter.put("logo_small", logoSmallUrl);
                String open_custom_service= request.getParameter("open_custom_service");
                String open_agency_profit= request.getParameter("open_agency_profit");
                if("1".equals(open_custom_service)){
                    parameter.put("open_custom_service", true);
                }if("0".equals(open_custom_service)){
                    parameter.put("open_custom_service", false);
                }
                if("1".equals(open_agency_profit)){
                    parameter.put("open_agency_profit", true);
                }if("0".equals(open_agency_profit)){
                    parameter.put("open_agency_profit", false);
                }
                String  open_credit_apply = request.getParameter("open_credit_apply");
                if("1".equals(open_credit_apply)){
                    parameter.put("open_credit_apply", true);
                }if("0".equals(open_credit_apply)){
                    parameter.put("open_credit_apply", false);
                }
                String open_borrow = request.getParameter("open_borrow");
                if ("1".equals(open_borrow)) {
                    parameter.put("open_borrow", true);
                }
                if ("0".equals(open_borrow)) {
                    parameter.put("open_borrow", false);
                }
                String  open_register = request.getParameter("open_register");
                if("1".equals(open_register)){
                    parameter.put("open_register", true);
                } if("0".equals(open_register)){
                    parameter.put("open_register", false);
                }
                String  open_agent_grant = request.getParameter("open_agent_grant");
                if("1".equals(open_agent_grant)){
                    parameter.put("open_agent_grant", true);
                }if("0".equals(open_agent_grant)){
                    parameter.put("open_agent_grant", false);
                }

                String weixinName = (String)parameter.get("weixin_name");
                String weixinAppId = (String)parameter.get("weixin_appId");
                String weixinRedirectUri = (String)parameter.get("weixin_redirect_uri");
                String weixinSecret = (String)parameter.get("weixin_secret");
                String weixinQrcode = SftpUtils.uploadMultipart(weixin_qrcode, "/logo");
                parameter.put("weixin_qrcode",weixinQrcode);

                SpecificRecordUtils.parse(e, parameter);
                String linkmanMobile = e.getLinkmanMobile();
                String password = linkmanMobile.substring(5);
                String signedPassword = PwdUtil.sign(password);
                Date today = new Date();
                User user = User.newBuilder()
                        .setUsername(linkmanMobile)
                        .setPassword(signedPassword)
                        .setFamilyName(_linkman)
                        .setIdcard(e.getLinkmanIdcard())
                        .setEnable(true)
                        .setAvailableAmt(0)
                        .setHistoryAmt(0)
                        .setRate("0")
                        .setExtendAgent(true)
                        .setRegisterTime(DateUtils.formatDateTime(today))
                        .setType(UserType.ORG_ROOT)
                        .setToken(UUID.randomUUID().toString().replace("-", ""))
                        .setSex(Sex.UNKNOWN)
                        .build();
                Organization organization = organizationRpcProtocol.create(e, user, userRequest);
                initOrgResource(LoginUtil.getLoginUserOrg(req), LoginUtil.getLoginUser(req), req);
                return true;
            }

        });
    }

    /**
     * 更新
     *
     * @param parameter
     * @return
     */
    @RequestMapping("/update")
    @ResponseBody
    public Response<Boolean> update(HttpServletRequest request, final @RequestPart(required = false) MultipartFile logo,
                                    final @RequestPart(required = false) MultipartFile logo_small,
                                    final @RequestPart(required = false) MultipartFile weixin_qrcode) {
        return this.doRequest(new HttpRequestTemplate<Boolean>() {
            @Override
            public Boolean doRequest(UserRequest userRequest, HttpServletRequest req, HttpServletResponse resp) throws Exception {
                Map<String, Object> parameter = WebUtils.getParametersStartingWith(request, "");
                String _linkman = (String) parameter.get("linkman");

                String logoUrl = SftpUtils.uploadMultipart(logo, "/logo");
                String logoSmallUrl = SftpUtils.uploadMultipart(logo_small, "/logo");
                String weixinQrcode = SftpUtils.uploadMultipart(weixin_qrcode, "/logo");
                final Organization e = newInstance();
                parameter.put("logo", logoUrl);
                parameter.put("logo_small", logoSmallUrl);
                parameter.put("weixin_qrcode",weixinQrcode);
                String open_custom_service= request.getParameter("open_custom_service");
                if("1".equals(open_custom_service)){
                    parameter.put("open_custom_service", true);
                }if("0".equals(open_custom_service)){
                    parameter.put("open_custom_service", false);
                }
                String  open_credit_apply = request.getParameter("open_credit_apply");
                if("1".equals(open_credit_apply)){
                    parameter.put("open_credit_apply", true);
                }if("0".equals(open_credit_apply)){
                    parameter.put("open_credit_apply", false);
                }
                String open_agency_profit= request.getParameter("open_agency_profit");
                if("1".equals(open_agency_profit)){
                    parameter.put("open_agency_profit", true);
                }if("0".equals(open_agency_profit)){
                    parameter.put("open_agency_profit", false);
                }
                String  open_borrow = request.getParameter("open_borrow");
                if("1".equals(open_borrow)){
                    parameter.put("open_borrow", true);
                } if("0".equals(open_borrow)){
                    parameter.put("open_borrow", false);
                }
                String  open_register = request.getParameter("open_register");
                if("1".equals(open_register)){
                    parameter.put("open_register", true);
                } if("0".equals(open_register)){
                    parameter.put("open_register", false);
                }
                String  open_agent_grant = request.getParameter("open_agent_grant");
                if("1".equals(open_agent_grant)){
                    parameter.put("open_agent_grant", true);
                }if("0".equals(open_agent_grant)){
                    parameter.put("open_agent_grant", false);
                }
                SpecificRecordUtils.parse(e, parameter);
                getProtocol().update(e, userRequest);
                return true;
            }
        });
    }

    /**
     * 删除
     *
     * @param parameter
     * @return
     */
    @RequestMapping("/delete")
    @ResponseBody
    public Response<Boolean> delete(final String id) {
        return this.doRequest(new HttpRequestTemplate<Boolean>() {
            @Override
            public Boolean doRequest(UserRequest userRequest, HttpServletRequest req, HttpServletResponse resp) throws Exception {
                return getProtocol().delete(id, userRequest);
            }
        });
    }

    /**
     * 获取机构详情
     * 机构超级管理员
     * 收款银行卡
     *
     * @param orgCode
     * @return
     */
    @RequestMapping("/detail")
    @ResponseBody
    public Response<Map<String, Object>> detail(@RequestParam final String orgCode) {
        return this.doRequest(new HttpRequestTemplate<Map<String, Object>>() {
            @Override
            public Map<String, Object> doRequest(UserRequest userRequest, HttpServletRequest req, HttpServletResponse resp) throws Exception {
                Map<String, Object> result = new HashMap<>();
                User user = userRpcProtocol.findOrgRoot(orgCode);
                Card card = cardRpcProtocol.getDefaultByUserId(user.getId());
                result.put("user", user);
                result.put("card", card);
                return result;
            }
        });
    }

    /**
     * 用户机构权限
     *
     * @param org
     * @param loginUser
     * @param request
     * @return
     */
    private void initOrgResource(Organization org, User loginUser, HttpServletRequest request) throws CodeException {
        UserType type = loginUser.getType();
        String userId = loginUser.getId();
        List<Organization> resources = new ArrayList<Organization>();
        resources.add(org);
        String orgCode = org.getCode();
        if (UserType.ORG_ROOT.equals(type)) {//拥有下级机构权限
            List<Organization> organizationList = organizationRpcProtocol.findByParentCode(orgCode);
            if (CollectionUtils.isNotEmpty(organizationList)) {
                resources.addAll(organizationList);
            }
        } else if (UserType.ORG_ADMIN.equals(type)) {
            List<Organization> organizationList = privilegeRpcProtocol.findOrgResourceByUser(userId);
            if (CollectionUtils.isNotEmpty(organizationList)) {
                resources.addAll(organizationList);
            }
        }
        LoginUtil.initOrgResource(request, resources);
    }
  /*  *//**
     * 获取机构名称
     *
     * @param orgCode
     * @return
     *//*
    @RequestMapping("/getAppName")
    @ResponseBody
    public Response<String> getOrgName(@RequestParam final String code) {
        return this.doRequest(new HttpRequestTemplate<String>() {
            @Override
            public String doRequest(UserRequest userRequest, HttpServletRequest req, HttpServletResponse resp) throws Exception {
                Organization organization = organizationRpcProtocol.getByCode(code);
                return organization.getAppName();

            }
        });
    }*/

    /**
     * 启用/禁用
     *
     * @param userId
     * @param _enable
     * @param request
     * @return
     */
    @RequestMapping("/change")
    @ResponseBody
    public Response<Boolean> change(final String id, final Boolean _enable,  final HttpServletRequest request) {

        return this.doRequest((new HttpRequestTemplate<Boolean>() {

            @Override
            public Boolean doRequest(UserRequest userRequest, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
                if (org.apache.commons.lang.StringUtils.isNotEmpty(id) && _enable != null) {

                    return getProtocol().updateEnable(id, _enable, new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()), userRequest.getPrincipal());
                }
                return false;
            }
        }));
    }
}