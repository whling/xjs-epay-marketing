package org.xiajinsuo.epay.filter;

import org.springframework.util.StringUtils;
import org.xiajinsuo.epay.security.User;
import org.xiajinsuo.epay.util.LoginUtil;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Administrator on 2016/10/11.
 */
public class LoginFilter implements Filter {
    //白名单，不过滤
    private String[] whiteList;
    //重定向页面
    private String redirect = "login.html";//default

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        String whiteList = filterConfig.getInitParameter("whiteList");
        if (!StringUtils.isEmpty(whiteList)) {
            this.whiteList = whiteList.split(",");
        }
        String redirect = filterConfig.getInitParameter("redirect");
        if (!StringUtils.isEmpty(redirect)) {
            this.redirect = redirect;
        }
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        String path = request.getServletPath().toLowerCase();
        for (String url : whiteList) {
            if (path.endsWith(url)) {
                filterChain.doFilter(servletRequest, servletResponse);
                return;
            }
        }
        User user = LoginUtil.getLoginUser(request);
        if (user == null) {
            response.sendRedirect(redirect);
        } else {
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    @Override
    public void destroy() {

    }
}
