package org.xiajinsuo.epay.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by Administrator on 2016/11/14.
 */
@Component
public final class ConfigUtils {
    private static String ftp_user;
    private static String ftp_password;
    private static String ftp_path;
    private static String ftp_url;
    private static String qr_img_api;
    private static String epay_mypays_cn;
    private static String ftp_secondPath;


    /**
     * 短信配置
     */
    private static String msg_futurewireless_url;
    private static String msg_futurewireless_cust_code;
    private static String msg_futurewireless_password;
    private static String msg_futurewireless_sp_code;

    @Value("#{applicationCfg['msg.futurewireless.url']}")
    public void setMsg_futurewireless_url(String msg_futurewireless_url) {
        ConfigUtils.msg_futurewireless_url = msg_futurewireless_url;
    }
    @Value("#{applicationCfg['sys.ftp.secondPath']}")
    public void setFtp_secondPath(String ftp_secondPath) {
        ConfigUtils.ftp_secondPath = ftp_secondPath;
    }
    @Value("#{applicationCfg['msg.futurewireless.cust_code']}")
    public void setMsg_futurewireless_cust_code(String msg_futurewireless_cust_code) {
        ConfigUtils.msg_futurewireless_cust_code = msg_futurewireless_cust_code;
    }

    @Value("#{applicationCfg['msg.futurewireless.password']}")
    public void setMsg_futurewireless_password(String msg_futurewireless_password) {
        ConfigUtils.msg_futurewireless_password = msg_futurewireless_password;
    }

    @Value("#{applicationCfg['msg.futurewireless.sp_code']}")
    public void setMsg_futurewireless_sp_code(String msg_futurewireless_sp_code) {
        ConfigUtils.msg_futurewireless_sp_code = msg_futurewireless_sp_code;
    }

    @Value("#{applicationCfg['sys.epay_mypays_cn']}")
    public  void setEpay_mypays_cn(String epay_mypays_cn) {
        ConfigUtils.epay_mypays_cn = epay_mypays_cn;
    }

    @Value("#{applicationCfg['sys.qr_img_api']}")
    public void setQr_img_api(String qr_img_api) {
        ConfigUtils.qr_img_api = qr_img_api;
    }


    @Value("#{applicationCfg['ftp.user']}")
    public void setFtp_user(String ftp_user) {
        ConfigUtils.ftp_user = ftp_user;
    }

    @Value("#{applicationCfg['ftp.password']}")
    public void setFtp_password(String ftp_password) {
        ConfigUtils.ftp_password = ftp_password;
    }

    @Value("#{applicationCfg['ftp.path']}")
    public void setFtp_path(String ftp_path) {
        ConfigUtils.ftp_path = ftp_path;
    }

    @Value("#{applicationCfg['ftp.url']}")
    public void setFtp_url(String ftp_url) {
        ConfigUtils.ftp_url = ftp_url;
    }

    public static String getMsg_futurewireless_url() {
        return msg_futurewireless_url;
    }

    public static String getMsg_futurewireless_cust_code() {
        return msg_futurewireless_cust_code;
    }

    public static String getMsg_futurewireless_password() {
        return msg_futurewireless_password;
    }

    public static String getMsg_futurewireless_sp_code() {
        return msg_futurewireless_sp_code;
    }


    public static String getFtp_user() {
        return ftp_user;
    }

    public static String getFtp_password() {
        return ftp_password;
    }

    public static String getFtp_path() {
        return ftp_path;
    }

    public static String getFtp_url() {
        return ftp_url;
    }

    public static String getFtp_secondPath() {
        return ftp_secondPath;
    }

    public static String getQr_img_api() {
        return qr_img_api;
    }

    public static String getEpay_mypays_cn() {
        return epay_mypays_cn;
    }
}
