package org.xiajinsuo.epay.util;

import io.bestpay.framework.base.SpecificRecordUtils;
import io.bestpay.framework.resource.Resource;
import io.bestpay.framework.resource.ResourceType;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.xiajinsuo.epay.vo.ResourceNode;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/12/20.
 */
public class TreeNodeUtils {
    /**
     * 资源三级分类：应用->模块->视图/资源
     *
     * @param resources
     * @return
     */
    public static List<ResourceNode> buildNode(List<Resource> resources) {
        List<ResourceNode> top = new ArrayList();
        if (CollectionUtils.isEmpty(resources)) {
            return top;
        }
        List<ResourceNode> second = new ArrayList();
        List<ResourceNode> three = new ArrayList();
        for (Resource item : resources) {//1 找出 top
            ResourceNode resourceNode = toTreeNode(item);
            if (StringUtils.isEmpty(item.getParentCode())) {
                top.add(resourceNode);
            } else {
                second.add(resourceNode);
            }
        }
        for (ResourceNode item : second) {//2 匹配second 同时取出三级
            String parentCode = item.getParentCode();
            boolean isThree = true;
            for (ResourceNode node : top) {
                if (node.getCode().equals(parentCode)) {
                    node.getChildren().add(item);
                    isThree = false;
                    break;
                }
            }
            if (isThree) {
                three.add(item);
            }
        }
        for (ResourceNode item : three) {//3 匹配three
            String parentCode = item.getParentCode();
            for (ResourceNode node : second) {
                if (node.getCode().equals(parentCode)) {
                    node.getChildren().add(item);
                    break;
                }
            }
        }
        return top;
    }

    /**
     * 根据顶层应用，找出资源二级分类：模块->视图
     *
     * @param resources
     * @param moduleResources
     * @return
     */
    public static List<ResourceNode> buildMenu(List<Resource> resources, List<Resource> moduleResources) {
        List<ResourceNode> second = new ArrayList();
        if (CollectionUtils.isEmpty(resources)) {
            return second;
        }
        for (Resource item : moduleResources) {//模型
            String parentCode = item.getCode();
            for (Resource node : resources) {
                String nodeParentCode = node.getParentCode();
                if (StringUtils.isNotEmpty(nodeParentCode) && nodeParentCode.equals(parentCode)) {
                    second.add(toTreeNode(item));
                    break;
                }
            }
        }
        List<ResourceNode> three = new ArrayList();
        for (Resource item : resources) {//1 找出 second
            ResourceNode resourceNode = toTreeNode(item);
            if (item.getResourceType() == ResourceType.VIEW) {
                three.add(resourceNode);
            }
        }
        for (ResourceNode item : three) {//2 匹配 three
            String parentCode = item.getParentCode();
            for (ResourceNode node : second) {
                if (node.getCode().equals(parentCode)) {
                    node.getChildren().add(item);
                    break;
                }
            }
        }
        return second;
    }

    /**
     * @param item
     * @return
     */
    public static ResourceNode toTreeNode(Resource item) {
        if (item == null) {
            return null;
        }
        ResourceNode node = (ResourceNode) SpecificRecordUtils.newInstanceAndCopy(ResourceNode.class, item);
        node.setText(item.getName());
        return node;
    }
}
