package org.xiajinsuo.epay.util;

import java.io.UnsupportedEncodingException;

/**
 * Created by Administrator on 2016/12/23.
 */
public class StringUtils extends org.apache.commons.lang.StringUtils {
    public static String iso_8859_1_2_utf8(String value) {
        try {
            return isEmpty(value) ? "" : new String(value.getBytes("iso-8859-1"), "utf-8");
        } catch (UnsupportedEncodingException e) {
            return "";
        }
    }
}
