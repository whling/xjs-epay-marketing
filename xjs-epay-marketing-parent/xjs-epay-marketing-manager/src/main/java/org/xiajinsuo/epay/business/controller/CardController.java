/**
 * Autogenerated by bestpay-maven-plugin
 * DO NOT EDIT DIRECTLY
 *
 * @Copyright 2016 www.bestpay.io Inc. All rights reserved.
 */
package org.xiajinsuo.epay.business.controller;

import io.bestpay.framework.base.*;
import io.bestpay.framework.controller.HttpRequestTemplate;
import io.bestpay.framework.query.QueryType;
import io.bestpay.framework.request.UserHttpRequest;
import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.xiajinsuo.epay.business.Card;
import org.xiajinsuo.epay.security.Organization;
import org.xiajinsuo.epay.security.UserType;
import org.xiajinsuo.epay.util.DataGridUtils;
import org.xiajinsuo.epay.util.ExcelUtil;
import org.xiajinsuo.epay.util.LoginUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.util.Map;

@SuppressWarnings("all")
/** 用户 RequestBody Controller */
@org.apache.avro.specific.AvroGenerated
@Controller
@RequestMapping("/card")
public class CardController extends io.bestpay.framework.controller.RequestBodyAbstractController<Card, String> {

    @javax.annotation.Resource
    private org.xiajinsuo.epay.business.protocol.rpc.CardRpcProtocol cardRpcProtocol;


    protected org.xiajinsuo.epay.business.protocol.rpc.CardRpcProtocol getProtocol() {
        return this.cardRpcProtocol;
    }

    /**
     * 银行卡列表
     *
     * @param model
     * @param request
     * @return
     * @throws CodeException
     */
    @RequestMapping("/index")
    public String index(Model model, HttpServletRequest request) throws CodeException {
        Organization org = LoginUtil.getLoginUserOrg(request);
        model.addAttribute("orgList", LoginUtil.getOrgResource(request));
        model.addAttribute("showOrg", LoginUtil.getLoginUser(request).getType() == UserType.ORG_ROOT && StringUtils.isEmpty(org.getParentCode()));
        return "/card/index";
    }


    /**
     * 查询
     *
     * @param page
     * @param rows
     * @param username
     * @param family_name
     * @param telphone
     * @param email
     * @return
     * @throws CodeException
     */
    @RequestMapping("/query")
    @ResponseBody
    public Map<String, Object> query(Long page, Long rows,
                                     String username,
                                     String mobile,
                                     String family_name,
                                     String card_type,
                                     String bank_name,
                                     String org_code,
                                     HttpServletRequest request
    ) throws CodeException, UnsupportedEncodingException {
        AbstractUserRequest abstractUserRequest = new UserHttpRequest();
        SpecificPage<Card> specificPage = cardRpcProtocol.query(abstractUserRequest, LoginUtil.getOrgStr(request), page, rows, username, mobile, family_name, card_type, bank_name, org_code);
        return DataGridUtils.format(specificPage);
    }

    /**
     * 用户默认收款卡
     *
     * @param userId
     * @return
     */
    @RequestMapping("/default")
    @ResponseBody
    public Response<Card> queryDefault(@RequestParam final String userId) {

        return this.doRequest(new HttpRequestTemplate<Card>() {

            @Override
            public Card doRequest(UserRequest userRequest, HttpServletRequest req, HttpServletResponse resp) throws Exception {
                return cardRpcProtocol.getDefaultByUserId(userId);
            }

        });
    }

    /**
     * 导出
     *
     * @param username
     * @param mobile
     * @param family_name
     * @param card_type
     * @param bank_name
     * @param response
     * @throws CodeException
     * @throws UnsupportedEncodingException
     */
    @RequestMapping("/export")
    public void export(
            String username,
            String mobile,
            String family_name,
            String card_type,
            String bank_name, HttpServletResponse response
    ) throws CodeException, UnsupportedEncodingException {
        Long page = 1l, rows = 1000L;
        AbstractUserRequest abstractUserRequest = new UserHttpRequest();
        Map<String, Object> parameters = new HashedMap();
        parameters.put(QueryType.createQueryParameter(QueryType.EQ, "username"), username);
        parameters.put(QueryType.createQueryParameter(QueryType.EQ, "mobile"), mobile);
        parameters.put(QueryType.createQueryParameter(QueryType.EQ, "card_type"), card_type);
        parameters.put(QueryType.createQueryParameter(QueryType.LIKE, "bank_name"), bank_name);
        parameters.put(QueryType.createQueryParameter(QueryType.LIKE, "family_name"), family_name);
        parameters.put(QueryType.createOrderParameter("username"), QueryType.OrderType.DESC);
        abstractUserRequest.addParameters(parameters);
        SpecificPage<Card> specificPage = getProtocol().query(abstractUserRequest, page, rows);
        ExcelUtil.exportCard(specificPage.getContent(), response);
    }
}