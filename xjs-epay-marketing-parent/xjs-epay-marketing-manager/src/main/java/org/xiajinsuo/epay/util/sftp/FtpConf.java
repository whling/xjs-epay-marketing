package org.xiajinsuo.epay.util.sftp;

/**
 * Created by tums on 2017/8/7.
 */
public class FtpConf {
    public String sftpHost;
    public String sftpUsername;
    public String sftpPassword;
    public String sftpDir;
    public int sftpPort;

    public FtpConf(String sftpHost, String sftpUsername, String sftpPassword, int sftpPort) {
        this.sftpHost = sftpHost;
        this.sftpUsername = sftpUsername;
        this.sftpPassword = sftpPassword;
        this.sftpPort = sftpPort;
    }

    public FtpConf() {
    }

    public String getSftpHost() {
        return sftpHost;
    }

    public void setSftpHost(String sftpHost) {
        this.sftpHost = sftpHost;
    }

    public String getSftpUsername() {
        return sftpUsername;
    }

    public void setSftpUsername(String sftpUsername) {
        this.sftpUsername = sftpUsername;
    }

    public String getSftpPassword() {
        return sftpPassword;
    }

    public void setSftpPassword(String sftpPassword) {
        this.sftpPassword = sftpPassword;
    }

    public int getSftpPort() {
        return sftpPort;
    }

    public void setSftpPort(int sftpPort) {
        this.sftpPort = sftpPort;
    }

    public FtpConf(String sftpHost, String sftpUsername, String sftpPassword, String sftpDir, int sftpPort) {
        this.sftpHost = sftpHost;
        this.sftpUsername = sftpUsername;
        this.sftpPassword = sftpPassword;
        this.sftpDir = sftpDir;
        this.sftpPort = sftpPort;
    }

    public String getSftpDir() {
        return sftpDir;
    }

    public void setSftpDir(String sftpDir) {
        this.sftpDir = sftpDir;
    }
}
