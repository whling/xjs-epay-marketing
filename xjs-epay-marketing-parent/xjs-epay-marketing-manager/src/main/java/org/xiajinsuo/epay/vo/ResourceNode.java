package org.xiajinsuo.epay.vo;

import io.bestpay.framework.resource.Resource;

import java.util.ArrayList;
import java.util.List;

/**
 * 构建 treegrid 节点
 */
public class ResourceNode extends Resource {
    /**
     * note 显示用
     */
    private String text;
    private final List<ResourceNode> children = new ArrayList();

    public List<ResourceNode> getChildren() {
        return children;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
