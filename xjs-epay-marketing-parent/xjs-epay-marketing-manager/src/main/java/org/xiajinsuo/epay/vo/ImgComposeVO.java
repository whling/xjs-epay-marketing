package org.xiajinsuo.epay.vo;

import io.bestpay.framework.resource.Resource;

/**
 * Created by jhgj on 2017/7/28.
 */
public class ImgComposeVO {
    private String rate_img_url;
    private String logo_x;
    private String logo_y;
    private String logo_t;
    private String logo_l;
    private String code_x;
    private String code_y;
    private String code_t;
    private String code_l;
    private String rate_x;
    private String rate_y;
    private String rate_t;
    private String rate_l;
    public String getRate_img_url() {
        return rate_img_url;
    }

    public void setRate_img_url(String rate_img_url) {
        this.rate_img_url = rate_img_url;
    }

    public String getLogo_x() {
        return logo_x;
    }

    public void setLogo_x(String logo_x) {
        this.logo_x = logo_x;
    }

    public String getLogo_y() {
        return logo_y;
    }

    public void setLogo_y(String logo_y) {
        this.logo_y = logo_y;
    }

    public String getLogo_t() {
        return logo_t;
    }

    public void setLogo_t(String logo_t) {
        this.logo_t = logo_t;
    }

    public String getLogo_l() {
        return logo_l;
    }

    public void setLogo_l(String logo_l) {
        this.logo_l = logo_l;
    }

    public String getCode_x() {
        return code_x;
    }

    public void setCode_x(String code_x) {
        this.code_x = code_x;
    }

    public String getCode_y() {
        return code_y;
    }

    public void setCode_y(String code_y) {
        this.code_y = code_y;
    }

    public String getCode_t() {
        return code_t;
    }

    public void setCode_t(String code_t) {
        this.code_t = code_t;
    }

    public String getCode_l() {
        return code_l;
    }

    public void setCode_l(String code_l) {
        this.code_l = code_l;
    }

    public String getRate_x() {
        return rate_x;
    }

    public void setRate_x(String rate_x) {
        this.rate_x = rate_x;
    }

    public String getRate_y() {
        return rate_y;
    }

    public void setRate_y(String rate_y) {
        this.rate_y = rate_y;
    }

    public String getRate_t() {
        return rate_t;
    }

    public void setRate_t(String rate_t) {
        this.rate_t = rate_t;
    }

    public String getRate_l() {
        return rate_l;
    }

    public void setRate_l(String rate_l) {
        this.rate_l = rate_l;
    }

    @Override
    public String toString() {
        return "ImgComposeVO{" +
                "rate_img_url='" + rate_img_url + '\'' +
                ", logo_x='" + logo_x + '\'' +
                ", logo_y='" + logo_y + '\'' +
                ", logo_t='" + logo_t + '\'' +
                ", logo_l='" + logo_l + '\'' +
                ", code_x='" + code_x + '\'' +
                ", code_y='" + code_y + '\'' +
                ", code_t='" + code_t + '\'' +
                ", code_l='" + code_l + '\'' +
                ", rate_x='" + rate_x + '\'' +
                ", rate_y='" + rate_y + '\'' +
                ", rate_t='" + rate_t + '\'' +
                ", rate_l='" + rate_l + '\'' +
                '}';
    }
}
