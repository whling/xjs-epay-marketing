package org.xiajinsuo.epay.util.sftp;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;
import org.xiajinsuo.epay.util.ConfigUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.UUID;

/**
 * sftp 操作工具类
 */
public class SftpUtils {
    private final static Logger logger = LoggerFactory.getLogger(SftpUtils.class);

    /**
     * 文件上传
     *
     * @param file
     * @param fileDir 文件目录 epay/*
     * @param extName 文件扩展名
     * @return
     * @throws IOException
     */
    public static String upload(File file, String fileDir, String extName) {
        String ftp_user = ConfigUtils.getFtp_user();
        String ftp_password = ConfigUtils.getFtp_password();
        String ftp_path = ConfigUtils.getFtp_path();
        String ftp_url = ConfigUtils.getFtp_url();
        String baseDir = ConfigUtils.getFtp_secondPath();
        FtpConf ftpConf = new FtpConf(ftp_url, ftp_user, ftp_password, ftp_path, 22);
        ChannelSftp channelSftp = null;
        String uuid = (UUID.randomUUID() + "").replaceAll("-", "");
        String dst = baseDir + "/" + fileDir + "/" + uuid + extName;
        try {
            channelSftp = SftpFactory.getInstance().getChannel(ftpConf, 10000);
        } catch (JSchException e) {
            logger.error("channelSftp create error", e);
        }
        try {
            channelSftp.mkdir(baseDir + "/" + fileDir);
        } catch (SftpException e) {
            logger.error("channelSftp mkdir error", e);
        }
        FileInputStream inputStream = null;
        try {
            inputStream = new FileInputStream(file);
            channelSftp.put(inputStream, dst);
            return "http://" + ftp_url + ftp_path + dst;
        } catch (Exception e) {
            logger.error("channelSftp put error", e);
        } finally {
            try {
                inputStream.close();
                SftpFactory.getInstance().closeChannel();
            } catch (Exception e) {
                logger.error("channel close error", e);
            }
        }
        return null;
    }

    /**
     * 文件上传
     *
     * @param MultipartFile file
     * @return
     */
    public static String uploadMultipart(MultipartFile file, String fileDir) throws IOException {
        if (file == null || file.isEmpty()) {
            return "";
        }
        String[] fileType = {".jpg", ".gif", ".png", ".bmp"};
        String fileName = file.getOriginalFilename();
        String extName = fileName.substring(fileName.indexOf("."));
        //限制文件格式
        if (!Arrays.asList(fileType).contains(extName)) {
            logger.warn("上传文件扩展名是不允许的扩展名。只允许" + org.apache.commons.lang.StringUtils.join(fileType) + "格式。");
            return "";
        }
        String ftp_user = ConfigUtils.getFtp_user();
        String ftp_password = ConfigUtils.getFtp_password();
        String ftp_path = ConfigUtils.getFtp_path();
        String ftp_url = ConfigUtils.getFtp_url();
        String baseDir = ConfigUtils.getFtp_secondPath();
        FtpConf ftpConf = new FtpConf(ftp_url, ftp_user, ftp_password, ftp_path, 22);
        ChannelSftp channelSftp = null;
        String uuid = (UUID.randomUUID() + "").replaceAll("-", "");
        String dst = baseDir + "/" + fileDir + "/" + uuid + extName;

        try {
            channelSftp = SftpFactory.getInstance().getChannel(ftpConf, 10000);
        } catch (JSchException e) {
            e.printStackTrace();
        }
        try {//创建目录
            channelSftp.mkdir(baseDir + "/" + fileDir);
        } catch (SftpException e) {
            e.printStackTrace();
        }
        try {
            channelSftp.put(file.getInputStream(), dst);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                SftpFactory.getInstance().closeChannel();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return "http://"+ftp_url + ftp_path + dst;
    }

}
