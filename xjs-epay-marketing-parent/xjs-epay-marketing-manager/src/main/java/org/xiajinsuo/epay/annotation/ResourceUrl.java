package org.xiajinsuo.epay.annotation;

import java.lang.annotation.*;

/**
 * Created by tums on 2016/9/9.
 * 自定义Controller注解
 */
@Target({ElementType.PARAMETER, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ResourceUrl {
    /**
     * 请求资源地址
     *
     * @return
     */
    String value() default "";
}
