package org.xiajinsuo.epay.util;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.VerticalAlignment;
import jxl.write.*;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xiajinsuo.epay.business.*;
import org.xiajinsuo.epay.security.AuthStatus;
import org.xiajinsuo.epay.security.User;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * Created by tums on 2016/8/16.
 */
public final class ExcelUtil {
    private static Logger logger = LoggerFactory.getLogger(ExcelUtil.class);

    /**
     * @param fileName    EXCEL文件名称
     * @param sheetName   EXCEL文件名称-工作表
     * @param Title       EXCEL文件第一行列标题集合
     * @param listContent EXCEL文件正文数据集合
     * @param response
     */
    public static boolean export(String fileName, String sheetName, String[] Title, List<Object> listContent, HttpServletResponse response) {
        try {
            OutputStream os = response.getOutputStream();
            response.reset();// 清空输出流
            response.setHeader("Content-disposition", "attachment; filename=" + new String(fileName.getBytes("GB2312"), "ISO8859-1"));
            response.setContentType("application/msexcel");// 定义输出类型
            /** **********创建工作簿************ */
            WritableWorkbook workbook = Workbook.createWorkbook(os);
            /** **********创建工作表************ */
            WritableSheet sheet = workbook.createSheet(sheetName, 0);
            /** **********设置纵横打印（默认为纵打）、打印纸***************** */
            jxl.SheetSettings sheetset = sheet.getSettings();
            sheetset.setProtected(false);
            /** ************设置单元格字体************** */
            WritableFont NormalFont = new WritableFont(WritableFont.ARIAL, 10);
            WritableFont BoldFont = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD);
            /** ************以下设置三种单元格样式，灵活备用************ */
            // 用于标题居中
            WritableCellFormat wcf_center = new WritableCellFormat(BoldFont);
            wcf_center.setBorder(Border.ALL, BorderLineStyle.THIN); // 线条
            wcf_center.setVerticalAlignment(VerticalAlignment.CENTRE); // 文字垂直对齐
            wcf_center.setAlignment(Alignment.CENTRE); // 文字水平对齐
            wcf_center.setWrap(false); // 文字是否换行
            // 用于正文居左
            WritableCellFormat wcf_left = new WritableCellFormat(NormalFont);
            wcf_left.setBorder(Border.NONE, BorderLineStyle.THIN); // 线条
            wcf_left.setVerticalAlignment(VerticalAlignment.CENTRE); // 文字垂直对齐
            wcf_left.setAlignment(Alignment.LEFT); // 文字水平对齐
            wcf_left.setWrap(false); // 文字是否换行
            /** ***************以下是EXCEL开头大标题，暂时省略********************* */
            //sheet.mergeCells(0, 0, colWidth, 0);
            //sheet.addCell(new Label(0, 0, "XX报表", wcf_center));
            /** ***************以下是EXCEL第一行列标题********************* */
            for (int i = 0; i < Title.length; i++) {
                sheet.addCell(new Label(i, 0, Title[i], wcf_center));
            }
            /** ***************以下是EXCEL正文数据********************* */
            Field[] fields = null;
            int i = 1;
            for (Object obj : listContent) {
                fields = obj.getClass().getDeclaredFields();
                int j = 0;
                for (Field v : fields) {
                    v.setAccessible(true);
                    Object va = v.get(obj);
                    if (va == null) {
                        va = "";
                    }
                    sheet.addCell(new Label(j, i, va.toString(), wcf_left));
                    j++;
                }
                i++;
            }
            workbook.write();
            workbook.close();
            return true;
        } catch (IOException | WriteException | IllegalAccessException e) {
            logger.error(MessageFormat.format("excel export fail:", e));
        }
        return false;
    }


    /**
     * 创建工作簿
     *
     * @param fileName
     * @param response
     * @return
     * @throws IOException
     */
    private static WritableWorkbook initExcel(String fileName, HttpServletResponse response) throws IOException {
        OutputStream os = response.getOutputStream();
        response.reset();// 清空输出流
        response.setHeader("Content-disposition", "attachment; filename=" + new String(fileName.getBytes("UTF-8"), "ISO8859-1"));
        response.setContentType("APPLICATION/OCTET-STREAM");
        /** **********创建工作簿************ */
        return Workbook.createWorkbook(os);
    }

    /**
     * 创建工作表
     *
     * @param workbook
     * @param sheetName
     * @return
     */
    private static WritableSheet createSheet(WritableWorkbook workbook, String sheetName) {
        WritableSheet sheet = workbook.createSheet(sheetName, 0);
        /** **********设置纵横打印（默认为纵打）、打印纸***************** */
        jxl.SheetSettings sheetset = sheet.getSettings();
        sheetset.setProtected(false);
        /** ***************以下是EXCEL开头大标题，暂时省略********************* */
        //sheet.mergeCells(0, 0, colWidth, 0);
        //sheet.addCell(new Label(0, 0, "XX报表", wcf_center));
        return sheet;
    }

    /**
     * 用于标题居中
     *
     * @return
     * @throws WriteException
     */
    private static WritableCellFormat initWcf_Center() throws WriteException {
        WritableFont BoldFont = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD);
        WritableCellFormat wcf_center = new WritableCellFormat(BoldFont);
        wcf_center.setBorder(Border.ALL, BorderLineStyle.THIN); // 线条
        wcf_center.setVerticalAlignment(VerticalAlignment.CENTRE); // 文字垂直对齐
        wcf_center.setAlignment(Alignment.CENTRE); // 文字水平对齐
        wcf_center.setWrap(false); // 文字是否换行
        return wcf_center;
    }

    /**
     * 用于正文居左
     *
     * @return
     * @throws WriteException
     */
    private static WritableCellFormat initWcf_Left() throws WriteException {
        WritableFont NormalFont = new WritableFont(WritableFont.ARIAL, 10);
        WritableCellFormat wcf_left = new WritableCellFormat(NormalFont);
        wcf_left.setBorder(Border.NONE, BorderLineStyle.THIN); // 线条
        wcf_left.setVerticalAlignment(VerticalAlignment.CENTRE); // 文字垂直对齐
        wcf_left.setAlignment(Alignment.LEFT); // 文字水平对齐
        wcf_left.setWrap(false); // 文字是否换行
        return wcf_left;
    }

    /**
     * 初始化title
     *
     * @param sheet
     * @param wcf_center
     * @param titles
     * @throws WriteException
     */
    private static void initTitle(WritableSheet sheet, WritableCellFormat wcf_center, String[] titles) throws WriteException {
        /** ***************以下是EXCEL第一行列标题********************* */
        for (int i = 0; i < titles.length; i++) {
            sheet.addCell(new Label(i, 0, titles[i], wcf_center));
        }
    }


    private static String null2str(Object obj) {
        if (Objects.isNull(obj)) {
            return "";
        }
        return obj.toString();
    }

    private static String date2str(Object obj) {
        if (Objects.isNull(obj)) {
            return "";
        }
        SimpleDateFormat dateFormater = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        return dateFormater.format(obj);
    }

    private static String formatDateStr(String date) {
        if (StringUtils.isEmpty(date)) {
            return "";
        }
        String year = date.substring(0, 4);
        String month = date.substring(4, 6);
        String day = date.substring(6, 8);
        String hour = date.substring(8, 10);
        String minute = date.substring(10, 12);
        String second = date.substring(12);
        return year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second;
    }
    private static String formatDate(String date) {
        if (StringUtils.isEmpty(date)) {
            return "";
        }
        String year = date.substring(0, 4);
        String month = date.substring(4, 6);
        String day = date.substring(6, 8);
        return year + "-" + month + "-" + day ;
    }

    private static String formatDuring(Long minute) {
        Long days = minute / (60 * 24);
        Long hours = (minute % (60 * 24)) / 60;
        Long minutes = minute % 60;
        String formatString = "";
        if (days >= 1) formatString = days + "天";
        if (hours >= 1) formatString += hours + "小时";
        if (minutes >= 1) formatString += minutes + "分钟";
        if (days < 1 && hours < 1 && minutes < 1) formatString = "1分钟内";
        return formatString;
    }

    public static Long calcDateDiff(Date d1, Date d2) {
        Long diff = d1.getTime() - d2.getTime();//这样得到的差值是毫秒级别
        return diff / (1000 * 60);    //获得分钟数
    }

    /**
     * 导出银行卡
     *
     * @param listContent
     * @param response
     * @return
     */
    public static boolean exportCard(List<Card> listContent, HttpServletResponse response) {
        String fileName = "银行卡.xls";
        String sheetName = "银行卡";
        String[] titles = {"用户名", "姓名", "身份证", "银行代码", "卡类型", "银行卡号", "手机号", "是否是收款卡", "所在省", "所在市", "支行"};
        try {
            WritableWorkbook workbook = initExcel(fileName, response);
            WritableSheet sheet = createSheet(workbook, sheetName);
            WritableCellFormat wcf_center = initWcf_Center();
            WritableCellFormat wcf_left = initWcf_Left();
            initTitle(sheet, wcf_center, titles);
            int i = 1, j;
            for (Card item : listContent) {
                j = 0;
                sheet.addCell(new Label(j++, i, item.getUsername(), wcf_left));
                sheet.addCell(new Label(j++, i, item.getFamilyName(), wcf_left));
                sheet.addCell(new Label(j++, i, item.getIdcard() + "", wcf_left));
                sheet.addCell(new Label(j++, i, item.getBankName() + "", wcf_left));
                sheet.addCell(new Label(j++, i, item.getCardType().equals("2") ? "信用卡" : "借记卡", wcf_left));
                sheet.addCell(new Label(j++, i, item.getBankNo() + "", wcf_left));
                sheet.addCell(new Label(j++, i, item.getMobile() + "", wcf_left));
                sheet.addCell(new Label(j++, i, item.getEnable() ? "是" : "否", wcf_left));
                sheet.addCell(new Label(j++, i, null2str(item.getAccProvince()), wcf_left));
                sheet.addCell(new Label(j++, i, null2str(item.getAccCity()), wcf_left));
                sheet.addCell(new Label(j++, i, null2str(item.getBranchCode()), wcf_left));
                sheet.addCell(new Label(j++, i, null2str(item.getBranchName()), wcf_left));
                i++;
            }
            workbook.write();
            workbook.close();
            return true;
        } catch (Exception e) {
            logger.error("excel export fail,{}", e);
        }
        return false;
    }


    /**
     * 导出e汇通用户
     *
     * @param listContent
     * @param response
     * @return
     */
    public static boolean exportUser(List<User> listContent, HttpServletResponse response) {
        String fileName = "注册用户.xls";
        String sheetName = "注册用户";
        String[] titles = {"用户名", "所属合作方", "姓名", " 身份证", "实名认证", "最后登录IP", "最后登录时间", "状态"};
        try {
            WritableWorkbook workbook = initExcel(fileName, response);
            WritableSheet sheet = createSheet(workbook, sheetName);
            WritableCellFormat wcf_center = initWcf_Center();
            WritableCellFormat wcf_left = initWcf_Left();
            initTitle(sheet, wcf_center, titles);
            int i = 1, j;
            for (User item : listContent) {
                j = 0;
                sheet.addCell(new Label(j++, i, item.getUsername(), wcf_left));
                sheet.addCell(new Label(j++, i, item.getRefereeFamilyName(), wcf_left));
                sheet.addCell(new Label(j++, i, item.getFamilyName(), wcf_left));
                sheet.addCell(new Label(j++, i, item.getIdcard(), wcf_left));
                sheet.addCell(new Label(j++, i, item.getStatus() == AuthStatus.SUCCESS ? "认证通过" : item.getStatus() == AuthStatus.TO_BE_CONFIRMED ? "未认证" : "认证失败", wcf_left));
                sheet.addCell(new Label(j++, i, item.getLastLoginIp(), wcf_left));
                sheet.addCell(new Label(j++, i, formatDateStr(item.getLastLoginTime()), wcf_left));
                sheet.addCell(new Label(j++, i, item.getEnable() ? "启用" : "禁用", wcf_left));
                i++;
            }
            workbook.write();
            workbook.close();
            return true;
        } catch (Exception e) {
            logger.error("excel export fail,{}", e);
        }
        return false;
    }

    /**
     * 导出收款订单
     *
     * @param listContent
     * @param response
     * @return
     */
    public static boolean exportTransShortCut(List<Trans> listContent, HttpServletResponse response,boolean topUserFlag) {
        if(!topUserFlag) {
            String fileName = "收款订单.xls";
            String sheetName = "收款订单";
            String[] titles = {"交易时间", "订单号", "用户名", "姓名", "交易金额", "费率", "操作手续费","手续费", "到账金额",
                    "支付状态", "到账类型", "交易类型", "推荐人姓名", "代理姓名"};
            try {
                WritableWorkbook workbook = initExcel(fileName, response);
                WritableSheet sheet = createSheet(workbook, sheetName);
                WritableCellFormat wcf_center = initWcf_Center();
                WritableCellFormat wcf_left = initWcf_Left();
                initTitle(sheet, wcf_center, titles);
                int i = 1, j;
                for (Trans item : listContent) {
                    j = 0;
                    sheet.addCell(new Label(j++, i, formatDateStr(item.getTransTime()), wcf_left));
                    sheet.addCell(new Label(j++, i, item.getClientTransId(), wcf_left));
                    sheet.addCell(new Label(j++, i, doUsernameHide(item.getUsername()), wcf_left));
                    sheet.addCell(new Label(j++, i, doNameHide(item.getFamilyName()), wcf_left));
                    sheet.addCell(new Label(j++, i, fenToYuan(item.getAmount()), wcf_left));
                    sheet.addCell(new Label(j++, i, item.getRate(), wcf_left));
                    String counterFee = item.getCounterFee();
                    sheet.addCell(new Label(j++, i, StringUtils.isEmpty(counterFee) ? "0" : fenToYuan(counterFee), wcf_left));
                    String operationFee = item.getOperationFee();
                    sheet.addCell(new Label(j++, i, StringUtils.isEmpty(operationFee) ? "0" : fenToYuan(operationFee), wcf_left));
                    String payAmount = item.getPayAmount();
                    sheet.addCell(new Label(j++, i, StringUtils.isEmpty(payAmount) ? "0" : fenToYuan(payAmount), wcf_left));
                    PayStatus payStatus = item.getPayStatus();
                    sheet.addCell(new Label(j++, i, payStatus == PayStatus.PAY_SUBMIT ? "未支付" : payStatus == PayStatus.PAY_SUCCESS ? "支付成功" : "支付失败", wcf_left));
                    String clearingType = item.getClearingType();
                    sheet.addCell(new Label(j++, i, StringUtils.isNotEmpty(clearingType) && clearingType.equals("1") ? "次日到账" : "实时到账", wcf_left));
                    String transType = item.getTransType();
                    sheet.addCell(new Label(j++, i, transType.equals("SHORTCUTPAY") ? "快捷支付" : transType, wcf_left));
                    sheet.addCell(new Label(j++, i, doNameHide(item.getRefereeFamilyName()), wcf_left));
                    sheet.addCell(new Label(j++, i, doNameHide(item.getAgentFamilyName()), wcf_left));
                    i++;
                }
                workbook.write();
                workbook.close();
                return true;
            } catch (Exception e) {
                logger.error("excel export fail,{}", e);
            }
            return false;
        }else{
            String fileName = "收款订单.xls";
            String sheetName = "收款订单";
            String[] titles = {"交易时间", "订单号", "用户名", "姓名", "交易金额", "费率", "操作手续费","手续费", "到账金额",
                    "支付状态", "到账类型", "交易类型", "推荐人姓名", "代理姓名"};
            try {
                WritableWorkbook workbook = initExcel(fileName, response);
                WritableSheet sheet = createSheet(workbook, sheetName);
                WritableCellFormat wcf_center = initWcf_Center();
                WritableCellFormat wcf_left = initWcf_Left();
                initTitle(sheet, wcf_center, titles);
                int i = 1, j;
                for (Trans item : listContent) {
                    j = 0;
                    sheet.addCell(new Label(j++, i, formatDateStr(item.getTransTime()), wcf_left));
                    sheet.addCell(new Label(j++, i, item.getClientTransId(), wcf_left));
                    sheet.addCell(new Label(j++, i, item.getUsername(), wcf_left));
                    sheet.addCell(new Label(j++, i, item.getFamilyName(), wcf_left));
                    sheet.addCell(new Label(j++, i, fenToYuan(item.getAmount()), wcf_left));
                    sheet.addCell(new Label(j++, i, item.getRate(), wcf_left));
                    String counterFee = item.getCounterFee();
                    sheet.addCell(new Label(j++, i, StringUtils.isEmpty(counterFee) ? "0" : fenToYuan(counterFee), wcf_left));
                    String operationFee = item.getOperationFee();
                    sheet.addCell(new Label(j++, i, StringUtils.isEmpty(operationFee) ? "0" : fenToYuan(operationFee), wcf_left));
                    String payAmount = item.getPayAmount();
                    sheet.addCell(new Label(j++, i, StringUtils.isEmpty(payAmount) ? "0" : fenToYuan(payAmount), wcf_left));
                    PayStatus payStatus = item.getPayStatus();
                    sheet.addCell(new Label(j++, i, payStatus == PayStatus.PAY_SUBMIT ? "未支付" : payStatus == PayStatus.PAY_SUCCESS ? "支付成功" : "支付失败", wcf_left));
                    String clearingType = item.getClearingType();
                    sheet.addCell(new Label(j++, i, StringUtils.isNotEmpty(clearingType) && clearingType.equals("1") ? "次日到账" : "实时到账", wcf_left));
                    String transType = item.getTransType();
                    sheet.addCell(new Label(j++, i, transType.equals("SHORTCUTPAY") ? "快捷支付" : transType, wcf_left));
                    sheet.addCell(new Label(j++, i, item.getRefereeFamilyName(), wcf_left));
                    sheet.addCell(new Label(j++, i, item.getAgentFamilyName(), wcf_left));
                    i++;
                }
                workbook.write();
                workbook.close();
                return true;
            } catch (Exception e) {
                logger.error("excel export fail,{}", e);
            }
            return false;

        }
    }
    /**
     * 导出收分润
     *
     * @param listContent
     * @param response
     * @return
     */
    public static boolean exportDailyGain(List<DailyGain> listContent, HttpServletResponse response,boolean topUserFlag) {
        if(!topUserFlag) {
            String fileName = "分润统计.xls";
            String sheetName = "分润统计";
            String[] titles = {"分润时间", "交易类型", "用户名", "姓名", "分润来源", "订单号", "交易金额", "分润比例",
                    "分润金额"};
            try {
                WritableWorkbook workbook = initExcel(fileName, response);
                WritableSheet sheet = createSheet(workbook, sheetName);
                WritableCellFormat wcf_center = initWcf_Center();
                WritableCellFormat wcf_left = initWcf_Left();
                initTitle(sheet, wcf_center, titles);
                int i = 1, j;
                for (DailyGain item : listContent) {
                    j = 0;
                    sheet.addCell(new Label(j++, i, formatDate(item.getDay()), wcf_left));
                    String transType = item.getTransType();
                    sheet.addCell(new Label(j++, i, transType.equals("SHORTCUTPAY") ? "快捷支付" : transType, wcf_left));
                    sheet.addCell(new Label(j++, i, doUsernameHide(item.getUsername()), wcf_left));
                    sheet.addCell(new Label(j++, i, doNameHide(item.getFamilyName()), wcf_left));
                    sheet.addCell(new Label(j++, i, doNameHide(item.getFromFamilyName()), wcf_left));
                    sheet.addCell(new Label(j++, i, item.getClientTransId(), wcf_left));
                    String transAmt = item.getTransAmt();
                    sheet.addCell(new Label(j++, i, StringUtils.isEmpty(transAmt) ? "0" : fenToYuan(transAmt), wcf_left));
                    String rate = item.getRate();
                    sheet.addCell(new Label(j++, i, StringUtils.isEmpty(rate) ? "0" : rate, wcf_left));
                    String amount = item.getAmount();
                    sheet.addCell(new Label(j++, i, StringUtils.isEmpty(amount) ? "0" : fenToYuan(amount), wcf_left));
                    i++;
                }
                workbook.write();
                workbook.close();
                return true;
            } catch (Exception e) {
                logger.error("excel export fail,{}", e);
            }
            return false;
        }else{
            String fileName = "分润统计.xls";
            String sheetName = "分润统计";
            String[] titles = {"分润时间", "交易类型", "用户名", "姓名", "分润来源", "订单号", "交易金额", "分润比例",
                    "分润金额"};
            try {
                WritableWorkbook workbook = initExcel(fileName, response);
                WritableSheet sheet = createSheet(workbook, sheetName);
                WritableCellFormat wcf_center = initWcf_Center();
                WritableCellFormat wcf_left = initWcf_Left();
                initTitle(sheet, wcf_center, titles);
                int i = 1, j;
                for (DailyGain item : listContent) {
                    j = 0;
                    sheet.addCell(new Label(j++, i, formatDate(item.getDay()), wcf_left));
                    String transType = item.getTransType();
                    sheet.addCell(new Label(j++, i, transType.equals("SHORTCUTPAY") ? "快捷支付" : transType, wcf_left));
                    sheet.addCell(new Label(j++, i, item.getUsername(), wcf_left));
                    sheet.addCell(new Label(j++, i, item.getFamilyName(), wcf_left));
                    sheet.addCell(new Label(j++, i, item.getFromFamilyName(), wcf_left));
                    sheet.addCell(new Label(j++, i, item.getClientTransId(), wcf_left));
                    String transAmt = item.getTransAmt();
                    sheet.addCell(new Label(j++, i, StringUtils.isEmpty(transAmt) ? "0" : fenToYuan(transAmt), wcf_left));
                    String rate = item.getRate();
                    sheet.addCell(new Label(j++, i, StringUtils.isEmpty(rate) ? "0" : rate, wcf_left));
                    String amount = item.getAmount();
                    sheet.addCell(new Label(j++, i, StringUtils.isEmpty(amount) ? "0" : fenToYuan(amount), wcf_left));
                    i++;
                }
                workbook.write();
                workbook.close();
                return true;
            } catch (Exception e) {
                logger.error("excel export fail,{}", e);
            }
            return false;
        }
    }


    /**
     * 导出清算订单
     *
     * @param listContent
     * @param response
     * @return
     */
    public static boolean exportTransClearing(List<TransClearing> listContent, HttpServletResponse response) {
        String fileName = "清算订单.xls";
        String sheetName = "清算订单";
        String[] titles = {"交易时间", "订单号", "用户名", "姓名", "所属合作方", "交易金额", "费率", "手续费", "到账金额", "清算类型", "清算状态", "外部订单号", "错误代码", "错误消息", "备注"};
        try {
            WritableWorkbook workbook = initExcel(fileName, response);
            WritableSheet sheet = createSheet(workbook, sheetName);
            WritableCellFormat wcf_center = initWcf_Center();
            WritableCellFormat wcf_left = initWcf_Left();
            initTitle(sheet, wcf_center, titles);
            int i = 1, j;
            for (TransClearing item : listContent) {
                j = 0;
                sheet.addCell(new Label(j++, i, formatDateStr(item.getTransTime()), wcf_left));
                sheet.addCell(new Label(j++, i, item.getClientTransId(), wcf_left));
                sheet.addCell(new Label(j++, i, item.getUsername(), wcf_left));
                sheet.addCell(new Label(j++, i, item.getFamilyName(), wcf_left));
                sheet.addCell(new Label(j++, i, item.getAmount(), wcf_left));
                String counterFee = item.getCounterFee();
                sheet.addCell(new Label(j++, i, StringUtils.isEmpty(counterFee) ? "0" : counterFee, wcf_left));
                String payAmount = item.getPayAmount();
                sheet.addCell(new Label(j++, i, StringUtils.isEmpty(payAmount) ? "0" : payAmount, wcf_left));
                sheet.addCell(new Label(j++, i, clearingStatus2text(item.getClearingStatus()), wcf_left));
                sheet.addCell(new Label(j++, i, item.getErrorCode(), wcf_left));
                sheet.addCell(new Label(j++, i, item.getErrorMsg(), wcf_left));
                sheet.addCell(new Label(j++, i, item.getMemo(), wcf_left));
                i++;
            }
            workbook.write();
            workbook.close();
            return true;
        } catch (Exception e) {
            logger.error("excel export fail,{}", e);
        }
        return false;
    }


    private static String clearingStatus2text(ClearingStatus status) {
        switch (status) {
            case CLEARING_BEFORE:
                return "未支付";
            case CLEARING_ING:
                return "待清算";
            case CLEARING_END:
                return "清算中";
            case CLEARING_SUCCESS:
                return "清算成功";
            case CLEARING_FAILURE:
                return "清算失败";
            default:
                return "未知状态";
        }
    }

    /**
     * 敏感字段部分用*代替
     * @param str
     * @return
     */

    private static String doNameHide(String str){
        String newStr="";
        if(StringUtils.isNotEmpty(str)) {
            String str1 = str.substring(1,2);
             newStr = str.replace(str1, "*");
        }
        return newStr;

    }
    private static String  fenToYuan(String v1){
        String v2="100.00";
        BigDecimal b1 = new BigDecimal(v1);
        BigDecimal b2 = new BigDecimal(v2);
        return  b1.divide(b2, 2, BigDecimal.ROUND_HALF_UP).toString();
    }
    private static String doUsernameHide(String str){
        int len = str.length();
        if(len>3) {
            String s1 = str.substring(0, 3);
            String s2 = str.substring(len - 3, len);
            return s1+"******"+s2;
        }
        return str;
    }



}
