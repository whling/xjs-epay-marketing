package org.xiajinsuo.epay.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.xiajinsuo.epay.annotation.ResourceUrl;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;

/**
 * Created by tums on 2016/12/20.
 */
@Aspect
@Component
public class PermissionsAspect {
    private static final Logger LOGGER = LoggerFactory.getLogger(PermissionsAspect.class);

    public Object process(ProceedingJoinPoint call) throws Throwable {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        String ip = request.getRemoteAddr();
        String method = call.getTarget().getClass().getName() + "." + call.getSignature().getName() + "()";
        String resourceUrl = getResourceUrl(call);
        LOGGER.info("Method[{}],resourceUrl[{}],RemoteIp[{}]", method, resourceUrl, ip);
//        if (StringUtils.isNotEmpty(resourceUrl)) {
//            boolean permissions = LoginUtil.permissions(request, resourceUrl);
//            if (!permissions) {
//                throw new NoPermissionsException();
//            }
//        }
        return call.proceed();
    }

    /**
     * 获取注解中对方法的描述信息 用于Controller层注解
     *
     * @param joinPoint 切点
     * @return 方法描述
     */
    private static String getResourceUrl(JoinPoint joinPoint) {
        String targetName = joinPoint.getTarget().getClass().getName();
        String methodName = joinPoint.getSignature().getName();
        Object[] arguments = joinPoint.getArgs();
        Class targetClass = null;
        try {
            targetClass = Class.forName(targetName);
        } catch (ClassNotFoundException e) {
            LOGGER.error("获取Controller.ResourceUrl异常:[{}]", e);
            return "";
        }
        Method[] methods = targetClass.getMethods();
        String url = "";
        for (Method method : methods) {
            if (method.getName().equals(methodName)) {
                Class[] clazzs = method.getParameterTypes();
                if (clazzs.length == arguments.length) {
                    ResourceUrl annotation = method.getAnnotation(ResourceUrl.class);
                    if (annotation != null) {
                        url = annotation.value();
                        break;
                    }
                }
            }
        }
        return url;
    }
}
