package org.xiajinsuo.epay.vo;

/**
 * Created by tums on 2017/8/5.
 */
public class SelectNode {
    private int ordinal;
    private String name;
    private String text;

    public SelectNode(int ordinal, String name, String text) {
        this.ordinal = ordinal;
        this.name = name;
        this.text = text;
    }

    public SelectNode() {
    }

    public int getOrdinal() {
        return ordinal;
    }

    public void setOrdinal(int ordinal) {
        this.ordinal = ordinal;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
