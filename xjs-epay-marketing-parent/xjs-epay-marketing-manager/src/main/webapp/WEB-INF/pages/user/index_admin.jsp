<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Mypays后台管理系统</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <%@ include file="/WEB-INF/pages/common/head.jsp" %>
    <script src="${base}/pages/user/index_admin.js?v=<%=Math.random()%>"></script>
</head>
<body>
<input type="hidden" id="isTopOrg" value="${isTopOrg}">
<div>
    <form id="searchForm" class="search-condition__form" method="post" novalidate>
        <div  id="searchByOrg">
            <select  class="easyui-combobox" name="org_code" label="所属机构" style="width:100%">
                <option value="" selected>--请选择--</option>
                <c:forEach items="${orgList}" var="item">
                    <option value="${item.code }">${item.name }</option>
                </c:forEach>
            </select>
        </div>
        <div>
            <input name="username" class="easyui-textbox" label="登录账号：" style="width:100%">
        </div>
        <div>
            <input name="family_name" class="easyui-textbox" label="姓名：" style="width:100%">
        </div>
        <a id="searchBtn" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-search'">查询</a>
    </form>
</div>
<div id="dataGrid"></div>
<div id="dialogs">
    <div id="editDialog" class="easyui-dialog" data-options="resizable:true,modal:true" style="width:400px"
         closed="true" buttons="#dialog-buttons-edit">
        <form id="editForm" method="post" novalidate style="margin:0;padding:20px 50px">
            <input name="id" type="hidden">
            <div style="margin-bottom:10px">
                <input name="username" class="easyui-textbox" label="登录账号" style="width:100%" readonly>
            </div>
            <div style="margin-bottom:10px">
                <input name="family_name" class="easyui-textbox" label="姓名" style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input name="idcard" class="easyui-textbox" label="身份证" style="width:100%">
            </div>
        </form>
    </div>
    <div id="addDialog" class="easyui-dialog" data-options="resizable:true,modal:true" style="width:400px" closed="true"
         buttons="#dialog-buttons-add">
        <form id="addForm" method="post" novalidate style="margin:0;padding:20px 50px">
            <div style="margin-bottom:10px">
                <input name="username" id="checkName" class="easyui-textbox" required="true" label="登录账号" style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input name="password" class="easyui-passwordbox" required="true" label="登录密码" style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input name="family_name" class="easyui-textbox" label="姓名" style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input name="idcard" class="easyui-textbox" label="身份证" style="width:100%">
            </div>
        </form>
    </div>
    <div id="grantDialog" class="easyui-dialog" data-options="resizable:true,modal:true" style="width:400px"
         closed="true" buttons="#dialog-buttons-grant">
        <form id="grantForm" method="post" novalidate style="margin:0;padding:20px 50px">
            <input name="id" type="hidden">
            <div style="margin-bottom:10px">
                <input name="username" class="easyui-textbox" label="账号" style="width:100%">
            </div>
            <input class="easyui-combobox" id="rolesCombobox" name="resourceIds" style="width:100%;"
                   data-options="
                    valueField:'id',
                    textField:'name',
                    multiple:true,
                    multiline:true,
                    panelHeight:'auto',
                    label: '角色列表:',
                    labelPosition: 'left'
                    ">
        </form>
    </div>

    <div id="orgDialog" class="easyui-dialog" data-options="resizable:true,modal:true" style="width:400px"
         closed="true" buttons="#dialog-buttons-org">
        <form id="orgForm" method="post" novalidate style="margin:0;padding:20px 50px">
            <input name="id" type="hidden">
            <div style="margin-bottom:10px">
                <input name="username" class="easyui-textbox" label="账号" style="width:100%">
            </div>
         <%--   <div style="margin-bottom:10px">
                <select class="easyui-combobox" name="resourceIds" data-options="multiple:true,panelHeight:'auto'"
                        id="orgCombobox" label="机构列表" style="width:100%">
                    <option value="">全部</option>
                    <c:forEach var="item" items="${orgList}">
                        <option value="${item.code}">${item.name}</option>
                    </c:forEach>
                </select>
            </div>--%>
            <div style="margin-bottom:10px">
                    <input type="button" onclick="showList()" value="选择授权机构">
                <div  id="orgList"  style="display:none">
                    <p>
                        <a href="javascript:;" title="确定" hidefocus="true" class="a_0">确定</a><a
                            href="javascript:;" title="取消" hidefocus="true" class="a_1">取消</a>
                    </p>
                </div>
            </div>


        </form>
    </div>
    <div id="dialog-buttons-edit">
        <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="Page.editSubmit()"
           style="width:90px">保存</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel"
           onclick="javascript:$('#editDialog').dialog('close')" style="width:90px">取消</a>
    </div>
    <div id="dialog-buttons-add">
        <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="Page.addSubmit()"
           style="width:90px">保存</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel"
           onclick="javascript:$('#addDialog').dialog('close')" style="width:90px">取消</a>
    </div>
    <div id="dialog-buttons-grant">
        <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="Page.grantSubmit()"
           style="width:90px">保存</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel"
           onclick="javascript:$('#grantDialog').dialog('close')" style="width:90px">取消</a>
    </div>
    <div id="dialog-buttons-org">
        <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="Page.orgSubmit()"
           style="width:90px">保存</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel"
           onclick="javascript:$('#orgDialog').dialog('close')" style="width:90px">取消</a>
    </div>
</div>
</body>
</html>