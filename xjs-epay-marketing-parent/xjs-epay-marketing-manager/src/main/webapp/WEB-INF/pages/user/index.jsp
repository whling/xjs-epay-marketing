<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Mypays后台管理系统</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <%@ include file="/WEB-INF/pages/common/head.jsp" %>
    <script src="${base}/pages/user/index.js?v=<%=Math.random()%>"></script>
</head>
<body>
<input type="hidden" id="showOrg" value="${showOrg}">
<input type="hidden" id="isOpenAgentGrant" value="${isOpenAgentGrant}">
<div>
    <form id="searchForm" class="search-condition__form" method="post" novalidate>
        <div id="searchByOrg">
            <select class="easyui-combobox" name="org_code" label="所属机构" style="width:100%">
                <option value="" selected>--请选择--</option>
                <c:forEach items="${orgList}" var="item">
                    <option value="${item.code }">${item.name }</option>
                </c:forEach>
            </select>
        </div>
        <div>
            <select class="easyui-combobox" id="statusValue" name="status" label="认证状态" style="width:100%">
                <option value="" selected>--请选择--</option>
                <c:forEach items="${statusList}" var="item">
                    <option value="${item.name}">${item.text }</option>
                </c:forEach>
            </select>
        </div>
        <div>
            <input name="username" class="easyui-textbox" label="用户名：" style="width:100%">
        </div>
        <div>
            <input name="family_name" class="easyui-textbox" label="姓名：" style="width:100%">
        </div>
        <div>
            <input name="agent_family_name" class="easyui-textbox" label="代理：" style="width:100%">
        </div>
        <div>
            <input name="referee_family_name" class="easyui-textbox" label="推荐人：" style="width:100%">
        </div>
        <div>
            <select class="easyui-combobox" name="type" label="用户类型" style="width:100%">
                <option value="" selected>--请选择--</option>
                <c:forEach items="${typeList}" var="item">
                    <option value="${item.name}">${item.text }</option>
                </c:forEach>
                </select>
            </select>
        </div>
        <div>
            <input name="time_start" id="timeStart" class="easyui-datebox"
                   data-options="formatter:easyuiUtils.datebox_formatter,parser:easyuiUtils.datebox_parse"
                   label="注册起始开始">
        </div>
        <div>
            <input name="time_end" id="timeEnd" class="easyui-datebox"
                   data-options="formatter:easyuiUtils.datebox_formatter,parser:easyuiUtils.datebox_parse"
                   label="注册截止时间">
        </div>
        <a id="searchBtn" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-search'">查询</a>
    </form>
</div>
<div id="dataGrid"></div>
<div id="dialogs">
    <div id="editDialog" class="easyui-dialog" data-options="resizable:true,modal:true" style="width:400px"
         closed="true" buttons="#dialog-buttons-edit">
        <form id="editForm" method="post" novalidate style="margin:0;padding:20px 50px">
            <input name="id" type="hidden">
            <div style="margin-bottom:10px">
                <input name="username" class="easyui-textbox" label="用户名称" style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input name="family_name" class="easyui-textbox" label="姓名" style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input name="idcard" class="easyui-textbox" label="身份证" style="width:100%">
            </div>
        </form>
    </div>
    <div id="addDialog" class="easyui-dialog" data-options="resizable:true,modal:true" style="width:400px" closed="true"
         buttons="#dialog-buttons-add">
        <form id="addForm" method="post" novalidate style="margin:0;padding:20px 50px">
            <div style="margin-bottom:10px">
                <input name="username" class="easyui-textbox" required="true" label="用户名称" style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input name="password" class="easyui-passwordbox" required="true" label="登录密码" style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input name="family_name" class="easyui-textbox" label="姓名" style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input name="idcard" class="easyui-textbox" label="身份证" style="width:100%">
            </div>
        </form>
    </div>
    <div id="grantDialog" class="easyui-dialog" data-options="resizable:true,modal:true" style="width:400px"
         closed="true" buttons="#dialog-buttons-grant">
        <form id="grantForm" method="post" novalidate style="margin:0;padding:20px 50px">
            <input name="id" type="hidden">
            <div style="margin-bottom:10px">
                <input name="username" class="easyui-textbox" label="用户名称" style="width:100%">
            </div>
            <input class="easyui-combobox" id="rolesCombobox" name="resourceIds" style="width:100%;"
                   data-options="
                    valueField:'id',
                    textField:'name',
                    multiple:true,
                    multiline:true,
                    panelHeight:'auto',
                    label: '角色列表:',
                    labelPosition: 'left'
                    ">
        </form>
    </div>

    <div id="viewDialog" class="easyui-dialog" data-options="resizable:true,modal:true" style="width:540px"
         closed="true" buttons="#dialog-buttons-view">
        <div class="easyui-tabs" style="height: 580px">
            <div title="用户详情" style="margin:0;padding:10px 20px">
                <form id="viewForm" method="post" novalidate style="margin:0;padding:20px 50px">
                    <div style="margin-bottom:10px">
                        <input id="username" name="username" class="easyui-textbox" readonly="readonly" label="用户名"
                               style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="org_name" class="easyui-textbox" readonly="readonly" label="所属机构"
                               style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="family_name" class="easyui-textbox" readonly="readonly" label="姓名"
                               style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="idcard" class="easyui-textbox" readonly="readonly" label="身份证"
                               style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="sex" id="sex" class="easyui-textbox" readonly="readonly" label="性别"
                               style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input id="type" name="type" class="easyui-textbox" readonly="readonly" label="类型"
                               style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input id="status" name="status" class="easyui-textbox" readonly="readonly" label="认证状态"
                               style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="available_amt" id="available_amt" class="easyui-textbox" readonly="readonly"
                               label="可用金额"
                               style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="history_amt" id="history_amt" class="easyui-textbox" readonly="readonly"
                               label="总金额"
                               style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="rate" class="easyui-textbox" readonly="readonly" label="分润"
                               style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="register_time" id="register_time" class="easyui-textbox" readonly="readonly"
                               label="注册时间"
                               style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="referee_family_name" class="easyui-textbox" readonly="readonly" label="推荐人姓名"
                               style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="agent_family_name" class="easyui-textbox" readonly="readonly" label="代理姓名"
                               style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="open_id" class="easyui-textbox" readonly="readonly" label="微信id"
                               style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="token" class="easyui-textbox" readonly="readonly" label="app登录凭证"
                               style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="last_login_time" id="last_login_time" class="easyui-textbox" readonly="readonly"
                               label="最后登录时间"
                               style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="enable" id="enable" class="easyui-textbox" readonly="readonly" label="状态"
                               style="width:100%">
                    </div>
                </form>
            </div>
            <div title="用户收款卡信息" style="margin:0;padding:20px 50px">
                <form id="viewUserForm" method="post" novalidate>
                    <div style="margin-bottom:10px">
                        <input name="username" class="easyui-textbox" readonly="readonly" label="用户名"
                               style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="family_name" class="easyui-textbox" readonly="readonly" label="姓名"
                               style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="idcard" class="easyui-textbox" readonly="readonly" label="身份证" style="width:100%">
                    </div>

                    <div style="margin-bottom:10px">
                        <input name="bank_name" class="easyui-textbox" readonly="readonly" label="银行代码"
                               style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="bank_no" class="easyui-textbox" readonly="readonly" label="银行卡号"
                               style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="mobile" class="easyui-textbox" readonly="readonly" label="预留手机号"
                               style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="acc_province" class="easyui-textbox" readonly="readonly" label="所在省"
                               style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="acc_city" class="easyui-textbox" readonly="readonly" label="所在市"
                               style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="branch_code" class="easyui-textbox" readonly="readonly" label="联行号"
                               style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="branch_name" class="easyui-textbox" readonly="readonly" label="支行名称"
                               style="width:100%">
                    </div>
                </form>
            </div>
            <div title="推广二维码" style="margin:0;padding:20px 50px">
                <div style="margin-bottom:10px" ;text-align="center;">
                    <img id="qr_img" src="" style="width:200px;height: 200px">
                </div>


            </div>
        </div>

    </div>
    <div id="editAgentDialog" class="easyui-dialog" data-options="resizable:true,modal:true" style="width:400px"
         closed="true" buttons="#dialog-buttons-agent">
        <input type="hidden" id="loginUserType" value="${loginUser.type}"/>
        <form id="editAgentForm" method="post" novalidate style="margin:0;padding:20px 50px">
            <input type="hidden" name="id"/>
            <input type="hidden" name="type"/>
            <input type="hidden" name="rate"/>
            <input type="hidden" name="referee"/>
            <input type="hidden" name="extend_agent"/>
            <input type="hidden" id="agentRate"/>
            <div style="margin-bottom:10px">
                <input name="username" class="easyui-textbox" readonly label="用户名称" style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input name="family_name" class="easyui-textbox" readonly label="姓名" style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <select class="easyui-combobox" name="modify_type" id="modify_type" label="代理" style="width:100%">
                    <option value="1">是</option>
                    <option value="0">否</option>
                </select>
            </div>
            <div style="margin-bottom:10px">
                <input name="modify_rate" id="modify_rate" class="easyui-textbox" label="分润" value="0" required="true"
                       style="width:80%"><span id="agentRateView">[ <= 0]</span>
            </div>
            <div style="margin-bottom:10px">
                <select class="easyui-combobox" name="modify_extend_agent" id="modify_extend_agent" label="发展下级代理"
                        style="width:100%">
                    <option value="1">是</option>
                    <option value="0">否</option>
                </select>
            </div>
        </form>
    </div>
    <div id="dialog-buttons-agent">
        <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="Page.editAgentSubmit()"
           style="width:90px">保存</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel"
           onclick="javascript:$('#editAgentDialog').dialog('close')" style="width:90px">取消</a>
    </div>
    <div id="dialog-buttons-edit">
        <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="Page.editSubmit()"
           style="width:90px">保存</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel"
           onclick="javascript:$('#editDialog').dialog('close')" style="width:90px">取消</a>
    </div>
    <div id="dialog-buttons-add">
        <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="Page.addSubmit()"
           style="width:90px">保存</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel"
           onclick="javascript:$('#addDialog').dialog('close')" style="width:90px">取消</a>
    </div>
    <div id="dialog-buttons-grant">
        <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="Page.grantSubmit()"
           style="width:90px">保存</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel"
           onclick="javascript:$('#grantDialog').dialog('close')" style="width:90px">取消</a>
    </div>
    <div id="dialog-buttons-view">
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel"
           onclick="javascript:$('#viewDialog').dialog('close')" style="width:90px">关闭</a>
    </div>
</div>
</body>
</html>