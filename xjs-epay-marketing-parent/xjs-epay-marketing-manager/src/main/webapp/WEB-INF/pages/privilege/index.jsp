<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>后台管理系统</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <%@ include file="/WEB-INF/pages/common/head.jsp" %>
    <script src="${base}/pages/privilege/index.js?v=<%=Math.random()%>"></script>
</head>
<body>
<div id="dataGrid"></div>
<div id="dialogs">
    <div id="editDialog" class="easyui-dialog" data-options="resizable:true,modal:true"  style="width:400px" closed="true" buttons="#dialog-buttons-edit">
        <form id="editForm" class="search-condition__form"  method="post" novalidate >
            <input name="id" type="hidden">
            <div >
                <input name="grant_id" class="easyui-textbox" required="true" label="权限ID" style="width:100%">
            </div>
            <div >
                <select class="easyui-combobox" name="grant_type" label="权限类型" style="width:100%">
                    <c:forEach var="item" items="${authGrantTypes}">
                        <option value="${item}">${item}</option>
                    </c:forEach>
                </select>
            </div>
            <div >
                <input name="resource_id" class="easyui-textbox" required="true" label="资源ID" style="width:100%">
            </div>
            <div >
                <select class="easyui-combobox" name="resource_type" label="资源类型" style="width:100%">
                    <c:forEach var="item" items="${authPrivilegeTypes}">
                        <option value="${item}">${item}</option>
                    </c:forEach>
                </select>
            </div>
        </form>
    </div>
    <div id="addDialog" class="easyui-dialog" data-options="resizable:true,modal:true"  style="width:400px" closed="true" buttons="#dialog-buttons-add">
        <form id="addForm" method="post" novalidate style="margin:0;padding:20px 50px">
            <div style="margin-bottom:10px">
                <input name="grant_id" class="easyui-textbox" required="true" label="权限ID" style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <select class="easyui-combobox" name="grant_type" label="权限类型" style="width:100%">
                    <c:forEach var="item" items="${authGrantTypes}">
                        <option value="${item}">${item}</option>
                    </c:forEach>
                </select>
            </div>
            <div style="margin-bottom:10px">
                <input name="resource_id" class="easyui-textbox" required="true" label="资源ID" style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <select class="easyui-combobox" name="resource_type" label="资源类型" style="width:100%">
                    <c:forEach var="item" items="${authPrivilegeTypes}">
                        <option value="${item}">${item}</option>
                    </c:forEach>
                </select>
            </div>
        </form>
    </div>
    <div id="dialog-buttons-edit">
        <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="Page.editSubmit()"
           style="width:90px">Save</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel"
           onclick="javascript:$('#editDialog').dialog('close')" style="width:90px">Cancel</a>
    </div>
    <div id="dialog-buttons-add">
        <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="Page.addSubmit()"
           style="width:90px">Save</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel"
           onclick="javascript:$('#addDialog').dialog('close')" style="width:90px">Cancel</a>
    </div>
</div>
</body>
</html>