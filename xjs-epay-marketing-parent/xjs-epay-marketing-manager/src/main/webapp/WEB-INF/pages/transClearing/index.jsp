<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>后台管理系统</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <%@ include file="/WEB-INF/pages/common/head.jsp" %>
    <script src="${base}/pages/transClearing/index.js?v=<%=Math.random()%>"></script>
</head>
<body>
<input type="hidden" id="showOrg" value="${showOrg}">
<div style="padding:5px;">
    <form id="searchForm" class="search-condition__form" method="post" novalidate>
        <div  id="searchByOrg">
            <select  class="easyui-combobox" name="org_code" label="所属机构" style="width:100%">
                <option value="" selected>--请选择--</option>
                <c:forEach items="${orgList}" var="item">
                    <option value="${item.code }">${item.name }</option>
                </c:forEach>
            </select>
        </div>
        <div>
            <input name="family_name" class="easyui-textbox" label="姓名">
        </div>
        <div>
            <input name="username" class="easyui-textbox" label="用户名">
        </div>
        <div>
            <select class="easyui-combobox" name="clearing_status" label="清算状态" style="width:100%">
                <option value="" selected>--请选择--</option>
                <c:forEach var="item" items="${clearStatus}">
                    <option value="${item.name}">${item.text}</option>
                </c:forEach>
            </select>
        </div>

        <div>
            <input name="trans_time_start" class="easyui-datebox"
                   data-options="formatter:easyuiUtils.datebox_formatter,parser:easyuiUtils.datebox_parse"
                   label="交易时间">
        </div>
        <div>
            <input name="trans_time_end" class="easyui-datebox"
                   data-options="formatter:easyuiUtils.datebox_formatter,parser:easyuiUtils.datebox_parse"
                   label="-">
        </div>
        <a id="searchBtn" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-search'">查询</a>
    </form>
</div>
<div id="dataGrid"></div>
<div style="padding:10px;">
    合计 :
    交易笔数 [<span id="counter-cnt" style="margin-left: 5px;margin-right:5px;color: red">0</span>]
    交易金额 [<span id="counter-amount" style="margin-left: 5px;margin-right:5px;color: red">0</span>]
    操作手续费 [<span id="counter-counter-fee" style="margin-left: 5px;margin-right:5px;color: red">0</span>]
    手续费     [<span id="counter-operation-fee" style="margin-left: 5px;margin-right:5px;color: red">0</span>]
    到账金额 [<span id="counter-pay-amount" style="margin-left: 5px;margin-right:5px;color: red">0</span>]
</div>
<div id="dialogs">
    <div id="viewDialog" class="easyui-dialog" data-options="resizable:true,modal:true" style="width:540px"
         closed="true" buttons="#dialog-buttons-view">
        <form id="viewForm" method="post" novalidate style="margin:0;padding:20px 50px">
            <div style="margin-bottom:10px">
                <input id="view_trans_time" name="trans_time" class="easyui-textbox" readonly="readonly" label="交易时间"
                       style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input name="client_trans_id" class="easyui-textbox" readonly="readonly" label="订单号"
                       style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input name="username" class="easyui-textbox" readonly="readonly" label="用户名"
                       style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input name="family_name" class="easyui-textbox" readonly="readonly" label="姓名"
                       style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input name="idcard" class="easyui-textbox" readonly="readonly" label="身份证" style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input name="bank_name" class="easyui-textbox" readonly="readonly" label="银行名称" style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input name="bank_code" class="easyui-textbox" readonly="readonly" label="银行代码" style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input name="mobile" class="easyui-textbox" readonly="readonly" label="预留手机号" style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input name="branch_name" class="easyui-textbox" readonly="readonly" label="支行名称" style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input name="branch_code" class="easyui-textbox" readonly="readonly" label="联行号" style="width:100%">
            </div>

            <div style="margin-bottom:10px">
                <input id="view_amount" name="amount" class="easyui-textbox" readonly="readonly" label="交易金额"
                       style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input name="rate" class="easyui-textbox" readonly="readonly" label="扣率"
                       style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input id="view_operation_fee" name="operation_fee" class="easyui-textbox" readonly="readonly"
                       label="手续费"
                       style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input id="view_counter_fee" name="counter_fee" class="easyui-textbox" readonly="readonly" label="操作手续费"
                       style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input id="view_pay_amount" name="pay_amount" class="easyui-textbox" readonly="readonly" label="到账金额"
                       style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input id="view_clearing_status" name="clearing_status" class="easyui-textbox" readonly="readonly"
                       label="清算状态"
                       style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input id="view_clearing_type" name="clearing_type" class="easyui-textbox" readonly="readonly"
                       label="清算类型"
                       style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input name="referee_family_name" class="easyui-textbox" readonly="readonly" label="所属合作方"
                       style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input name="out_trans_no" class="easyui-textbox" readonly="readonly" label="外部订单号"
                       style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input name="memo" class="easyui-textbox" readonly="readonly" label="备注"
                       style="width:100%">
            </div>
        </form>
    </div>
    <div id="treeDialog" class="easyui-dialog" data-options="resizable:true,modal:true"
         style="width:380px;height: 420px;" closed="true"
         buttons="#dialog-buttons-tree">
        <form id="treeForm" method="post" novalidate style="margin:0;padding:10px 10px">
            <div style="margin-bottom:10px">
                <input name="search_family_name" class="easyui-searchbox" style="width: 320px"
                       data-options="searcher:Page.loadTree">
            </div>
            <div style="margin-bottom:10px">
                <div id="treeGrid" style="width: 320px;"></div>
            </div>
        </form>
    </div>
    <div id="dialog-buttons-tree">
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel"
           onclick="javascript:Page.clearTree();" style="width:90px">清除</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-ok"
           onclick="javascript:$('#treeDialog').dialog('close')" style="width:90px">确定</a>
    </div>
    <div id="dialog-buttons-view">
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel"
           onclick="javascript:$('#viewDialog').dialog('close')" style="width:90px">关闭</a>
    </div>
</div>
</body>
</html>