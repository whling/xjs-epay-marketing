<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="common/taglibs.jsp" %>
<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <title>MyPays后台管理系统</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <%@ include file="common/head.jsp" %>
</head>
<body class="easyui-layout">
<div data-options="region:'north'" class="navbar-top">
    <input type="hidden" id="orgCode" value="${orgCode}">
    <div class="logo"><img src="${base}/resources/images/logo.png" alt=""> MyPays后台管理系统</div>
    <div class="pull-right navbar-top-menu">
        欢迎您 ${familyName} <a href="javascript:changePwd();" class="">修改密码</a> <a href="javascript:logout();"
                                                                         class="logout">退出</a>
    </div>
</div>
<div data-options="region:'west',split:true" title="菜单" style="width:180px;" class="west">
    <div class="easyui-accordion" data-options="fit:true,border:false">
        <c:forEach items="${menu}" var="item">
            <div title="${item.name}" style="padding:10px;">
                <c:forEach items="${item.children}" var="subItem">
                    <div class="aside-submenu"><a href="#"
                                                  onclick="addTab('${subItem.name}','${base}/${subItem.url}')">${subItem.name}</a>
                    </div>
                </c:forEach>
            </div>
        </c:forEach>
    </div>
</div>
<div data-options="region:'center',title:'MyPays.cn'">
    <div id="main-tabs" class="easyui-tabs" data-options="fit:true,border:false,plain:true">
        <div title="About" style="padding:10px">
            hello world!
        </div>
    </div>
</div>
<div id="dialogs">
    <div id="pwdDialog" class="easyui-dialog" data-options="resizable:true,modal:true" style="width:380px"
         closed="true" buttons="#dialog-buttons-pwd">
        <form id="pwdForm" method="post" novalidate style="margin:0;padding:20px 50px">
            <input name="id" type="hidden">
            <div style="margin-bottom:10px">
                <input name="oldPassword" class="easyui-passwordbox" required="true" label="旧密码" style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input name="newPassword" class="easyui-passwordbox" required="true" label="新密码" style="width:100%">
            </div>
        </form>
    </div>
    <div id="dialog-buttons-pwd">
        <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="changePwdSubmit()"
           style="width:90px">保存</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel"
           onclick="javascript:$('#pwdDialog').dialog('close')" style="width:90px">取消</a>
    </div>
</div>
</body>
<script type="text/javascript">
    function addTab(title, url) {
        var $main = $('#main-tabs');
        if ($main.tabs('exists', title)) {//如果已经存在，激活
            $main.tabs('select', title);
        } else {//不存在，新增
            $main.tabs('add', {
                title: title,
                content: '<iframe scrolling="auto" frameborder="0"  src="' + url + '" style="width:100%;height:100%;"></iframe>',
                closable: true
            });
        }
    }
    function logout() {
        var orgCode = $("#orgCode").val();
        $.post(_webApp + "/user/logout.json", function (result) {
            if (result && result.success && result.data) {
                location.href = _webApp + "/login.html?id=" + orgCode;
            }
        }, 'json');
    }
    function changePwd() {
        $('#pwdDialog').dialog('open').dialog('center').dialog('setTitle', '重置密码');
    }
    function changePwdSubmit() {
        $('#pwdForm').form('submit', {
            url: _webApp + "/user/modifyPwd.json",
            success: function (response) {
                response = JSON.parse(response);
                if (response && response.success && response.data) {
                    $('#pwdDialog').dialog('close');        // close the dialog
                } else if (response.message) {
                    $.messager.alert('提示', response.message, "error");
                }
            }
        });
    }
</script>
</html>