<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>后台管理系统</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <%@ include file="/WEB-INF/pages/common/head.jsp" %>
    <script src="${base}/pages/organization/index.js?v=<%=Math.random()%>"></script>
</head>
<body>
<div>
    <form id="searchForm" class="search-condition__form" method="post" novalidate>
        <div>
            <input name="name" class="easyui-textbox" label="机构名称">
        </div>
        <div>
            <input name="linkman" class="easyui-textbox" label="联系人姓名">
        </div>
        <div>
            <select class="easyui-combobox" name="type" label="机构类型" style="width:100%">
                <option value="" selected>--请选择--</option>
                <option value="01">个人</option>
                <option value="02">企业</option>
            </select>
        </div>
        <div>
            <select class="easyui-combobox" name="enable" label="状态" style="width:100%">
                <option value="" selected>--请选择--</option>
                <option value="1">启用</option>
                <option value="0">禁用</option>
            </select>
        </div>
        <a id="searchBtn" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-search'">查询</a>
    </form>
</div>
<div id="dataGrid"></div>
<div id="dialogs">
    <div id="addDialog" class="easyui-dialog" data-options="resizable:true,modal:true" style="width:580px"
         closed="true" buttons="#dialog-buttons-add">
        <form id="addForm" method="post" novalidate style="height: 580px" enctype="multipart/form-data">
            <div class="easyui-tabs">
                <div title="基本信息" style="margin:0;padding:10px 20px">
                    <div style="margin-bottom:10px">
                        <input name="name" class="easyui-textbox" label="机构名称" style="width:100%" required="true">
                    </div>
                    <div style="margin-bottom:10px">
                        <select class="easyui-combobox" name="parent_code" label="所属机构" style="width:100%" required="true">
                            <c:forEach items="${orgList}" var="item">
                                <option value="${item.code }">${item.name }</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="app_name" class="easyui-textbox" label="应用名称" style="width:100%" required="true">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="linkman" class="easyui-textbox" label="联系人姓名" style="width:100%" required="true">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="linkman_idcard" class="easyui-textbox" label="联系人身份证" style="width:100%" required="true">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="telephone" class="easyui-textbox" label="固定电话" style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="linkman_mobile" class="easyui-textbox" label="手机号码" style="width:100%" required="true">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="linkman_email" class="easyui-textbox" label="电子邮箱" style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="address" class="easyui-textbox" label="联系地址" style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="withdrawal_fee" class="easyui-textbox" label="提现手续费" style="width:100%" required="true">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="custom_service_url" class="easyui-textbox" label="客服皮肤地址" style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="logo" class="easyui-filebox"
                               data-options="buttonText:'logo',buttonAlign: 'left'" style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="logo_small" class="easyui-filebox"
                               data-options="buttonText:'logo小图标',buttonAlign: 'left'" style="width:100%">
                    </div>
                    <%--<div style="margin-bottom:10px">--%>
                        <%--<select class="easyui-combobox" value="" name="pay_others" label="开放他人收款" style="width:100%">--%>
                            <%--<option value="1">是</option>--%>
                            <%--<option value="0">否</option>--%>
                        <%--</select>--%>
                    <%--</div>--%>
                    <div style="margin-bottom:10px">
                        <select class="easyui-combobox" value="" name="open_custom_service" label="开启人工客服" style="width:100%">
                            <option value="1">是</option>
                            <option value="0">否</option>
                        </select>
                    </div>
                    <div style="margin-bottom:10px">
                        <select class="easyui-combobox" value="" name="open_credit_apply" label="开放信用卡申请" style="width:100%">
                            <option value="1">是</option>
                            <option value="0">否</option>
                        </select>
                    </div>
                    <div style="margin-bottom:10px">
                        <select class="easyui-combobox" value="" name="open_borrow" label="开启借款功能" style="width:100%">
                            <option value="1">是</option>
                            <option value="0">否</option>
                        </select>
                    </div>
                    <div style="margin-bottom:10px">
                        <select class="easyui-combobox" value="" name="open_agency_profit" label="是否开启代理分润" style="width:100%">
                            <option value="1">是</option>
                            <option value="0">否</option>
                        </select>
                    </div>
                    <div style="margin-bottom:10px">
                        <select class="easyui-combobox" value="" name="open_register" label="是否开放注册" style="width:100%">
                            <option value="1">是</option>
                            <option value="0">否</option>
                        </select>
                    </div>
                    <div style="margin-bottom:10px">
                        <select class="easyui-combobox" value="" name="open_agent_grant" label="开放代理授权" style="width:100%">
                            <option value="1">是</option>
                            <option value="0">否</option>
                        </select>
                    </div>
                    <div style="margin-bottom:10px">
                        <select class="easyui-combobox" value="" name="type" label="机构类型" style="width:100%">
                            <option value="01">个人</option>
                            <option value="02">企业</option>
                        </select>
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="memo" class="easyui-textbox" data-options="multiline:true" label="说明"
                               style="width:100%">
                    </div>
                </div>
                <div title="微信公众号配置" style="margin:0;padding:10px 20px">
                    <div style="margin-bottom:10px">
                        <input name="weixin_name" class="easyui-textbox" label="名称" style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="weixin_appId" class="easyui-textbox" label="appId" style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="weixin_redirect_uri" class="easyui-textbox" label="redirect_uri"
                               style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="weixin_secret" class="easyui-textbox" label="secret" style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="weixin_qrcode" class="easyui-filebox"
                               data-options="buttonText:'微信二维码',buttonAlign: 'left'" style="width:100%">
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div id="editDialog" class="easyui-dialog" data-options="resizable:true,modal:true" style="width:580px"
         closed="true" buttons="#dialog-buttons-edit">
        <form id="editForm" method="post" novalidate style="height: 580px" enctype="multipart/form-data">
            <input name="id" type="hidden">
            <div class="easyui-tabs">
                <div title="基本信息" style="margin:0;padding:10px 20px">
                    <div style="margin-bottom:10px">
                        <input name="name" class="easyui-textbox" label="机构名称" style="width:100%" required="true">
                    </div>
                    <div style="margin-bottom:10px">
                        <select class="easyui-combobox" name="parent_code" label="所属机构" style="width:100%" required="true">
                            <c:forEach items="${orgList}" var="item">
                                <option value="${item.code }">${item.name }</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="app_name" class="easyui-textbox" label="应用名称" style="width:100%" required="true">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="linkman" class="easyui-textbox" label="联系人姓名" style="width:100%" required="true">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="linkman_idcard" class="easyui-textbox" label="联系人身份证" style="width:100%" required="true">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="telephone" class="easyui-textbox" label="固定电话" style="width:100%" required="true">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="linkman_mobile" class="easyui-textbox" label="手机号码" style="width:100%" required="true">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="linkman_email" class="easyui-textbox" label="电子邮箱" style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="address" class="easyui-textbox" label="联系地址" style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="withdrawal_fee" class="easyui-textbox" label="提现手续费" style="width:100%" required="true">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="custom_service_url" class="easyui-textbox" label="客服皮肤地址" style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="logo" class="easyui-filebox"
                               data-options="buttonText:'logo',buttonAlign: 'left'" style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="logo_small" class="easyui-filebox"
                               data-options="buttonText:'logo小图标',buttonAlign: 'left'" style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <select class="easyui-combobox" value="" name="pay_others" label="开放他人收款" style="width:100%">
                            <option value="1">是</option>
                            <option value="0">否</option>
                        </select>
                    </div>
                    <div style="margin-bottom:10px">
                        <select class="easyui-combobox" value="" name="open_agency_profit" label="开启代理分润" style="width:100%">
                            <option value="1">是</option>
                            <option value="0">否</option>
                        </select>
                    </div>
                    <div style="margin-bottom:10px">
                        <select class="easyui-combobox" value="" name="open_custom_service" label="开启人工客服" style="width:100%">
                            <option value="1">是</option>
                            <option value="0">否</option>
                        </select>
                    </div>
                    <div style="margin-bottom:10px">
                        <select class="easyui-combobox" value="" name="open_credit_apply" label="开放信用卡申请" style="width:100%">
                            <option value="1">是</option>
                            <option value="0">否</option>
                        </select>
                    </div>
                    <div style="margin-bottom:10px">
                        <select class="easyui-combobox" value="" name="open_agent_grant" label="开放代理授权" style="width:100%">
                            <option value="1">是</option>
                            <option value="0">否</option>
                        </select>
                    </div>
                    <div style="margin-bottom:10px">
                        <select class="easyui-combobox" value="" name="open_borrow" label="开启借款功能" style="width:100%">
                            <option value="1">是</option>
                            <option value="0">否</option>
                        </select>
                    </div>
                    <div style="margin-bottom:10px">
                        <select class="easyui-combobox" value="" name="open_register" label="是否开放注册" style="width:100%">
                            <option value="1">是</option>
                            <option value="0">否</option>
                        </select>
                    </div>
                    <div style="margin-bottom:10px">
                        <select class="easyui-combobox" value="" name="type" label="机构类型" style="width:100%">
                            <option value="01">个人</option>
                            <option value="02">企业</option>
                        </select>
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="memo" class="easyui-textbox" data-options="multiline:true" label="说明"
                               style="width:100%">
                    </div>
                </div>
                <div title="微信公众号配置" style="margin:0;padding:10px 20px">
                    <div style="margin-bottom:10px">
                        <input name="weixin_name" class="easyui-textbox" label="名称" style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="weixin_appId" class="easyui-textbox" label="appId" style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="weixin_redirect_uri" class="easyui-textbox" label="redirect_uri"
                               style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="weixin_secret" class="easyui-textbox" label="secret" style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="weixin_qrcode" class="easyui-filebox"
                               data-options="buttonText:'微信二维码',buttonAlign: 'left'" style="width:100%">
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div id="viewDialog" class="easyui-dialog" data-options="resizable:true,modal:true" style="width:580px"
         closed="true" buttons="#dialog-buttons-view">
        <div class="easyui-tabs" style="height: 580px">
            <div title="机构详情" style="margin:0;padding:10px 20px">
                <form id="viewOrgForm" method="post" novalidate>
                    <div style="margin-bottom:10px">
                        <input name="name" class="easyui-textbox" readonly="readonly" label="机构名称"
                               style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="linkman" class="easyui-textbox" readonly="readonly" label="联系人姓名"
                               style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="linkman_idcard" class="easyui-textbox" readonly="readonly" label="联系人身份证"
                               style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="telephone" class="easyui-textbox" readonly="readonly" label="固定电话"
                               style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="linkman_mobile" class="easyui-textbox" readonly="readonly" label="联系手机"
                               style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="custom_service_url" class="easyui-textbox" label="客服皮肤地址" style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="linkman_email" class="easyui-textbox" readonly="readonly" label="电子邮箱"
                               style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <select class="easyui-combobox" value="" name="open_custom_service" label="开启人工客服" readonly="readonly" style="width:100%">
                            <option value="1">是</option>
                            <option value="0">否</option>
                        </select>
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="enable" id="enable" class="easyui-textbox" readonly="readonly" label="是否启用"
                               style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="pay_others" id="pay_others" class="easyui-textbox" readonly="readonly"
                               label="是否开放收款"
                               style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <select class="easyui-combobox" value="" name="open_credit_apply" readonly="readonly" label="开放信用卡申请" style="width:100%">
                            <option value="1">是</option>
                            <option value="0">否</option>
                        </select>
                    </div>
                    <div style="margin-bottom:10px">
                        <select class="easyui-combobox" value="" name="open_agency_profit" label="开启代理分润" style="width:100%">
                            <option value="1">是</option>
                            <option value="0">否</option>
                        </select>
                    </div>
                    <div style="margin-bottom:10px">
                        <select class="easyui-combobox" value="" name="open_borrow" readonly="readonly" label="是否开启借款功能" style="width:100%">
                            <option value="1">是</option>
                            <option value="0">否</option>
                        </select>
                    </div>

                    <div style="margin-bottom:10px">
                        <select class="easyui-combobox" value="" name="open_agent_grant" readonly="readonly" label="开放代理授权" style="width:100%">
                            <option value="1">是</option>
                            <option value="0">否</option>
                        </select>
                    </div>


                    <div style="margin-bottom:10px">
                        <input name="address" class="easyui-textbox" readonly="readonly" label="联系地址"
                               style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="type" id="type" class="easyui-textbox" readonly="readonly" label="机构类型"
                               style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="logo" class="easyui-textbox" label="logo" style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="weixin_name" class="easyui-textbox" label="微信名称" style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="weixin_appId" class="easyui-textbox" label="微信appId" style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="weixin_redirect_uri" class="easyui-textbox" label="微信redirect_uri"
                               style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="weixin_secret" class="easyui-textbox" label="微信secret" style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="weixin_qrcode" class="easyui-textbox" label="微信二维码" style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="memo" class="easyui-textbox" readonly="readonly" label="说明"
                               style="width:100%">
                    </div>
                </form>
            </div>
            <div title="用户收款卡信息" style="margin:0;padding:20px 50px">
                <form id="viewUserForm" method="post" novalidate>
                    <div style="margin-bottom:10px">
                        <input name="username" class="easyui-textbox" readonly="readonly" label="用户名"
                               style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="family_name" class="easyui-textbox" readonly="readonly" label="姓名"
                               style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="idcard" class="easyui-textbox" readonly="readonly" label="身份证" style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="rate" class="easyui-textbox" readonly="readonly" label="分润"
                               style="width:100%">
                    </div>
                </form>
                <form id="viewCardForm" method="post" novalidate>
                    <div style="margin-bottom:10px">
                        <input name="bank_name" class="easyui-textbox" readonly="readonly" label="银行代码"
                               style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="bank_no" class="easyui-textbox" readonly="readonly" label="银行卡号"
                               style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="mobile" class="easyui-textbox" readonly="readonly" label="预留手机号"
                               style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="acc_province" class="easyui-textbox" readonly="readonly" label="所在省"
                               style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="acc_city" class="easyui-textbox" readonly="readonly" label="所在市"
                               style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="branch_code" class="easyui-textbox" readonly="readonly" label="联行号"
                               style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="branch_name" class="easyui-textbox" readonly="readonly" label="支行名称"
                               style="width:100%">
                    </div>
                </form>
            </div>
        </div>
        <div id="dialog-buttons-add">
            <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="Page.addSubmit()"
               style="width:90px">保存</a>
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel"
               onclick="javascript:$('#addDialog').dialog('close')" style="width:90px">取消</a>
        </div>
        <div id="dialog-buttons-edit">
            <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="Page.editSubmit()"
               style="width:90px">保存</a>
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel"
               onclick="javascript:$('#editDialog').dialog('close')" style="width:90px">取消</a>
        </div>
        <div id="dialog-buttons-view">
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel"
               onclick="javascript:$('#viewDialog').dialog('close')" style="width:90px">关闭</a>
        </div>
    </div>
</body>
</html>