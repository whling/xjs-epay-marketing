<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>后台管理系统</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <%@ include file="/WEB-INF/pages/common/head.jsp" %>
    <script src="${base}/pages/transSubChannel/index.js?v=<%=Math.random()%>"></script>
</head>
<body>
<div id="dataGrid"></div>
<div id="dialogs">
    <div id="addDialog" class="easyui-dialog" data-options="resizable:true,modal:true" style="width:480px"
         closed="true" buttons="#dialog-buttons-add">
        <form id="addForm" method="post" novalidate style="margin:0;padding:20px 50px">
            <div style="margin-bottom:10px">
                <select class="easyui-combobox"  name="trans_type" label="类型" style="width:100%" required="true">
                    <option value="" selected>--请选择--</option>
                    <option value="WEIXINPAY_QR">微信支付</option>
                    <option value="ALIPAY_QR">支付宝</option>
                    <option value="UNIONPAY_CGI">银联网关</option>
                    <option value="SHORTCUTPAY">快捷支付</option>
                    <option value="SHORTCUTPAY_INTEGRAL">快捷支付[有积分]</option>
                </select>
            </div>
            <div style="margin-bottom:10px">
                <input name="name" class="easyui-textbox" label="名称" style="width:100%" required="true">
            </div>
            <div style="margin-bottom:10px">
                <input name="sub_trans_type" class="easyui-textbox" label="子渠道"   style="width:100%" required="true">
            </div>
            <div style="margin-bottom:10px">
                <input name="memo" class="easyui-textbox" label="说明" style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input name="pay_limit_min" class="easyui-textbox" label="限额最低" style="width:100%" required="true">
            </div>
            <div style="margin-bottom:10px">
                <input name="pay_limit_max" class="easyui-textbox" label="限额最高" style="width:100%" required="true">
            </div>
            <div style="margin-bottom:10px">
                <input name="time_limit_start" class="easyui-textbox" label="交易时间开始" style="width:100%" required="true">
            </div>
            <div style="margin-bottom:10px">
                <input name="time_limit_end" class="easyui-textbox" label="交易时间结束" style="width:100%" required="true">
            </div>
            <div style="margin-bottom:10px">
                <input name="priority" class="easyui-textbox" label="优先级" style="width:100%" required="true">
            </div>
            <div style="margin-bottom:10px">
                <input name="notify_url" class="easyui-textbox" label="通知地址" style="width:100%" >
            </div>
            <div style="margin-bottom:10px">
                <input name="page_url" class="easyui-textbox" label="结果页面" style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input name="day_limit_max" class="easyui-textbox" label="当日限额" style="width:100%" required="true">
            </div>
        </form>
    </div>

    <div id="editDialog" class="easyui-dialog" data-options="resizable:true,modal:true" style="width:480px"
         closed="true" buttons="#dialog-buttons-edit">
        <form id="editForm" method="post" novalidate style="margin:0;padding:20px 50px">
            <input name="id" type="hidden">
            <div style="margin-bottom:10px">
                <select class="easyui-combobox"  name="trans_type" label="类型" style="width:100%"  required="true">
                    <option value="" selected>--请选择--</option>
                    <option value="WEIXINPAY_QR">微信支付</option>
                    <option value="ALIPAY_QR">支付宝</option>
                    <option value="UNIONPAY_CGI">银联网关</option>
                    <option value="SHORTCUTPAY">快捷支付</option>
                    <option value="SHORTCUTPAY_INTEGRAL">快捷支付[有积分]</option>
                </select>
            </div>
            <div style="margin-bottom:10px">
                <input name="name" class="easyui-textbox" label="名称" style="width:100%"  required="true">
            </div>
            <div style="margin-bottom:10px">
                <input name="sub_trans_type" class="easyui-textbox" label="子渠道"  readonly="readonly" style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input name="memo" class="easyui-textbox" label="说明" style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input name="pay_limit_min" class="easyui-textbox" label="限额最低" style="width:100%"  required="true">
            </div>
            <div style="margin-bottom:10px">
                <input name="pay_limit_max" class="easyui-textbox" label="限额最高" style="width:100%"  required="true">
            </div>
            <div style="margin-bottom:10px">
                <input name="time_limit_start" class="easyui-textbox" label="交易时间开始" style="width:100%"  required="true">
            </div>
            <div style="margin-bottom:10px">
                <input name="time_limit_end" class="easyui-textbox" label="交易时间结束" style="width:100%"  required="true">
            </div>
            <div style="margin-bottom:10px">
                <input name="priority" class="easyui-textbox" label="优先级" style="width:100%"  required="true">
            </div>
            <div style="margin-bottom:10px">
                <input name="notify_url" class="easyui-textbox" label="通知地址" style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input name="page_url" class="easyui-textbox" label="结果页面" style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input name="day_limit_max" class="easyui-textbox" label="当日限额" style="width:100%"  required="true">
            </div>
        </form>
    </div>
    <div id="dialog-buttons-add">
        <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="Page.addSubmit()"
           style="width:90px">保存</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel"
           onclick="javascript:$('#addDialog').dialog('close')" style="width:90px">取消</a>
    </div>
    <div id="dialog-buttons-edit">
        <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="Page.editSubmit()"
           style="width:90px">保存</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel"
           onclick="javascript:$('#editDialog').dialog('close')" style="width:90px">取消</a>
    </div>
</div>
</body>
</html>