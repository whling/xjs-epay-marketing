<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>后台管理系统</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <%@ include file="/WEB-INF/pages/common/head.jsp" %>
    <script src="${base}/pages/poster/index.js?v=<%=Math.random()%>"></script>
</head>
<body>
<div>
    <form id="searchForm" class="search-condition__form" method="post" novalidate>
        <div>
            <select class="easyui-combobox" name="org_code" label="所属机构" style="width:100%">
                <option value="" selected>--请选择--</option>
                <c:forEach items="${orgList}" var="item">
                    <option value="${item.code }">${item.name }</option>
                </c:forEach>
            </select>
        </div>
        <div>
            <input name="time_begin" class="easyui-datebox"
                   data-options="formatter:easyuiUtils.datebox_formatter,parser:easyuiUtils.datebox_parse"
                   label="开始时间">
        </div>
        <div>
            <input name="time_end" class="easyui-datebox"
                   data-options="formatter:easyuiUtils.datebox_formatter,parser:easyuiUtils.datebox_parse"
                   label="结束时间">
        </div>
        <a id="searchBtn" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-search'">查询</a>
    </form>
</div>
<div id="dataGrid"></div>
<div id="dialogs">
    <input type="hidden" id="loginUserOrgCode" value="${loginUserOrgCode}">
    <div id="editDialog" class="easyui-dialog" data-options="resizable:true,modal:true" style="width:550px;height:700px"
         closed="true" buttons="#dialog-buttons-edit">
        <form id="editForm" method="post" novalidate style="margin:0;padding:20px 50px" enctype="multipart/form-data">
            <input name="id" type="hidden">
            <div style="margin-bottom:10px">
                <select class="easyui-combobox" name="org_code" id="editForm_org_code" label="所属机构" style="width:100%">
                    <c:forEach var="item" items="${orgList}">
                        <option value="${item.code}">${item.name}</option>
                    </c:forEach>
                </select>
                <input type="hidden" name="org_name">
            </div>
            <div style="margin-bottom:10px">
                <input name="title" class="easyui-textbox" required="true" label="标题" style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input name="logo_x" class="easyui-textbox" label="logo宽度" style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input name="logo_y" class="easyui-textbox" label="logo高度" style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input name="logo_t" class="easyui-textbox" label="logo距顶偏移" style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input name="logo_l" class="easyui-textbox" label="logo距左偏移" style="width:100%">
            </div>

            <div style="margin-bottom:10px">
                <input name="code_x" class="easyui-textbox" label="code宽度" style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input name="code_y" class="easyui-textbox" label="code高度" style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input name="code_t" class="easyui-textbox" label="code距顶偏移" style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input name="code_l" class="easyui-textbox" label="code距左偏移" style="width:100%">
            </div>

            <div style="margin-bottom:10px">
                <input name="rate_x" class="easyui-textbox" label="rate宽度" style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input name="rate_y" class="easyui-textbox" label="rate高度" style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input name="rate_t" class="easyui-textbox" label="rate距顶偏移" style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input name="rate_l" class="easyui-textbox" label="rate距左偏移" style="width:100%">
            </div>

            <div style="margin-bottom:10px">
                <input name="poster_img" class="easyui-filebox"
                       data-options="buttonText:'海报图片',buttonAlign: 'left'" style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <div style="margin-bottom:10px;display: none" id="postDiv">
                    <img src="" id="postUrl" width="100" height="100">
                </div>
                <div style="margin-bottom:10px">
                    <input name="rate_img" class="easyui-filebox"
                           data-options="buttonText:'费率图片',buttonAlign: 'left'" style="width:100%">
                </div>
                <div style="margin-bottom:10px">
                <div style="margin-bottom:10px;display: none" id="rateDiv">
                    <img src="" id="rateUrl" width="100" height="100">
                </div>
                <%--<div style="margin-bottom:10px">--%>
                <%--<input name="rate_img_url" class="easyui-textbox"  label="费率图片地址" style="width:100%">--%>
                <%--</div>--%>
                <div style="margin-bottom:10px">
                    <input name="slogan" class="easyui-textbox" label="广告语" style="width:100%">
                </div>
                <div style="margin-bottom:10px">
                    <input name="time_begin" class="easyui-datebox"
                           data-options="formatter:easyuiUtils.datebox_formatter,parser:easyuiUtils.datebox_parse"
                           required="true" label="开始时间:" style="width:100%">
                </div>
                <div style="margin-bottom:10px">
                    <input name="time_end" class="easyui-datebox"
                           data-options="formatter:easyuiUtils.datebox_formatter,parser:easyuiUtils.datebox_parse"
                           required="true" label="结束时间:" style="width:100%">
                </div>
        </form>
    </div>
    <div id="addDialog" class="easyui-dialog" style="width:480px" data-options="resizable:true,modal:true" closed="true"
         buttons="#dialog-buttons-add">
        <form id="addForm" method="post" novalidate style="margin:0;padding:20px 50px" enctype="multipart/form-data">
            <input name="id" type="hidden">
            <div style="margin-bottom:10px">
                <select class="easyui-combobox" name="org_code" id="addForm_org_code" label="所属机构" style="width:100%">
                    <c:forEach var="item" items="${orgList}">
                        <option value="${item.code}">${item.name}</option>
                    </c:forEach>
                </select>
                <input type="hidden" name="org_name">
            </div>
            <div style="margin-bottom:10px">
                <input name="title" class="easyui-textbox" required="true" label="标题" style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input name="logo_x" class="easyui-textbox" label="logo宽度" style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input name="logo_y" class="easyui-textbox" label="logo高度" style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input name="logo_t" class="easyui-textbox" label="logo距顶偏移" style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input name="logo_l" class="easyui-textbox" label="logo距左偏移" style="width:100%">
            </div>

            <div style="margin-bottom:10px">
                <input name="code_x" class="easyui-textbox" label="code宽度" style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input name="code_y" class="easyui-textbox" label="code高度" style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input name="code_t" class="easyui-textbox" label="code距顶偏移" style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input name="code_l" class="easyui-textbox" label="code距左偏移" style="width:100%">
            </div>


            <div style="margin-bottom:10px">
                <input name="rate_x" class="easyui-textbox" label="rate宽度" style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input name="rate_y" class="easyui-textbox" label="rate高度" style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input name="rate_t" class="easyui-textbox" label="rate距顶偏移" style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input name="rate_l" class="easyui-textbox" label="rate距左偏移" style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input name="poster_img" class="easyui-filebox"
                       data-options="buttonText:'海报图片',buttonAlign: 'left'" style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input name="rate_img" class="easyui-filebox"
                       data-options="buttonText:'费率图片',buttonAlign: 'left'" style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input name="slogan" class="easyui-textbox" label="广告语" style="width:100%">
            </div>

            <div style="margin-bottom:10px">
                <input name="time_begin" class="easyui-datebox"
                       data-options="formatter:easyuiUtils.datebox_formatter,parser:easyuiUtils.datebox_parse"
                       required="true" label="开始时间:" style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input name="time_end" class="easyui-datebox"
                       data-options="formatter:easyuiUtils.datebox_formatter,parser:easyuiUtils.datebox_parse"
                       required="true" label="结束时间:" style="width:100%">
            </div>
        </form>
    </div>
    <div id="dialog-buttons-edit">
        <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="Page.editSubmit()"
           style="width:90px">Save</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel"
           onclick="javascript:$('#editDialog').dialog('close')" style="width:90px">Cancel</a>
    </div>
    <div id="dialog-buttons-add">
        <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="Page.addSubmit()"
           style="width:90px">Save</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel"
           onclick="javascript:$('#addDialog').dialog('close')" style="width:90px">Cancel</a>
    </div>
</div>
</body>
</html>