<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>后台管理系统</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <%@ include file="/WEB-INF/pages/common/head.jsp" %>
    <script src="${base}/pages/trans/statistics.js?v=<%=Math.random()%>"></script>
</head>
<body>
<input type="hidden" id="showOrg" value="${showOrg}">
<div style="padding:5px;">
    <form id="searchForm" class="search-condition__form" method="post" novalidate>
        <div id="searchByOrg">
            <select class="easyui-combobox" name="org_code" label="所属机构" style="width:100%">
                <option value="" selected>--请选择--</option>
                <c:forEach items="${orgList}" var="item">
                    <option value="${item.code }">${item.name }</option>
                </c:forEach>
            </select>
        </div>
        <div>
            <input name="username" class="easyui-textbox" label="用户名">
        </div>
        <div>
            <input name="family_name" class="easyui-textbox" label="姓名">
        </div>
        <div>
            <select class="easyui-combobox" name="trans_type" label="支付方式" style="width:100%">
                <option value="" selected>--请选择--</option>
                <option value="WEIXINPAY_QR">微信支付</option>
                <option value="ALIPAY_QR">支付宝</option>
                <option value="SHORTCUTPAY">快捷支付</option>
            </select>
        </div>
        <div id="searchBySub_trans_type">
            <select class="easyui-combobox" name="sub_trans_type" label="支付渠道" style="width:100%">
                <option value="" selected>--请选择--</option>
                <option value="WEIXINPAY_QR">微信支付</option>
                <option value="ALIPAY_QR">支付宝</option>
                <option value="SHORTCUTPAY_MILIAN">米联无积分</option>
                <option value="SHORTCUTPAY_MILIAN_INTEGRAL">米联有积分</option>
                <option value="SHORTCUTPAY_EFFERSONPAY">卡友</option>
                <option value="SHORTCUTPAY_HANYIN">瀚银</option>
            </select>
        </div>
        <div class="spec-label">
            <input name="trans_time_start" class="easyui-datebox"
                   data-options="formatter:easyuiUtils.datebox_formatter,parser:easyuiUtils.datebox_parse"
                   label="交易开始时间:">
        </div>
        <div class="spec-label">
            <input name="trans_time_end" class="easyui-datebox"
                   data-options="formatter:easyuiUtils.datebox_formatter,parser:easyuiUtils.datebox_parse"
                   label="交易结束时间:">
        </div>
        <a id="searchBtn" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-search'">查询</a>
    </form>
</div>
<div id="dataGrid"></div>
<div style="padding:10px;">
    合计 : 交易笔数 [<span id="counter-cnt" style="margin-left: 5px;margin-right:5px;color: red">0</span>] 交易金额[<span
        id="counter-amount" style="margin-left: 5px;margin-right:5px;color: red">0</span>] 到账金额[<span
        id="counter-pay-amount" style="margin-left: 5px;margin-right:5px;color: red">0</span>]
</div>
<div id="dialogs">
    <div id="treeDialog" class="easyui-dialog" style="width:380px;height: 420px;" closed="true"
         buttons="#dialog-buttons-tree">
        <form id="treeForm" method="post" novalidate style="margin:0;padding:10px 10px">
            <div style="margin-bottom:10px">
                <input name="search_family_name" class="easyui-searchbox" style="width: 320px"
                       data-options="searcher:Page.loadTree">
            </div>
            <div style="margin-bottom:10px">
                <div id="treeGrid" style="width: 320px;"></div>
            </div>
        </form>
    </div>
    <div id="dialog-buttons-tree">
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel"
           onclick="javascript:Page.clearTree();" style="width:90px">清除</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-ok"
           onclick="javascript:$('#treeDialog').dialog('close')" style="width:90px">确定</a>
    </div>
</div>
</body>
</html>