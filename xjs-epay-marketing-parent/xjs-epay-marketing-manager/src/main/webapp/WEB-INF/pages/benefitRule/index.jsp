<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>后台管理系统</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <%@ include file="/WEB-INF/pages/common/head.jsp" %>
    <script src="${base}/pages/benefitRule/index.js?v=<%=Math.random()%>"></script>
</head>
<body>
<div>
    <form id="searchForm" class="search-condition__form" method="post" novalidate>
        <div>
            <select class="easyui-combobox" name="org_code" label="所属机构" style="width:100%">
                <option value="" selected>--请选择--</option>
                <c:forEach items="${orgList}" var="item">
                <option value="${item.code }">${item.name }</option>
                </c:forEach>
            </select>
        </div>
        <div>
            <input name="rate" class="easyui-textbox" label="分润比率">
        </div>
        <div>
            <input name="referee_cnt_start" class="easyui-textbox" label="分润起点">
        </div>
        <div>
            <input name="referee_cnt_end" class="easyui-textbox" label="分润终点">
        </div>
        <a id="searchBtn" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-search'">查询</a>
    </form>
</div>
<div id="dataGrid"></div>
<div id="dialogs">
    <div id="addDialog" class="easyui-dialog" data-options="resizable:true,modal:true" style="width:500px"
         closed="true" buttons="#dialog-buttons-add">
        <form id="addForm" method="post" novalidate style="margin:0;padding:20px 50px">
            <div style="margin-bottom:10px">
                <select class="easyui-combobox" name="org_code" label="所属机构" style="width:100%">
                    <option value="" selected>--请选择--</option>
                    <c:forEach items="${orgList}" var="item">
                        <option value="${item.code }">${item.name }</option>
                    </c:forEach>
                </select>
            </div>
            <div style="margin-bottom:10px">
                <input name="rate" class="easyui-textbox" label="分润比率" style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input name="referee_cnt_start" class="easyui-textbox" label="分润起点" style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input name="referee_cnt_end" class="easyui-textbox" label="分润终点" style="width:100%">
            </div>
        </form>
    </div>
    <div id="editDialog" class="easyui-dialog" data-options="resizable:true,modal:true" style="width:480px"
         closed="true" buttons="#dialog-buttons-edit">
        <form id="editForm" method="post" novalidate style="margin:0;padding:20px 50px">
            <input name="id" type="hidden">
            <div style="margin-bottom:10px">
                <select class="easyui-combobox" name="org_code" label="所属机构" style="width:100%">
                    <option value="" selected>--请选择--</option>
                    <c:forEach items="${orgList}" var="item">
                        <option value="${item.code }">${item.name }</option>
                    </c:forEach>
                </select>
            </div>
            <div style="margin-bottom:10px">
                <input name="rate" class="easyui-textbox" label="分润比率" style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input name="referee_cnt_start" class="easyui-textbox" label="分润起点" style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input name="referee_cnt_end" class="easyui-textbox" label="分润终点" style="width:100%">
            </div>
        </form>
    </div>
    <div id="dialog-buttons-add">
        <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="Page.addSubmit()"
           style="width:90px">保存</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel"
           onclick="javascript:$('#addDialog').dialog('close')" style="width:90px">取消</a>
    </div>
    <div id="dialog-buttons-edit">
        <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="Page.editSubmit()"
           style="width:90px">保存</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel"
           onclick="javascript:$('#editDialog').dialog('close')" style="width:90px">取消</a>
    </div>
</div>
</body>
</html>