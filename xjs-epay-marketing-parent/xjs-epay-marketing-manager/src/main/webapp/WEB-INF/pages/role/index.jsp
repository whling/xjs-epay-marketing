<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>后台管理系统</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <%@ include file="/WEB-INF/pages/common/head.jsp" %>
    <script src="${base}/pages/role/index.js?v=<%=Math.random()%>"></script>
</head>
<body>
<div id="dataGrid"></div>
<div id="dialogs">
    <input type="hidden" id="loginUserOrgCode" value="${loginUserOrgCode}">
    <div id="editDialog" class="easyui-dialog" data-options="resizable:true,modal:true" style="width:400px"
         closed="true" buttons="#dialog-buttons-edit">
        <form id="editForm" method="post" novalidate style="margin:0;padding:20px 50px">
            <input name="id" type="hidden">
            <div>
                <input name="name" class="easyui-textbox" required="true" label="角色名称" style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <select class="easyui-combobox" name="org_code" id="editForm_org_code" label="所属机构" style="width:100%">
                    <c:forEach var="item" items="${orgs}">
                        <option value="${item.code}">${item.name}</option>
                    </c:forEach>
                </select>
                <input type="hidden" name="org_name">
            </div>
            <div>
                <input name="start_date" class="easyui-datebox"
                       data-options="formatter:easyuiUtils.datebox_formatter,parser:easyuiUtils.datebox_parse"
                       required="true" label="开始时间:" style="width:100%">
            </div>
            <div>
                <input name="end_date" class="easyui-datebox"
                       data-options="formatter:easyuiUtils.datebox_formatter,parser:easyuiUtils.datebox_parse"
                       required="true" label="结束时间:" style="width:100%">
            </div>
        </form>
    </div>
    <div id="addDialog" class="easyui-dialog" style="width:400px" data-options="resizable:true,modal:true" closed="true"
         buttons="#dialog-buttons-add">
        <form id="addForm" method="post" novalidate style="margin:0;padding:20px 50px">
            <div style="margin-bottom:10px">
                <input name="name" class="easyui-textbox" required="true" label="角色名称" style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <select class="easyui-combobox" id="addForm_org_code" name="org_code" label="所属机构" style="width:100%">
                    <c:forEach var="item" items="${orgs}">
                        <option value="${item.code}">${item.name}</option>
                    </c:forEach>
                </select>
                <input type="hidden" name="org_name">
            </div>
            <div style="margin-bottom:10px">
                <input name="start_date" class="easyui-datebox"
                       data-options="formatter:easyuiUtils.datebox_formatter,parser:easyuiUtils.datebox_parse"
                       required="true" label="开始时间:" style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input name="end_date" class="easyui-datebox"
                       data-options="formatter:easyuiUtils.datebox_formatter,parser:easyuiUtils.datebox_parse"
                       required="true" label="结束时间:" style="width:100%">
            </div>
        </form>
    </div>
    <div id="grantDialog" class="easyui-dialog" data-options="resizable:true,modal:true" style="width:400px"
         closed="true" buttons="#dialog-buttons-grant">
        <form id="grantForm" method="post" novalidate style="margin:0;padding:20px 50px">
            <input name="id" type="hidden">
            <input id="resourceIds" name="resourceIds" type="hidden">
            <div style="margin-bottom:10px">
                <input name="name" class="easyui-textbox" label="角色名称" style="width:100%">
            </div>
            <div id="resourceDataGrid"></div>
        </form>
    </div>
    <div id="dialog-buttons-edit">
        <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="Page.editSubmit()"
           style="width:90px">Save</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel"
           onclick="javascript:$('#editDialog').dialog('close')" style="width:90px">Cancel</a>
    </div>
    <div id="dialog-buttons-add">
        <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="Page.addSubmit()"
           style="width:90px">Save</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel"
           onclick="javascript:$('#addDialog').dialog('close')" style="width:90px">Cancel</a>
    </div>
    <div id="dialog-buttons-grant">
        <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="Page.grantSubmit()"
           style="width:90px">Save</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel"
           onclick="javascript:$('#grantDialog').dialog('close')" style="width:90px">Cancel</a>
    </div>
</div>
</body>
</html>