<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>后台管理系统</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <%@ include file="/WEB-INF/pages/common/head.jsp" %>
    <script src="${base}/pages/resource/index.js?v=<%=Math.random()%>"></script>
</head>
<body>
<div id="dataGrid"></div>
<div id="dialogs">
    <div id="editDialog" class="easyui-dialog" data-options="resizable:true,modal:true"  style="width:400px" closed="true" buttons="#dialog-buttons-edit">
        <form id="editForm" class="search-condition__form" method="post" novalidate >
            <input name="id" type="hidden">
            <div >
                <input name="class_icon" class="easyui-textbox" label="图标" style="width:100%">
            </div>
            <div >
                <input name="class_name" class="easyui-textbox" label="类名" style="width:100%">
            </div>
            <div >
                <input name="name" class="easyui-textbox" required="true" label="资源名称" style="width:100%">
            </div>
            <div >
                <input id="parentResourcesComboboxEdit" class="easyui-combotree" name="parent_code" style="width:100%"
                       data-options="panelHeight:'auto',label: '所属资源:',labelPosition: 'left'">
                </input>
            </div>
            <div >
                <select class="easyui-combobox" name="resource_type" required="true" label="资源类型" style="width:100%">
                    <c:forEach var="item" items="${resourceTypes}">
                        <option value="${item}">${item}</option>
                    </c:forEach>
                </select>
            </div>
            <div >
                <input name="url" class="easyui-textbox" label="URL" style="width:100%">
            </div>
            <div >
                <input name="view_class" class="easyui-textbox" label="视图类" style="width:100%">
            </div>
            <div >
                <input name="sort_no" class="easyui-numberbox" value="0" label="排序号" data-options="min:0,max:100,value:0" style="width:100%">
            </div>
        </form>
    </div>
    <div id="addDialog" class="easyui-dialog" data-options="resizable:true,modal:true"  style="width:400px" closed="true" buttons="#dialog-buttons-add">
        <form id="addForm" method="post" style="margin:0;padding:20px 50px">
            <div style="margin-bottom:10px">
                <input name="class_icon" class="easyui-textbox" label="图标" style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input name="class_name" class="easyui-textbox" label="类名" style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input name="name" class="easyui-textbox" required="true" label="资源名称" style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input id="parentResourcesComboboxAdd" class="easyui-combotree" name="parent_code" style="width:100%"
                       data-options="panelHeight:'auto',label: '所属资源:',labelPosition: 'left'">
                </input>
            </div>
            <div style="margin-bottom:10px">
                <select class="easyui-combobox" name="resource_type" required="true" label="资源类型" style="width:100%">
                    <c:forEach var="item" items="${resourceTypes}">
                        <option value="${item}">${item}</option>
                    </c:forEach>
                </select>
            </div>
            <div style="margin-bottom:10px">
                <input name="url" class="easyui-textbox" label="URL" style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input name="view_class" class="easyui-textbox" label="视图类" style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input name="sort_no" class="easyui-numberbox" value="0" label="排序号" data-options="min:0,max:100,value:0" style="width:100%">
            </div>
        </form>
    </div>
    <div id="dialog-buttons-edit">
        <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="Page.editSubmit()"
           style="width:90px">Save</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel"
           onclick="javascript:$('#editDialog').dialog('close')" style="width:90px">Cancel</a>
    </div>
    <div id="dialog-buttons-add">
        <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="Page.addSubmit()"
           style="width:90px">Save</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel"
           onclick="javascript:$('#addDialog').dialog('close')" style="width:90px">Cancel</a>
    </div>
</div>
</body>
</html>