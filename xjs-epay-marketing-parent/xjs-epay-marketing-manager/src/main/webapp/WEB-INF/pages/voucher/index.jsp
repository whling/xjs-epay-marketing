<%@ page import="org.xiajinsuo.epay.security.Organization" %>
<%@ page import="org.xiajinsuo.epay.util.LoginUtil" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>${orgName}后台管理系统</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <%@ include file="/WEB-INF/pages/common/head.jsp" %>
    <script src="${base}/pages/voucher/index.js?v=<%=Math.random()%>"></script>
</head>
<body>
<div>
    <form id="searchForm" class="search-condition__form" method="post" novalidate>
        <div style="margin-bottom:10px">
            <select class="easyui-combobox" name="org_code" id="org_code" label="所属机构" style="width:100%">
                <option value="">全部</option>
                <c:forEach var="item" items="${orgs}">
                    <option value="${item.code}">${item.name}</option>
                </c:forEach>
            </select>
            <input type="hidden" name="org_name" id="edit_org_name">
        </div>
        <div>
            <input name="time_start" id="timeStart" class="easyui-datebox"
                   data-options="formatter:easyuiUtils.datebox_formatter,parser:easyuiUtils.datebox_parse"
                   label="有效期开始">
        </div>
        <div>
            <input name="time_end" id="timeEnd" class="easyui-datebox"
                   data-options="formatter:easyuiUtils.datebox_formatter,parser:easyuiUtils.datebox_parse"
                   label="有效期截止">
        </div>
        <div>
            <select class="easyui-combobox" name="enable" label="是否可用" style="width:100%">
                <option value="" selected>--请选择--</option>
                <option value="1">启用</option>
                <option value="0">禁用</option>
            </select>
        </div>
        <a id="searchBtn" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-search'">查询</a>
    </form>
</div>


<div id="dataGrid"></div>
<div id="dialogs">
    <input type="hidden" id="loginUserOrgCode" value="${loginUserOrgCode}">
<div id="editDialog" class="easyui-dialog" data-options="resizable:true,modal:true" style="width:480px"
     closed="true" buttons="#dialog-buttons-edit">
    <form id="editForm" method="post" novalidate style="margin:0;padding:20px 50px">
        <input name="id" type="hidden">
        <div style="margin-bottom:10px">
            <select class="easyui-combobox" name="enable" id="editenable" label="启用状态" style="width:100%">
                <option value="1">可用</option>
                <option value="0">不可用</option>

            </select>
        </div>
        <div style="margin-bottom:10px">
            <input name="amount" class="easyui-textbox" id="editamount" label="交易金额" style="width:100%" >
        </div>
        <div style="margin-bottom:10px">
            <input class="easyui-textbox" data-options="multiline:true" name="memo"   label="备注" style="width:100%">
        </div>
        <div style="margin-bottom:10px">
            <select class="easyui-combobox" id="editForm_org_code" name="org_code" label="所属机构" style="width:100%">
                <c:forEach var="item" items="${orgs}">
                    <option value="${item.code}">${item.name}</option>
                </c:forEach>
            </select>
            <input type="hidden" name="org_name">
        </div>

        <div style="margin-bottom:10px">
            <input class="easyui-textbox" data-options="multiline:true" id="editpaylimit" name="pay_limit"  label="最低消费" style="width:100%">
        </div>
        <div style="margin-bottom:10px">

            <select class="easyui-combobox"  id="edit_type" name="type" label="类型" style="width:100%">
                <option value="00" selected="true">通用</option>
                <option value="01">微信</option>
                <option value="02">支付宝</option>
                <option value="03">快捷</option>

            </select>
        </div>
        <div style="margin-bottom:10px">
            <input class="easyui-textbox" data-options="multiline:true" name="title"  label="活动标题" style="width:100%">
        </div>
        <div style="margin-bottom:10px">
            <input name="time_start" id="edit_time_start" class="easyui-datebox"
                   data-options="formatter:easyuiUtils.datebox_formatter,parser:easyuiUtils.datebox_parse"
                   label="开始时间" style="width:100%">
        </div>
        <div style="margin-bottom:10px">
            <input name="time_end" id="edit_time_end" class="easyui-datebox"
                   data-options="formatter:easyuiUtils.datebox_formatter,parser:easyuiUtils.datebox_parse"
                   label="结束时间" style="width:100%">
        </div>

    </form>

</div>

<div id="addDialog" class="easyui-dialog" data-options="resizable:true,modal:true" style="width:480px"
     closed="true" buttons="#dialog-buttons-add">
    <form id="addForm" method="post" novalidate style="margin:0;padding:20px 50px">
        <input name="id" type="hidden">
        <div style="margin-bottom:10px">
            <select class="easyui-combobox" name="enable" id="addenable" label="启用状态" style="width:100%">
                <option value="1">可用</option>
                <option value="0">不可用</option>

            </select>
        </div>
        <div style="margin-bottom:10px">
            <input name="amount" class="easyui-textbox" id="addamount" label="交易金额" style="width:100%">
        </div>
        <div style="margin-bottom:10px">
            <input class="easyui-textbox" data-options="multiline:true" name="memo"  label="备注" style="width:100%">
        </div>
        <div style="margin-bottom:10px">
            <select class="easyui-combobox" id="addForm_org_code" name="org_code" label="所属机构" style="width:100%">
                <c:forEach var="item" items="${orgs}">
                    <option value="${item.code}">${item.name}</option>
                </c:forEach>
            </select>
            <input type="hidden" name="org_name">
        </div>
        <div style="margin-bottom:10px">
            <input class="easyui-textbox" data-options="multiline:true" id="addpaylimit" onkeyup="this.value=this.value.replace(/\D/gi,'')" name="pay_limit"  label="最低消费" style="width:100%">
        </div>
        <div style="margin-bottom:10px">

            <select class="easyui-combobox"  id="add_type" name="type" label="类型" style="width:100%">
                <option value="00" selected="true">通用</option>
                <option value="01">微信</option>
                <option value="02">支付宝</option>
                <option value="03">快捷</option>

            </select>
        </div>
        <div style="margin-bottom:10px">
            <input class="easyui-textbox" data-options="multiline:true" name="title"  label="活动标题" style="width:100%">
        </div>
        <div style="margin-bottom:10px">
            <input name="time_start" class="easyui-datebox"
                   data-options="formatter:easyuiUtils.datebox_formatter,parser:easyuiUtils.datebox_parse"
                   label="开始时间" id="add_time_start" style="width:100%">
        </div>
        <div style="margin-bottom:10px">
            <input name="time_end" class="easyui-datebox"
                   data-options="formatter:easyuiUtils.datebox_formatter,parser:easyuiUtils.datebox_parse"
                   label="结束时间" id="add_time_end" style="width:100%">
        </div>

    </form>

</div>
    <div id="dialog-buttons-add">
        <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="Page.addSubmit()"
           style="width:90px">保存</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel"
           onclick="javascript:$('#addDialog').dialog('close')" style="width:90px">取消</a>
    </div>
    <div id="dialog-buttons-edit">
        <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="Page.editSubmit()"
           style="width:90px">保存</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel"
           onclick="javascript:$('#editDialog').dialog('close')" style="width:90px">取消</a>
    </div>
</div>


</body>
</html>