<link rel="shortcut icon" href="${base}/favicon.ico">
<%-- css --%>
<link rel="stylesheet" type="text/css" href="${base}/resources/assets/easyui/themes/metro/easyui.css">
<link rel="stylesheet" type="text/css" href="${base}/resources/assets/easyui/themes/icon.css">
<link rel="stylesheet" type="text/css" href="${base}/resources/assets/easyui/themes/color.css">
<link rel="stylesheet" type="text/css" href="${base}/resources/css/backend.css">
<%-- js --%>
<script>
    var port = location.port ? ":" + location.port : "";
    var _webApp = location.protocol + "//" + location.hostname + port;
</script>
<script src="${base}/resources/assets/easyui/jquery.min.js"></script>
<script src="${base}/resources/assets/easyui/jquery.easyui.min.js"></script>
<script src="${base}/resources/assets/easyui/easyui-lang-zh_CN.js"></script>
<script src="${base}/resources/assets/easyui/datagrid-groupview.js"></script>
<script src="${base}/pages/common.js"></script>