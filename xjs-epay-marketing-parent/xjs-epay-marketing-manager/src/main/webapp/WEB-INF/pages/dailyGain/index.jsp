<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>后台管理系统</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <%@ include file="/WEB-INF/pages/common/head.jsp" %>
    <script src="${base}/pages/dailyGain/index.js?v=<%=Math.random()%>"></script>
</head>
<body>
<input type="hidden" id="showOrg" value="${showOrg}">
<div>
    <form id="searchForm" class="search-condition__form" method="post" novalidate>
        <div  id="searchByOrg">
            <select  class="easyui-combobox" name="org_code" label="所属机构" style="width:100%">
                <option value="" selected>--请选择--</option>
                <c:forEach items="${orgList}" var="item">
                    <option value="${item.code }">${item.name }</option>
                </c:forEach>
            </select>
        </div>
        <div>
            <input name="time_start" class="easyui-datebox"
                   data-options="formatter:easyuiUtils.datebox_formatter,parser:easyuiUtils.datebox_parse"
                   label="分润日期:"> <input name="time_end" class="easyui-datebox"
                                         data-options="formatter:easyuiUtils.datebox_formatter,parser:easyuiUtils.datebox_parse"
                                         label="-">
        </div>
        <div>
            <input name="client_trans_id" class="easyui-textbox" label="订单号" width="200px">
        </div>
        <a id="searchBtn" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-search'">查询</a>
    </form>
</div>
<div id="dataGrid"></div>
</body>
</html>