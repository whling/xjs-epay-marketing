//spinner加减 整数
(function ($) {
    $('.spinner').on('click','.spinner-minus',function() {
        var $spinner = $(this).closest('.spinner');
        $spinner.find('input.form-control').val( parseInt($spinner.find('input.form-control').val(), 10) - 1);

        //var val = $spinner.find('input.form-control').val();
        //if(val<2){
        //    $spinner.find('input.form-control').val(parseInt(2 - 1));
        //}
    });
    $('.spinner').on('click', '.spinner-plus',function() {
        var $spinner = $(this).closest('.spinner');
        $spinner.find('input.form-control').val( parseInt($spinner.find('input.form-control').val(), 10) + 1);
    });

    $('.spinner').on('click','.spinner-minus-float',function() {
        var $spinner = $(this).closest('.spinner');
        var minusVal = $spinner.find('input.form-control').val();
        var tempMinusVal=((minusVal*100-10)/100).toFixed(1);
        $spinner.find('input.form-control').val( tempMinusVal);
    });
    $('.spinner').on('click', '.spinner-plus-float',function() {
        var $spinner = $(this).closest('.spinner');
        var plusVal = $spinner.find('input.form-control').val();
        var tempPlusVal=((plusVal*100+10)/100).toFixed(1);
        $spinner.find('input.form-control').val( tempPlusVal);
    });
})(jQuery);


//table-editable
//编辑
$('.table-editable').on('click','.btn-editable-edit',function(){
    var $tr = $(this).closest('tr');
    $tr.find('.d-reslut').addClass('hidden');
    $tr.find('.d-edit').addClass('hidden');
    $tr.find('.d-editable').removeClass('hidden');

    $tr.find('td').each(function(){
        var tempInput = $(this).find('.d-reslut').text();
        $(this).find('input').val(tempInput);
    });

});
//删除
$('.table-editable').on('click','.btn-editable-remove',function(){
    var $tr = $(this).closest('tr');
    var removeDialog = '<div class="modal-del modal modal-green in" style="display:block;margin-top:60px;">' +
        '<div class="modal-dialog">' +
        '<div class="modal-content">' +
        '<div class="modal-body" style="padding-bottom: 0">' +
        '<div class="pd15">' +
        '<form class="form-horizontal clearfix">' +
        '<div class="form-group">' +
        '<div class="col-md-12">' +
        '<p class="form-control-static text-success text-center">' +
        '确认要删除么？删除后将无法恢复，请问是否删除？' +
        '</p>' +
        '</div>' +
        '</div>' +
        '<div class="form-group>">' +
        '<div class="col-md-12 text-right">' +
        '<a href="javascript:;" class="btn g-btn-gray3" id="delCancel">取 消</a>' +
        '<a href="javascript:;" class="btn g-btn-blue ml10" id="delConfirm">确 认</a>' +
        '</div>' +
        '</div>' +
        '</form>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '<div class="modal-backdrop fade in"></div>' ;

    $('body').append(removeDialog);

    $('body').on('click','#delCancel',function(){
       $('.modal-del').remove();
       $('.modal-backdrop').remove();
        return false;
    });

    $('body').on('click','#delConfirm',function(){
        $('.modal-del').remove();
        $('.modal-backdrop').remove();
        $tr.remove();
    });

});
//保存
$('.table-editable').on('click','.btn-editable-save',function(){
    var $tr = $(this).closest('tr');
    $tr.find('.d-reslut').removeClass('hidden');
    $tr.find('.d-edit').removeClass('hidden');
    $tr.find('.d-editable').addClass('hidden');

    $tr.find('td').each(function(){
        var tempInput = $(this).find('input').val();
        $(this).find('.d-reslut').text(tempInput);
    });

});
//取消
$('.table-editable').on('click','.btn-editable-cancel',function(){
    var $tr = $(this).closest('tr');
    $tr.find('.d-reslut').removeClass('hidden');
    $tr.find('.d-edit').removeClass('hidden');
    $tr.find('.d-editable').addClass('hidden');
});

//采购价格编辑
$('#btn_modify_purchase_price').on('click',function(){
   $('.d-reslut').each(function(){
       $(this).addClass('hidden');
       $(this).next('.d-editable').removeClass('hidden');
       $(this).next('.d-editable').find('input').val($(this).text());
   });

   $('.div-purchase-count').removeClass('hidden');
});
//采购价格确认
$('.btn_pruchase_price_confirm').on('click',function(){
    $('.d-reslut').each(function(){
        $(this).removeClass('hidden');
        $(this).next('.d-editable').addClass('hidden');
        $(this).text($(this).next('.d-editable').find('input').val());
    });
    $(this).closest('.div-purchase-count').addClass('hidden');
});

//弹窗
$('body').on('hidden.bs.modal', '.modal', function () {
    $(this).removeData('bs.modal');
});
    //新增物品-弹窗
    $('#btn_create_wupin').on('click',function(){
        var scanInput = $(this).closest('.app-mainarea').find('.scanInput');
        if(scanInput.length == 0){
            var scanDiv = '<div class="scanInput">' +
                '<input href="#" type="text" class="inputCreateGoods w100" placeholder="请扫描／输入物品条码" autofocus="autofocus">' +
                '</div>';
            $(this).closest('.app-mainarea').find('.dtitle').after(scanDiv);
        }else{
            $('.scanInput').remove();
        }
    });
    $('body').on('keypress','.inputCreateGoods',function(){
        if($('.inputCreateGoods').val() != '' && event.keyCode == "13")
        {
            //$(this).d_modal({
            //    title: '新增物品',
            //    cssclass:'modal-green',
            //    content: "loading....",
            //    cache:false,
            //    remote:"modal/create_wupin.html"
            //});
            $('#modalCreateGoods').modal();
        }
    });
     //价格审批-弹窗口
    $('.btn-price-agree').on('click',function(){
        $('#modalPriceAgree').modal();
        //$(this).d_modal({
        //    title: '修改价格审批',
        //    cssclass:'modal-green',
        //    content: "loading....",
        //    cache:false,
        //    remote:"modal/price-agree.html"
        //});
    });
    $('.btn-price-reject').on('click',function(){
        $('#modalPriceReject').modal();
        //$(this).d_modal({
        //    title: '修改价格审批',
        //    cssclass:'modal-green',
        //    content: "loading....",
        //    cache:false,
        //    remote:"modal/price-reject.html"
        //});
    });
    //新增货架
    $('#btn_create_huojia').on('click',function(){
        $('#modalCreateShelf').modal();
        //$(this).d_modal({
        //    title: '新增货架',
        //    cssclass:'modal-green',
        //    content: "loading....",
        //    cache:false,
        //    remote:"modal/create_huojia.html"
        //});
    });
    //新增目录
   $('#btn_create_wpml').on('click',function(){
        var scanInput = $(this).closest('.app-mainarea').find('.scanInput');
        if(scanInput.length == 0){
            var scanDiv = '<div class="scanInput">' +
                '<input href="#" type="text" class="inputCreateCatalog w100" placeholder="请扫描／输入物品条码" autofocus="autofocus">' +
                '</div>';
            $(this).closest('.app-mainarea').find('.dtitle').after(scanDiv);
        }else{
            $('.scanInput').remove();
        }

    });
    $('body').on('keypress','.inputCreateCatalog',function(){
        if($('.inputCreateCatalog').val() != '' && event.keyCode == "13")
        {
            $('#modalCreateCatalog').modal();
            //$(this).d_modal({
            //    title: '新增目录',
            //    cssclass:'modal-green',
            //    content: "loading....",
            //    cache:false,
            //    remote:"modal/create_catalog.html"
            //});
        }
    });
    //修改价格
    $('.btn-modify-price').on('click',function(){
        $('#modalModifyPrice').modal();
        //$(this).d_modal({
        //    title: '修改价格',
        //    cssclass:'modal-green',
        //    content: "loading....",
        //    cache:false,
        //    remote:"modal/modify_price.html"
        //});
    });
    //新增废弃物品
    $('body').on('keypress','.inputCreateDrop',function(){
        if($('.inputCreateDrop').val() != '' && event.keyCode == "13")
        {
            $('#modalShelfDiscard').modal();
            //$(this).d_modal({
            //    title: '新增废弃物品',
            //    cssclass:'modal-green',
            //    content: "loading....",
            //    cache:false,
            //    remote:"modal/feiqi.html"
            //});
        }
    });
    //新增移架物品
    $('body').on('keypress','.inputCreateTrans',function(){
        if($('.inputCreateTrans').val() != '' && event.keyCode == "13")
        {
            $('#modalShelfTrans').modal();
            //$(this).d_modal({
            //    title: '物品移架',
            //    cssclass:'modal-green',
            //    content: "loading....",
            //    cache:false,
            //    remote:"modal/yijia.html"
            //});
        }
    });
    //新增采购单
    $('#btn_create_caigoudan').on('click',function(){
        $('#modalPurchaseOrder').modal();
        //$(this).d_modal({
        //    title: '新增采购单',
        //    cssclass:'modal-green',
        //    content: "loading....",
        //    cache:false,
        //    remote:"modal/create_caigoudan.html"
        //});
    });
    //确认采购单
    $('.btn_order_confirm').on('click',function(){
        $('#modalPurchaseOrderConfirm').modal();
        //$(this).d_modal({
        //    title: '确认采购单',
        //    cssclass:'modal-green',
        //    content: "loading....",
        //    cache:false,
        //    remote:"modal/purchaseOrder_confirm.html"
        //});
    });
    //新增入库物品
    $('#btn_create_goods_storage').on('click',function(){
        var scanInput = $(this).closest('.app-mainarea').find('.scanInput');
        if(scanInput.length == 0){
            var scanDiv = '<div class="scanInput">' +
                '<input href="#" type="text" class="inputGoodsStorage w100" placeholder="请扫描／输入物品条码" autofocus="autofocus">' +
                '</div>';
            $(this).closest('.app-mainarea').find('.dtitle').after(scanDiv);
        }else{
            $('.scanInput').remove();
        }

    });
    $('body').on('keypress','.inputGoodsStorage',function(){
        if($('.inputGoodsStorage').val() != '' && event.keyCode == "13")
        {
            $('#modalCreateStorageGoods').modal();
            //$(this).d_modal({
            //    title: '新增入库物品',
            //    cssclass:'modal-green',
            //    content: "loading....",
            //    cache:false,
            //    remote:"modal/create_goods_storage.html"
            //});
        }
    });
    //新增盘点单
    $('#btn_create_checkList').on('click',function(){
        $('#modalCreateCheckOrder').modal();
        //$(this).d_modal({
        //    title: '新增盘点单',
        //    cssclass:'modal-green',
        //    content: "loading....",
        //    cache:false,
        //    remote:"modal/create_check_list.html"
        //});
    });
    //新增盘点物品
    $('#btn_create_checking_goods').on('click',function(){
        var scanInput = $(this).closest('.app-mainarea').find('.scanInput');
        if(scanInput.length == 0){
            var scanDiv = '<div class="scanInput">' +
                '<input href="#" type="text" class="inputCheckingGoods w100" placeholder="请扫描／输入物品条码" autofocus="autofocus">' +
                '</div>';
            $(this).closest('.app-mainarea').find('.dtitle').after(scanDiv);
        }else{
            $('.scanInput').remove();
        }

    });
    $('body').on('keypress','.inputCheckingGoods',function(){
        if($('.inputCheckingGoods').val() != '' && event.keyCode == "13")
        {
            $('#modalCreateCheckGoods').modal();
            //$(this).d_modal({
            //    title: '新增盘点物品',
            //    cssclass:'modal-green',
            //    content: "loading....",
            //    cache:false,
            //    remote:"modal/create_checking_goods.html"
            //});
        }
    });

    //提交盘点单
    $('.btn_checkOrder_confirm').on('click',function(){
        $('#modalSubmitCheckOrder').modal();
        //$(this).d_modal({
        //    title: '提交盘点单',
        //    cssclass:'modal-green',
        //    content: "loading....",
        //    cache:false,
        //    remote:"modal/checkOrder_confirm.html"
        //});
    });
    //提交货架盘点
    $('.btn_shelf_confirm').on('click',function(){
        $('#modalShelfConfirm').modal();
        //$(this).d_modal({
        //    title: '提交货架盘点',
        //    cssclass:'modal-green',
        //    content: "loading....",
        //    cache:false,
        //    remote:"modal/checkOrder_confirm.html"
        //});
    });






//新增物品类型
var trWupinType = '<tr>'
    +'<td>'
    +'<span class="d-reslut hidden "></span>'
    +'<div class="d-editable edittype-text">'
    +'<input type="text"  class="w100">'
    +'</div>'
    +'</td>'
    +'<td>'
    +'<span class="d-edit hidden">'
    +'<a href="javascript:;" class="btn-link btn-editable-edit">编辑</a>'
    +'<a href="javascript:;" class="btn-link btn-editable-remove">删除</a>'
    +'</span>'
    +'<div class="d-editable">'
    +'<a href="javascript:;" class="btn-link btn-editable-cancel">取消</a>'
    +'<a href="javascript:;" class="btn-link btn-editable-save ml5">保存</a>'
    +'</div>'
    +'</td>'
    +'</tr>';
$('#btnCreateWupinType').on('click',function(){
    $('.table-wupintype').find('tbody').append(trWupinType);
});
$('#btnCreateYTJ').on('click',function(){
    $('.table-YTJ').find('tbody').append(trWupinType);
});

//美化单/复选框
$('.input-ic').iCheck({
    checkboxClass: 'icheckbox_square-red',
    radioClass: 'iradio_square-red'
});

//select2
$('.select2').select2({
    theme: "bootstrap",
    language: "zh-CN",
    minimumResultsForSearch: Infinity,
});

$('.select2_huojia').select2({
    theme: "bootstrap",
    language: "zh-CN",
    minimumResultsForSearch: Infinity,
    placeholder: '请选择物品货架',
});

//原生select美化
$('body').one('click','.selectBeautify',function () {
    $(this).removeClass("selectEmpty");
});
$('body').on('change','.selectBeautify',function () {
    var selectedIndex = $(this).get(0).selectedIndex;
    if(selectedIndex != 0) {$(this).removeClass("selectEmpty");}
    else {$(this).addClass("selectEmpty");return false;}
});
$('body').on('change','.selectBeautify_green',function () {
    var selectedIndex = $(this).get(0).selectedIndex;
    if(selectedIndex != 0) {$(this).removeClass("selectEmpty").addClass('c-1');}
    else {$(this).addClass("selectEmpty");return false;}
});




//选择供应商类型
$('body').on('change','.select_gysType',function(){
    if($(this).val()==1){
        $(this).addClass('c-1');
        $(this).next('.help-block').removeClass('hidden');
    }else{
        $(this).addClass('c-1');
        $(this).next('.help-block').addClass('hidden');
    }
});

