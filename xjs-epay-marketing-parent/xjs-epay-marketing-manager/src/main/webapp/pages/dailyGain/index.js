/**
 * Created by tums on 2016/12/05.
 */
$(document).ready(function () {
    Page.init();
});
var Page = {
    listUrl: _webApp + "/dailyGain/query.json",
    indexUrl: _webApp + "/dailyGain/index",
    delUrl: _webApp + "/dailyGain/delete.json",
    exportUrl: _webApp + "/dailyGain/export",
    init: function () {
        var showOrg = $("#showOrg").val();
        if (showOrg == 'false') {
            $("#searchByOrg").hide();
        }
        Page.initDataGrid(showOrg);
        $("#searchBtn").click(function () {
            Page.search();
        });
    },
    initDataGrid: function (showOrg) {
        this.dataGrid = $('#dataGrid').datagrid({
                url: this.listUrl,
                sortOrder: "desc",
                idField: "id",
                nowrap: true,
                pagination: true,
                pageList: [10, 25, 50, 100],
                pageSize: 50,
                rownumbers: true,
                autoRowHeight: true,
                singleSelect: true,
                checkOnSelect: true,
                selectOnCheck: true,
                columns: [[{
                    field: 'checkbox',
                    checkbox: true
                }, {
                    field: 'id',
                    title: 'ID',
                    hidden: true
                }, {
                    field: 'org_name',
                    title: '机构名称',
                    width: 180
                }, {
                    field: 'day',
                    title: '分润日期',
                    width: 180,
                    formatter: function (value, row, index) {
                        return DateUtils.formatDateStr(value);
                    }
                }, {
                    field: 'trans_type',
                    title: '交易类型',
                    width: 180,
                    formatter: function (value, row, index) {
                        return Page.transTypeText(value);
                    }
                }, {
                    field: 'username',
                    title: '用户名',
                    width: 180
                }, {
                    field: 'family_name',
                    title: '姓名',
                    width: 180
                }, {
                    field: 'from_family_name',
                    title: '分润来源',
                    width: 180
                }, {
                    field: 'client_trans_id',
                    title: '交易流水号',
                    width: 180
                }, {
                    field: 'trans_amt',
                    title: '交易金额',
                    width: 180,
                    formatter: function (value, row, index) {
                        return StringUtils.formatLongMoney(value);
                    }
                }, {
                    field: 'rate',
                    title: '分润比例',
                    width: 180
                }, {
                    field: 'amount',
                    title: '分润金额',
                    width: 180,
                    formatter: function (value, row, index) {
                        return StringUtils.formatLongMoney(value);
                    }
                }]],
                onLoadSuccess: function (data) {
                    if (showOrg == 'false') {
                        $("#dataGrid").datagrid("hideColumn", "org_name");
                    }
                },
            toolbar: [
                {
                    text: '导出',
                    iconCls: 'icon-add',
                    handler: function () {
                        Page.export();
                    }
                }]
            }
        )
        ;
    },
    /**
     * 搜索
     */
    search: function () {
        var searchData = $("#searchForm").serializeJson();
        this.dataGrid.datagrid('load', searchData);
        this.dataGrid.datagrid("clearChecked");
        this.dataGrid.datagrid("clearSelections");
    }
    ,
    reload: function () {
        this.dataGrid.datagrid('reload');
        this.dataGrid.datagrid("clearChecked");
        this.dataGrid.datagrid("clearSelections");
    },
    transTypeText: function (value) {
        if (value == 'WEIXINPAY_QR') {
            return '微信支付';
        } else if (value == 'ALIPAY_QR') {
            return '支付宝';
        } else if (value == 'SHORTCUTPAY') {
            return '快捷支付';
        } else if (value == 'SHORTCUTPAY_INTEGRAL') {
            return '快捷支付[有积分]';
        } else {
            return value;
        }
    },
    export: function () {
        $.messager.confirm('Confirm', '确定要导出吗?', function (flag) {
            if (flag) {
                var org_code = $("#searchForm").find("input[name='org_code']").val();
                var time_start = $("#searchForm").find("input[name='time_start']").val();
                var time_end = $("#searchForm").find("input[name='time_end']").val();
                var client_trans_id = $("#searchForm").find("input[name='client_trans_id']").val();
                var bodyParams = [];
                bodyParams.push("org_code=" + org_code);
                bodyParams.push("client_trans_id=" + client_trans_id);
                bodyParams.push("time_start=" + time_start);
                bodyParams.push("time_end=" + time_end);
                window.location.href = Page.exportUrl + "?" + bodyParams.join("&");
            }
        });
    }
};