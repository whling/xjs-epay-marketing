/**
 * Created by tums on 2016/12/05.
 */
$(document).ready(function () {
    Page.init();
});
var Page = {
    listUrl: _webApp + "/transChannel/query.json",
    indexUrl: _webApp + "/transChannel/index",
    createUrl: _webApp + "/transChannel/create.json",
    updateUrl: _webApp + "/transChannel/update.json",
    changeUrl: _webApp + "/transChannel/change",
    init: function () {
        Page.initDataGrid();
        $("#searchBtn").click(function () {
            Page.search();
        })
    },
    initDataGrid: function () {
        this.dataGrid = $('#dataGrid').datagrid({
                url: this.listUrl,
                sortOrder: "desc",
                idField: "id",
                nowrap: true,
                pagination: true,
                pageList: [10, 25, 50, 100],
                pageSize: 50,
                rownumbers: true,
                autoRowHeight: true,
                singleSelect: true,
                checkOnSelect: true,
                selectOnCheck: true,
                columns: [[{
                    field: 'checkbox',
                    checkbox: true
                }, {
                    field: 'id',
                    title: 'ID',
                    sortable: false,
                    hidden: true
                }, {
                    field: 'org_name',
                    title: '所属机构',
                    sortable: true,
                    width: 180
                }, {
                    field: 'name',
                    title: '支付方式名称',
                    sortable: true,
                    width: 180

                }, {
                    field: 'trans_type',
                    title: '支付方式编码',
                    sortable: true,
                    width: 180,
                    formatter: function (value, row, index) {
                        if (value == '00') {
                            return '<font style="color: green">通用</font>';
                        }
                        if (value == '01') {
                            return '<font style="color: green">微信支付</font>';
                        }
                        if (value == '02') {
                            return '<font style="color: green">支付宝支付</font>';
                        } else {
                            return '<font style="color: green">快捷支付</font>';
                        }
                    }
                }, {
                    field: 'rate_t0',
                    title: 'T0费率',
                    sortable: true,
                    width: 180
                }, {
                    field: 'rate_t1',
                    title: 'T1费率',
                    sortable: true,
                    width: 180
                }, {
                    field: 'counter_fee_t0',
                    title: 'T0手续费',
                    sortable: true,
                    width: 180
                }, {
                    field: 'counter_fee_t1',
                    title: 'T1手续费',
                    sortable: true,
                    width: 180
                }, {
                    field: 'pay_limit_min',
                    title: '单笔最低',
                    sortable: true,
                    width: 180
                }, {
                    field: 'pay_limit_max',
                    title: '单笔最高',
                    sortable: true,
                    width: 180
                }, {
                    field: 'time_limit_start',
                    title: '交易时间开始',
                    sortable: true,
                    width: 180,
                    formatter: function (value, row, index) {
                        return DateUtils.formatHoursStr(value);
                    }
                }, {
                    field: 'time_limit_end',
                    title: '交易时间结束',
                    sortable: true,
                    width: 120,
                    formatter: function (value, row, index) {
                        return DateUtils.formatHoursStr(value);
                    }
                }, {
                    field: 'priority',
                    title: '优先级',
                    sortable: true,
                    width: 120
                }, {
                    field: 'icon_url',
                    title: '图标',
                    sortable: true,
                    width: 120,
                    formatter: function (value, row, index) {
                        if (value) {
                            return '<img src="' + value + '" style="width: 80px;height: 80px;">';
                        } else {
                            return '暂无图标';
                        }
                    }
                }, {
                    field: 'day_limit_max',
                    title: '当日限额',
                    sortable: true,
                    width: 120
                }, {
                    field: 'memo',
                    title: '说明',
                    sortable: true,
                    width: 120
                }, {
                    field: 'pay_others',
                    title: '是否开放他们收款',
                    sortable: true,
                    width: 120,
                    formatter: function (value, row, index) {
                        if (value) {
                            return '是';
                        } else {
                            return '否';
                        }
                    }
                }, {
                    field: 'enable',
                    title: '操作',
                    width: 180,
                    formatter: function (value, row, index) {
                        var html = [];
                        if (value) {
                            html.push('<a href="javascript:Page.change(\'' + row.id + '\',\'0\',\'' + '\');" class="">禁用</a>');
                        } else {
                            html.push('<a href="javascript:Page.change(\'' + row.id + '\',\'1\',\'' + '\');" class="">启用</a>');
                        }
                        return html.join("");
                    }
                }
                ]],
                onDblClickRow: function (rowIndex, rowData) {
                    Page.edit();
                }
                ,
                toolbar: [{
                    text: '新增',
                    iconCls: 'icon-add',
                    handler: function () {
                        Page.add();
                    }
                }, {
                    text: '修改',
                    iconCls: 'icon-edit',
                    handler: function () {
                        Page.edit();
                    }
                }]
            }
        )
        ;
    },
    /**
     * 搜索
     */
    search: function () {
        var searchData = $("#searchForm").serializeJson();
        this.dataGrid.datagrid('load', searchData);
        this.dataGrid.datagrid("clearChecked");
        this.dataGrid.datagrid("clearSelections");
    },
    reload: function () {
        this.dataGrid.datagrid('reload');
        this.dataGrid.datagrid("clearChecked");
        this.dataGrid.datagrid("clearSelections");
    },
    add: function () {
        $('#addForm').form('clear');
        $('#addDialog').dialog('open').dialog('center').dialog('setTitle', '渠道配置');
        $("#addForm_org_code").combobox("setValue", $("#loginUserOrgCode").val());
        var org_name = $("#addForm_org_code").combobox("getText");
        $('#addForm').find("input[name='org_name']").val(org_name);

    },
    addSubmit: function () {
        $('#addForm').form('submit', {
            url: Page.createUrl,
            onSubmit: function () {
                $('#addDialog').dialog('close');
                return $(this).form('validate');
            },
            success: function (result) {
                if (result && result.success) {
                    $('#addDialog').dialog('close');        // close the dialog
                    Page.reload();
                } else if (result.message) {
                    $.messager.alert('提示', result.message, "error");
                }
            }
        });
    },
    edit: function () {
        var row = this.dataGrid.datagrid('getSelected');
        if (row) {
            $('#editForm').form('clear');
            $('#editDialog').dialog('open').dialog('center').dialog('setTitle', '渠道配置');
            $('#editForm').form('load', row);
        }
    },
    editSubmit: function () {
        $('#editForm').form('submit', {
            url: Page.updateUrl,
            onSubmit: function () {
                $('#editDialog').dialog('close');
                return $(this).form('validate');
            },
            success: function (result) {
                $('#editDialog').dialog('close');
                Page.reload();
                if (result && result.success) {
                    $('#editDialog').dialog('close');        // close the dialog
                    Page.reload();
                } else if (result.message) {
                    $.messager.alert('提示', result.message, "error");
                }
            }
        });
    },
    change: function (id, _enable) {
        var msg = "确定要禁用当前用户吗？";
        if (_enable == '1') {
            msg = "确定要启用当前用户吗？";
        }
        $.messager.confirm('Confirm', msg, function (flag) {
            if (flag) {
                $.get(Page.changeUrl, {id: id, _enable: _enable}, function (result) {
                    if (result && result.success && result.data) {
                        Page.reload();
                    } else {
                        $.messager.alert('提示', result.message, "error");
                    }
                }, 'json');
            }
        });
    }
};