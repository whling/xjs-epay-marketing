/**
 * Created by tums on 2016/12/05.
 */
$(document).ready(function () {
    Page.init();
});
var Page = {
    listUrl: _webApp + "/resource/query.json",
    indexUrl: _webApp + "/resource/index",
    delUrl: _webApp + "/resource/delete.json",
    createUrl: _webApp + "/resource/create.json",
    updateUrl: _webApp + "/resource/update.json",
    queryByParentUrl: _webApp + "/resource/queryParent.json",
    init: function () {
        Page.initDataGrid();
        $("#searchBtn").click(function () {
            Page.search();
        })
    },
    initDataGrid: function () {
        this.dataGrid = $('#dataGrid').treegrid({
            url: this.listUrl,
            idField: "id",
            treeField: 'name',
            nowrap: true,
            rownumbers: true,
            autoRowHeight: true,
            checkOnSelect: true,
            selectOnCheck: true,
            columns: [[{
                field: 'checkbox',
                checkbox: true
            }, {
                field: 'id',
                title: 'ID',
                hidden: true
            }, {
                field: 'name',
                title: '资源名称',
                sortable: true,
                width: 280
            },{
                field: 'resource_type',
                title: '资源类型',
                sortable: true,
                width: 120
            }, {
                field: 'sort_no',
                title: '序号',
                sortable: true,
                width: 120
            }, {
                field: 'url',
                title: 'URL',
                sortable: true,
                width: 120
            }, {
                field: 'view_class',
                title: '视图类',
                sortable: true,
                width: 120
            }, {
                field: 'class_icon',
                title: '图标',
                width: 120
            }, {
                field: 'class_name',
                title: '类名',
                width: 120
            }, {
                field: 'code',
                title: '编码',
                hidden: true
            }]],
            toolbar: [{
                text: '添加',
                iconCls: 'icon-add',
                handler: function () {
                    Page.add();
                }
            }, {
                text: '修改',
                iconCls: 'icon-edit',
                handler: function () {
                    Page.edit();
                }
            }, {
                text: '删除',
                iconCls: 'icon-remove',
                handler: function () {
                    Page.del();
                }
            }]
        });
    },
    /**
     * 搜索
     */
    search: function () {
        var searchData = $("#searchForm").serializeJson();
        this.dataGrid.treegrid('load', searchData);
        this.dataGrid.treegrid("clearChecked");
        this.dataGrid.treegrid("clearSelections");
    },
    add: function () {
        $('#addDialog').dialog('open').dialog('center').dialog('setTitle', '添加');
        $('#addForm').form('clear');
        $("#parentResourcesComboboxAdd").combotree({
            "url": Page.queryByParentUrl, "onLoadSuccess": function (node, data) {
                $.each(data, function (i, item) {
                    item.id = item.code;
                    $.each(item.children, function (j, sub) {
                        sub.id = sub.code;
                    });
                });
            }
        });
    },
    addSubmit: function () {
        $('#addForm').form('submit', {
            url: Page.createUrl,
            onSubmit: function () {
                return $(this).form('validate');
            },
            success: function (result) {
                console.log(result);
                result = eval('(' + result + ')');
                if (result && result.success) {
                    $('#addDialog').dialog('close');        // close the dialog
                    Page.reload();
                } else if (result.message) {
                    $.messager.alert('提示', result.message, "error");
                }
            }
        });
    },
    edit: function () {
        var row = this.dataGrid.datagrid('getSelected');
        if (row) {
            $('#editDialog').dialog('open').dialog('center').dialog('setTitle', '编辑');
            $('#editForm').form('load', row);
            $("#parentResourcesComboboxEdit").combotree({
                "url": Page.queryByParentUrl, "onLoadSuccess": function (node, data) {
                    var parent_code = row.parent_code;
                    $.each(data, function (index, item) {
                        item.id = item.code;
                        if (item.code == parent_code) {
                            $("#parentResourcesComboboxEdit").combotree("setValue", item.code);
                        }
                        $.each(item.children, function (j, sub) {
                            sub.id = sub.code;
                            if (sub.code == parent_code) {
                                $("#parentResourcesComboboxEdit").combotree("setValue", sub.code);
                            }
                        });
                    });
                }
            });
        } else {
            $.messager.alert('提示', "未选中行", "info");
        }
    },
    editSubmit: function () {
        $('#editForm').form('submit', {
            url: Page.updateUrl,
            onSubmit: function () {
                return $(this).form('validate');
            },
            success: function (result) {
                result = eval('(' + result + ')');
                if (result && result.success) {
                    $('#editDialog').dialog('close');        // close the dialog
                    Page.reload();
                } else if (result.message) {
                    $.messager.alert('提示', result.message, "error");
                }
            }
        });
    },
    del: function () {
        var row = this.dataGrid.datagrid('getSelected');
        if (row) {
            $.messager.confirm('Confirm', '确定要删除?', function (flag) {
                if (flag) {
                    var parentCode = row.parent_code;
                    var isTop = false;
                    if (parentCode) {
                        isTop = true;
                    }
                    $.post(Page.delUrl, {isTop: isTop, id: row.id, code: row.code}, function (result) {
                        if (result && result.success && result.data) {
                            Page.reload();
                        } else {
                            $.messager.alert('提示', "资源存在引用，不可删除", "error");
                        }
                    }, 'json');
                }
            });
        } else {
            $.messager.alert('提示', "未选中行", "info");
        }
    },
    reload: function () {
        this.dataGrid.treegrid('reload');
        this.dataGrid.treegrid("clearChecked");
        this.dataGrid.treegrid("clearSelections");
    }
};