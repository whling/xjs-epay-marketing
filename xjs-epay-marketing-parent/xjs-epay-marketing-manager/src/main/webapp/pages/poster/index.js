/**
 * Created by tums on 2016/12/05.
 */
$(document).ready(function () {
    Page.init();
});
var Page = {
    listUrl: _webApp + "/poster/query.json",
    indexUrl: _webApp + "/poster/index",
    createUrl: _webApp + "/poster/create.json",
    updateUrl: _webApp + "/poster/update.json",
    init: function () {
        Page.initDataGrid();
        $("#searchBtn").click(function () {
            Page.search();
        });
    },
    initDataGrid: function () {
        this.dataGrid = $('#dataGrid').datagrid({
            url: this.listUrl,
            sortOrder: "desc",
            idField: "id",
            nowrap: true,
            pagination: true,
            pageList: [10, 25, 50, 100],
            pageSize: 50,
            rownumbers: true,
            autoRowHeight: true,
            singleSelect: true,
            checkOnSelect: true,
            selectOnCheck: true,
            columns: [[{
                field: 'checkbox',
                checkbox: true
            }, {
                field: 'id',
                title: 'ID',
                hidden: true
            }, {
                field: 'org_name',
                title: '机构名称',
                width: 180
            }, {
                field: 'title',
                title: '标题',
                width: 150
            }, {
                field: 'img_url',
                title: '图片地址',
                width: 280
            }, {
                field: 'rate_img_url',
                title: '费率地址',
                width: 280
            } ,{
                field: 'show_params',
                title: '配置参数',
                width: 480
            },
                {
                    field: 'slogan',
                    title: '广告语',
                    width: 480
                },{
                field: 'time_begin',
                title: '开始时间',
                width: 180,
                formatter: function (value, row, index) {
                    return DateUtils.formatDateStr(value);
                }
            }, {
                field: 'time_end',
                title: '结束时间',
                width: 180,
                formatter: function (value, row, index) {
                    return DateUtils.formatDateStr(value);
                }
            }]],
            toolbar: [{
                text: '添加',
                iconCls: 'icon-add',
                handler: function () {
                    Page.add();
                }
            }, {
                text: '修改',
                iconCls: 'icon-edit',
                handler: function () {
                    Page.edit();
                }
            }],
            onDblClickRow: function (row) {
                Page.edit();
            }
        });
    },
    /**
     * 搜索
     */
    search: function () {
        var searchData = $("#searchForm").serializeJson();
        this.dataGrid.datagrid('load', searchData);
        this.dataGrid.datagrid("clearChecked");
        this.dataGrid.datagrid("clearSelections");
    },
    reload: function () {
        this.dataGrid.datagrid('reload');
        this.dataGrid.datagrid("clearChecked");
        this.dataGrid.datagrid("clearSelections");
    },
    add: function () {
        $('#addDialog').dialog('open').dialog('center').dialog('setTitle', '添加');
        $('#addForm').form('clear');
        $("#addForm_org_code").combobox("setValue", $("#loginUserOrgCode").val());
    },
    addSubmit: function () {
        var org_name = $("#addForm_org_code").combobox("getText");
        $('#addForm').find("input[name='org_name']").val(org_name);
        $('#addForm').form('submit', {
            url: Page.createUrl,
            onSubmit: function () {
                return $(this).form('validate');
            },
            success: function (result) {
                result = JSON.parse(result);
                if (result && result.success) {
                    $('#addDialog').dialog('close');        // close the dialog
                    Page.reload();
                } else if (result.message) {
                    $.messager.alert('提示', result.message, "error");
                }
            }
        });
    },
    edit: function () {
        var row = this.dataGrid.datagrid('getSelected');
        $("#postUrl").attr("src","");
        $("#rateUrl").attr("src","");
        if (row) {
            $('#editDialog').form('clear');
            $('#editDialog').dialog('open').dialog('center').dialog('setTitle', '编辑');
            var str = row.show_params;
            var obj= JSON.parse(str);
            $('#editForm').form('load', row);
            $('#editForm').form('load', obj);
            var postUrl = row.img_url;
            var rateUrl = row.rate_img_url;
            if(postUrl){
                $("#postDiv").attr("style","margin-bottom:10px");
                $("#postUrl").attr("src",row.img_url);
            }else{
                $("#postDiv").attr("style","margin-bottom:10px;display:none");
            }
            if(rateUrl){
                $("#rateDiv").attr("style","margin-bottom:10px");
                $("#rateUrl").attr("src",row.rate_img_url);
            }else{
                $("#rateDiv").attr("style","margin-bottom:10px;display:none");
            }
            $("#Form_org_code").combobox("setValue", $("#loginUserOrgCode").val());
        } else {
            $.messager.alert('提示', "未选中行", "info");
        }
    },
    editSubmit: function () {
        var org_name = $("#editForm_org_code").combobox("getText");
        $('#editForm').find("input[name='org_name']").val(org_name);
        $('#editForm').form('submit', {
            url: Page.updateUrl,
            onSubmit: function () {
                return $(this).form('validate');
            },
            success: function (result) {
                result = JSON.parse(result);
                if (result && result.success) {
                    $('#editDialog').dialog('close');        // close the dialog
                    Page.reload();
                } else if (result.message) {
                    $.messager.alert('提示', result.message, "error");
                }
            }
        });
    }
};