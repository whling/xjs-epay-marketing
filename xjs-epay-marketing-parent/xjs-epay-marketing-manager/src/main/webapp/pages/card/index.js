/**
 * Created by tums on 2016/12/05.
 */
$(document).ready(function () {
    Page.init();
});
var Page = {
    listUrl: _webApp + "/card/query.json",
    indexUrl: _webApp + "/card/index",
    exportUrl: _webApp + "/card/export",
    init: function () {
        var showOrg = $("#showOrg").val();
        if (showOrg == 'false') {
            $("#searchByOrg").hide();
        }
        Page.initDataGrid(showOrg);
        $("#searchBtn").click(function () {
            Page.search();
        });
    },
    initDataGrid: function (showOrg) {
        this.dataGrid = $('#dataGrid').datagrid({
            url: this.listUrl,
            sortOrder: "desc",
            idField: "id",
            nowrap: true,
            pagination: true,
            pageList: [10, 25, 50, 100],
            pageSize: 50,
            rownumbers: true,
            autoRowHeight: true,
            singleSelect: true,
            checkOnSelect: true,
            selectOnCheck: true,
            columns: [[{
                field: 'checkbox',
                checkbox: true
            }, {
                field: 'id',
                title: 'ID',
                hidden: true
            }, {
                field: 'org_name',
                title: '机构名称',
                width: 180
            }, {
                field: 'username',
                title: '用户名',
                width: 120
            }, {
                field: 'family_name',
                title: '姓名',
                width: 120
            }, {
                field: 'idcard',
                title: '身份证',
                width: 160,
                formatter: function (value, row, index) {
                    return StringUtils.formatIdCard(value);
                }
            }, {
                field: 'bank_name',
                title: '银行名称',
                width: 180
            }, {
                field: 'bank_code',
                title: '银行简码',
                width: 180
            }, {
                field: 'card_type',
                title: '卡类型',
                width: 100,
                formatter: function (value, row, index) {
                    if (value == '2') {
                        return '信用卡';
                    } else {
                        return '<font style="color: red">借记卡</font>';
                    }
                }
            }, {
                field: 'bank_no',
                title: '银行卡号',
                width: 120,
                formatter: function (value, row, index) {
                    return StringUtils.formatBankCard(value);
                }
            }, {
                field: 'mobile',
                title: '手机号',
                width: 120
            }, {
                field: 'acc_province',
                title: '所在省',
                width: 120
            }, {
                field: 'acc_city',
                title: '所在市',
                width: 120
            }, {
                field: 'branch_code',
                title: '支行代码',
                width: 280
            }, {
                field: 'branch_name',
                title: '支行名称',
                width: 280
            }, {
                field: 'enable',
                title: '是否是收款卡',
                width: 120,
                formatter: function (value, row, index) {
                    if (value) {
                        return '<font style="color: red">是</font>';
                    } else {
                        return '否';
                    }
                }
            }]],
            onLoadSuccess: function (data) {
                if (showOrg == 'false') {
                    $("#dataGrid").datagrid("hideColumn", "org_name");
                }
            }
        });
    },
    /**
     * 搜索
     */
    search: function () {
        var searchData = $("#searchForm").serializeJson();
        this.dataGrid.datagrid('load', searchData);
        this.dataGrid.datagrid("clearChecked");
        this.dataGrid.datagrid("clearSelections");
    },
    reload: function () {
        this.dataGrid.datagrid('reload');
        this.dataGrid.datagrid("clearChecked");
        this.dataGrid.datagrid("clearSelections");
    },
    export: function () {
        $.messager.confirm('Confirm', '确定要导出吗?', function (flag) {
            if (flag) {
                var username = $("#searchForm").find("input[name='username']").val();
                var family_name = $("#searchForm").find("input[name='family_name']").val();
                var bank_name = $("#searchForm").find("input[name='bank_name']").val();
                var mobile = $("#searchForm").find("input[name='mobile']").val();
                var card_type = $("#searchForm").find("input[name='card_type']").val();
                var bodyParams = [];
                bodyParams.push("username=" + username);
                bodyParams.push("family_name=" + family_name);
                bodyParams.push("bank_name=" + bank_name);
                bodyParams.push("mobile=" + mobile);
                bodyParams.push("card_type=" + card_type);
                window.location.href = Page.exportUrl + "?" + bodyParams.join("&");
            }
        });
    }
};