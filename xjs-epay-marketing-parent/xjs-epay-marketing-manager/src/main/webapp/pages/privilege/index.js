/**
 * Created by tums on 2016/12/05.
 */
$(document).ready(function () {
    Page.initDataGrid();
});
var Page = {
    listUrl: _webApp + "/privilege/query.json",
    indexUrl: _webApp + "/privilege/index",
    delUrl: _webApp + "/privilege/delete.json",
    createUrl: _webApp + "/privilege/create.json",
    updateUrl: _webApp + "/privilege/update.json",
    initDataGrid: function () {
        this.dataGrid = $('#dataGrid').datagrid({
            url: this.listUrl,
            sortOrder: "desc",
            idField: "id",
            nowrap: true,
            pagination: true,
            pageList: [10, 25, 50, 100],
            pageSize: 50,
            rownumbers: true,
            autoRowHeight: true,
            singleSelect: true,
            checkOnSelect: true,
            selectOnCheck: true,
            columns: [[{
                field: 'checkbox',
                checkbox: true
            }, {
                field: 'id',
                title: 'ID',
                sortable: false,
                hidden: true
            }, {
                field: 'grant_id',
                title: '授权ID',
                sortable: true,
                width: 280
            }, {
                field: 'grant_type',
                title: '授权类型',
                sortable: true,
                width: 120
            }, {
                field: 'resource_id',
                title: '资源ID',
                sortable: true,
                width: 220
            }, {
                field: 'resource_type',
                title: '资源类型',
                sortable: true,
                width: 120
            }]],
            toolbar: [{
                text: '添加',
                iconCls: 'icon-add',
                handler: function () {
                    Page.add();
                }
            }, {
                text: '修改',
                iconCls: 'icon-edit',
                handler: function () {
                    Page.edit();
                }
            }, {
                text: '删除',
                iconCls: 'icon-remove',
                handler: function () {
                    Page.del();
                }
            }, {
                text: '搜索栏',
                iconCls: 'fa fa-list color-blue',
                handler: function () {
                }
            }],
            onDblClickRow: function (row) {
            }
        });
    },
    add: function () {
        $('#addDialog').dialog('open').dialog('center').dialog('setTitle', '添加');
        $('#addForm').form('clear');
    },
    addSubmit: function () {
        $('#addForm').form('submit', {
            url: Page.createUrl,
            onSubmit: function () {
                return $(this).form('validate');
            },
            success: function (result) {
                console.log(result);
                result = eval('(' + result + ')');
                if (result && result.success) {
                    $('#addDialog').dialog('close');        // close the dialog
                    Page.reload();
                } else if (result.message) {
                    $.messager.alert('提示', result.message, "error");
                }
            }
        });
    },
    edit: function () {
        var row = this.dataGrid.datagrid('getSelected');
        if (row) {
            $('#editDialog').dialog('open').dialog('center').dialog('setTitle', '编辑');
            $('#editForm').form('load', row);
        } else {
            $.messager.alert('提示', "未选中行", "info");
        }
    },
    editSubmit: function () {
        $('#editForm').form('submit', {
            url: Page.updateUrl,
            onSubmit: function () {
                return $(this).form('validate');
            },
            success: function (result) {
                console.log(result);
                result = eval('(' + result + ')');
                if (result && result.success) {
                    $('#editDialog').dialog('close');        // close the dialog
                    Page.reload();
                } else if (result.message) {
                    $.messager.alert('提示', result.message, "error");
                }
            }
        });
    },
    del: function () {
        var row = this.dataGrid.datagrid('getSelected');
        if (row) {
            $.messager.confirm('Confirm', '确定要删除角色?', function (flag) {
                if (flag) {
                    $.post(Page.delUrl, {id: row.id}, function (result) {
                        if (result && result.success && result.data) {
                            Page.reload();
                        } else {
                            $.messager.alert('提示', result.message, "error");
                        }
                    }, 'json');
                }
            });
        } else {
            $.messager.alert('提示', "未选中行", "info");
        }
    },
    reload: function () {
        this.dataGrid.datagrid('reload');
        // 清楚所有选中项，防止出现删除后的项目依然存在于选中项中
        this.dataGrid.datagrid("clearChecked");
        this.dataGrid.datagrid("clearSelections");
    }
};