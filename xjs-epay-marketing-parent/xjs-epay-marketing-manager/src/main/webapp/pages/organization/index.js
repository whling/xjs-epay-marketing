/**
 * Created by tums on 2016/12/05.
 */
$(document).ready(function () {
    Page.init();
});
var Page = {
    listUrl: _webApp + "/organization/query.json",
    indexUrl: _webApp + "/organization/index",
    createUrl: _webApp + "/organization/create.json",
    updateUrl: _webApp + "/organization/update.json",
    delUrl: _webApp + "/organization/delete.json",
    detailUrl: _webApp + "/organization/detail",
    changeUrl: _webApp + "/organization/change",
    init: function () {
        Page.initDataGrid();
        $("#searchBtn").click(function () {
            Page.search();
        })
    },
    initDataGrid: function () {
        this.dataGrid = $('#dataGrid').datagrid({
                url: this.listUrl,
                sortOrder: "desc",
                idField: "id",
                nowrap: true,
                pagination: true,
                pageList: [10, 25, 50, 100],
                pageSize: 50,
                rownumbers: true,
                autoRowHeight: true,
                singleSelect: true,
                checkOnSelect: true,
                selectOnCheck: true,
                columns: [[{
                    field: 'checkbox',
                    checkbox: true
                }, {
                    field: 'id',
                    title: 'ID',
                    hidden: true
                }, {
                    field: 'name',
                    title: '机构名称',
                    width: 180
                }, {
                    field: 'code',
                    title: '机构编号',
                    width: 180
                }, {
                    field: 'app_name',
                    title: '应用名称',
                    width: 180
                }, {
                    field: 'linkman',
                    title: '联系人姓名',
                    width: 180
                }, {
                    field: 'linkman_idcard',
                    title: '联系人身份证',
                    width: 180
                }, {
                    field: 'telephone',
                    title: '固定电话',
                    width: 180
                }, {
                    field: 'linkman_mobile',
                    title: '手机号码',
                    width: 180
                }, {
                    field: 'linkman_email',
                    title: '邮箱',
                    width: 180
                }, {
                    field: 'withdrawal_fee',
                    title: '提现手续费',
                    width: 180
                }, {
                    field: 'pay_others',
                    title: '是否开放他人收款',
                    width: 180,
                    formatter: function (value, row, index) {
                        if (value) {
                            return '是';
                        } else {
                            return '<font style="color: red">否</font>';
                        }
                    }
                },{
                    field: 'open_custom_service',
                    title: '是否开启人工客服',
                    width: 180,
                    formatter: function (value, row, index) {
                        if (value) {
                            return '是';
                        } else {
                            return '<font style="color: red">否</font>';
                        }
                    }
                }, {
                    field: 'address',
                    title: '地址',
                    width: 180
                }, {
                        field: 'custom_service_url',
                        title: '客服皮肤地址',
                        width: 180
                    }, {
                    field: 'type',
                    title: '机构类型',
                    width: 180,
                    formatter: function (value, row, index) {
                        if (value === '01') {//PERSONAL, ENTERPRISE
                            return '个人';
                        } else if (value === '02') {
                            return '企业';
                        } else {
                            return '未知';
                        }
                    }
                }, {
                    field: 'memo',
                    title: '说明',
                    width: 180
                }, {
                    field: 'logo',
                    title: 'logo',
                    width: 180
                }, {
                    field: 'logo_small',
                    title: '小图标',
                    width: 180
                },{
                    field: 'weixin_name',
                    title: '微信名称',
                    width: 180
                }, {
                    field: 'weixin_appId',
                    title: '微信appid',
                    width: 180
                }, {
                    field: 'weixin_redirect_uri',
                    title: '微信redirect_uri',
                    width: 180
                }, {
                    field: 'weixin_secret',
                    title: '微信secret',
                    width: 180
                }, {
                    field: 'weixin_qrcode',
                    title: '微信二维码',
                    width: 180
                }, {
                    field: 'enable',
                    title: '操作',
                    width: 180,
                    formatter: function (value, row, index) {
                        var html = [];
                        if (value) {
                            html.push('<a href="javascript:Page.change(\'' + row.id + '\',\'0\',\'' + '\');" class="">禁用</a>');
                        } else {
                            html.push('<a href="javascript:Page.change(\'' + row.id + '\',\'1\',\'' + '\');" class="">启用</a>');
                        }
                        return html.join("");
                    }
                }]],
                onDblClickRow: function (rowIndex, rowData) {
                    Page.view();
                },
                toolbar: [{
                    text: '查看',
                    iconCls: 'icon-add',
                    handler: function () {
                        Page.view();
                    }
                }, {
                    text: '新增',
                    iconCls: 'icon-add',
                    handler: function () {
                        Page.add();
                    }
                }, {
                    text: '修改',
                    iconCls: 'icon-add',
                    handler: function () {
                        Page.edit();
                    }
                }]
            }
        )
        ;
    },
    /**
     * 搜索
     */
    search: function () {
        var searchData = $("#searchForm").serializeJson();
        this.dataGrid.datagrid('load', searchData);
        this.dataGrid.datagrid("clearChecked");
        this.dataGrid.datagrid("clearSelections");
    }
    ,
    reload: function () {
        this.dataGrid.datagrid('reload');
        this.dataGrid.datagrid("clearChecked");
        this.dataGrid.datagrid("clearSelections");
    }
    ,
    add: function () {
        $('#addForm').form('clear');
        $('#addDialog').dialog('open').dialog('center').dialog('setTitle', '新增');
    },
    addSubmit: function () {
        $('#addForm').form('submit', {
            url: Page.createUrl,
            onSubmit: function () {
                return $(this).form('validate');
            },
            success: function (result) {
                result = JSON.parse(result);
                if (result && result.success) {
                    $('#addDialog').dialog('close');        // close the dialog
                    Page.reload();
                } else if (result.message) {
                    $.messager.alert('提示', result.message, "error");
                }
            }
        });
    }
    ,
    edit: function () {
        var row = this.dataGrid.datagrid('getSelected');
        if (row) {
            $('#editForm').form('clear');
            $('#editDialog').dialog('open').dialog('center').dialog('setTitle', '修改');
            $('#editForm').form('load', row);
        }
    },
    editSubmit: function () {
        $('#editForm').form('submit', {
            url: Page.updateUrl,
            onSubmit: function () {
                return $(this).form('validate');
            },
            success: function (result) {
                result = JSON.parse(result);
                if (result && result.success) {
                    $('#editDialog').dialog('close');        // close the dialog
                    Page.reload();
                } else if (result.message) {
                    $.messager.alert('提示', result.message, "error");
                }
            }
        });
    },
    del: function () {
        var row = this.dataGrid.datagrid('getSelected');
        if (row) {
            $.messager.confirm('Confirm', '确定要删除?', function (flag) {
                if (flag) {
                    $.post(Page.delUrl, {id: row.id}, function (result) {
                        if (result && result.success && result.data) {
                            Page.reload();
                        } else {
                            $.messager.alert('提示', result.message, "error");
                        }
                    }, 'json');
                }
            });
        } else {
            $.messager.alert('提示', "未选中行", "info");
        }
    },
    view: function () {
        var row = this.dataGrid.datagrid('getSelected');
        if (row) {
            $('#viewOrgForm').form('clear');
            $('#viewCardForm').form('clear');
            $('#viewUserForm').form('clear');
            $('#viewDialog').dialog('open').dialog('center').dialog('setTitle', '机构信息');
            $('#viewOrgForm').form('load', row);
            var code = row.code;
            var enable = row.enable;
            var pay_others = row.pay_others;
            var type = row.type;
            $('#enable').textbox("setValue", Page.enableText(enable));
            $('#pay_others').textbox("setValue", Page.payOthersText(pay_others));
            $('#type').textbox("setValue", Page.typeText(type));
            $.get(Page.detailUrl, {orgCode: code}, function (result) {
                if (result && result.success) {
                    var data = result.data;
                    if (data) {
                        var user = data.user;
                        var card = data.card;
                        $('#viewUserForm').form('load', user);
                        $('#viewCardForm').form('load', card);
                    }
                } else {
                    $.messager.alert('提示', result.message, "error");
                }
            }, 'json');
        } else {
            $.messager.alert('提示', "未选中行", "info");
        }
    },
    payOthersText: function (value) {
        if (value) {
            return '是';
        } else {
            return '否';
        }
    },
    enableText: function (value) {
        if (value) {
            return '启用';
        } else {
            return '禁用';
        }
    },
    typeText: function (value) {
        if (value === '01') {//PERSONAL, ENTERPRISE
            return '个人';
        } else if (value === '02') {
            return '企业';
        } else {
            return '未知';
        }
    },
    change: function (id, _enable) {
        var msg = "确定要禁用当前用户吗？";
        if (_enable == '1') {
            msg = "确定要启用当前用户吗？";
        }
        $.messager.confirm('Confirm', msg, function (flag) {
            if (flag) {
                $.get(Page.changeUrl, {id: id, _enable: _enable}, function (result) {
                    if (result && result.success && result.data) {
                        Page.reload();
                    } else {
                        $.messager.alert('提示', result.message, "error");
                    }
                }, 'json');
            }
        });
    }
};