/**
 * Created by tums on 2016/12/05.
 */
$(document).ready(function () {
    /* $("#addamount").keyup(function(){
     alert("aaa");
     var c=$(this);
     if(/[^\d]/.test(c.val())){//替换非数字字符
     var temp_amount=c.val().replace(/[^\d]/g,'');
     $(this).val(temp_amount);
     }
     })*/
    Page.init();

});
var Page = {
    listUrl: _webApp + "/voucher/query.json",
    indexUrl: _webApp + "/voucher/index",
    delUrl: _webApp + "/voucher/delete",
    updateUrl: _webApp + "/voucher/update",
    createUrl: _webApp + "/voucher/create",
    changeUrl: _webApp + "/voucher/change",
    imgTestUrl: _webApp + "/image/imageCompose",
    init: function () {
        Page.initDataGrid();
        $("#searchBtn").click(function () {
            Page.search();
        });

    },
    initDataGrid: function () {
        this.dataGrid = $('#dataGrid').datagrid({
            url: this.listUrl,
            sortOrder: "desc",
            idField: "id",
            nowrap: true,
            pagination: true,
            pageList: [10, 25, 50, 100],
            pageSize: 50,
            rownumbers: true,
            autoRowHeight: true,
            singleSelect: true,
            checkOnSelect: true,
            selectOnCheck: true,
            columns: [[{
                field: 'checkbox',
                checkbox: true
            }, {
                field: 'id',
                title: 'ID',
                sortable: false,
                hidden: true
            }, {
                field: 'org_name',
                title: '所属机构',
                sortable: true,
                width: 180
            }, {
                field: 'title',
                title: '活动标题',
                sortable: true,
                width: 180
            }, {
                field: 'time_start',
                title: '有效期开始',
                sortable: true,
                width: 120,
                formatter: function (value, row, index) {
                    return DateUtils.formatDateStr(value);
                }
            }, {
                field: 'time_end',
                title: '有效期截止',
                sortable: true,
                width: 120,
                formatter: function (value, row, index) {
                    return DateUtils.formatDateStr(value);
                }
            }, {
                field: 'pay_limit',
                title: '最低消费(元)',
                sortable: true,
                width: 80,
                formatter: function (value, row, index) {
                    return StringUtils.formatLongMoney(value);
                }
            }, {
                field: 'amount',
                title: '交易金额',
                sortable: true,
                width: 80,
                formatter: function (value, row, index) {
                    return StringUtils.formatLongMoney(value);
                }
            }, {
                field: 'memo',
                title: '说明',
                sortable: true,
                width: 80
            }, {
                field: 'type',
                title: '抵用券类型',
                sortable: true,
                width: 80,
                formatter: function (value, row, index) {
                    if (value == '00') {
                        return '<font style="color: green">通用</font>';
                    }
                    if (value == '01') {
                        return '<font style="color: green">微信支付</font>';
                    }
                    if (value == '02') {
                        return '<font style="color: green">支付宝支付</font>';
                    } else {
                        return '<font style="color: green">快捷支付</font>';
                    }
                }
            }, {
                field: 'enable',
                title: '操作',
                width: 180,
                formatter: function (value, row, index) {
                    var html = [];
                    if (value) {
                        html.push('<a href="javascript:Page.change(\'' + row.id + '\',\'0\',\'' + '\');" class="">禁用</a>');
                    } else {
                        html.push('<a href="javascript:Page.change(\'' + row.id + '\',\'1\',\'' + '\');" class="">启用</a>');
                    }
                    return html.join("");
                }
            }]],
            toolbar: [
                {
                    text: '新增',
                    iconCls: 'icon-add',
                    handler: function () {
                        Page.add();
                    }
                }, {
                    text: '修改',
                    iconCls: 'icon-edit',
                    handler: function () {
                        Page.edit();
                    }
                }
                , {
                    text: '测试',
                    iconCls: 'icon-edit',
                    handler: function () {
                        Page.imgTest();
                    }
                }]
        });
    },
    /**
     * 搜索
     */
    search: function () {
        var timeStart = $("#timeStart").val();
        var timeEnd = $("#timeEnd").val();
        if (timeStart != '' && timeEnd != '') {
            if (timeStart > timeEnd) {
                alert("开始时间不能大于结束时间!")
                return false;
            }
        }
        var searchData = $("#searchForm").serializeJson();
        this.dataGrid.datagrid('load', searchData);
        this.dataGrid.datagrid("clearChecked");
        this.dataGrid.datagrid("clearSelections");

    },
    change: function (id, _enable) {
        var msg = "确定要禁用当前用户吗？";
        if (_enable == '1') {
            msg = "确定要启用当前用户吗？";
        }
        $.messager.confirm('Confirm', msg, function (flag) {
            if (flag) {
                $.get(Page.changeUrl, {id: id, _enable: _enable}, function (result) {
                    if (result && result.success && result.data) {
                        Page.reload();
                    } else {
                        $.messager.alert('提示', result.message, "error");
                    }
                }, 'json');
            }
        });
    },
    reload: function () {
        this.dataGrid.datagrid('reload');
        this.dataGrid.datagrid("clearChecked");
        this.dataGrid.datagrid("clearSelections");
    },
    delete: function () {
        var row = this.dataGrid.datagrid('getSelected');
        if (row) {
            $.messager.confirm('Confirm', '确定要删除?', function (flag) {
                if (flag) {
                    $.post(Page.delUrl, {id: row.id}, function (result) {
                        if (result && result.success && result.data) {
                            Page.reload();
                        } else {
                            $.messager.alert('提示', result.message, "error");
                        }
                    }, 'json');
                }
            });
        } else {
            $.messager.alert('提示', "未选中行", "info");
        }
    },
    edit: function () {
        var row = this.dataGrid.datagrid('getSelected');
        if (row) {
            $('#editForm').form('clear');
            $('#editDialog').dialog('open').dialog('center').dialog('setTitle', '编辑抵用用');
            $('#editForm').form('load', row);
            $("#editForm_org_code").combobox("setValue", $("#loginUserOrgCode").val());


        }
    },
    editSubmit: function () {
        var org_name = $("#addForm_org_code").combobox("getText");
        $('#editForm').find("input[name='org_name']").val(org_name);

        var editamount = $("#addamount").val();
        if (/[^\d]/.test(editamount)) {//替换非数字字符
            alert("非法交易金额");
            return false;
        }
        var editpaylimit = $("#addpaylimit").val();
        if (/[^\d]/.test(editpaylimit)) {//替换非数字字符
            alert("非法最低消费金额");
            return false;
        }
        var timeStart = $("#edit_time_start").val();
        var timeEnd = $("#edit_time_end").val();
        if (timeStart != '' && timeEnd != '') {
            if (timeStart > timeEnd) {
                alert("开始时间不能大于结束时间!")
                return false;
            }
        }
        $('#editForm').form('submit', {
            url: Page.updateUrl,
            onSubmit: function () {
                $('#editDialog').dialog('close');
                return $(this).form('validate');
            },
            success: function (result) {
                $('#editDialog').dialog('close');
                Page.reload();
                if (result && result.success) {
                    $('#editDialog').dialog('close');        // close the dialog
                    Page.reload();
                } else if (result.message) {
                    $.messager.alert('提示', result.message, "error");
                }
            }
        });
    },
    add: function () {
        $('#addForm').form('clear');

        $('#addDialog').dialog('open').dialog('center').dialog('setTitle', '新增抵用券');

        $("#addForm_org_code").combobox("setValue", $("#loginUserOrgCode").val());


    },
    addSubmit: function () {
        var org_name = $("#addForm_org_code").combobox("getText");
        $('#addForm').find("input[name='org_name']").val(org_name);
        $("#add_type option:first").prop("selected", 'selected');
        var addamount = $("#addamount").val();
        if (/[^\d]/.test(addamount)) {//替换非数字字符
            alert("非法交易金额");
            return false;
        }
        var addpaylimit = $("#addpaylimit").val();
        if (/[^\d]/.test(addpaylimit)) {//替换非数字字符
            alert("非法最低消费金额");
            return false;
        }
        var timeStart = $("#add_time_start").val();
        var timeEnd = $("#add_time_end").val();
        if (timeStart != '' && timeEnd != '') {
            if (timeStart > timeEnd) {
                alert("开始时间不能大于结束时间!")
                return false;
            }
        }
        $('#addForm').form('submit', {
            url: Page.createUrl,
            onSubmit: function () {
                $('#addDialog').dialog('close');
                return $(this).form('validate');
            },
            success: function (result) {
                $('#addDialog').dialog('close');
                Page.reload();
                if (result && result.success) {
                    $('#addDialog').dialog('close');        // close the dialog
                    Page.reload();
                } else if (result.message) {
                    $.messager.alert('提示', result.message, "error");
                }
            }
        });
    },
    imgTest:function(){
        var haibaoUrl="http://epay.mypays.cn/resources/images/org_poster/random0.jpg";
        var port = location.port ? ":" + location.port : "";
        var projectPath = location.protocol + "//" + location.hostname + port;
      //  var logoUrl = projectPath + '/resources/images/follow_404.png';
        var logoUrl="http://epay.mypays.cn/resources/images/logo/epay_logo.png";
        var QRUrl="http://pan.baidu.com/share/qrcode?w=200&h=200&url=http://" +
            "epay.mypays.cn/register.html?id=8daeb5d25ee84460aa356115c606911b_";
        var userId="12345678";
        $.post(Page.imgTestUrl, {haibaoUrl: haibaoUrl,logoUrl:logoUrl,QRUrl:QRUrl,userId:userId}, function (result) {
            if (result && result.success && result.data) {
                Page.reload();
            } else {
                $.messager.alert('提示', result.message, "error");
            }
        }, 'json');;
    }
};