/**
 * Created by tums on 2016/12/05.
 */
$(document).ready(function () {
    Page.init();
});

var Page = {
    listUrl: _webApp + "/user/queryAdmin.json",
    delUrl: _webApp + "/user/delete.json",
    createUrl: _webApp + "/user/create.json",
    updateUrl: _webApp + "/user/update.json",
    grantUrl: _webApp + "/user/grant.json",
    setOrgUrl: _webApp + "/user/setOrg.json",
    orgListUrl: _webApp + "/user/orgList.json",
    userRolesUrl: _webApp + "/user/roles.json",
    roleAvailableUrl: _webApp + "/role/available.json",
    enableUrl: _webApp + "/user/enable",
    resetPwdUrl: _webApp + "/user/resetPwd",
    checkUserNameUrl: _webApp + "/user/checkUserName.json",
    init: function () {
        var isTopOrg = $("#isTopOrg").val();
        if (isTopOrg == "false") {
            $("#searchByOrg").hide();
        }
        Page.initDataGrid(isTopOrg);
        $("#searchBtn").click(function () {
            Page.search();
        });
    },
    initDataGrid: function (isTopOrg) {
        this.dataGrid = $('#dataGrid').datagrid({
            url: this.listUrl,
            sortOrder: "desc",
            idField: "id",
            nowrap: true,
            pagination: true,
            pageList: [10, 25, 50, 100],
            pageSize: 50,
            rownumbers: true,
            autoRowHeight: true,
            singleSelect: true,
            checkOnSelect: true,
            selectOnCheck: true,
            columns: [[{
                field: 'checkbox',
                checkbox: true
            }, {
                field: 'id',
                title: 'ID',
                hidden: true
            }, {
                field: 'org_name',
                title: '所属机构',
                width: 200
            }, {
                field: 'username',
                title: '用户名',
                width: 160
            }, {
                field: 'family_name',
                title: '姓名',
                width: 160
            }, {
                field: 'idcard',
                title: '身份证',
                width: 160
            }, {
                field: 'register_time',
                title: '注册时间',
                width: 180,
                formatter: function (value, row, index) {
                    return DateUtils.formatTimeStr(value);
                }
            }, {
                field: 'last_login_time',
                title: '最后登录时间',
                width: 180,
                formatter: function (value, row, index) {
                    return DateUtils.formatTimeStr(value);
                }
            }, {
                field: 'last_login_ip',
                title: '最后登录IP',
                width: 180
            }, {
                field: 'enable',
                title: '操作',
                width: 180,
                formatter: function (value, row, index) {
                    var html = [];
                    if (value) {
                        html.push('<a href="javascript:Page.enable(\'' + row.id + '\',\'0\',\'' + row.username + '\');" class="">禁用</a>');
                    } else {
                        html.push('<a href="javascript:Page.enable(\'' + row.id + '\',\'1\',\'' + row.username + '\');" class="">启用</a>');
                    }
                    html.push('&nbsp;&nbsp;|&nbsp;&nbsp;<a href="javascript:Page.resetPwd(\'' + row.id + '\',\'' + row.username + '\');" class="">密码重置</a>');
                    return html.join("");
                }
            }]],
            toolbar: [{
                text: '添加',
                iconCls: 'icon-add',
                handler: function () {
                    Page.add();
                }
            }, {
                text: '修改',
                iconCls: 'icon-edit',
                handler: function () {
                    Page.edit();
                }
            }, {
                text: '角色授权',
                iconCls: 'icon-add',
                handler: function () {
                    Page.grant();
                }
            },
                {
                    text: '机构授权',
                    iconCls: 'icon-add',
                    handler: function () {
                        Page.org();
                    }
            }],
            onLoadSuccess: function (data) {
                if (isTopOrg == "false") {
                    $("#dataGrid").datagrid("hideColumn", "org_name");
                }
            }
        });
    },
    /**
     * 搜索
     */
    search: function () {
        var searchData = $("#searchForm").serializeJson();
        this.dataGrid.datagrid('load', searchData);
        this.dataGrid.datagrid("clearChecked");
        this.dataGrid.datagrid("clearSelections");
    },
    add: function () {
        $('#addDialog').dialog('open').dialog('center').dialog('setTitle', '添加');
        $('#addForm').form('clear');
    },
    addSubmit: function () {
        //验证用户名是否存在
        var username = $("#checkName").val();
        $.post(Page.checkUserNameUrl, {username: username}, function (response) {
            if (response && response.success) {
                var flag = response.data;
                if(flag){
                    $('#addForm').form('submit', {
                        url: Page.createUrl,
                        onSubmit: function () {
                            return $(this).form('validate');
                        },
                        success: function (result) {
                            result = JSON.parse(result);
                            if (result && result.success) {
                                $('#addDialog').dialog('close');        // close the dialog
                                Page.reload();
                            } else if (result.message) {
                                $.messager.alert('提示', result.message, "error");
                            }
                        }
                    });
                }else{
                    alert('用户名已存在!');
                    $('#addDialog').dialog('close');
                    return;
                }

            }
        });

    },
    edit: function () {
        var row = this.dataGrid.datagrid('getSelected');
        if (row) {
            $('#editDialog').dialog('open').dialog('center').dialog('setTitle', '编辑');
            $('#editForm').form('load', row);
        } else {
            $.messager.alert('提示', "未选中行", "info");
        }
    },
    editSubmit: function () {
        $('#editForm').form('submit', {
            url: Page.updateUrl,
            onSubmit: function () {
                return $(this).form('validate');
            },
            success: function (result) {
                result = JSON.parse(result);
                if (result && result.success) {
                    $('#editDialog').dialog('close');        // close the dialog
                    Page.reload();
                } else if (result.message) {
                    $.messager.alert('提示', result.message, "error");
                }
            }
        });
    },
    enable: function (userId, _enable, username) {
        var msg = "确定要禁用当前用户吗？";
        if (_enable == '1') {
            msg = "确定要启用当前用户吗？";
        }
        $.messager.confirm('Confirm', msg, function (flag) {
            if (flag) {
                $.get(Page.enableUrl, {userId: userId, _enable: _enable, username: username}, function (result) {
                    if (result && result.success && result.data) {
                        Page.reload();
                    } else {
                        $.messager.alert('提示', result.message, "error");
                    }
                }, 'json');
            }
        });
    },
    grant: function () {
        var row = this.dataGrid.datagrid('getSelected');
        if (row) {
            $('#grantDialog').dialog('open').dialog('center').dialog('setTitle', '角色授权');
            $('#grantForm').form('load', row);
            Page.loadRoles(row.id);
        } else {
            $.messager.alert('提示', "未选中行", "info");
        }
    },
    grantSubmit: function () {
        $('#grantForm').form('submit', {
            url: Page.grantUrl,
            onSubmit: function () {
                return $(this).form('validate');
            },
            success: function (result) {
                result = JSON.parse(result);
                if (result && result.success) {
                    $('#grantDialog').dialog('close');        // close the dialog
                    Page.reload();
                } else if (result.message) {
                    $.messager.alert('提示', result.message, "error");
                }
            }
        });
    },
    //机构授权
    org: function () {
        var data = $("#orgList").val();
        var row = this.dataGrid.datagrid('getSelected');
        if (row) {
            $('#orgDialog').dialog('open').dialog('center').dialog('setTitle', '机构授权');
            $('#orgForm').form('load', row);
            Page.loadOrgs();
        } else {
            $.messager.alert('提示', "未选中行", "info");
        }
    },
    orgSubmit: function () {
        $('#orgForm').form('submit', {
            url: Page.setOrgUrl,
            onSubmit: function () {
                return $(this).form('validate');
            },
            success: function (result) {
                result = JSON.parse(result);
                if (result && result.success) {
                    $('#orgDialog').dialog('close');        // close the dialog
                    Page.reload();
                } else if (result.message) {
                    $.messager.alert('提示', result.message, "error");
                }
            }
        });
    },

    reload: function () {
        this.dataGrid.datagrid('reload');
        this.dataGrid.datagrid("clearChecked");
        this.dataGrid.datagrid("clearSelections");
    },
    loadRoles: function (id) {//加载用户拥有的角色和当前可用的角色列表,并进行匹配
        $.post(Page.userRolesUrl, {id: id}, function (result) {
            if (result && result.success) {
                var roles = result.data;
                $("#rolesCombobox").combobox({
                    "url": Page.roleAvailableUrl,
                    "onLoadSuccess": function (data, item) {
                        if (!(roles && roles.length > 0)) {
                            return;
                        }
                        $.each(data, function (i, x) {
                            $.each(roles, function (j, y) {
                                if (x.id == y.resource_id) {
                                    $("#rolesCombobox").combobox("select", x.id);
                                }
                            });
                        });
                    }
                });
            }
        });
    },
   /* loadOrgs:function(){
        $('#combobox').combobox({
                         onChange: function() {
                             var value = $('#combobox').combobox('getValues');
                             console.log(value);
                             console.log(value.join(','));
                           alert(value);
                        }
                });
    }*/

   loadOrgs:function(){
       $.post(Page.orgListUrl, {}, function (result) {
           if (result) {
               $("#orgList").html('');
               $("#orgList").append("<ul>");
               $.each(result, function(key,value) {
                   $("#orgList").append('<li class=""><input class="easyui-combobox" name="resourceIds" type="checkbox" value='+key+'><span>'+value+'</span>')
               });
               $("#orgList").append("</ul>");
           }
       });
   }

};
function showList(){
    if($("#orgList").css("display")=='none'){//如果show是隐藏的

        $("#orgList").css("display","block");//show的display属性设置为block（显示）

    }else{//如果show是显示的

        $("#orgList").css("display","none");//show的display属性设置为none（隐藏）

    }
}