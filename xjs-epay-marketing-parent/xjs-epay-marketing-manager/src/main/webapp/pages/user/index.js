/**
 * Created by tums on 2016/12/05.
 */
$(document).ready(function () {
    Page.init();
});
var Page = {
    listUrl: _webApp + "/user/query.json",
    delUrl: _webApp + "/user/delete.json",
    updateUrl: _webApp + "/user/update.json",
    editAgentUrl: _webApp + "/user/editAgent.json",
    // autoQryUrl: _webApp + '/user/autoQuery',
    enableUrl: _webApp + "/user/enable",
    resetPwdUrl: _webApp + "/user/resetPwd",
    userRolesUrl: _webApp + "/user/roles.json",
    roleAvailableUrl: _webApp + "/role/available.json",
    grantUrl: _webApp + "/user/grant.json",
    getUrl: _webApp + "/user/get.json",
    detailUrl: _webApp + "/user/cardDetail",
    qrcodeUrl: _webApp + "/user/getQRcode",
    init: function () {
        var showOrg = $("#showOrg").val();
        if (showOrg == 'false') {
            $("#searchByOrg").hide();
        }
        Page.initDataGrid(showOrg);
        $("#searchBtn").click(function () {
            Page.search();
        });
    },
    initDataGrid: function (showOrg) {
        this.dataGrid = $('#dataGrid').datagrid({
            url: this.listUrl,
            sortOrder: "desc",
            idField: "id",
            nowrap: true,
            pagination: true,
            pageList: [10, 25, 50, 100],
            pageSize: 50,
            rownumbers: true,
            autoRowHeight: true,
            singleSelect: true,
            checkOnSelect: true,
            selectOnCheck: true,
            columns: [[{
                field: 'checkbox',
                checkbox: true
            }, {
                field: 'id',
                title: 'ID',
                hidden: true
            }, {
                field: 'org_name',
                title: '所属机构',
                width: 180
            }, {
                field: 'agent_family_name',
                title: '代理',
                width: 80
            }, {
                field: 'referee_family_name',
                title: '推荐人',
                width: 80
            }, {
                field: 'username',
                title: '用户名',
                width: 120
            }, {
                field: 'family_name',
                title: '姓名',
                width: 120
            }, {
                field: 'idcard',
                title: '身份证',
                width: 160,
                formatter: function (value, row, index) {
                    return StringUtils.formatIdCard(value);

                }
            }, {
                field: 'sex',
                title: '性别',
                width: 80,
                formatter: function (value, row, index) {
                    if (value === 'MALE') {
                        return '男';
                    } else if (value === 'FEMALE') {
                        return '女';
                    } else if (value === 'UNKNOWN') {
                        return '未知';
                    }
                }
            }, {
                field: 'type',
                title: '类型',
                width: 80,
                formatter: function (value, row, index) {
                    return Page.typeText(value);
                }
            }, {
                field: 'status',
                title: '认证状态',
                width: 120,
                formatter: function (value, row, index) {
                    return Page.statusText(value);
                }
            }, {
                field: 'rate',
                title: '分润',
                width: 80
            }, {
                field: 'available_amt',
                title: '可用金额',
                width: 80,
                formatter: function (value, row, index) {
                    return StringUtils.formatLongMoney(value);
                }
            }, {
                field: 'history_amt',
                title: '总金额',
                width: 80,
                formatter: function (value, row, index) {
                    return StringUtils.formatLongMoney(value);
                }
            }, {
                field: 'register_time',
                title: '注册时间',
                width: 180,
                formatter: function (value, row, index) {
                    return DateUtils.formatTimeStr(value);
                }
            }, {
                field: 'last_login_time',
                title: '最后登录时间',
                width: 180,
                formatter: function (value, row, index) {
                    return DateUtils.formatTimeStr(value);
                }
            }, {
                field: 'last_login_ip',
                title: '最后登录IP',
                width: 180
            }, {
                field: 'enable',
                title: '操作',
                width: 180,
                formatter: function (value, row, index) {
                    var html = [];
                    if (value) {
                        html.push('<a href="javascript:Page.enable(\'' + row.id + '\',\'0\',\'' + row.username + '\');" class="">禁用</a>');
                    } else {
                        html.push('<a href="javascript:Page.enable(\'' + row.id + '\',\'1\',\'' + row.username + '\');" class="">启用</a>');
                    }
                    html.push('&nbsp;&nbsp;|&nbsp;&nbsp;<a href="javascript:Page.resetPwd(\'' + row.id + '\',\'' + row.username + '\');" class="">密码重置</a>');
                    return html.join("");
                }
            }]],
            onDblClickRow: function (rowIndex, rowData) {
                Page.view();
            },
            toolbar: [{
                text: '查看',
                iconCls: 'icon-add',
                handler: function () {
                    Page.view();
                }
            }, {
                text: '修改',
                iconCls: 'icon-edit',
                handler: function () {
                    Page.edit();
                }
            }, {
                id: "toolbar-agent-grant",
                text: '代理设置',
                iconCls: 'icon-edit',
                handler: function () {
                    Page.editAgent();
                }
            }, {
                id: "toolbar-grant",
                text: '授权',
                iconCls: 'icon-add',
                handler: function () {
                    Page.grant();
                }
            }],
            onLoadSuccess: function (data) {
                if (showOrg == 'false') {
                    $("#dataGrid").datagrid("hideColumn", "org_name");
                    var isOpenAgentGrant = $("#isOpenAgentGrant").val();
                    if (isOpenAgentGrant != "true") {
                        $("#toolbar-agent-grant").hide();
                        $("#toolbar-grant").hide();
                    }
                }
            }
        });
    },
    /**
     * 搜索
     */
    search: function () {
        var searchData = $("#searchForm").serializeJson();
        this.dataGrid.datagrid('load', searchData);
        this.dataGrid.datagrid("clearChecked");
        this.dataGrid.datagrid("clearSelections");
    },
    edit: function () {
        var row = this.dataGrid.datagrid('getSelected');
        if (row) {
            if (row.status == "SUCCESS") {//已经实名认证
                $.messager.alert('提示', "已认证的用户无法修改", "warning");
                return;
            }
            $('#editDialog').dialog('open').dialog('center').dialog('setTitle', '编辑');
            $('#editForm').form('load', row);
        } else {
            $.messager.alert('提示', "未选中行", "info");
        }
    },
    editSubmit: function () {
        $('#editForm').form('submit', {
            url: Page.updateUrl,
            onSubmit: function () {
                return $(this).form('validate');
            },
            success: function (response) {
                response = JSON.parse(response);
                if (response && response.success) {
                    $('#editDialog').dialog('close');
                    Page.reload();
                } else if (response.message) {
                    $.messager.alert('提示', response.message, "error");
                }
            }
        });
    },
    reload: function () {
        this.dataGrid.datagrid('reload');
        this.dataGrid.datagrid("clearChecked");
        this.dataGrid.datagrid("clearSelections");
    },
    editAgent: function () {
        var row = this.dataGrid.datagrid('getSelected');
        if (row) {
            var from = $('#editAgentForm');
            from.form('clear');
            var loginUserType = $("#loginUserType").val();
            if (loginUserType != "AGENT" && loginUserType != "ORG_ROOT") {
                $.messager.alert('提示', "权限不足", "warning");
                return;
            }
            if (row.type == "ORG_ROOT" || row.type == "ORG_ADMIN") {
                $.messager.alert('提示', "权限不足", "warning");
                return;
            }
            if (row.status != "SUCCESS") {//未认证通过
                $.messager.alert('提示', "该用户未认证,无法设置代理", "warning");
                return;
            }
            $('#editAgentDialog').dialog('open').dialog('center').dialog('setTitle', '代理设置');
            from.form('load', row);
            var ss = row.rate;
            var rate = parseInt(ss * 100);
            $("#modify_rate").textbox("setValue", rate);
            $("#modify_extend_agent").combobox("setValue", row.extend_agent ? "1" : "0");
            $("#modify_type").combobox("setValue", row.type == "AGENT" ? "1" : "0");
            Page.loadAgentRate(row.agent);
        } else {
            $.messager.alert('提示', "未选中行", "info");
        }
    },
    editAgentSubmit: function () {
        var form = $("#editAgentForm");
        var modify_rate = form.find("input[name=modify_rate]").val();
        var modify_extend_agent = form.find("input[name=modify_extend_agent]").val();
        var modify_type = form.find("input[name=modify_type]").val();
        var modifyRate = $("#modify_rate").val();
        //检验修改后的分润是否合法
        if (/[^\d]/.test(modifyRate)) {//替换非数字字符
            alert("请输入整数型的分润值");
            return false;
        }

        form.form('submit', {
            url: Page.editAgentUrl,
            onSubmit: function () {
                $('#editAgentDialog').dialog('close');
                return $(this).form('validate');
            },
            success: function (result) {
                result = JSON.parse(result);
                $('#editAgentDialog').dialog('close');
                Page.reload();
                if (result && result.success) {
                    $('#editAgentDialog').dialog('close');        // close the dialog
                    Page.reload();
                } else if (result.message) {
                    $.messager.alert('提示', result.message, "error");
                }
            }
        });
    },
    view: function () {
        var row = this.dataGrid.datagrid('getSelected');
        if (row) {
            $('#viewForm').form('clear');
            $('#viewDialog').dialog('open').dialog('center').dialog('setTitle', '用户详情');
            $('#viewForm').form('load', row);
            var sex = row.sex;
            var type = row.type;
            var status = row.status;
            var available_amt = row.available_amt;
            var history_amt = row.history_amt;
            var register_time = row.register_time;
            var last_login_time = row.last_login_time;
            var enable = row.enable;
            $('#sex').textbox("setValue", Page.sexText(sex));
            $('#type').textbox("setValue", Page.typeText(type));
            $('#status').textbox("setValue", Page.statusText(status));
            $('#available_amt').textbox("setValue", StringUtils.formatLongMoney(available_amt));
            $('#history_amt').textbox("setValue", StringUtils.formatLongMoney(history_amt));
            $('#register_time').textbox("setValue", DateUtils.formatTimeStr(register_time));
            $('#last_login_time').textbox("setValue", DateUtils.formatTimeStr(last_login_time));
            $('#enable').textbox("setValue", Page.enableText(enable));
            var code = row.org_code;
            var userId = row.id;
            //收款方银行信息
            $.get(Page.detailUrl, {id: userId}, function (result) {
                if (result && result.success) {
                    var data = result.data;
                    if (data) {
                        $('#viewUserForm').form('load', data);
                    }
                } else {
                    $.messager.alert('提示', result.message, "error");
                }
            }, 'json');
            //二维码
            $.get(Page.qrcodeUrl, {userId: userId}, function (result) {
                if (result && result.success) {
                    var data = result.data;
                    if (data) {
                        $('#qr_img').attr("src", data);
                    }
                } else {
                    $.messager.alert('提示', result.message, "error");
                }
            }, 'json');
        } else {
            $.messager.alert('提示', "未选中行", "info");
        }
    },
    sexText: function (value) {
        if (value === 'MALE') {
            return '男';
        } else if (value === 'FEMALE') {
            return '女';
        } else if (value === 'UNKNOWN') {
            return '未知';
        }
    },
    typeText: function (value) {
        if (value === 'USER') {
            return '普通用户';
        } else if (value === 'AGENT') {
            return '代理';
        } else if (value === 'ORG_ADMIN') {
            return '系统管理员';
        } else if (value === 'ORG_ROOT') {
            return '机构管理员';
        } else {
            return value;
        }
    },
    statusText: function (value) {
        if (value == 'SUCCESS') {
            return '认证成功';
        } else if (value == 'FAIL') {
            return '<font style="color: yellow">认证失败</font>';
        } else {
            return '<font style="color: red">未认证</font>';
        }
    },
    enableText: function (value) {
        if (value) {
            return '启用';
        } else {
            return '禁用';
        }
    },
    enable: function (userId, _enable, username) {
        var msg = "确定要禁用当前用户吗？";
        if (_enable == '1') {
            msg = "确定要启用当前用户吗？";
        }
        $.messager.confirm('Confirm', msg, function (flag) {
            if (flag) {
                $.get(Page.enableUrl, {userId: userId, _enable: _enable, username: username}, function (result) {
                    if (result && result.success && result.data) {
                        Page.reload();
                    } else {
                        $.messager.alert('提示', result.message, "error");
                    }
                }, 'json');
            }
        });
    },
    resetPwd: function (userId, username) {
        var pwd = username.substring(5);
        $.get(Page.resetPwdUrl, {userId: userId, pwd: pwd}, function (result) {
            if (result && result.success && result.data) {
                $.messager.alert('提示', "密码已经重置", "info");
            } else {
                $.messager.alert('提示', result.message, "error");
            }
        }, 'json');
    },
    grant: function () {
        var row = this.dataGrid.datagrid('getSelected');
        if (row) {
            $('#grantDialog').dialog('open').dialog('center').dialog('setTitle', '授权');
            $('#grantForm').form('load', row);
            Page.loadRoles(row.id);
        } else {
            $.messager.alert('提示', "未选中行", "info");
        }
    },
    loadRoles: function (id) {//加载用户拥有的角色和当前可用的角色列表,并进行匹配
        $.post(Page.userRolesUrl, {id: id}, function (result) {
            if (result && result.success) {
                var roles = result.data;
                $("#rolesCombobox").combobox({
                    "url": Page.roleAvailableUrl,
                    "onLoadSuccess": function (data, item) {
                        if (!(roles && roles.length > 0)) {
                            return;
                        }
                        $.each(data, function (i, x) {
                            $.each(roles, function (j, y) {
                                if (x.id == y.resource_id) {
                                    $("#rolesCombobox").combobox("select", x.id);
                                }
                            });
                        });
                    }
                });
            }
        });
    },
    loadAgentRate: function (id) {//获取当前代理用户的分润
        $.get(Page.getUrl, {userId: id}, function (result) {
            if (result && result.success) {
                var user = result.data;
                var userRate = parseInt(user.rate * 100);
                $("#agentRate").val(user.rate);
                $("#agentRateView").text('[ <= ' + userRate + ']');
            }
        });
    },
    grantSubmit: function () {
        $('#grantForm').form('submit', {
            url: Page.grantUrl,
            onSubmit: function () {
                return $(this).form('validate');
            },
            success: function (result) {
                result = JSON.parse(result);
                if (result && result.success) {
                    $('#grantDialog').dialog('close');        // close the dialog
                    Page.reload();
                } else if (result.message) {
                    $.messager.alert('提示', result.message, "error");
                }
            }
        });
    }
};