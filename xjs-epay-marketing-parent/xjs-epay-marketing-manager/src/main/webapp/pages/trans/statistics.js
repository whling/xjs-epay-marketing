/**
 * Created by tums on 2016/12/05.
 */
$(document).ready(function () {
    Page.init();
});
var Page = {
    listUrl: _webApp + "/trans/queryStatistics.json",
    treeNodesUrl: _webApp + "/user/treeNodes",
    init: function () {
        var showOrg = $("#showOrg").val();
        if (showOrg == "false") {
            $("#searchByOrg").hide();
            $("#searchBySub_trans_type").hide();
        }
        Page.initDataGrid(showOrg);
        $("#searchBtn").click(function () {
            Page.search();
        })
    },
    initDataGrid: function (showOrg) {
        this.dataGrid = $('#dataGrid').datagrid({
            url: this.listUrl,
            sortOrder: "desc",
            idField: "id",
            nowrap: true,
            pagination: true,
            pageList: [10, 25, 50, 100],
            pageSize: 50,
            rownumbers: true,
            autoRowHeight: true,
            singleSelect: true,
            checkOnSelect: true,
            selectOnCheck: true,
            columns: [[{
                field: 'checkbox',
                checkbox: true
            }, {
                field: 'id',
                title: 'ID',
                hidden: true
            }, {
                field: 'org_name',
                title: '所属机构',
                width: 120
            }, {
                field: 'username',
                title: '用户名',
                width: 120
            }, {
                field: 'family_name',
                title: '姓名',
                width: 120
            }, {
                field: 'agent_family_name',
                title: '代理',
                width: 120
            }, {
                field: 'referee_family_name',
                title: '推荐人',
                width: 120
            }, {
                field: 'trans_type',
                title: '支付方式',
                width: 120,
                formatter: function (value, row, index) {
                    return Page.transTypeText(value);
                }
            }, {
                field: 'sub_trans_type',
                title: '支付渠道',
                width: 120,
                formatter: function (value, row, index) {
                    return Page.transSubTypeText(value);
                }
            }, {
                field: 'total_cnt',
                title: '交易笔数',
                width: 120
            }, {
                field: 'total_amount',
                title: '交易金额',
                width: 120,
                formatter: function (value, row, index) {
                    return StringUtils.formatLongMoney(value);
                }
            }, {
                field: 'total_pay_amount',
                title: '到账金额',
                width: 120,
                formatter: function (value, row, index) {
                    return StringUtils.formatLongMoney(value);
                }
            }]],
            onLoadSuccess: function (obj, data) {
                if (showOrg == "false") {
                    $("#dataGrid").datagrid("hideColumn", "org_name");
                    $("#dataGrid").datagrid("hideColumn", "sub_trans_type");
                }
                var cnt = 0, amount = 0, pay_amount = 0;
                if (obj && obj.rows.length > 0) {
                    $.each(obj.rows, function (i, item) {
                        amount += item.total_amount;
                        cnt += item.total_cnt;
                        pay_amount += item.total_pay_amount;
                    });
                }
                $("#counter-cnt").text(cnt);
                $("#counter-amount").text(StringUtils.formatLongMoney(amount));
                $("#counter-pay-amount").text(StringUtils.formatLongMoney(pay_amount));
            }
        });
    },
    /**
     * 搜索
     */
    search: function () {
        var searchData = $("#searchForm").serializeJson();
        this.dataGrid.datagrid('load', searchData);
        this.dataGrid.datagrid("clearChecked");
        this.dataGrid.datagrid("clearSelections");
    },
    reload: function () {
        this.dataGrid.datagrid('reload');
        this.dataGrid.datagrid("clearChecked");
        this.dataGrid.datagrid("clearSelections");
    },
    loadTree: function (value) {
        Page.loadTreeDataGrid(value);
    },
    clearTree: function () {
        $("#search_referee_name").textbox("setValue", "");
        $("#search_referee_id").val("");
        $('#treeDialog').dialog('close')
    },
    loadTreeDataGrid: function (value) {
        this.treeGrid = $('#treeGrid').treegrid({
            url: this.treeNodesUrl,
            queryParams: {family_name: value},
            idField: "id",
            treeField: 'family_name',
            nowrap: true,
            rownumbers: true,
            autoRowHeight: true,
            checkOnSelect: true,
            selectOnCheck: true,
            columns: [[{
                field: 'checkbox',
                checkbox: true
            }, {
                field: 'id',
                title: 'ID',
                hidden: true
            }, {
                field: 'family_name',
                title: '姓名',
                sortable: true,
                width: '100%'
            }]],
            onLoadSuccess: function (row, data) {
                $('#treeDialog').dialog('open').dialog('center').dialog('setTitle', '选择用户');
            },
            onClickRow: function (row) {
                $("#search_referee_name").textbox("setValue", row.family_name);
                $("#search_referee_id").val(row.id);
            }
        });
    },
    transTypeText: function (value) {
        if (value == 'WEIXINPAY_QR') {
            return '微信支付';
        } else if (value == 'ALIPAY_QR') {
            return '支付宝';
        } else if (value == 'UNIONPAY_CGI') {
            return '银联网关';
        } else if (value == 'SHORTCUTPAY') {
            return '快捷支付';
        } else if (value == 'SHORTCUTPAY_INTEGRAL') {
            return '快捷支付[有积分]';
        } else {
            return value;
        }
    },
    transSubTypeText: function (value) {
        if (value == 'WEIXINPAY_QR') {
            return '微信支付';
        } else if (value == 'ALIPAY_QR') {
            return '支付宝';
        } else if (value == 'SHORTCUTPAY') {
            return '快捷支付[广州银联]';
        } else if (value == 'SHORTCUTPAY_MILIAN') {
            return '快捷支付[米联无积分]';
        } else if (value == 'SHORTCUTPAY_MILIAN_INTEGRAL') {
            return '快捷支付[米联有积分]';
        } else if (value == 'SHORTCUTPAY_EXT3') {
            return '快捷支付[支付通]';
        } else if (value == 'SHORTCUTPAY_EFFERSONPAY') {
            return '快捷支付[卡友]';
        } else if (value == 'SHORTCUTPAY_HANYIN') {
            return '快捷支付[瀚银]';
        } else {
            return value;
        }
    }
};