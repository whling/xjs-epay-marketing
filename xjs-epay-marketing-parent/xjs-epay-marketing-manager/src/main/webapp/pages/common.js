/**
 * form参数序列化为json格式
 *
 */
(function ($) {
    $.fn.serializeJson = function () {
        var serializeObj = {};
        var array = this.serializeArray();
        $(array).each(
            function () {
                if (serializeObj[this.name]) {
                    if ($.isArray(serializeObj[this.name])) {
                        serializeObj[this.name].push(this.value);
                    } else {
                        serializeObj[this.name] = [
                            serializeObj[this.name], this.value];
                    }
                } else {
                    serializeObj[this.name] = this.value;
                }
            });
        return serializeObj;
    };
})(jQuery);
/**
 * 重新easyui部分方法
 * @type {{datebox_formatter: easyuiUtils.datebox_formatter}}
 */
var easyuiUtils = {
    datebox_formatter: function (date) {
        var y = date.getFullYear();
        var m = date.getMonth() + 1;
        m = m < 10 ? '0' + m : m;
        var d = date.getDate();
        d = d < 10 ? '0' + d : d;
        return "" + y + m + d;
    },
    datebox_parse: function (str) {
        if (str.length != 8) {
            return new Date();
        }
        var y = str.substr(0, 4);
        var m = str.substr(4, 2);
        var d = str.substr(6, 2);
        var t = Date.parse(y + "-" + m + "-" + d);
        if (!isNaN(t)) {
            return new Date(t);
        } else {
            return new Date();
        }
    }
};
/**
 * 日期格式化
 * @param format
 * @returns {*}
 */
Date.prototype.format = function (format) {
    var o = {
        "M+": this.getMonth() + 1, //month
        "d+": this.getDate(), //day
        "h+": this.getHours(), //hour
        "m+": this.getMinutes(), //minute
        "s+": this.getSeconds(), //second
        "q+": Math.floor((this.getMonth() + 3) / 3), //quarter
        "S": this.getMilliseconds() //millisecond
    };
    if (/(y+)/.test(format)) {
        format = format.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    }
    for (var k in o) {
        if (new RegExp("(" + k + ")").test(format)) {
            format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k] : ("00" + o[k]).substr(("" + o[k]).length));
        }
    }
    return format;
};
/**
 * 日期格式化
 * @type {{formatLong: DateUtils.formatLong, formatString: DateUtils.formatString}}
 */
DateUtils = {
    formatTimeMillis: function (lTime) {
        return new Date(parseInt(lTime)).format("yyyy-MM-dd hh:mm:ss");
    },
    formatTimeStr: function (time) {
        if (!time) {
            return "";
        }
        var year = time.substring(0, 4);
        var month = time.substring(4, 6);
        var date = time.substring(6, 8);
        var hour = time.substring(8, 10);
        var minute = time.substring(10, 12);
        var second = time.substring(12);
        return year + "-" + month + "-" + date + " " + hour + ":" + minute + ":" + second;
    },
    formatDateStr: function (time) {
        if (!time) {
            return "";
        }
        var year = time.substring(0, 4);
        var month = time.substring(4, 6);
        var date = time.substring(6, 8);
        return year + "-" + month + "-" + date;
    },
    formatDateMonStr: function (time) {
        var year = time.substring(0, 4);
        var month = time.substring(4, 6);
        return year + "-" + month;
    },
    formatHoursStr: function (value) {
        if (!value) {
            return "";
        }
       // alert(value);
        var text = value + "";
        var length = text.length;
        if (length == 4) {
            var hour = value.substring(0, 2);
            var minute = value.substring(2, 4);
            return hour + ":" + minute;
        } else {
            var year = value.substring(0, 4);
            var month = value.substring(4, 6);
            var date = value.substring(6, 8);
            var hour = value.substring(8, 10);
            var minute = value.substring(10, 12);
            var second = value.substring(12);
            return year + "-" + month + "-" + date + " " + hour + ":" + minute + ":" + second;
        }
    }
};
StringUtils = {
    formatLongMoney: function (value) {
        if (value && value > 0) {
            var text = value + "";
            var length = text.length;
            if (length == 2) {
                return "0." + text;
            } else if (length == 1) {
                return "0.0" + text;
            }
            var last = text.substring(length - 2);
            return text.substring(0, length - 2) + "." + last;
        }
        else return 0;
    },
    formatBankCard: function (value) {//银行卡格式化
        if (!value) {
            return "";
        }
        var start = value.substring(0, 4);
        var end = value.substring(value.length - 4);
        return start + "***" + end;
    },
    formatIdCard: function (value) {//身份证格式化
        if (!value) {
            return "";
        }
        var start = value.substring(0, 6);
        var end = value.substring(value.length - 4);
        return start + "***" + end;
    }

};