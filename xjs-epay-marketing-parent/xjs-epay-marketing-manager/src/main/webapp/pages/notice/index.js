/**
 * Created by tums on 2016/12/05.
 */
$(document).ready(function () {
    Page.init();
});
var Page = {
    listUrl: _webApp + "/notice/query.json",
    indexUrl: _webApp + "/notice/index",
    createUrl: _webApp + "/notice/create",
    updateUrl: _webApp + "/notice/update",
    delUrl: _webApp + "/notice/delete.json",
    init: function () {
        var showOrg = $("#showOrg").val();
        if (showOrg == 'false') {
            $("#searchByOrg").hide();
        }
        Page.initDataGrid(showOrg);
        $("#searchBtn").click(function () {
            Page.search();
        })
    },
    initDataGrid: function (showOrg) {
        this.dataGrid = $('#dataGrid').datagrid({
                url: this.listUrl,
                sortOrder: "desc",
                idField: "id",
                nowrap: true,
                pagination: true,
                pageList: [10, 25, 50, 100],
                pageSize: 50,
                rownumbers: true,
                autoRowHeight: true,
                singleSelect: true,
                checkOnSelect: true,
                selectOnCheck: true,
                columns: [[{
                    field: 'checkbox',
                    checkbox: true
                }, {
                    field: 'id',
                    title: 'ID',
                    sortable: false,
                    hidden: true
                }, {
                    field: 'org_name',
                    title: '所属机构',
                    width: 280
                }, {
                    field: 'time_begin',
                    title: '开始时间',
                    width: 180,
                    formatter: function (value, row, index) {
                        return DateUtils.formatDateStr(value);
                    }
                }, {
                    field: 'time_end',
                    title: '结束时间',
                    width: 180,
                    formatter: function (value, row, index) {
                        return DateUtils.formatDateStr(value);
                    }
                }, {
                    field: 'title',
                    title: '标题',
                    width: 180
                }, {
                    field: 'content',
                    title: '内容',
                    width: 580
                }]],
                onDblClickRow: function (rowIndex, rowData) {
                    Page.edit();
                },
                toolbar: [{
                    text: '新增',
                    iconCls: 'icon-add',
                    handler: function () {
                        Page.add();
                    }
                }, {
                    text: '修改',
                    iconCls: 'icon-add',
                    handler: function () {
                        Page.edit();
                    }
                }],
                onLoadSuccess: function (data) {
                    if (showOrg == 'false') {
                        $("#dataGrid").datagrid("hideColumn", "org_name");
                    }
                }
            }
        )
        ;
    },
    /**
     * 搜索
     */
    search: function () {
        var searchData = $("#searchForm").serializeJson();
        this.dataGrid.datagrid('load', searchData);
        this.dataGrid.datagrid("clearChecked");
        this.dataGrid.datagrid("clearSelections");
    }
    ,
    reload: function () {
        this.dataGrid.datagrid('reload');
        this.dataGrid.datagrid("clearChecked");
        this.dataGrid.datagrid("clearSelections");
    }
    ,
    add: function () {
        $('#addForm').form('clear');
        $('#addDialog').dialog('open').dialog('center').dialog('setTitle', '活动公告配置');
    },
    addSubmit: function () {
        $('#addForm').form('submit', {
            url: Page.createUrl,
            onSubmit: function () {
                $('#addDialog').dialog('close');
                return $(this).form('validate');
            },
            success: function (result) {
                $('#addDialog').dialog('close');
                Page.reload();
                if (result && result.success) {
                    $('#addDialog').dialog('close');        // close the dialog
                    Page.reload();
                } else if (result.message) {
                    $.messager.alert('提示', result.message, "error");
                }
            }
        });
    }
    ,
    edit: function () {
        var row = this.dataGrid.datagrid('getSelected');
        if (row) {
            $('#editForm').form('clear');
            $('#editDialog').dialog('open').dialog('center').dialog('setTitle', '活动公告配置');
            $('#editForm').form('load', row);
        }
    },
    editSubmit: function () {
        $('#editForm').form('submit', {
            url: Page.updateUrl,
            onSubmit: function () {
                $('#editDialog').dialog('close');
                return $(this).form('validate');
            },
            success: function (result) {
                $('#editDialog').dialog('close');
                Page.reload();
                if (result && result.success) {
                    $('#editDialog').dialog('close');        // close the dialog
                    Page.reload();
                } else if (result.message) {
                    $.messager.alert('提示', result.message, "error");
                }
            }
        });
    },
    del: function () {
        var row = this.dataGrid.datagrid('getSelected');
        if (row) {
            $.messager.confirm('Confirm', '确定要删除?', function (flag) {
                if (flag) {
                    $.post(Page.delUrl, {id: row.id}, function (result) {
                        if (result && result.success && result.data) {
                            Page.reload();
                        } else {
                            $.messager.alert('提示', result.message, "error");
                        }
                    }, 'json');
                }
            });
        } else {
            $.messager.alert('提示', "未选中行", "info");
        }
    }
};