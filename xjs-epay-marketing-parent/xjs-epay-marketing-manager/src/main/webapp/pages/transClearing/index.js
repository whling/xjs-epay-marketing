/**
 * Created by tums on 2016/12/05.
 */
$(document).ready(function () {
    Page.init();
});
var Page = {
    listUrl: _webApp + "/transClearing/query.json",
    exportUrl: _webApp + "/transClearing/export",
    init: function () {
        var showOrg = $("#showOrg").val();
        if (showOrg == 'false') {
            $("#searchByOrg").hide();
        }
        Page.initDataGrid(showOrg);
        $("#searchBtn").click(function () {
            Page.search();
        })
    },
    initDataGrid: function (showOrg) {
        this.dataGrid = $('#dataGrid').datagrid({
            url: this.listUrl,
            sortOrder: "desc",
            idField: "id",
            nowrap: true,
            pagination: true,
            pageList: [10, 25, 50, 100],
            pageSize: 50,
            rownumbers: true,
            autoRowHeight: true,
            singleSelect: true,
            checkOnSelect: true,
            selectOnCheck: true,
            columns: [[{
                field: 'checkbox',
                checkbox: true
            }, {
                field: 'id',
                title: 'ID',
                hidden: true
            },{
                field: 'org_name',
                title: '机构名称',
                width: 180
            }, {
                field: 'trans_time',
                title: '交易时间',
                width: 180,
                formatter: function (value, row, index) {
                    return DateUtils.formatTimeStr(value);
                }
            },{
                field: 'client_trans_id',
                title: '交易流水',
                width: 180
            }, {
                field: 'username',
                title: '用户名',
                width: 120
            },{
                field: 'family_name',
                title: '姓名',
                width: 120
            },{
                field: 'idcard',
                title: '身份证',
                width: 120
            },{
                field: 'mobile',
                title: '手机号码',
                width: 120
            },{
                field: 'bank_no',
                title: '银行卡号',
                width: 120
            },{
                field: 'bank_name',
                title: '银行名称',
                width: 120
            }, {
                field: 'amount',
                title: '交易金额',
                width: 80,
                formatter: function (value, row, index) {
                    return StringUtils.formatLongMoney(value);
                }
            }, {
                field: 'counter_fee',
                title: '操作手续费',
                width: 80,
                formatter: function (value, row, index) {
                    return StringUtils.formatLongMoney(value);
                }
            }, {
                field: 'pay_amount',
                title: '到账金额',
                width: 120,
                formatter: function (value, row, index) {
                    return StringUtils.formatLongMoney(value);
                }
            }, {
                field: 'clearing_time',
                title: '清算时间',
                width: 180,
                formatter: function (value, row, index) {
                    return DateUtils.formatTimeStr(value);
                }
            }, {
                field: 'clearing_status',
                title: '清算状态',
                width: 120,
                formatter: function (value, row, index) {
                    return Page.clearingStatus2text(value);
                }
            }, {
                field: 'bank_id',
                title: '银行代码',
                width: 120
            },{
                field: 'branch_code',
                title: '支行代码',
                width: 120
            },{
                field: 'branch_name',
                title: '支行名称',
                width: 120
            },{
                field: 'error_code',
                title: '错误代码',
                width: 120
            },{
                field: 'error_msg',
                title: '错误信息',
                width: 120
            },  {
                field: 'memo',
                title: '说明',
                width: 120
            }]],
            onLoadSuccess: function (obj, data) {
                if (showOrg == 'false') {
                    $("#dataGrid").datagrid("hideColumn", "org_name");
                }
                var cnt = 0, amount = 0, pay_amount = 0, counter_fee = 0, operation_fee = 0;
                if (obj && obj.rows.length > 0) {
                    cnt = obj.rows.length;
                    $.each(obj.rows, function (i, item) {
                        amount += parseInt(item.amount);
                        pay_amount += parseInt(item.pay_amount);
                        counter_fee += parseInt(item.counter_fee);
                        operation_fee += parseInt(item.operation_fee);
                    });
                }
                $("#counter-cnt").text(cnt);
                $("#counter-amount").text(StringUtils.formatLongMoney(amount));
                $("#counter-pay-amount").text(StringUtils.formatLongMoney(pay_amount));
                $("#counter-counter-fee").text(StringUtils.formatLongMoney(counter_fee));
                $("#counter-operation-fee").text(StringUtils.formatLongMoney(operation_fee));
            },
            onDblClickRow: function (rowIndex, rowData) {
                Page.view();
            }
        });
    },
    /**
     * 搜索
     */
    search: function () {
        var searchData = $("#searchForm").serializeJson();
        this.dataGrid.datagrid('load', searchData);
        this.dataGrid.datagrid("clearChecked");
        this.dataGrid.datagrid("clearSelections");
    },
    reload: function () {
        this.dataGrid.datagrid('reload');
        this.dataGrid.datagrid("clearChecked");
        this.dataGrid.datagrid("clearSelections");
    },
    loadTree: function (value) {
        Page.loadTreeDataGrid(value);
    },
    clearTree: function () {
        $("#search_referee_name").textbox("setValue", "");
        $("#search_referee_id").val("");
        $('#treeDialog').dialog('close')
    },
    export: function () {
        $.messager.confirm('Confirm', '确定要导出吗?', function (flag) {
            if (flag) {
                var client_trans_id = $("#searchForm").find("input[name='client_trans_id']").val();
                var referee = $("#searchForm").find("input[name='referee']").val();
                var family_name = $("#searchForm").find("input[name='family_name']").val();
                var username = $("#searchForm").find("input[name='username']").val();
                var trans_type = $("#searchForm").find("input[name='trans_type']").val();
                var clearing_status = $("#searchForm").find("input[name='clearing_status']").val();
                var trans_time_start = $("#searchForm").find("input[name='trans_time_start']").val();
                var trans_time_end = $("#searchForm").find("input[name='trans_time_end']").val();
                var bodyParams = [];
                bodyParams.push("client_trans_id=" + client_trans_id);
                bodyParams.push("username=" + username);
                bodyParams.push("referee=" + referee);
                bodyParams.push("family_name=" + family_name);
                bodyParams.push("trans_type=" + trans_type);
                bodyParams.push("clearing_status=" + clearing_status);
                bodyParams.push("trans_time_start=" + trans_time_start);
                bodyParams.push("trans_time_end=" + trans_time_end);
                window.location.href = Page.exportUrl + "?" + bodyParams.join("&");
            }
        });
    },
/*    loadTreeDataGrid: function (value) {
        this.treeGrid = $('#treeGrid').treegrid({
            url: this.treeNodesUrl,
            queryParams: {family_name: value},
            idField: "id",
            treeField: 'family_name',
            nowrap: true,
            rownumbers: true,
            autoRowHeight: true,
            checkOnSelect: true,
            selectOnCheck: true,
            columns: [[{
                field: 'checkbox',
                checkbox: true
            }, {
                field: 'id',
                title: 'ID',
                hidden: true
            }, {
                field: 'family_name',
                title: '姓名',
                sortable: true,
                width: '100%'
            }]],
            onLoadSuccess: function (row, data) {
                $('#treeDialog').dialog('open').dialog('center').dialog('setTitle', '选择用户');
            },
            onClickRow: function (row) {
                $("#search_referee_name").textbox("setValue", row.family_name);
                $("#search_referee_id").val(row.id);
            }
        });
    },*/
    view: function () {
        var row = this.dataGrid.datagrid('getSelected');
        if (row) {
            $('#viewForm').form('clear');
            $('#viewDialog').dialog('open').dialog('center').dialog('setTitle', '订单详情');
            $('#viewForm').form('load', row);
            var trans_time = row.trans_time;
            var clearing_status = row.clearing_status;
            var clearing_type = row.clearing_type;
            var amount = row.amount;
            var operation_fee = row.operation_fee;
            var counter_fee = row.counter_fee;
            var pay_amount = row.pay_amount;
            $('#view_trans_time').textbox("setValue", DateUtils.formatTimeStr(trans_time));
            $('#view_clearing_status').textbox("setValue", Page.clearingStatus2text(clearing_status));
            $('#view_clearing_type').textbox("setValue", clearing_type == "1" ? "次日到账" : "实时到账");
            $('#view_amount').textbox("setValue", StringUtils.formatLongMoney(amount));
            $('#view_operation_fee').textbox("setValue", StringUtils.formatLongMoney(operation_fee));
            $('#view_counter_fee').textbox("setValue", StringUtils.formatLongMoney(counter_fee));
            $('#view_pay_amount').textbox("setValue", StringUtils.formatLongMoney(pay_amount));
        } else {
            $.messager.alert('提示', "未选中行", "info");
        }
    },
    clearingStatus2text: function (value) {
        var msg = "";
        switch (value) {
            case 'CLEARING_BEFORE':
                msg = "未支付";
                break;
            case 'CLEARING_ING':
                msg = "等待清算";
                break;
            case 'CLEARING_END':
                msg = "清算中";
                break;
            case 'CLEARING_SUCCESS':
                msg = "清算成功";
                break;
            case 'CLEARING_FAILURE':
                msg = "清算失败";
                break;
            default:
                msg = value;
                break;
        }
        return msg;
    }
};