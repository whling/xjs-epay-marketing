/**
 * Created by tums on 2016/12/05.
 */
$(document).ready(function () {
    Page.initDataGrid();
});
var Page = {
    listUrl: _webApp + "/role/query.json",
    indexUrl: _webApp + "/role/index",
    delUrl: _webApp + "/role/delete.json",
    createUrl: _webApp + "/role/create.json",
    updateUrl: _webApp + "/role/update.json",
    grantUrl: _webApp + "/role/grant.json",
    listResourceUrl: _webApp + "/resource/query.json",
    listRoleResourceUrl: _webApp + "/role/resource.json",
    grantForRoleUrl: _webApp + "/role/grantNewRole.json",
    loadResourceDataGrid: function (roleId) {
        $.post(Page.listRoleResourceUrl, {id: roleId}, function (result) {
            if (result && result.success) {
                var resources = result.data;
                $('#resourceDataGrid').treegrid({
                    url: Page.listResourceUrl,
                    idField: "id",
                    treeField: 'name',
                    nowrap: true,
                    autoRowHeight: true,
                    checkOnSelect: true,
                    selectOnCheck: true,
                    singleSelect: false,
                    checkbox: true,
                    columns: [[{
                        field: 'id',
                        title: 'ID',
                        hidden: true
                    }, {
                        field: 'name',
                        title: '资源名称',
                        width: 280
                    }]],
                    onLoadSuccess: function (row, data) {
                        if (!(resources && resources.length > 0)) {
                            $.each(data, function (i, item) {
                                $('#resourceDataGrid').treegrid("uncheckNode", item.id);
                            });
                            return;
                        }
                        Page.checkNode(resources, data);
                    }
                });
            } else {
                $.messager.alert('提示', result.message, "error");
            }
        }, 'json');
    },
    /**
     * 匹配当前角色权限
     * @param resources
     * @param data
     * @returns {boolean}
     */
    checkNode: function (resources, data) {
        if (!(data && data.length > 0)) {
            return false;
        }
        $.each(data, function (i, item) {
            var id = item.id;
            var exists = false;
            $.each(resources, function (j, resource) {
                if (resource.resource_id == id) {
                    $('#resourceDataGrid').treegrid("checkNode", id);
                    exists = true;
                }
            });
            if (!exists) {
                $('#resourceDataGrid').treegrid("uncheckNode", id);
            }
            Page.checkNode(resources, item.children);
        });
    },
    initDataGrid: function () {
        this.dataGrid = $('#dataGrid').datagrid({
            url: Page.listUrl,
            sortOrder: "desc",
            idField: "id",
            nowrap: true,
            pagination: true,
            pageList: [10, 25, 50, 100],
            pageSize: 10,
            rownumbers: true,
            autoRowHeight: true,
            singleSelect: true,
            checkOnSelect: true,
            selectOnCheck: true,
            columns: [[{
                field: 'checkbox',
                checkbox: true
            }, {
                field: 'id',
                title: 'ID',
                sortable: false,
                hidden: true
            }, {
                field: 'org_name',
                title: '所属机构',
                sortable: true,
                width: 280
            }, {
                field: 'name',
                title: '角色名称',
                sortable: true,
                width: 280
            }, {
                field: 'start_date',
                title: '开始时间',
                sortable: true,
                width: 120
            }, {
                field: 'end_date',
                title: '结束时间',
                sortable: true,
                width: 120
            }]],
            toolbar: [{
                text: '添加',
                iconCls: 'icon-add',
                handler: function () {
                    Page.add();
                }
            }, {
                text: '修改',
                iconCls: 'icon-edit',
                handler: function () {
                    Page.edit();
                }
            }, {
                text: '授权',
                iconCls: 'icon-add',
                handler: function () {
                    Page.grant();
                }
            }],
            onDblClickRow: function (row) {
            }
        });
    },
    add: function () {
        $('#addDialog').dialog('open').dialog('center').dialog('setTitle', '添加');
        $('#addForm').form('clear');
        $("#addForm_org_code").combobox("setValue",$("#loginUserOrgCode").val());
    },
    addSubmit: function () {
        var org_name=$("#addForm_org_code").combobox("getText");
        $('#addForm').find("input[name='org_name']").val(org_name);
        $('#addForm').form('submit', {
            url: Page.createUrl,
            onSubmit: function () {
                return $(this).form('validate');
            },
            success: function (result) {
                result = JSON.parse(result);
                if (result && result.success) {
                    $('#addDialog').dialog('close');        // close the dialog
                    var data=result.data;
                    var roleId=data.id;
                    //角色新增成功后，为当前登录用户设置该权限
                    $.post(Page.grantForRoleUrl, {resourceIds: roleId}, function (result) {
                        if (result && result.success) {
                            Page.reload();
                        }
                    });
                    Page.reload();
                } else if (result.message) {
                    $.messager.alert('提示', result.message, "error");
                }
            }
        });
    },
    edit: function () {
        var row = this.dataGrid.datagrid('getSelected');
        if (row) {
            $('#editDialog').dialog('open').dialog('center').dialog('setTitle', '编辑');
            $('#editForm').form('load', row);
            $("#Form_org_code").combobox("setValue",$("#loginUserOrgCode").val());
        } else {
            $.messager.alert('提示', "未选中行", "info");
        }
    },
    editSubmit: function () {
        var org_name=$("#editForm_org_code").combobox("getText");
        $('#editForm').find("input[name='org_name']").val(org_name);
        $('#editForm').form('submit', {
            url: Page.updateUrl,
            onSubmit: function () {
                return $(this).form('validate');
            },
            success: function (result) {
                result = JSON.parse(result);
                if (result && result.success) {
                    $('#editDialog').dialog('close');        // close the dialog
                    Page.reload();
                } else if (result.message) {
                    $.messager.alert('提示', result.message, "error");
                }
            }
        });
    },
    grant: function () {
        var row = this.dataGrid.datagrid('getSelected');
        if (row) {
            $('#grantDialog').dialog('open').dialog('center').dialog('setTitle', '授权');
            $('#grantForm').form('load', row);
            Page.loadResourceDataGrid(row.id);
        } else {
            $.messager.alert('提示', "未选中行", "info");
        }
    },
    grantSubmit: function () {
        var rows = $('#resourceDataGrid').treegrid('getCheckedNodes');
        if (!rows) {
            $.messager.alert('提示', "请选择资源", "info");
            return;
        }
        var resourceIds = [];
        $.each(rows, function (i, item) {
            resourceIds.push(item.id);
        });
        $("#resourceIds").val(resourceIds);
        $('#grantForm').form('submit', {
            url: Page.grantUrl,
            onSubmit: function () {
                return $(this).form('validate');
            },
            success: function (result) {
                result = eval('(' + result + ')');
                if (result && result.success) {
                    $('#grantDialog').dialog('close');        // close the dialog
                    Page.reload();
                } else if (result.message) {
                    $.messager.alert('提示', result.message, "error");
                }
            }
        });
    },
    del: function () {
        var row = this.dataGrid.datagrid('getSelected');
        if (row) {
            $.messager.confirm('Confirm', '确定要删除?', function (flag) {
                if (flag) {
                    $.post(Page.delUrl, {id: row.id}, function (result) {
                        if (result && result.success && result.data) {
                            Page.reload();
                        } else {
                            $.messager.alert('提示', result.message, "error");
                        }
                    }, 'json');
                }
            });
        } else {
            $.messager.alert('提示', "未选中行", "info");
        }
    },
    reload: function () {
        this.dataGrid.datagrid('reload');
        this.dataGrid.datagrid("clearChecked");
        this.dataGrid.datagrid("clearSelections");
    }
};