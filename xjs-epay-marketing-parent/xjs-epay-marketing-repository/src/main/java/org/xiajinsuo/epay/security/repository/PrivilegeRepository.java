/**
 * Autogenerated by bestpay-maven-plugin
 * DO NOT EDIT DIRECTLY
 *
 * @Copyright 2016 www.bestpay.io Inc. All rights reserved.
 */
package org.xiajinsuo.epay.security.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.query.Param;
import org.xiajinsuo.epay.security.AuthGrantType;
import org.xiajinsuo.epay.security.AuthPrivilegeType;
import org.xiajinsuo.epay.security.domain.Organization;
import org.xiajinsuo.epay.security.domain.Privilege;
import org.xiajinsuo.epay.security.domain.Role;

import java.util.List;

@SuppressWarnings("all")
/** 权限 JPA Repository */
@org.apache.avro.specific.AvroGenerated
public interface PrivilegeRepository extends io.bestpay.framework.repository.AbstractRepository<org.xiajinsuo.epay.security.domain.Privilege, String> {
    /**
     * 获取某一类资源权限
     *
     * @param parentCode
     * @return
     */
    @org.springframework.data.jpa.repository.Query(value = "SELECT p.* FROM tbl_bp_privilege p INNER JOIN tbl_bp_resource r ON r.id=p.resource_id WHERE p.resource_type = :privilegeType AND r.parent_code = :parentCode", nativeQuery = true)
    public List<Privilege> findResourceByParentCode(@Param("parentCode") String parentCode, @Param("privilegeType") AuthPrivilegeType privilegeType);

    /**
     * 删除某一类资源
     *
     * @param code
     * @return
     */
    @Modifying
    @org.springframework.data.jpa.repository.Query(value = "delete from tbl_bp_resource WHERE parent_code = :code", nativeQuery = true)
    public int deleteResourceByParentCode(@Param(value = "code") String code);

    /**
     * 删除权限
     *
     * @param grantType
     * @param grantId
     * @return
     */
    @Modifying
    @org.springframework.data.jpa.repository.Query(value = "delete from Privilege WHERE grantType = :grantType and grantId = :grantId", nativeQuery = false)
    public int deleteByGrantId(@Param("grantType") AuthGrantType grantType, @Param("grantId") String grantId);


    /**
     * 获取用户的权限
     *
     * @param userId
     * @return
     */
    @org.springframework.data.jpa.repository.Query("SELECT DISTINCT a FROM Resource a, Privilege b_u_r, Privilege b_r_r, User c WHERE a.id = b_r_r.resourceId AND b_u_r.grantId = c.id AND b_u_r.grantType = :urGrantType AND b_r_r.grantId = b_u_r.resourceId AND b_r_r.grantType = :rrGrantType AND c.id = :userId order by  a.sortNo desc")
    public List<io.bestpay.framework.resource.domain.Resource> findResourceByUser(@Param("userId") String userId, @Param("urGrantType") AuthGrantType urGrantType, @Param("rrGrantType") AuthGrantType rrGrantType);

    /**
     * 用户所拥有的机构：用户-机构
     *
     * @param grantType
     * @param resourceType
     * @param type
     * @return
     */
    @org.springframework.data.jpa.repository.Query("SELECT DISTINCT a FROM Organization a, Privilege b WHERE a.id = b.resourceId" +
            " AND b.grantType = :uGrantType" +
            " AND b.resourceType = :rPrivilegeType" +
            " AND b.grantId = :userId")
    List<Organization> findOrgResourceByUser(@Param("uGrantType") AuthGrantType uGrantType,
                                             @Param("rPrivilegeType") AuthPrivilegeType rPrivilegeType,
                                             @Param("userId") String userId);

    /**
     * 用户所拥有的机构：用户-角色-机构
     *
     * @param rgType
     * @param rpType
     * @param ugType
     * @param upType
     * @param userId
     * @return
     */
    @org.springframework.data.jpa.repository.Query("SELECT DISTINCT a FROM Organization a, Privilege b, Privilege c " +
            "WHERE a.id = b.resourceId AND b.grantId = c.resourceId AND b.grantType = :rgType AND b.resourceType = :rpType " +
            "AND c.grantType = :ugType AND c.resourceType = :upType AND c.grantId = :userId")
    List<Organization> findOrgResourceByUserRole(
            @Param("rgType") AuthGrantType rgType,
            @Param("rpType") AuthPrivilegeType rpType,
            @Param("ugType") AuthGrantType ugType,
            @Param("upType") AuthPrivilegeType upType,
            @Param("userId") String userId);

    /**
     * 用户所拥有的机构：用户-角色-机构
     *
     * @param rgType
     * @param rpType
     * @param ugType
     * @param upType
     * @param userId
     * @return
     */
    @org.springframework.data.jpa.repository.Query("SELECT DISTINCT a FROM Role a, Privilege b WHERE a.id = b.resourceId AND b.grantId = :userId AND b.grantType = :grantType AND b.resourceType = :privilegeType")
    List<Role> findRoleResourceByUser(@Param("grantType") AuthGrantType grantType, @Param("privilegeType") AuthPrivilegeType privilegeType, @Param("userId") String userId);
}
