/**
 * Autogenerated by bestpay-maven-plugin
 * DO NOT EDIT DIRECTLY
 *
 * @Copyright 2017 www.bestpay.io Inc. All rights reserved.
 *
 */
package org.xiajinsuo.epay.business.repository;

@SuppressWarnings("all")
/** 公告 JPA Repository */
@org.apache.avro.specific.AvroGenerated
public interface NoticeRepository extends io.bestpay.framework.repository.AbstractRepository<org.xiajinsuo.epay.business.domain.Notice, java.lang.String> {

}
