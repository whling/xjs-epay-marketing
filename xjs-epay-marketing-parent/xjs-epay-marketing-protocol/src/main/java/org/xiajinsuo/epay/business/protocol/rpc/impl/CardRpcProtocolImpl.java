/**
 * Autogenerated by bestpay-maven-plugin
 * YOU CAN EDIT DIRECTLY
 *
 * @Copyright 2017 www.bestpay.io Inc. All rights reserved.
 */
package org.xiajinsuo.epay.business.protocol.rpc.impl;

import io.bestpay.framework.base.CodeException;
import io.bestpay.framework.base.SpecificPage;
import io.bestpay.framework.base.UserRequest;
import io.bestpay.framework.service.AbstractService;
import org.xiajinsuo.epay.business.Card;

import javax.validation.constraints.Min;
import java.util.List;

@SuppressWarnings("all")
/** 银行卡  RpcProtocol */
@org.springframework.stereotype.Component("cardRpcProtocol")
@org.apache.avro.specific.AvroGenerated
public class CardRpcProtocolImpl extends io.bestpay.framework.protocol.rpc.RpcProtocol<org.xiajinsuo.epay.business.domain.Card, org.xiajinsuo.epay.business.Card, java.lang.String>
        implements org.xiajinsuo.epay.business.protocol.rpc.CardRpcProtocol {

    @javax.annotation.Resource
    private org.xiajinsuo.epay.business.service.CardService cardService;

    /**
     * 通过id删除
     */
    public boolean delete(java.lang.String id, UserRequest userRequest) throws CodeException {
        return super.delete(id, userRequest);
    }

    /**
     * 更新
     */
    public Card update(Card e, UserRequest userRequest) throws CodeException {
        return super.update(e, userRequest);
    }

    /**
     * 分页查询
     */
    public io.bestpay.framework.base.SpecificPage<Card> query(UserRequest userRequest, @Min(1L) Long currentPage, @Min(1L) Long pageSize) throws CodeException {
        return super.query(userRequest, currentPage, pageSize);
    }

    /**
     * 创建对像
     */
    public Card create(Card e, UserRequest userRequest) throws CodeException {
        return super.create(e, userRequest);
    }

    /**
     * 通过id获取对像
     */
    public Card get(java.lang.String id, UserRequest userRequest) throws CodeException {
        return super.get(id, userRequest);
    }

    @Override
    public boolean updateDefault(String userId, String bankNo) {
        return cardService.updateDefault(userId, bankNo);
    }

    @Override
    public Card getDefaultByUserId(String userId) {
        return cardService.getDefaultByUserId(userId);
    }

    @Override
    public List<Card> findByUserId(String userId) {
        return cardService.findByUserId(userId);
    }

    @Override
    public Card getByBankNo(String userId, String bankNo) {
        return cardService.getByBankNo(userId, bankNo);
    }

    @Override
    protected AbstractService<org.xiajinsuo.epay.business.domain.Card, java.lang.String> getService() {
        return (AbstractService) cardService;
    }

    @Override
    public SpecificPage<Card> query(UserRequest userRequest, List<String> orgCodes, Long currentPage, Long pageSize, String username, String mobile, String family_name, String card_type, String bank_name, String org_code) throws CodeException{
        return cardService.query(userRequest,orgCodes,currentPage,pageSize,username,mobile,family_name,card_type,bank_name,org_code);
    }

    @Override
    public List<Card> findByIdCard(String orgCode, String idcard) {
        return cardService.findByIdCard(orgCode, idcard);
    }
}