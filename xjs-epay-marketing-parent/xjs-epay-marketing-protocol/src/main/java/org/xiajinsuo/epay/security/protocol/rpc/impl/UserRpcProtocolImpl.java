/**
 * Autogenerated by bestpay-maven-plugin
 * YOU CAN EDIT DIRECTLY
 *
 * @Copyright 2017 www.bestpay.io Inc. All rights reserved.
 */
package org.xiajinsuo.epay.security.protocol.rpc.impl;

import io.bestpay.framework.base.CodeException;
import io.bestpay.framework.base.SpecificPage;
import io.bestpay.framework.base.UserRequest;
import io.bestpay.framework.service.AbstractService;
import org.xiajinsuo.epay.security.User;
import org.xiajinsuo.epay.security.vo.UserExt;

import javax.validation.constraints.Min;
import java.util.List;
import java.util.Map;

@SuppressWarnings("all")
/** 用户  RpcProtocol */
@org.springframework.stereotype.Component("userRpcProtocol")
@org.apache.avro.specific.AvroGenerated
public class UserRpcProtocolImpl extends io.bestpay.framework.protocol.rpc.RpcProtocol<org.xiajinsuo.epay.security.domain.User, org.xiajinsuo.epay.security.User, java.lang.String>
        implements org.xiajinsuo.epay.security.protocol.rpc.UserRpcProtocol {

    @javax.annotation.Resource
    private org.xiajinsuo.epay.security.service.UserService userService;

    /**
     * 通过id删除
     */
    public boolean delete(java.lang.String id, UserRequest userRequest) throws CodeException {
        return super.delete(id, userRequest);
    }

    /**
     * 更新
     */
    public User update(User e, UserRequest userRequest) throws CodeException {
        return super.update(e, userRequest);
    }

    /**
     * 分页查询
     */
    public io.bestpay.framework.base.SpecificPage<User> query(UserRequest userRequest, @Min(1L) Long currentPage, @Min(1L) Long pageSize) throws CodeException {
        return super.query(userRequest, currentPage, pageSize);
    }

    /**
     * 创建对像
     */
    public User create(User e, UserRequest userRequest) throws CodeException {
        return userService.create(e, userRequest);
    }

    /**
     * 通过id获取对像
     */
    public User get(java.lang.String id, UserRequest userRequest) throws CodeException {
        return super.get(id, userRequest);
    }

    @Override
    public User getFrontendLoginInfo(String orgCode, String username) {
        return userService.getFrontendLoginInfo(orgCode, username);
    }

    @Override
    public User getBackendLoginInfo(String orgCode, String username) {
        return userService.getBackendLoginInfo(orgCode, username);
    }

    @Override
    public boolean updateLoginIpTime(String id, String ip, String loginTime) {
        return userService.updateLoginIpTime(id, ip, loginTime);
    }

    @Override
    public boolean updateLoginIpTimeOpenId(String id, String ip, String openId, String loginTime) {
        return userService.updateLoginIpTimeOpenId(id, ip, openId, loginTime);
    }

    @Override
    public boolean updatePassword(String id, String password, String updateTime) {
        return userService.updatePassword(id, password, updateTime);
    }

    @Override
    public boolean updateEnable(String id, Boolean enable, String updateTime, String updateUser) {
        return userService.updateEnable(id, enable, updateTime, updateUser);
    }

    @Override
    public boolean updateAuditSucc(String id, String updateTime) {
        return userService.updateAuditSucc(id, updateTime);
    }

    @Override
    public boolean updateAvailableAmtClear(String id, String updateTime) {
        return userService.updateAvailableAmtClear(id, updateTime);
    }

    @Override
    public boolean updateAvailableAmtPlus(String id, Integer amt, String updateTime) {
        return userService.updateAvailableAmtPlus(id, amt, updateTime);
    }

    @Override
    public List<Map<String, Object>> countRefereeByWeek(String userId) {
        return userService.countRefereeByWeek(userId);
    }

    @Override
    public List countRefereeByStatus(String userId) {
        return userService.countRefereeByStatus(userId);
    }

    @Override
    public int countRefereeByAuditSucc(String userId) {
        return userService.countRefereeByAuditSucc(userId);
    }

    @Override
    public List<UserExt> findAgentUpper(String org, String agent) {
        return userService.findAgentUpper(org, agent);
    }

    @Override
    public SpecificPage<User> query(UserRequest userRequest, List<String> orgCodes, Long currentPage, Long pageSize, Map<String, Object> queryParams) throws CodeException {
        return userService.query(userRequest, orgCodes, currentPage, pageSize, queryParams);
    }


    @Override
    public User findOneByOpendId(String openid) {
        return userService.findOneByOpendId(openid);
    }

    @Override
    public User findOrgRoot(String orgCode) {
        return userService.findOrgRoot(orgCode);
    }

    @Override
    public boolean updateAgent(User e, UserRequest userRequest) throws CodeException {
        return userService.updateAgent(e, userRequest);
    }

    @Override
    public SpecificPage<User> queryByAgent(UserRequest userRequest, String orgCode, String agent, Long currentPage, Long pageSize, Map<String, Object> queryParams) throws CodeException {
        return userService.queryByAgent(userRequest, orgCode, agent, currentPage, pageSize, queryParams);
    }

    @Override
    public boolean checkUserName(String username) throws CodeException {
        return userService.checkUserName(username);
    }

    @Override
    protected AbstractService<org.xiajinsuo.epay.security.domain.User, java.lang.String> getService() {
        return (AbstractService) userService;
    }


}