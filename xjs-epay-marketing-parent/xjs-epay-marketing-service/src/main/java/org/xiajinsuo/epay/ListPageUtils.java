package org.xiajinsuo.epay;

import org.apache.commons.collections.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tums on 2017/8/30.
 * 集合分页对象
 */
public class ListPageUtils {
    /**
     * @param pageNo   当前页码
     * @param pageSize 页数
     * @param list     所有集合
     * @return
     */
    public static List page(int pageNo, int pageSize, List list) {
        if (CollectionUtils.isEmpty(list)) {
            return null;
        }
        List result = new ArrayList();
        int allCount = list.size();
        int pageCount = (allCount + pageSize - 1) / pageSize;
        if (pageNo >= pageCount) {
            pageNo = pageCount;
        }
        int start = (pageNo - 1) * pageSize;
        int end = pageNo * pageSize;
        if (end >= allCount) {
            end = allCount;
        }
        for (int i = start; i < end; i++) {
            result.add(list.get(i));
        }
        return result;
    }
}
