package org.xiajinsuo.epay.generate;

import io.bestpay.framework.resource.IdGenerate;
import io.bestpay.framework.util.StringUtils;

import java.text.DecimalFormat;


public class IdGenerateFactory {
    private final static String SYS_SIGN = "TP"; // TODO SYS_SIGN

    /**
     * 生成机构号
     *
     * @param idGenerate
     * @return
     */
    public static String generateOrgCode(IdGenerate idGenerate) {
        DecimalFormat versionDf = new DecimalFormat(StringUtils.getFormatPattern(idGenerate.getIdVersion(), 2));
        DecimalFormat idDf = new DecimalFormat(StringUtils.getFormatPattern(idGenerate.getIdValue(), 6));
        return String.format("%s%s%s", SYS_SIGN, versionDf.format(idGenerate.getIdVersion()), idDf.format(idGenerate.getIdValue()));
    }

    /**
     * 生成商户号
     *
     * @param idGenerate
     * @return
     */
    public static String generateMchCode(IdGenerate idGenerate) {
        DecimalFormat versionDf = new DecimalFormat(StringUtils.getFormatPattern(idGenerate.getIdVersion(), 2));
        DecimalFormat idDf = new DecimalFormat(StringUtils.getFormatPattern(idGenerate.getIdValue(), 7));
        return String.format("%s%s%s", SYS_SIGN, versionDf.format(idGenerate.getIdVersion()), idDf.format(idGenerate.getIdValue()));
    }
}
