/**
 * Created by Administrator on 2017/4/26.
 */
/**
 * 分转换成元
 * @param amount
 * @returns {*}
 */
var NumberUtils = {
    format: function (amount) {//分->元，保留两位小数
        if (amount) {
            amount = amount + "";
            var len = amount.length;
            if (len == 2) {
                return "0." + amount;
            } else if (len > 2) {
                return amount.slice(0, len - 2) + '.' + amount.slice(len - 2);
            } else {
                return "0.0" + amount;
            }
        } else {
            return "0.00";
        }
    }
};
/**
 * 日期格式化
 * @type {{formatLong: DateUtils.formatLong, formatString: DateUtils.formatString}}
 */
var DateUtils = {
    formatTimeMillis: function (lTime) {
        return new Date(parseInt(lTime)).format("yyyy-MM-dd hh:mm:ss");
    },
    formatTimeStr: function (time) {
        var year = time.substring(0, 4);
        var month = time.substring(4, 6);
        var date = time.substring(6, 8);
        var hour = time.substring(8, 10);
        var minute = time.substring(10, 12);
        var second = time.substring(12);
        return year + "-" + month + "-" + date + " " + hour + ":" + minute + ":" + second;
    },
    formatDateStr: function (time) {
        var year = time.substring(0, 4);
        var month = time.substring(4, 6);
        var date = time.substring(6, 8);
        return year + "-" + month + "-" + date;
    }
};
var TransUtils = {
    /**
     * 格式化银行卡号,去后面4位
     * @param bankNo
     * @returns {*}
     */
    formatBankNo: function (bankNo) {
        if (bankNo && bankNo.length >= 4) {
            return bankNo.substring(bankNo.length - 4);
        }
        return bankNo;
    },
    /**
     * 交易金额是否合法
     * @param transAmt
     * @param min
     * @param max
     * @returns {boolean}
     */
    filterAmt: function (transAmt, min, max) {
        if (!transAmt) {
            $.alert("请输入收款金额");
            return false;
        }
        if (transAmt < min) {
            $.alert("收款金额需大于等于" + min + "元");
            return false;
        }
        if (transAmt >= 1000) {
            var mod = transAmt.substring(transAmt.length - 3);
            if (mod % 111 === 0) {
                $.alert("收款金额后三位不能相同");
                return false;
            }
        }
        if (transAmt >= max) {
            $.alert("收款金额不能超过" + max);
            return false;
        }
        return true;
    }
    ,
    /**
     * 判断当前时间是否可以交易
     * @param timeStart
     * @param timeEnd
     * @returns {boolean}
     */
    isTransTime: function (timeStart, timeEnd) {
        //当前时间
        var today = new Date();
        var hour = today.getHours();
        var minute = today.getMinutes();
        //交易开始、结束时间
        var _endHour = timeEnd.slice(0, 2);
        var _endMinute = timeEnd.slice(2);
        var _startHour = timeStart.slice(0, 2);
        var _startMinute = timeStart.slice(2);
        if (hour < _startHour || hour > _endHour) {
            return false;
        }
        if (hour == _startHour && minute < _startMinute) {
            return false;
        }
        if (hour == _endHour && minute > _endMinute) {
            return false;
        }
        return true;
    }
};
//时间
Date.prototype.format = function (format) {
    var o = {
        "M+": this.getMonth() + 1, //month
        "d+": this.getDate(),    //day
        "h+": this.getHours(),   //hour
        "m+": this.getMinutes(), //minute
        "s+": this.getSeconds(), //second
        "q+": Math.floor((this.getMonth() + 3) / 3),  //quarter
        "S": this.getMilliseconds() //millisecond
    };
    if (/(y+)/.test(format)) format = format.replace(RegExp.$1,
        (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)if (new RegExp("(" + k + ")").test(format))
        format = format.replace(RegExp.$1,
            RegExp.$1.length == 1 ? o[k] :
                ("00" + o[k]).substr(("" + o[k]).length));
    return format;
};