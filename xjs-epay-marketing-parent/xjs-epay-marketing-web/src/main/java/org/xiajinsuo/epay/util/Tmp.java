package org.xiajinsuo.epay.util;

import junit.framework.TestCase;

import java.util.List;
import java.util.UUID;

/**
 * Created by tums on 2017/7/11.
 */
public class Tmp extends TestCase {
    public void testY(){
        System.out.println(UUID.randomUUID().toString().replace("-",""));
        System.out.println(UUID.randomUUID().toString().replace("-",""));
        double round = ArithUtil.round_floor(1024.529, 4);
        System.out.println(round);
    }
   /* public void testX() {
        System.out.println(RandomStringUtils.randomAlphabetic(4));
        System.out.println(UUID.randomUUID().toString().replace("-",""));
        System.out.println(UUID.randomUUID().toString().replace("-",""));

        List<Node> nodes = new ArrayList<>();
        nodes.add(new Node(2, 1));
        nodes.add(new Node(3, 1));
        nodes.add(new Node(4, 1));


        nodes.add(new Node(5, 2));
        nodes.add(new Node(6, 2));
        nodes.add(new Node(7, 3));


        nodes.add(new Node(8, 5));
        nodes.add(new Node(9, 5));
        nodes.add(new Node(10, 5));

        nodes.add(new Node(11, 6));
        nodes.add(new Node(12, 6));
        nodes.add(new Node(13, 6));
        int agent = 2;

        List<Node> result = new ArrayList<>();

        recursionLower(nodes, agent, result);
        System.out.println(result);
    }*/


    /**
     * 获取某个父节点下面的所有子节点
     *
     * @param list
     * @param pid
     * @return
     */
    public static void recursionLower(List<Node> list, int pid, List<Node> result) {
        for (Node item : list) {
            //遍历出父id等于参数的id，add进子节点集合
            if (item.getPid() == pid) {
                //递归遍历下一级
                recursionLower(list, item.getId(), result);
                result.add(item);
            }
        }
    }
}

class Node {
    private int id;
    private int pid;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public int getPid() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }

    public Node(int id, int pid) {
        this.id = id;
        this.pid = pid;
    }

    @Override
    public String toString() {
        return "Node{" +
                "id=" + id +
                ", pid=" + pid +
                '}';
    }
}