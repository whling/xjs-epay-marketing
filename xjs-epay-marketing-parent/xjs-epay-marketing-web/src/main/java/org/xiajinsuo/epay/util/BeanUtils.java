package org.xiajinsuo.epay.util;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class BeanUtils implements ApplicationContextAware {

    private ApplicationContext applicationContext;

    private static BeanUtils INSTANCE;

    public ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        synchronized (BeanUtils.class) {
            if (null == INSTANCE) {
                this.applicationContext = applicationContext;
                INSTANCE = this;
            }
        }
    }

    /**
     * 获取实例
     *
     * @param requiredType
     * @return
     */
    public static <T> T getBean(Class<T> requiredType) {
        return INSTANCE.getApplicationContext().getBean(requiredType);
    }

    /**
     * 通过名称获取bean
     *
     * @param name
     * @return
     */
    public static Object getBean(String name) {
        return INSTANCE.getApplicationContext().getBean(name);
    }

    /**
     * 获取同类型的Bean
     *
     * @param className
     * @return
     */
    public static <T> List<T> getBeans(Class<T> className) {
        List<T> list = new ArrayList<T>();
        Map<String, T> map = INSTANCE.getApplicationContext().getBeansOfType(className);
        for (String key : map.keySet()) {
            T bean = map.get(key);
            list.add(bean);
        }
        return list;
    }
}
