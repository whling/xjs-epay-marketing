package org.xiajinsuo.epay.cache;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import io.bestpay.framework.base.CodeException;
import io.bestpay.framework.base.SpecificPage;
import io.bestpay.framework.base.SpecificRecordUtils;
import io.bestpay.framework.base.UserRequest;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.xiajinsuo.epay.business.BankCardBinExt;
import org.xiajinsuo.epay.business.CityBankCode;
import org.xiajinsuo.epay.security.Organization;
import org.xiajinsuo.epay.util.BankUtil;
import org.xiajinsuo.epay.util.HttpUtils;
import org.xiajinsuo.epay.util.SysConfigUtils;
import redis.clients.jedis.JedisCluster;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/11/22.
 */
@Component
public class CacheUtils {
    private static Logger LOGGER = LoggerFactory.getLogger(CacheUtils.class);
    @javax.annotation.Resource
    private JedisCluster jedisCluster;
    @javax.annotation.Resource
    private org.xiajinsuo.epay.business.protocol.rpc.BankCardBinExtRpcProtocol bankCardBinRpcProtocol;
    @javax.annotation.Resource
    private org.xiajinsuo.epay.business.protocol.rpc.CityBankCodeRpcProtocol cityBankCodeRpcProtocol;
    @javax.annotation.Resource
    private org.xiajinsuo.epay.security.protocol.rpc.OrganizationRpcProtocol organizationRpcProtocol;

    /**
     * 根据银行卡号获取卡bin
     *
     * @param cardNo
     * @return
     */
    public BankCardBinExt getCardBin(String cardNo, UserRequest userRequest) {
        if (StringUtils.isEmpty(cardNo)) {
            return null;
        }
        BankCardBinExt result = null;
        List<BankCardBinExt> bankCardBins = null;
        Object cache = CacheFactory.getInstance().getCache(CacheKeys.CACHE_EPAY_MARKETING_BANK_CARD_BIN);
        if (cache != null) {
            bankCardBins = (List<BankCardBinExt>) cache;
        } else {
            try {
                SpecificPage<BankCardBinExt> specificPage = bankCardBinRpcProtocol.query(userRequest, 1L, (long) Integer.MAX_VALUE);
                bankCardBins = specificPage.getContent();
                if (CollectionUtils.isNotEmpty(bankCardBins)) {
                    CacheFactory.getInstance().createCache(CacheKeys.CACHE_EPAY_MARKETING_BANK_CARD_BIN, bankCardBins);
                }
            } catch (Exception e) {
                LOGGER.error("bankCardBins load fail,message[{}]", e);
            }
        }
        if (CollectionUtils.isEmpty(bankCardBins)) {
            return null;
        }
        int size = bankCardBins.size();
        for (int i = 0; i < size; i++) {
            BankCardBinExt item = bankCardBins.get(i);
            if (cardNo.startsWith(item.getCardBin())) {
                result = item;
                break;
            }
        }
        return result;
    }

    /**
     * 获取支行列表
     *
     * @param bankName
     * @param city
     * @param keywords
     * @return
     */
    public List<CityBankCode> getCityBankCodeList(String bankName, String city, String keywords) {
        if (StringUtils.isEmpty(bankName)) {
            return null;
        }
        bankName = BankUtil.filterName(bankName);
        List<CityBankCode> cityBankCodes = null;
        String key = String.format(CacheKeys.CACHE_EPAY_MARKETING_CITY_BANK_CODE, bankName);
        Object cache = CacheFactory.getInstance().getCache(key);
        if (cache != null) {
            cityBankCodes = (List<CityBankCode>) cache;
        } else {
            cityBankCodes = cityBankCodeRpcProtocol.findByBankName(bankName);
            CacheFactory.getInstance().createCache(key, cityBankCodes);
        }
        if (CollectionUtils.isEmpty(cityBankCodes)) {
            return null;
        }
        int size = cityBankCodes.size();
        List<CityBankCode> result = new ArrayList<>();
        if (StringUtils.isNotEmpty(city) && StringUtils.isNotEmpty(keywords)) {//过滤条件：城市+支行
            for (int i = 0; i < size; i++) {
                CityBankCode item = cityBankCodes.get(i);
                if (item.getCity().equals(city) && item.getBranchName().contains(keywords)) {
                    result.add(item);
                }
            }
        } else if (StringUtils.isNotEmpty(keywords)) {//过滤条件：支行
            for (int i = 0; i < size; i++) {
                CityBankCode item = cityBankCodes.get(i);
                if (item.getBranchName().contains(keywords)) {
                    result.add(item);
                }
            }
        } else if (StringUtils.isNotEmpty(city)) {//过滤条件：城市
            for (int i = 0; i < size; i++) {
                CityBankCode item = cityBankCodes.get(i);
                if (item.getCity().equals(city)) {
                    result.add(item);
                }
            }
        } else {//无过滤条件
            result = cityBankCodes;
        }
        return result;
    }


    /**
     * 获取微信接口访问凭证
     *
     * @param appid
     * @param secret
     * @return
     */
    public String getAccessToken(String appid, String secret) {
        String key = String.format(CacheKeys.CACHE_EPAY_MARKETING_ACCESS_TOKEN, appid);
        String result = jedisCluster.get(key);
        if (StringUtils.isEmpty(result)) {
            try {
                result = httpGetAccessToken(appid, secret);
                if (StringUtils.isNotEmpty(result)) {
                    jedisCluster.setex(key, 7200, result);
                }
            } catch (Exception e) {
                LOGGER.error("AccessToken load fail,message[{}]", e);
            }
        }
        return result;
    }

    /**
     * 获取微信接口访问凭证
     *
     * @return
     */
    public void refreshAccessToken(String appid, String secret) {
        String key = String.format(CacheKeys.CACHE_EPAY_MARKETING_ACCESS_TOKEN, appid);
        String result = httpGetAccessToken(appid, secret);
        if (StringUtils.isNotEmpty(result)) {
            jedisCluster.setex(key, 7200, result);
        }
    }


    private String httpGetAccessToken(String appid, String secret) {
        String result = null;
        try {
            JSONObject respJson = HttpUtils.doGetHttps(String.format(SysConfigUtils.getWeixin_api_access_token_url(), appid, secret));
            if (null != respJson) {
                result = respJson.getString("access_token");
                LOGGER.info("accessToken info =[{}]", respJson);
            }
        } catch (Exception e) {
            LOGGER.error("AccessToken load fail,message[{}]", e);
        }
        return result;
    }

    /**
     * 获取微信接口访问凭证
     *
     * @return
     */
    public void refreshJsapiTicket(String appid, String secret) {
        String key = String.format(CacheKeys.CACHE_EPAY_MARKETING_JSAPI_TICKET, appid);
        String result = httpGetJsapiTicket(getAccessToken(appid, secret));
        if (StringUtils.isNotEmpty(result)) {
            jedisCluster.setex(key, 7200, result);
        }
    }

    private String httpGetJsapiTicket(String accessToken) {
        if (StringUtils.isEmpty(accessToken)) {
            return null;
        }
        String result = null;
        try {
            JSONObject respJson = HttpUtils.doGetHttps(String.format(SysConfigUtils.getWeixin_api_jsapi_ticket_url(), accessToken));
            if (null != respJson) {
                result = respJson.getString("ticket");
                LOGGER.info("JsapiTicket info =[{}]", respJson);
            }
        } catch (Exception e) {
            LOGGER.error("JsapiTicket load fail,message[{}]", e);
        }
        return result;
    }

    public String getJsapiTicket(String appid, String secret) {
        String key = String.format(CacheKeys.CACHE_EPAY_MARKETING_JSAPI_TICKET, appid);
        String result = jedisCluster.get(key);
        if (StringUtils.isEmpty(result)) {
            try {
                result = httpGetJsapiTicket(getAccessToken(appid, secret));
                if (StringUtils.isNotEmpty(result)) {
                    jedisCluster.setex(key, 7200, result);
                }
            } catch (Exception e) {
                LOGGER.error("JsapiTicket load fail,message[{}]", e);
            }
        }
        return result;
    }

    /**
     * 机构信息
     *
     * @param orgCode
     * @return
     */
    public Organization getOrganization(String orgCode) {
        if (StringUtils.isEmpty(orgCode)) {
            return null;
        }
        Organization result = null;
        String key = String.format(CacheKeys.CACHE_EPAY_MARKETING_ORG, orgCode);
        String cache = jedisCluster.get(key);
        if (cache != null) {
            result = new Organization();
            try {
                SpecificRecordUtils.parse(result, JSON.parseObject(cache));
            } catch (CodeException e) {
                LOGGER.error("cache parse error,message [{}]", e);
                return null;
            }
        } else {
            try {
                result = organizationRpcProtocol.getByCode(orgCode);
                if (result != null) {
                    jedisCluster.set(key, result.toString());
                }
            } catch (Exception e) {
                LOGGER.error("cache query fail,message [{}]", e);
            }
        }
        return result;
    }
}
