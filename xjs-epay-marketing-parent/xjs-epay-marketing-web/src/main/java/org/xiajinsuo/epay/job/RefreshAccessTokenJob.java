package org.xiajinsuo.epay.job;

import com.dangdang.ddframe.job.api.ShardingContext;
import com.dangdang.ddframe.job.api.simple.SimpleJob;
import io.bestpay.framework.base.AbstractUserRequest;
import io.bestpay.framework.base.CodeException;
import io.bestpay.framework.base.SpecificPage;
import io.bestpay.framework.request.UserHttpRequest;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.xiajinsuo.epay.cache.CacheUtils;
import org.xiajinsuo.epay.security.Organization;
import org.xiajinsuo.epay.security.protocol.rpc.OrganizationRpcProtocol;

import java.util.List;

/**
 * Created by Administrator on 2016/11/29.
 * 更新微信凭证
 */
@Component
public class RefreshAccessTokenJob implements SimpleJob {
    private static Logger LOGGER = LoggerFactory.getLogger(RefreshAccessTokenJob.class);
    @javax.annotation.Resource
    private CacheUtils cacheUtils;
    @javax.annotation.Resource
    private OrganizationRpcProtocol organizationRpcProtocol;


    @Override
    public void execute(ShardingContext shardingContext) {
        LOGGER.info("查询更新微信AccessToken 和 JsapiTicket开始");
        AbstractUserRequest abstractUserRequest = new UserHttpRequest();
        abstractUserRequest.setPrincipal("SYS");
        try {
            SpecificPage<Organization> organizationSpecificPage = organizationRpcProtocol.query(abstractUserRequest, 0L, (long) Integer.MAX_VALUE);
            List<Organization> organizations = organizationSpecificPage.getContent();
            if (CollectionUtils.isEmpty(organizations)) {
                LOGGER.info("organizations is null");
                return;
            }
            for (Organization item : organizations) {
                String appId = item.getWeixinAppId();
                String secret = item.getWeixinSecret();
                cacheUtils.refreshAccessToken(appId, secret);
                Thread.sleep(3000);
                cacheUtils.refreshJsapiTicket(appId, secret);
                Thread.sleep(500);
            }
        } catch (CodeException | InterruptedException e) {
            LOGGER.error("process fail,{}", e);
        }
        LOGGER.info("查询更新微信AccessToken 和 JsapiTicket完成");
    }
}
