package org.xiajinsuo.epay.util.msg;

/**
 * Created by tums on 2017/6/22.
 */
public final class MsgTemplate {
    /**
     * 短信超时时间,10分钟
     */
    public static final int DEFAULT_TIMEOUT = 10;
    public static final String REGISTER = "尊敬的用户，您的注册验证码为：%s,有效期%s分钟。【乐汇通】";
    public static final String REGISTER_SUCCESS = "恭喜您成为乐汇通的会员，关注抠客（hxtc_kk）微信公众号即可使用。【乐汇通】";
    public static final String RESET_PASSWORD = "尊敬的用户，您正在找回密码，验证码是%s。有效期%s分钟。如非本人操作，请尽快修改密码以保证您的账号安全。【乐汇通】";
}
