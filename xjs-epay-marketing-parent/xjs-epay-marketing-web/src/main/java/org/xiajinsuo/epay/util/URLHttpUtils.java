package org.xiajinsuo.epay.util;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by Administrator on 2017/6/6.
 * 公共http请求
 */
public class URLHttpUtils {
    private static Logger LOGGER = LoggerFactory.getLogger(URLHttpUtils.class);
    private final static String DEFAULT_CHARSET_UTF8 = "utf-8";
    private final static String METHOD_POST = "POST";
    private final static String METHOD_GET = "GET";
    private final static int CONNECT_TIMEOUT = 180 * 1000;
    private final static int READ_TIMEOUT = 180 * 1000;

    /**
     * 执行http请求
     *
     * @param path
     * @param params
     * @param method
     * @param charset
     * @return
     */
    private static String execute(String path, String params, String method, String charset) {
        LOGGER.debug("url={},method={},params={},charset={}", path, method, params, charset);
        if (StringUtils.isEmpty(params)) {
            return null;
        }
        try {
            URL url = new URL(path);
            URLConnection rulConnection = url.openConnection();
            HttpURLConnection httpUrlConnection = (HttpURLConnection) rulConnection;
            httpUrlConnection.setDoOutput(true);
            httpUrlConnection.setDoInput(true);
            httpUrlConnection.setUseCaches(false);
            httpUrlConnection.setRequestMethod(method);
            httpUrlConnection.setConnectTimeout(CONNECT_TIMEOUT);
            httpUrlConnection.setReadTimeout(READ_TIMEOUT);
            httpUrlConnection.connect();
            if (StringUtils.isNotEmpty(params)) {
                OutputStream output = httpUrlConnection.getOutputStream();
                output.write(params.getBytes());
                output.flush();
                output.close();
            }
            InputStream inputStream = httpUrlConnection.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, charset));
            String line = "";
            StringBuilder result = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                result.append(line);
            }
            return result.toString();
        } catch (Exception e) {
            LOGGER.error("http request error,msg={}", e);
        }
        return null;
    }

    /**
     * http post
     *
     * @param url
     * @param params
     * @param charset
     * @return
     */
    public static String post(String url, String params, String charset) {
        return execute(url, params, METHOD_POST, charset);
    }

    /**
     * http post
     *
     * @param url
     * @param params
     * @return
     */
    public static String post(String url, String params) {
        return execute(url, params, METHOD_POST, DEFAULT_CHARSET_UTF8);
    }


    /**
     * http get
     *
     * @param url
     * @param params
     * @return
     */
    public static String get(String url, String params) {
        return execute(url, params, METHOD_GET, DEFAULT_CHARSET_UTF8);
    }

}
