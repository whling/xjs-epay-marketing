package org.xiajinsuo.epay.util.email;

import java.io.File;

/**
 * 邮件附件
 */
public class Attachment {
    private String cid;
    private File file;
    private String fileName;

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Attachment() {

    }

    public Attachment(File file, String fileName) {
        super();
        this.file = file;
        this.fileName = fileName;
    }
}