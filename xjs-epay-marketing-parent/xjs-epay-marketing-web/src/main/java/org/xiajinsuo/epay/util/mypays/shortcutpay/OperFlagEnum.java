package org.xiajinsuo.epay.util.mypays.shortcutpay;

/**
 * Created by tums on 2017/6/27.
 * A：新增
 * M：修改全部
 * M01:修改商户基本信息
 * M02:修改结算卡信息
 * M03:修改T0费率
 * M04:修改T1费率
 */
public enum OperFlagEnum {
    A("A", "新增"),
    M("M", "修改全部"),
    M01("M01", "修改商户基本信息"),
    M02("M02", "修改结算卡信息"),
    M03("M03", "修改T0费率"),
    M04("M04", "修改T1费率");

    OperFlagEnum(String value, String name) {
        this.value = value;
        this.name = name;
    }

    private String value;
    private String name;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
