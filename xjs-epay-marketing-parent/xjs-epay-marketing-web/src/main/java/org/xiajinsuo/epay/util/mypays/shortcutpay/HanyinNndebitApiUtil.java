package org.xiajinsuo.epay.util.mypays.shortcutpay;

import org.xiajinsuo.epay.business.Card;
import org.xiajinsuo.epay.business.TransChannel;
import org.xiajinsuo.epay.business.TransSubChannel;
import org.xiajinsuo.epay.security.User;
import org.xiajinsuo.epay.util.TransTypeEnum;

/**
 * Created by Administrator on 2017/6/13.
 * 瀚银快捷--4511
 */
public class HanyinNndebitApiUtil {
    private final static String CHANNEL = TransTypeEnum.SHORTCUTPAY_HANYIN_NNDEBIT.getValue();

    /**
     * 消费接口
     *
     * @param clientTransId
     * @param hpMerCode
     * @param transSubChannel
     * @param cardPay
     * @param _transAmtFen
     * @return
     */
    public static RespResult pay(String clientTransId, String hpMerCode, TransSubChannel transSubChannel, Card cardDefault, Card cardPay, String _transAmtFen, String rate, String counterFee, String operationFee, String payAmount, boolean payothers) {
        return PubApiUtil.pay(CHANNEL, clientTransId, hpMerCode, transSubChannel, cardDefault, cardPay, _transAmtFen, rate, counterFee, operationFee, payAmount, payothers);
    }

    /**
     * 入网接口
     *
     * @param loginUser
     * @param transChannel
     * @param card
     * @param operFlag
     * @return
     */
    public static RespResult register(User loginUser, TransChannel transChannel, Card card, OperFlagEnum operFlag, boolean payothers, String payothersRate) {
        return PubApiUtil.register(CHANNEL, loginUser, transChannel, card, operFlag, payothers, payothersRate);
    }
}
