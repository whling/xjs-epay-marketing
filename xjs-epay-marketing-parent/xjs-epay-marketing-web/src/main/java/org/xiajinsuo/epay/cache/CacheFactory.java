package org.xiajinsuo.epay.cache;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2016/11/21.
 */
public class CacheFactory {


    private static CacheFactory cachFactory = null;
    //缓存已经创建的Map对象
    private Map<String, Object> factoryMap = new HashMap<String, Object>();

    //单例模式
    private CacheFactory() {
    }

    //使用“懒加载”的单例模式
    public static CacheFactory getInstance() {
        if (cachFactory == null) {
            cachFactory = new CacheFactory();
        }
        return cachFactory;
    }

    /**
     * 创建缓存
     *
     * @param key
     * @param value
     */
    public void createCache(String key, Object value) {
        factoryMap.put(key, value);
    }

    /**
     * 移除对象
     *
     * @param key
     */
    public void removeCache(String key) {
        factoryMap.remove(key);
    }

    /**
     * 判断是否存在key为cachName的map
     *
     * @param key
     * @return
     */
    public boolean isExist(String key) {
        if (factoryMap.containsKey(key)) {
            return true;
        }
        return false;
    }

    /**
     * 获取key为cachName的map对象
     *
     * @param key
     * @return
     */
    public Object getCache(String key) {
        if (factoryMap.containsKey(key)) {
            return factoryMap.get(key);
        }
        return null;
    }
}
