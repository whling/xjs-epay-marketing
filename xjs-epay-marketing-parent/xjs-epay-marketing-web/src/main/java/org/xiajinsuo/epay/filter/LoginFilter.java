package org.xiajinsuo.epay.filter;

import org.springframework.util.StringUtils;
import org.xiajinsuo.epay.security.User;
import org.xiajinsuo.epay.util.LoginUtil;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Administrator on 2016/10/11.
 */
public class LoginFilter implements Filter {
    //重定向页面
    private String redirect = "/login.html";//default
    //需要过滤的请求后缀
    private String[] suffixList;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        String suffixList = filterConfig.getInitParameter("suffixList");
        if (!StringUtils.isEmpty(suffixList)) {
            this.suffixList = suffixList.split(",");
        }
        String redirect = filterConfig.getInitParameter("redirect");
        if (!StringUtils.isEmpty(redirect)) {
            this.redirect = redirect;
        }
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        String path = request.getServletPath().toLowerCase();
        if (path.contains("/nosession/")) {//支付回调
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }
        boolean flag = false;
        for (String suffix : suffixList) {
            if (path.endsWith(suffix)) {
                flag = true;
            }
        }
        if (!flag) {
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }
        User user = LoginUtil.getLoginUser(request);
        if (user == null) {
            response.sendRedirect(redirect);
        } else {
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    @Override
    public void destroy() {

    }
}
