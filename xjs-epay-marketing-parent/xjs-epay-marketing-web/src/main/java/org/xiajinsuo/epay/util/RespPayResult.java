package org.xiajinsuo.epay.util;

/**
 * 扫码支付请求结果
 */
public class RespPayResult extends RespResult {
    /**
     * 二维码地址
     */
    private String code_url;
    /**
     * 交易金额
     */
    private String trans_amt;
    /**
     * 商户名称
     */
    private String family_name;

    public String getTrans_amt() {
        return trans_amt;
    }

    public void setTrans_amt(String trans_amt) {
        this.trans_amt = trans_amt;
    }

    public String getFamily_name() {
        return family_name;
    }

    public void setFamily_name(String family_name) {
        this.family_name = family_name;
    }

    public String getCode_url() {
        return code_url;
    }

    public void setCode_url(String code_url) {
        this.code_url = code_url;
    }

    private RespPayResult(boolean success, String error_code, String error_msg, String code_url, String trans_amt, String family_name) {
        setSuccess(success);
        setError_code(error_code);
        setError_msg(error_msg);
        setCode_url(code_url);
        setTrans_amt(trans_amt);
        setFamily_name(family_name);
    }

    public static RespPayResult success(String code_url, String trans_amt, String family_name) {
        return new RespPayResult(true, "", "", code_url, trans_amt, family_name);
    }

    public static RespPayResult success() {
        return new RespPayResult(true, "", "", "", "", "");
    }

    public static RespPayResult fail(String error_code, String error_msg) {
        return new RespPayResult(false, error_code, error_msg, "", "", "");
    }
}
