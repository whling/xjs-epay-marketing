package org.xiajinsuo.epay.util.msg;

import junit.framework.TestCase;
import org.apache.commons.lang.RandomStringUtils;
import org.xiajinsuo.epay.util.URLHttpUtils;

/**
 * Created by Administrator on 2017/6/16.
 * 账号：x_xmc01
 * 密码：x123456
 * 登入链接：http://220.162.247.163:8060
 * 连接协议：HTTP
 * 用户编号(userID)：360
 * 用户密钥(cpKey)：DEB0A4D0C9EAAAEE5004AF750C484237
 * 下发地址：http://220.162.247.163:8060/protFace/subSmsApi.aspx
 * 发送速度：100条/秒
 * 签名：
 * <p>
 * 连接协议：CMPP2
 * 企业代码：600028
 * 连接密码：35FD99F16F
 * 接入号码：10690071895360
 * 连接IP：220.162.247.163
 * 连接端口：8955
 * 发送速度：100条/秒
 */
public class MsgApiUtil extends TestCase {
    private static final String userID = "360";//用户ID	是	string	向管理员索取
    private static final String cpKey = "DEB0A4D0C9EAAAEE5004AF750C484237";//用户密钥	是	string	向管理员索取(可修改)
    private static final String API_URL = "http://220.162.247.163:8060/protFace/subSmsApi.aspx";

    public void testX() {
        boolean flag = register("13178278987", "02464", "10");
        System.out.println(flag);
    }

    /**
     * 注册短信验证码
     *
     * @param phone
     * @param code
     * @param timeout
     * @return
     */
    public static boolean register(String phone, String code, String timeout) {
        String msg = String.format(MsgTemplate.REGISTER, code, timeout);
        StringBuffer params = new StringBuffer();
        params.append("userID=").append(userID);
        params.append("&cpKey=").append(cpKey);
        params.append("&uPhone=").append(phone);
        params.append("&cpSubID=").append(RandomStringUtils.randomAlphanumeric(14));
        params.append("&content=").append(msg);
        String reqParams = params.toString();
        System.out.println(reqParams);
        String result = URLHttpUtils.post(API_URL, reqParams);
        System.out.println(result);
        return "0".equals(result);
    }

    /**
     * 注册成功短信
     *
     * @param phone
     * @return
     */
    public static boolean register_success(String phone) {
        String msg = String.format(MsgTemplate.REGISTER_SUCCESS);
        StringBuffer params = new StringBuffer();
        params.append("userID=").append(userID);
        params.append("&cpKey=").append(cpKey);
        params.append("&uPhone=").append(phone);
        params.append("&cpSubID=").append(RandomStringUtils.randomAlphanumeric(14));
        params.append("&content=").append(msg);
        String reqParams = params.toString();
        System.out.println(reqParams);
        String result = URLHttpUtils.post(API_URL, reqParams);
        System.out.println(result);
        return "0".equals(result);
    }

    /**
     * 找回密码
     *
     * @param phone
     * @param code
     * @param timeout
     * @return
     */
    public static boolean reset_password(String phone, String code, String timeout) {
        String msg = String.format(MsgTemplate.RESET_PASSWORD, code, timeout);
        StringBuffer params = new StringBuffer();
        params.append("userID=").append(userID);
        params.append("&cpKey=").append(cpKey);
        params.append("&uPhone=").append(phone);
        params.append("&cpSubID=").append(RandomStringUtils.randomAlphanumeric(14));
        params.append("&content=").append(msg);
        String reqParams = params.toString();
        System.out.println(reqParams);
        String result = URLHttpUtils.post(API_URL, reqParams);
        System.out.println(result);
        return "0".equals(result);
    }
}
