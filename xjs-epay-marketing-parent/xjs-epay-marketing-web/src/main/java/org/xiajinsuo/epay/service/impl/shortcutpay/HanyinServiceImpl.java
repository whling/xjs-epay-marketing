package org.xiajinsuo.epay.service.impl.shortcutpay;

import io.bestpay.framework.base.CodeException;
import io.bestpay.framework.base.UserRequest;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.xiajinsuo.epay.business.Card;
import org.xiajinsuo.epay.business.Trans;
import org.xiajinsuo.epay.business.TransChannel;
import org.xiajinsuo.epay.business.TransSubChannel;
import org.xiajinsuo.epay.security.User;
import org.xiajinsuo.epay.security.UserThirdpart;
import org.xiajinsuo.epay.security.UserType;
import org.xiajinsuo.epay.service.ShortcutpayServiceApi;
import org.xiajinsuo.epay.util.*;
import org.xiajinsuo.epay.util.mypays.shortcutpay.ApiUtil;
import org.xiajinsuo.epay.util.mypays.shortcutpay.OperFlagEnum;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.Date;

import static org.xiajinsuo.epay.util.TransUtil.*;

/**
 * Created by Administrator on 2017/5/24.
 * 瀚银支付
 */
@Component
public class HanyinServiceImpl implements ShortcutpayServiceApi {
    private Logger LOGGER = LoggerFactory.getLogger(HanyinServiceImpl.class);
    private final static String TRANS_TYPE = TransChannelType.SHORTCUTPAY.getValue();
    private final static String CHANNEL = TransTypeEnum.SHORTCUTPAY_HANYIN.getValue();
    @javax.annotation.Resource
    private org.xiajinsuo.epay.business.protocol.rpc.CardRpcProtocol cardRpcProtocol;
    @javax.annotation.Resource
    private org.xiajinsuo.epay.security.protocol.rpc.UserThirdpartRpcProtocol userThirdpartRpcProtocol;
    @javax.annotation.Resource
    private org.xiajinsuo.epay.business.protocol.rpc.TransRpcProtocol transRpcProtocol;

    @Override
    public boolean support(String transType, String channel) {
        return TRANS_TYPE.equalsIgnoreCase(transType) && CHANNEL.equalsIgnoreCase(channel);
    }

    @Override
    public RespPayResult execute(UserRequest userRequest, TransChannel transChannel,
                                 TransSubChannel transSubChannel, User loginUser,
                                 HttpServletRequest req, String transAmtYuan,
                                 String cardId,
                                 boolean payOthers,
                                 String payOthersRate,
                                 String memo,
                                 User invitationUser) throws CodeException {
        Integer payLimitMin = transSubChannel.getPayLimitMin();
        Integer payLimitMax = transSubChannel.getPayLimitMax();
        int _amt = Integer.parseInt(transAmtYuan);
        if (StringUtils.isEmpty(transAmtYuan) || _amt < payLimitMin) {
            return RespPayResult.fail("-1", "交易金额需大于等于" + payLimitMin + "元");
        }
        if (_amt > payLimitMax) {
            return RespPayResult.fail("-1", "交易金额需小于" + payLimitMax + "元");
        }
        final String _transAmtFen = transAmtYuan + "00";
        String clientTransId = DateUtils.generateRundomDate(6);
        String transTime = DateUtils.formatDateTime(new Date());
        String familyName = loginUser.getFamilyName();
        String loginUserId = loginUser.getId();
        Card cardDefault = cardRpcProtocol.getDefaultByUserId(loginUserId);
        //渠道未先认证
        String transTypeCode = transChannel.getTransTypeCode();
        String hpMerCode = userThirdpartRpcProtocol.findByTransType(loginUserId, TRANS_TYPE, transTypeCode);
        if (StringUtils.isEmpty(hpMerCode)) {
            org.xiajinsuo.epay.util.mypays.shortcutpay.RespResult registerResult = ApiUtil.register(CHANNEL, loginUser, transChannel, cardDefault, OperFlagEnum.A, payOthers, payOthersRate);
            try {
                Thread.sleep(1500); //入网状态审核需要时间
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (!registerResult.isSuccess()) {
                return RespPayResult.fail(registerResult.getRespCode(), registerResult.getRespMsg());
            } else {
                hpMerCode = registerResult.getData();
                UserThirdpart thirdpart = UserThirdpart.newBuilder()
                        .setUserId(loginUserId)
                        .setTransType(TRANS_TYPE)
                        .setTransTypeCode(transTypeCode)
                        .setMchCode(hpMerCode)
                        .build();
                userThirdpartRpcProtocol.create(thirdpart, userRequest);
            }
        }
        Card cardPay = cardRpcProtocol.get(cardId, userRequest);

        // 设置订单交易扣率 默认为 交易渠道TO费率
        String rate = transChannel.getRateT0();
        // 高签费率差值
        String diffRate = "";
        if (payOthers) {
            // 如果交易订单为让他人收款，则覆盖为自定义费率
            rate = payOthersRate;

            // 计算用户自定义费率比通道原始费率高出的值, 必须大于等于0
            diffRate = TransUtil.calculateRateDiff(payOthersRate, transChannel.getRateT0());
            if (new BigDecimal(diffRate).compareTo(new BigDecimal("0")) < 0) {
                return RespPayResult.fail("-1", "费率异常，请重试！");
            }
        }

        int counterFee = transChannel.getCounterFeeT0();
        int operationFee = calculateOperationFee(rate, _transAmtFen);
        int payAmount = calculatePayAmount(_transAmtFen, operationFee, counterFee);
        org.xiajinsuo.epay.util.mypays.shortcutpay.RespResult respResult = ApiUtil.pay(clientTransId, hpMerCode, transSubChannel, cardDefault, cardPay, _transAmtFen, rate, counterFee + "", operationFee + "", payAmount + "", payOthers);
        if (respResult.isSuccess()) {
            String pageContent = respResult.getData();
            try {
                String codeUrl = generatorPage(req, pageContent, clientTransId);
                Trans trans = generalTransRecord(TRANS_TYPE, CHANNEL, loginUser, _transAmtFen, clientTransId, transTime, cardDefault,
                        rate, counterFee, operationFee, payAmount, codeUrl, cardPay.getBankNo(), transTypeCode, hpMerCode, payOthers, diffRate, memo);

                // 向他人收款
                if (payOthers) {
                    // 设置订单机构信息
                    String iOrgName = invitationUser.getOrgName();
                    String iOrgCode = invitationUser.getOrgCode();

                    // 设置订单代理人信息
                    String iAgent = invitationUser.getId();
                    String iAgentFamilyName = invitationUser.getFamilyName();
                    if (UserType.USER == invitationUser.getType()) {
                        iAgent = invitationUser.getAgent();
                        iAgentFamilyName = invitationUser.getAgentFamilyName();
                    }

                    // 设置订单推荐人信息
                    String iReferee = invitationUser.getId();
                    String iRefereeFamilyName = invitationUser.getFamilyName();

                    // 更新订单
                    trans = Trans.newBuilder(trans)
                            .setOrgName(iOrgName)
                            .setOrgCode(iOrgCode)
                            .setAgent(iAgent)
                            .setAgentFamilyName(iAgentFamilyName)
                            .setReferee(iReferee)
                            .setRefereeFamilyName(iRefereeFamilyName)
                            .build();
                }

                transRpcProtocol.create(trans, userRequest);
                return RespPayResult.success(codeUrl, _transAmtFen, familyName);
            } catch (Exception e) {
                LOGGER.error("生成快捷支付页面失败", e);
                return RespPayResult.fail("-1", "生成快捷支付页面失败");
            }
        } else {
            return RespPayResult.fail(respResult.getRespCode(), respResult.getRespMsg());
        }
    }
}
