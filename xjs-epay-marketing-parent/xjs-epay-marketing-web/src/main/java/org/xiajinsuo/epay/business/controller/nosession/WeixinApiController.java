package org.xiajinsuo.epay.business.controller.nosession;

import com.alibaba.fastjson.JSONObject;
import io.bestpay.framework.base.CodeException;
import io.bestpay.framework.base.Response;
import io.bestpay.framework.request.UserHttpRequest;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.xiajinsuo.epay.business.Poster;
import org.xiajinsuo.epay.cache.CacheUtils;
import org.xiajinsuo.epay.exception.OrgNotFoundException;
import org.xiajinsuo.epay.exception.RefereeNotFoundException;
import org.xiajinsuo.epay.security.Organization;
import org.xiajinsuo.epay.security.User;
import org.xiajinsuo.epay.util.DateUtils;
import org.xiajinsuo.epay.util.HttpUtils;
import org.xiajinsuo.epay.util.LoginUtil;
import org.xiajinsuo.epay.util.SysConfigUtils;

import javax.servlet.http.HttpServletRequest;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;


/**
 * Created by Administrator on 2017/4/27.
 */
@SuppressWarnings("all")
/** 微信api RequestBody Controller */
@org.apache.avro.specific.AvroGenerated
@Controller
@RequestMapping("/weixinApi")
public class WeixinApiController {
    final Logger LOGGER = LoggerFactory.getLogger(WeixinApiController.class);
    @javax.annotation.Resource
    private org.xiajinsuo.epay.security.protocol.rpc.UserRpcProtocol userRpcProtocol;
    @javax.annotation.Resource
    private org.xiajinsuo.epay.business.protocol.rpc.PosterRpcProtocol posterRpcProtocol;
    @javax.annotation.Resource
    private CacheUtils cacheUtils;

    /**
     * code
     *
     * @param parameter
     * @return
     */
    @RequestMapping("/code")
    @ResponseBody
    public String code(String signature, String timestamp, String nonce, String echostr, final HttpServletRequest request) {
        // 通过检验signature对请求进行校验，若校验成功则原样返回echostr，表示接入成功，否则接入失败
        if (SignUtil.checkSignature(signature, timestamp, nonce)) {
            return echostr;
        }
        return "fail";
    }

    /**
     * login
     *
     * @param parameter
     * @return
     */
    @RequestMapping("/loadWsConfig")
    @ResponseBody
    public Response<WsJsConfig> loadWsConfig(String reqUrl, String userId, final HttpServletRequest request) throws CodeException {
        UserHttpRequest userHttpRequest = new UserHttpRequest();
        userHttpRequest.setPrincipal("SYS");
        User user = userRpcProtocol.get(userId, userHttpRequest);
        if (user == null) {
            throw new RefereeNotFoundException();
        }
        Organization organization = cacheUtils.getOrganization(user.getOrgCode());
        if (organization == null) {
            throw new OrgNotFoundException();
        }
        String appid = organization.getWeixinAppId();
        String secret = organization.getWeixinSecret();
        String jsapi_ticket = cacheUtils.getJsapiTicket(appid, secret);
        if (StringUtils.isEmpty(jsapi_ticket)) {
            return Response.failure("jsapi_ticket is null");
        }
        String timestamp = System.currentTimeMillis() / 1000 + "";
        String nonceStr = RandomStringUtils.randomAlphanumeric(16);
        String signature = SignUtil.signatureWs(jsapi_ticket, timestamp, nonceStr, reqUrl);
        LOGGER.info("jsapi_ticket={},timestamp={},nonceStr={},reqUrl=[{}]", jsapi_ticket, timestamp, nonceStr, reqUrl, signature);
        if (StringUtils.isNotEmpty(signature)) {
            return Response.success(new WsJsConfig(appid, timestamp, nonceStr, signature));
        } else {
            return Response.failure("signature is null");
        }
    }

    /**
     * 根据用户或者机构编号获取机构信息
     *
     * @param orgCode
     * @param userId
     * @param request
     * @return
     * @throws CodeException
     */
    @RequestMapping("/user2org")
    @ResponseBody
    public Response<Map<String, Object>> user2org(final String orgCode, final String userId, final HttpServletRequest request) throws CodeException {
        Organization organization = null;
        if (StringUtils.isNotEmpty(orgCode)) {
            organization = cacheUtils.getOrganization(orgCode);
        } else if (StringUtils.isNotEmpty(userId)) {
            UserHttpRequest userHttpRequest = new UserHttpRequest();
            userHttpRequest.setPrincipal("SYS");
            User user = userRpcProtocol.get(userId, userHttpRequest);
            if (user == null) {
                return Response.failure("邀请用户不存在");
            }
            organization = cacheUtils.getOrganization(user.getOrgCode());
        }
        if (organization == null) {
            return Response.failure("机构不存在");
        }
        List<Poster> posters = posterRpcProtocol.findByOrgCode(organization.getCode());
        User orgRoot = userRpcProtocol.findOrgRoot(organization.getCode());
        Map<String, Object> result = new HashMap<>();
        result.put("org", organization);
        result.put("referee", orgRoot.getId());
        result.put("posters", posters);
        return Response.success(result);
    }

    /**
     * login
     *
     * @param parameter
     * @return
     */
    @RequestMapping("/login/{orgCode}")
    public String login(@PathVariable String orgCode, final HttpServletRequest request) {
        Organization organization = cacheUtils.getOrganization(orgCode);
        String appid = organization.getWeixinAppId();
        String redirect_uri = organization.getWeixinRedirectUri();
        return "redirect:" + String.format(SysConfigUtils.getWeixin_api_code_url(), appid, redirect_uri);
    }

    /**
     * auth2
     *
     * @param parameter
     * @return
     */
    @RequestMapping("/auth2/{orgCode}")
    public String auth2(RedirectAttributes attributes, @PathVariable String orgCode, String code, String state, final HttpServletRequest request) {
        attributes.addAttribute("id", orgCode + "_");
        Organization organization = cacheUtils.getOrganization(orgCode);
        if (StringUtils.isEmpty(code)) {
            return "redirect:/login.html";
        }
        String appId = organization.getWeixinAppId();
        String secret = organization.getWeixinSecret();
        JSONObject respJson = HttpUtils.doGetHttps(String.format(SysConfigUtils.getWeixin_api_openid_url(), appId, secret, code));
        if (respJson == null) {
            return "redirect:/login.html";
        }
        String openid = respJson.getString("openid");
        if (StringUtils.isEmpty(openid)) {
            return "redirect:/login.html";
        }
        User user = userRpcProtocol.findOneByOpendId(openid);
        if (user != null) {//用户已经存在,自动登录
            LoginUtil.setLoginUserOrg(request, organization);
            LoginUtil.setLoginUser(request, user);
            userRpcProtocol.updateLoginIpTimeOpenId(user.getId(), LoginUtil.getRemoteHost(request), openid, DateUtils.formatDateTime(new Date()));
            return "redirect:/page/home.html";
        } else {
            LoginUtil.setLoginOpenid(request, openid);
            return "redirect:/login.html";
        }
    }
}


class SignUtil {
    // 与接口配置信息中的Token要一致
    private static final String TOKEN = "weixin_mypays_tiepai";

    /**
     * 验证签名
     *
     * @param signature
     * @param timestamp
     * @param nonce
     * @return
     */
    public static boolean checkSignature(String signature, String timestamp, String nonce) {
        String[] arr = new String[]{TOKEN, timestamp, nonce};
        // 将token、timestamp、nonce三个参数进行字典序排序
        Arrays.sort(arr);
        StringBuilder content = new StringBuilder();
        for (int i = 0; i < arr.length; i++) {
            content.append(arr[i]);
        }
        MessageDigest md = null;
        String tmpStr = null;
        try {
            md = MessageDigest.getInstance("SHA-1");
            // 将三个参数字符串拼接成一个字符串进行sha1加密
            byte[] digest = md.digest(content.toString().getBytes());
            tmpStr = byteToStr(digest);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        // 将sha1加密后的字符串可与signature对比，标识该请求来源于微信
        return tmpStr != null && tmpStr.equals(signature.toUpperCase());
    }

    /**
     * 生成jsapi签名
     *
     * @param jsapi_ticket
     * @param timestamp
     * @param nonce
     * @param url
     * @return
     */
    public static String signatureWs(String jsapi_ticket, String timestamp, String nonce, String url) {
        String[] arr = new String[]{"jsapi_ticket=" + jsapi_ticket, "timestamp=" + timestamp, "noncestr=" + nonce, "url=" + url};
        Arrays.sort(arr);
        StringBuilder content = new StringBuilder();
        content.append(arr[0]).append("&");
        content.append(arr[1]).append("&");
        content.append(arr[2]).append("&");
        content.append(arr[3]);
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-1");
            byte[] digest = md.digest(content.toString().getBytes());
            return byteToStr(digest).toLowerCase();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 将字节数组转换为十六进制字符串
     *
     * @param byteArray
     * @return
     */
    private static String byteToStr(byte[] byteArray) {
        String strDigest = "";
        for (int i = 0; i < byteArray.length; i++) {
            strDigest += byteToHexStr(byteArray[i]);
        }
        return strDigest;
    }

    /**
     * 将字节转换为十六进制字符串
     *
     * @param mByte
     * @return
     */
    private static String byteToHexStr(byte mByte) {
        char[] Digit = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        char[] tempArr = new char[2];
        tempArr[0] = Digit[(mByte >>> 4) & 0X0F];
        tempArr[1] = Digit[mByte & 0X0F];
        return new String(tempArr);
    }

}

/**
 * 微信js接口配置参数
 */
class WsJsConfig {
    private String appid;
    private String timestamp;
    private String noncestr;
    private String signature;

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getNoncestr() {
        return noncestr;
    }

    public void setNoncestr(String noncestr) {
        this.noncestr = noncestr;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }


    public WsJsConfig(String appid, String timestamp, String noncestr, String signature) {
        this.appid = appid;
        this.timestamp = timestamp;
        this.noncestr = noncestr;
        this.signature = signature;
    }
}

