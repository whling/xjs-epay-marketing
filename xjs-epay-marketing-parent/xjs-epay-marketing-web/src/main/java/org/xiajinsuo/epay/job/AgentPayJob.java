package org.xiajinsuo.epay.job;

import com.dangdang.ddframe.job.api.ShardingContext;
import com.dangdang.ddframe.job.api.simple.SimpleJob;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.xiajinsuo.epay.business.ClearingStatus;
import org.xiajinsuo.epay.business.TransClearing;
import org.xiajinsuo.epay.util.DateUtils;
import org.xiajinsuo.epay.util.mypays.agent.AgentApiUtil;
import org.xiajinsuo.epay.util.mypays.agent.RespResult;

import java.util.Calendar;
import java.util.List;

/**
 * Created by Administrator on 2016/11/29.
 * mypays代付
 */
@Component
public class AgentPayJob implements SimpleJob {
    private static Logger LOGGER = LoggerFactory.getLogger(AgentPayJob.class);
    /**
     * 当前状态-未清算
     */
    private final static ClearingStatus CURRENT_STATUS = ClearingStatus.CLEARING_ING;
    @javax.annotation.Resource
    private org.xiajinsuo.epay.business.protocol.rpc.TransClearingRpcProtocol transClearingRpcProtocol;

    /**
     * 代付结果查询
     *
     * @param trans
     * @throws Exception
     */
    private void execute(TransClearing trans) throws Exception {
        if (trans == null) {
            return;
        }
        String clientTransId = trans.getClientTransId();
        try {
            //锁定当前状态为已经清算
            boolean updateClearingStatus = transClearingRpcProtocol.updateClearingStatus(clientTransId, ClearingStatus.CLEARING_END, CURRENT_STATUS, "", "清算中");
            if (updateClearingStatus) {
                RespResult respResult = AgentApiUtil.pay(trans);
                String errorCode = respResult.getRespCode();
                String errorMsg = respResult.getRespMsg();
                if (respResult.isSuccess()) {
                    if ("PAY_SUCCESS".equalsIgnoreCase(errorCode)) {
                        transClearingRpcProtocol.updateClearingStatus(clientTransId, ClearingStatus.CLEARING_SUCCESS, ClearingStatus.CLEARING_END,
                                StringUtils.isNotEmpty(errorCode) ? errorCode : "", StringUtils.isNotEmpty(errorMsg) ? errorMsg : "");
                    } else if ("PAY_FAILURE".equalsIgnoreCase(errorCode)) {
                        transClearingRpcProtocol.updateClearingStatus(clientTransId, ClearingStatus.CLEARING_FAILURE, ClearingStatus.CLEARING_END,
                                StringUtils.isNotEmpty(errorCode) ? errorCode : "", StringUtils.isNotEmpty(errorMsg) ? errorMsg : "");
                    }
                } else if (!respResult.isSuccess()) {
                    transClearingRpcProtocol.updateClearingStatus(clientTransId, ClearingStatus.CLEARING_FAILURE, ClearingStatus.CLEARING_END, errorCode, errorMsg);
                }
            } else {
                LOGGER.error("clientTransId=[{}],单笔代付失败,清算记录更新失败", clientTransId);
            }
        } catch (Exception e) {
            LOGGER.error("clientTransId=[{}],单笔代付失败{}", clientTransId, e);
        }
    }

    /**
     * 5分钟之前的交易记录
     *
     * @return
     */

    private String generateTransTimeEnd() {
        Calendar today = Calendar.getInstance();
        today.add(Calendar.MINUTE, -5);
        return DateUtils.formatDateTime(today.getTime());
    }

    /**
     * 7天有效期
     *
     * @return
     */
    private String generateTransTimeStar() {
        Calendar today = Calendar.getInstance();
        today.add(Calendar.DAY_OF_MONTH, -7);
        return DateUtils.formatDateTime(today.getTime());
    }

    @Override
    public void execute(ShardingContext shardingContext) {
        String transTimeStart = generateTransTimeStar();
        String transTimeEnd = generateTransTimeEnd();
        LOGGER.info("单笔代付开始,transTimeStart={},transTimeEnd={}", transTimeStart, transTimeEnd);
        try {
            List<TransClearing> transList = transClearingRpcProtocol.findByClearingStatus(CURRENT_STATUS, transTimeStart, transTimeEnd);
            if (CollectionUtils.isEmpty(transList)) {
                LOGGER.info("TransClearing is null");
                return;
            }
            for (int i = 0; i < transList.size(); i++) {
                TransClearing trans = transList.get(i);
                try {
                    execute(trans);
                    Thread.sleep(3000);
                } catch (Exception e) {
                    LOGGER.error("trans[{}],async execute query request fail,error=[{}]", trans, e);
                }
            }
        } catch (Exception e) {
            LOGGER.error("process fail,{}", e);
        }
        LOGGER.info("单笔代付结束");
    }
}
