package org.xiajinsuo.epay.util;

/**
 * Created by Administrator on 2017/6/20.
 */
public class BankUtil {
    public static String filterName(String bankName) {
        if (bankName.contains("中国") && !bankName.contains("中国银行")) {
            bankName = bankName.replaceAll("中国", "");
        }
        bankName = bankName.replaceAll("股份有限公司", "");
        bankName = bankName.replaceAll("有限公司", "");
        if (bankName.contains("工商银行")) {
            bankName = "工商银行";
        } else if (bankName.contains("建设银行")) {
            bankName = "建设银行";
        } else if (bankName.contains("交通银行")) {
            bankName = "交通银行";
        } else if (bankName.contains("中国银行")) {
            bankName = "中国银行";
        } else if (bankName.contains("青海银行")) {
            bankName = "青海银行";
        } else if (bankName.contains("邮储银行")) {
            bankName = "邮政储蓄银行";
        } else if (bankName.contains("邮政储蓄银行")) {
            bankName = "邮政储蓄银行";
        } else if (bankName.contains("兴业银行")) {
            bankName = "兴业银行";
        } else if (bankName.contains("平安银行")) {
            bankName = "平安银行";
        } else if (bankName.contains("上海银行")) {
            bankName = "上海银行";
        } else if (bankName.contains("民生银行")) {
            bankName = "民生银行";
        } else if (bankName.contains("光大银行")) {
            bankName = "光大银行";
        } else if (bankName.contains("恒生银行")) {
            bankName = "恒生银行";
        } else if (bankName.contains("浦东发展银行")) {
            bankName = "浦东发展银行";
        } else if (bankName.contains("华夏银行")) {
            bankName = "华夏银行";
        } else if (bankName.contains("农业银行")) {
            bankName = "农业银行";
        } else if (bankName.contains("宁波银行")) {
            bankName = "宁波银行";
        } else if (bankName.contains("招商银行")) {
            bankName = "招商银行";
        }
        return bankName;
    }
}
