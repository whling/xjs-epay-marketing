package org.xiajinsuo.epay.util;

import com.alibaba.fastjson.JSONObject;
import junit.framework.TestCase;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by tums on 2016/8/29.
 */
public class HttpClientApiTest extends TestCase {
    public void testY() throws ParseException {
        System.out.println(DateUtils.currentMonth(0));
        System.out.println(DateUtils.monthMinDay());
        System.out.println(DateUtils.monthMaxDay());

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat formatOut = new SimpleDateFormat("yyyy-MM-dd");

        System.out.println(formatOut.format(dateFormat.parse(DateUtils.currentMonth(0))));
    }
    public void testX() throws IOException {
        String clientId = "20170717455sadsad5";
//        String clientId = "20170714212471";
//        String clientId = "20170714950051204211";
        JSONObject data = new JSONObject();
        data.put("out_trade_no", clientId);
        data.put("wallet_no", clientId);
        data.put("resp_code", "SUCCESS");
        data.put("resp_msg", "支付成功");
        try {
//            HttpPost httpPost = new HttpPost("http://125.77.23.87:9112/trans/callback/shortcutpay/shortcutpay_ext4/" + clientId);
            HttpPost httpPost = new HttpPost("http://localhost:8895/trans/callback/shortcutpay/shortcutpay_ext5/" + clientId);
            StringEntity stringEntity = new StringEntity(data.toString());
//            StringEntity stringEntity = new StringEntity(data.toString(), Charset.forName("utf-8"));
            stringEntity.setContentEncoding("UTF-8");
            stringEntity.setContentType("application/json");
            httpPost.setEntity(stringEntity);
            CloseableHttpClient client = HttpClients.createDefault();
            CloseableHttpResponse response = client.execute(httpPost);
            Header[] headers = response.getAllHeaders();
            for (int i = 0; i < headers.length; i++) {
                System.out.println("Response header : " + headers[i].getName() + ":" + headers[i].getValue());
            }
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                System.out.println("Response content length: " + entity.getContentLength());
                System.out.println("Response content: " + EntityUtils.toString(entity));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

