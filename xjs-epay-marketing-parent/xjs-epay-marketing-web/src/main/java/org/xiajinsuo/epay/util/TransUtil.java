package org.xiajinsuo.epay.util;

import org.xiajinsuo.epay.exception.OtherErrorException;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by Administrator on 2017/5/24.
 */
public class TransUtil {
    public static final String DEFAULT_VERSION = "1.0";
    public static final String DEFAULT_MEMO = "Marketing";

    public static String getBankNo(String bankNo) {
        String start = bankNo.substring(0, 4);
        String end = bankNo.substring(bankNo.length() - 4);
        return start + "****" + end;
    }

    /**
     * 交易流水id
     *
     * @return
     */
    public static String generatorClientTransId() {
        return DateUtils.generateRundomDate(12);
    }

    /**
     * 计算T+1 清算时间 20170301094132
     *
     * @param transTime
     * @return
     */
    public static String addT1(String transTime) {
        String year = transTime.substring(0, 4);
        String month = transTime.substring(4, 6);
        String day = transTime.substring(6, 8);
        String hour_minute_seconds = transTime.substring(8);
        Calendar calendar = new GregorianCalendar(Integer.parseInt(year), Integer.parseInt(month) - 1, Integer.parseInt(day));
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        int m = calendar.get(Calendar.MONTH) + 1;
        int d = calendar.get(Calendar.DAY_OF_MONTH);
        int y = calendar.get(Calendar.YEAR);
        month = m < 10 ? "0" + m : m + "";
        day = d < 10 ? "0" + d : d + "";
        return y + month + day + hour_minute_seconds;
    }

    /**
     * 计算手续费:交易金额*费率
     *
     * @param rate
     * @param transAmt
     * @return
     */
    public static int calculateOperationFee(String rate, String transAmt) {
//        double _rate = Double.parseDouble(rate);
//        int _transAmt = Integer.parseInt(transAmt);
//        return (int) Math.ceil(_transAmt * _rate / 100);
        double _rate = ArithUtil.divide(Double.parseDouble(rate), 100D, 4, BigDecimal.ROUND_FLOOR);
        int _transAmt = Integer.parseInt(transAmt);
        return (int) Math.ceil(ArithUtil.multiply(_transAmt, _rate));
    }

    /**
     * 到账金额:交易金额-手续费-操作手续费
     *
     * @param transAmt
     * @param operationFee
     * @param counterFee
     * @return
     */
    public static int calculatePayAmount(String transAmt, int operationFee, int counterFee) {
        int _transAmt = Integer.parseInt(transAmt);
        double clearing = _transAmt - operationFee - counterFee;
        return (int) Math.floor(clearing);
    }

    /**
     * 高签费率差
     *
     * @param payOthersRate
     * @param rate
     * @return
     */
    public static String calculateRateDiff(String payOthersRate, String rate) {
        return ArithUtil.round_floor(ArithUtil.subtract(payOthersRate, rate), 2) + "";
    }

    /**
     * 生成快捷支付页面
     *
     * @param request
     * @param pageContent
     * @param clientTransId
     * @throws IOException
     */
    public static String generatorPage(HttpServletRequest request, String pageContent, String clientTransId) throws IOException {
        String shortcutpay = "shortcutpay";
        String filename = clientTransId + ".html";
        String realPath = request.getSession().getServletContext().getRealPath(shortcutpay);
        File file = new File(realPath + "/" + filename);
        if (!file.exists()) {
            file.createNewFile();
        } else {
            throw new OtherErrorException("支付订单已经存在");
        }
        Writer fileWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "utf-8"));
        BufferedWriter writer = new BufferedWriter(fileWriter);
        writer.write(pageContent);
        writer.flush();
        writer.close();
        return shortcutpay + "/" + filename;
    }
}
