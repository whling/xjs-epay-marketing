package org.xiajinsuo.epay.util;

import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.data.redis.connection.RedisClusterConfiguration;
import org.springframework.data.redis.connection.RedisNode;
import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.JedisCluster;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by tums on 2017/8/19.
 */
public class JedisClusterFactory implements FactoryBean<JedisCluster>, InitializingBean {
    private JedisCluster jedisCluster;
    private Integer timeout;
    private GenericObjectPoolConfig genericObjectPoolConfig;
    private RedisClusterConfiguration clusterConfiguration;

    public JedisClusterFactory(RedisClusterConfiguration clusterConfiguration, Integer timeout, GenericObjectPoolConfig genericObjectPoolConfig) {
        this.clusterConfiguration = clusterConfiguration;
        this.timeout = timeout;
        this.genericObjectPoolConfig = genericObjectPoolConfig;
    }

    @Override
    public JedisCluster getObject() throws Exception {
        return jedisCluster;
    }

    @Override
    public Class<?> getObjectType() {
        return (this.jedisCluster != null ? this.jedisCluster.getClass() : JedisCluster.class);
    }

    @Override
    public boolean isSingleton() {
        return true;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        Set<HostAndPort> haps = this.parseHostAndPort();
        jedisCluster = new JedisCluster(haps, timeout, clusterConfiguration.getMaxRedirects(), genericObjectPoolConfig);
    }

    private Set<HostAndPort> parseHostAndPort() throws Exception {
        try {
            Set<RedisNode> clusterNodes = clusterConfiguration.getClusterNodes();
            Set<HostAndPort> haps = new HashSet<HostAndPort>();
            for (RedisNode node : clusterNodes) {
                HostAndPort hap = new HostAndPort(node.getHost(), node.getPort());
                haps.add(hap);
            }
            return haps;
        } catch (IllegalArgumentException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new Exception("解析 jedis 配置文件失败", ex);
        }
    }

    public JedisCluster getJedisCluster() {
        return jedisCluster;
    }

    public void setJedisCluster(JedisCluster jedisCluster) {
        this.jedisCluster = jedisCluster;
    }

    public Integer getTimeout() {
        return timeout;
    }

    public void setTimeout(Integer timeout) {
        this.timeout = timeout;
    }

    public GenericObjectPoolConfig getGenericObjectPoolConfig() {
        return genericObjectPoolConfig;
    }

    public void setGenericObjectPoolConfig(GenericObjectPoolConfig genericObjectPoolConfig) {
        this.genericObjectPoolConfig = genericObjectPoolConfig;
    }

    public RedisClusterConfiguration getClusterConfiguration() {
        return clusterConfiguration;
    }

    public void setClusterConfiguration(RedisClusterConfiguration clusterConfiguration) {
        this.clusterConfiguration = clusterConfiguration;
    }
}
