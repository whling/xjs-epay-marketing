/**
 * Autogenerated by bestpay-maven-plugin
 * DO NOT EDIT DIRECTLY
 *
 * @Copyright 2016 www.bestpay.io Inc. All rights reserved.
 */
package org.xiajinsuo.epay.business.controller;

import io.bestpay.framework.base.NameValue;
import io.bestpay.framework.base.Response;
import io.bestpay.framework.base.UserRequest;
import io.bestpay.framework.controller.AbstractController;
import io.bestpay.framework.controller.HttpRequestTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.xiajinsuo.epay.business.CityBankCode;
import org.xiajinsuo.epay.cache.CacheUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@SuppressWarnings("all")
/** 省市区 RequestBody Controller */
@org.apache.avro.specific.AvroGenerated
@Controller
@RequestMapping("/region")
public class RegionController extends AbstractController {

    @javax.annotation.Resource
    private io.bestpay.framework.api.protocol.RegionProtocol regionProtocol;
    @javax.annotation.Resource
    private CacheUtils cacheUtils;
    @javax.annotation.Resource
    private org.xiajinsuo.epay.business.protocol.rpc.CityBankCodeRpcProtocol bankCodeRpcProtocol;
    private final static int INDEX_CITY = 1;
    private final static int INDEX_PROVINCE = 0;
    private final static int INDEX_COUNTY = 2;


    /**
     * 查询
     *
     * @param index
     * @param parameter
     * @return
     */
    @RequestMapping("/query/{index}")
    @ResponseBody
    public Response<List<NameValue>> region(@PathVariable final int index, final @RequestBody Map<String, Object> parameter) {

        return this.doRequest(new HttpRequestTemplate<List<NameValue>>() {

            @Override
            public List<NameValue> doRequest(UserRequest userRequest, HttpServletRequest req, HttpServletResponse resp) throws Exception {
                String key = (String) parameter.get("key");
                List<NameValue> result;
                if (index == INDEX_CITY) {
                    result = regionProtocol.getCities(key);
                } else {
                    result = regionProtocol.getProvinces();
                }
                return result;
            }

            private List<NameValue> toNameValues(List<CityBankCode> bankCodes) {
                if (CollectionUtils.isEmpty(bankCodes)) {
                    return null;
                }
                int size = bankCodes.size();
                List<NameValue> result = new ArrayList<NameValue>(size);
                for (int i = 0; i < size; i++) {
                    CityBankCode bankCode = bankCodes.get(i);
                    String branchName = bankCode.getBranchName();
                    String branchCode = bankCode.getBranchCode();
                    result.add(NameValue.newBuilder().setName(branchName).setValue(branchCode).build());
                }
                return result;
            }

        });
    }

    /**
     * 获取支行
     *
     * @param parameter
     * @return
     */
    @RequestMapping("/cityBankCodes")
    @ResponseBody
    public Response<List<CityBankCode>> citybankcode(final @RequestBody Map<String, String> parameter) {

        return this.doRequest(new HttpRequestTemplate<List<CityBankCode>>() {

            @Override
            public List<CityBankCode> doRequest(UserRequest userRequest, HttpServletRequest req, HttpServletResponse resp) throws Exception {
                String cityName = parameter.get("cityName");
                String bankName = parameter.get("bankName");
                String keywords = parameter.get("keywords");
                return cacheUtils.getCityBankCodeList(bankName, cityName, keywords);
            }
        });
    }

}