package org.xiajinsuo.epay.util.mypays.shortcutpay;

import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xiajinsuo.epay.business.Card;
import org.xiajinsuo.epay.business.TransChannel;
import org.xiajinsuo.epay.business.TransSubChannel;
import org.xiajinsuo.epay.sdk.HttpUtils;
import org.xiajinsuo.epay.sdk.RRParams;
import org.xiajinsuo.epay.sdk.ResponseDataWrapper;
import org.xiajinsuo.epay.security.User;
import org.xiajinsuo.epay.util.ArithUtil;
import org.xiajinsuo.epay.util.DateUtils;
import org.xiajinsuo.epay.util.SysConfigUtils;
import org.xiajinsuo.epay.util.TransChannelType;
import org.xiajinsuo.epay.util.mypays.LhtRsaDataEncryptUtil;
import org.xiajinsuo.epay.util.mypays.TpRsaDataEncryptUtil;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static org.xiajinsuo.epay.util.TransUtil.DEFAULT_MEMO;

/**
 * Created by Administrator on 2017/6/13.
 * 快捷公共接口
 */
public class PubApiUtil {
    private static Logger LOGGER = LoggerFactory.getLogger(PubApiUtil.class);
    private final static String APP_ID = "3e75e88fd618465590376078d3685911";

    /**
     * 消费接口
     *
     * @param clientTransId
     * @param hpMerCode
     * @param transSubChannel
     * @param cardPay
     * @param _transAmtFen
     * @return
     */
    public static RespResult pay(String channel, String clientTransId, String hpMerCode, TransSubChannel transSubChannel, Card cardDefault, Card cardPay, String _transAmtFen, String rate, String counterFee, String operationFee, String payAmount, boolean payOthers) {
        String transTime = DateUtils.formatDateTime(new Date());
        String mypaysPayApiUrl = SysConfigUtils.getMypays_pay_api_url();
        String notifyUrl = transSubChannel.getNotifyUrl();
        String pageUrl = transSubChannel.getPageUrl();
        if (payOthers) {
            pageUrl = transSubChannel.getPayOthersPageUrl();
        }
        long transTimestamp = System.currentTimeMillis();
        String subTransType = transSubChannel.getSubTransType().toLowerCase();
        String notifyUrlFormat = String.format(notifyUrl, subTransType, clientTransId);
        String pageUrlFormat = String.format(pageUrl, subTransType, clientTransId);
        LOGGER.info("notifyUrl=[{}]", notifyUrlFormat);
        LOGGER.info("pageUrl=[{}]", pageUrlFormat);

        Map<String, String> businessReq = new HashMap();
        businessReq.put("mobile", cardPay.getMobile());
        businessReq.put("family_name", cardPay.getFamilyName());
        businessReq.put("id_card", cardPay.getIdcard());
        businessReq.put("pay_bank_no", cardPay.getBankNo());

        businessReq.put("trans_time", transTime);
        businessReq.put("trans_date", DateUtils.formatDate(new Date()));
        businessReq.put("trans_amount", _transAmtFen);
        businessReq.put("rate_t0", rate);
        businessReq.put("counter_fee_t0", counterFee);
        businessReq.put("operation_fee", operationFee);
        businessReq.put("pay_amount", payAmount);

        businessReq.put("payee_bank_name", cardDefault.getBankName());
        businessReq.put("payee_bank_no", cardDefault.getBankNo());
        businessReq.put("back_notify_url", notifyUrlFormat);
        businessReq.put("front_notify_url", pageUrlFormat);
        businessReq.put("memo", DEFAULT_MEMO);
        businessReq.put("method", "pay");
        businessReq.put("channel", channel);
        businessReq.put("third_merchant_code", hpMerCode);

        RRParams requestData = RRParams.newBuilder()
                .setAppId(APP_ID)
                .setClientTransId(clientTransId)
                .setTransTimestamp(transTimestamp)
                .setTransType(TransChannelType.SHORTCUTPAY.getValue())
                .build();
        ResponseDataWrapper rdw = HttpUtils.post(mypaysPayApiUrl, requestData, businessReq, LhtRsaDataEncryptUtil.rsaDataEncryptPri, LhtRsaDataEncryptUtil.rsaDataEncryptPub);
        if (rdw.getRespCode().equals("000000")) {
            Map<String, String> responseData = rdw.getResponseData();
            return RespResult.success(rdw.getRespCode(), rdw.getRespMsg(), responseData.get("page_content"));
        } else {
            return RespResult.fail(rdw.getRespCode(), rdw.getRespMsg());
        }
    }

    /**
     * 入网接口
     *
     * @param channel
     * @param loginUser
     * @param transChannel
     * @param card
     * @param operFlag
     * @return
     */
    public static RespResult register(String channel, User loginUser, TransChannel transChannel, Card card, OperFlagEnum operFlag, boolean payOthers, String payOthersRate) {
        String clientTransId = DateUtils.generateRundomDate(6);
        String transTime = DateUtils.formatDateTime(new Date());
        String mypaysPayApiUrl = SysConfigUtils.getMypays_pay_api_url();
        long transTimestamp = System.currentTimeMillis();
        String familyName = loginUser.getFamilyName();
        String mobile = card.getMobile();
        String merCode = loginUser.getThirdpart() + transChannel.getTransTypeCode();
        String merName = "TP" + RandomStringUtils.randomAlphabetic(2) + familyName;

        Map<String, String> businessReq = new HashMap();
        businessReq.put("mobile", mobile);
        businessReq.put("trans_time", transTime);
        businessReq.put("family_name", familyName);
        businessReq.put("id_card", loginUser.getIdcard());
        businessReq.put("memo", DEFAULT_MEMO);
        businessReq.put("merchant_name", merName);
        businessReq.put("merchant_code", merCode);
        businessReq.put("merchant_province", card.getAccProvince());
        businessReq.put("merchant_city", card.getAccCity());
        businessReq.put("payee_bank_id", card.getBankId());
        businessReq.put("payee_bank_no", card.getBankNo());
        businessReq.put("payee_bank_name", card.getBankName());
        businessReq.put("payee_branch_name", card.getBranchName());
        businessReq.put("payee_branch_code", card.getBranchCode());
        businessReq.put("payee_bank_province", card.getAccProvince());
        businessReq.put("payee_bank_city", card.getAccCity());
        businessReq.put("merchant_address", card.getBranchName());
        String rateT0 = transChannel.getRateT0();
        if (payOthers) {
            rateT0 = payOthersRate;
        }
        businessReq.put("rate_t0", str2double(rateT0));
        businessReq.put("counter_fee_t0", transChannel.getCounterFeeT0() + "");
        businessReq.put("rate_t1", "0");
        businessReq.put("counter_fee_t1", "0");
        businessReq.put("merchant_oper_flag", operFlag.getValue());
        businessReq.put("channel", channel);
        businessReq.put("method", "register");

        RRParams requestData = RRParams.newBuilder()
                .setAppId(APP_ID)
                .setClientTransId(clientTransId)
                .setTransTimestamp(transTimestamp)
                .setTransType(TransChannelType.SHORTCUTPAY.getValue())
                .build();
        ResponseDataWrapper rdw = HttpUtils.post(mypaysPayApiUrl, requestData, businessReq, LhtRsaDataEncryptUtil.rsaDataEncryptPri, LhtRsaDataEncryptUtil.rsaDataEncryptPub);
        String respCode = rdw.getRespCode();
        String respMsg = rdw.getRespMsg();
        if (respCode.equals("000000")) {
            Map responseData = rdw.getResponseData();
            String thirdpartyMerchantCode = (String) responseData.get("third_merchant_code");
            return RespResult.success("", "", thirdpartyMerchantCode);
        } else {
            return RespResult.fail(respCode, respMsg);
        }
    }

    /**
     * 费率格式化,保留两位小数
     *
     * @param value
     * @return
     */
    private static String str2double(String value) {
        return ArithUtil.round_floor(new BigDecimal(value).doubleValue(), 2) + "";
    }
}
