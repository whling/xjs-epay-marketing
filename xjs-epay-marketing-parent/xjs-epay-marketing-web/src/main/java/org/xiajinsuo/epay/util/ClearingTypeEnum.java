package org.xiajinsuo.epay.util;

/**
 * 到账类型（清算类型）
 */
public enum ClearingTypeEnum {
    T0("0", "实时到账"), T1("1", "次日到账");

    ClearingTypeEnum(String value, String name) {
        this.value = value;
        this.name = name;
    }

    private String value;
    private String name;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
