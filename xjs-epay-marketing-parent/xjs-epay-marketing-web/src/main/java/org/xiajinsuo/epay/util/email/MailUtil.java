package org.xiajinsuo.epay.util.email;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.mail.*;
import javax.mail.Message.RecipientType;
import javax.mail.internet.*;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

/**
 * 邮箱工具类
 */
@Component
public final class MailUtil {
    private static String host;
    private static String username;
    private static String password;
    private static String from;

    @Value("#{applicationCfg['email.host']}")
    public void setHost(String host) {
        MailUtil.host = host;
    }


    @Value("#{applicationCfg['email.username']}")
    public void setUsername(String username) {
        MailUtil.username = username;
    }


    @Value("#{applicationCfg['email.password']}")
    public void setPassword(String password) {
        MailUtil.password = password;
    }

    @Value("#{applicationCfg['email.from']}")
    public void setFrom(String from) {
        MailUtil.from = from;
    }

    /**
     * 创建Session
     *
     * @return
     */
    private static Session createSession() {
        Properties prop = new Properties();
        prop.setProperty("mail.host", host);        // 指定主机
        prop.setProperty("mail.smtp.auth", "true"); // 指定验证为true
        // 创建验证器
        Authenticator auth = new Authenticator() {
            public PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        };
        // 获取session对象
        return Session.getInstance(prop, auth);
    }

    /**
     * 发送邮件
     *
     * @param email
     * @throws MessagingException
     * @throws IOException
     */
    public static void send(final Email email) throws MessagingException,
            IOException {
        MimeMessage msg = new MimeMessage(createSession());// 创建邮件对象
        msg.setFrom(new InternetAddress(from));// 设置发件人
        msg.addRecipients(RecipientType.TO, email.getToAddress());// 设置收件人
        // 设置抄送
        String cc = email.getCcAddress();
        if (!cc.isEmpty()) {
            msg.addRecipients(RecipientType.CC, cc);
        }
        // 设置暗送
        String bcc = email.getBccAddress();
        if (!bcc.isEmpty()) {
            msg.addRecipients(RecipientType.BCC, bcc);
        }
        msg.setSubject(email.getSubject());// 设置主题
        MimeMultipart parts = new MimeMultipart();// 创建部件集对象
        MimeBodyPart part = new MimeBodyPart();// 创建一个部件
        part.setContent(email.getContent(), "text/html;charset=utf-8");// 设置邮件文本内容
        parts.addBodyPart(part);// 把部件添加到部件集中
        // 添加附件
        List<Attachment> attachBeanList = email.getAttachs();// 获取所有附件
        if (CollectionUtils.isNotEmpty(attachBeanList)) {
            for (Attachment attach : attachBeanList) {
                MimeBodyPart attachPart = new MimeBodyPart();// 创建一个部件
                attachPart.attachFile(attach.getFile());// 设置附件文件
                attachPart.setFileName(MimeUtility.encodeText(attach
                        .getFileName()));// 设置附件文件名
                String cid = attach.getCid();
                if (cid != null) {
                    attachPart.setContentID(cid);
                }
                parts.addBodyPart(attachPart);
            }
        }
        msg.setContent(parts);// 给邮件设置内容
        Transport.send(msg);// 发邮件
    }

    /**
     * 通过默认账户发送标题和内容
     *
     * @param to
     * @param subject
     * @param content
     * @throws IOException
     * @throws MessagingException
     */
    public static void send(String to, String subject, String content) throws IOException, MessagingException {
        Email emailBean = new Email(from, to);
        emailBean.setSubject(subject);
        emailBean.setContent(content);
        send(emailBean);
    }
}