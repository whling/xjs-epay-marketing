package org.xiajinsuo.epay.util;

import org.apache.commons.codec.digest.DigestUtils;

/**
 * Created by Administrator on 2017/2/3.
 */
public class PwdUtil {
    /**
     * 密码MD5加密
     *
     * @param text
     * @return
     */
    public static String sign(String text) {
        return DigestUtils.md5Hex((text + "hxtc!@#5570707").getBytes()).toLowerCase();
    }

    /**
     * 密码校验
     *
     * @param text
     * @param signedText
     * @return
     */
    public static boolean verify(String text, String signedText) {
        return sign(text).equals(signedText);
    }
}
