package org.xiajinsuo.epay.util.mypays.agent;

import org.xiajinsuo.epay.business.TransClearing;
import org.xiajinsuo.epay.sdk.HttpUtils;
import org.xiajinsuo.epay.sdk.RRParams;
import org.xiajinsuo.epay.sdk.ResponseDataWrapper;
import org.xiajinsuo.epay.util.DateUtils;
import org.xiajinsuo.epay.util.SysConfigUtils;
import org.xiajinsuo.epay.util.mypays.TpRsaDataEncryptUtil;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by Administrator on 2017/6/13.
 */
public class AgentApiUtil {
    /**
     * 代付
     *
     * @param transClearing
     * @return
     */
    public static RespResult pay(TransClearing transClearing) {
        String transTime = DateUtils.formatDateTime(new Date());
        String appId = SysConfigUtils.getApp_id();
        String mypaysPayApiUrl = SysConfigUtils.getMypays_pay_api_url();
        long transTimestamp = System.currentTimeMillis();

        Map<String, String> businessReq = new HashMap();
        businessReq.put("trans_time", transTime);
        businessReq.put("trans_date", DateUtils.formatDate(new Date()));
        businessReq.put("trans_amount", transClearing.getPayAmount());
        businessReq.put("family_name", transClearing.getFamilyName());
        businessReq.put("id_card", transClearing.getIdcard());
        businessReq.put("bank_no", transClearing.getBankNo());
        businessReq.put("bank_name", transClearing.getBankName());
        businessReq.put("bank_id", transClearing.getBankId());
        businessReq.put("method", "pay");

        RRParams requestData = RRParams.newBuilder()
                .setAppId(appId)
                .setClientTransId(transClearing.getClientTransId())
                .setTransTimestamp(transTimestamp)
                .setTransType("AGENCY_PAYMENT")
                .build();
        ResponseDataWrapper rdw = HttpUtils.post(mypaysPayApiUrl, requestData, businessReq, TpRsaDataEncryptUtil.rsaDataEncryptPri, TpRsaDataEncryptUtil.rsaDataEncryptPub);
        String respCode = rdw.getRespCode();
        String respMsg = rdw.getRespMsg();
        if (respCode.equals("000000")) {
            return RespResult.success(respCode, respMsg);
        } else {
            return RespResult.fail(respCode, respMsg);
        }
    }

    /**
     * 订单查询
     *
     * @param origTranId
     * @return
     */
    public static RespResult qry(String origTranId) {
        String clientTransId = DateUtils.generateRundomDate(6);
        String appId = SysConfigUtils.getApp_id();
        String mypaysPayApiUrl = SysConfigUtils.getMypays_pay_api_url();
        long transTimestamp = System.currentTimeMillis();
        Map<String, String> businessReq = new HashMap();
        businessReq.put("orig_tran_id", origTranId);
        businessReq.put("method", "pay_qry");
        RRParams requestData = RRParams.newBuilder()
                .setAppId(appId)
                .setClientTransId(clientTransId)
                .setTransTimestamp(transTimestamp)
                .setTransType("AGENCY_PAYMENT")
                .build();
        ResponseDataWrapper rdw = HttpUtils.post(mypaysPayApiUrl, requestData, businessReq, TpRsaDataEncryptUtil.rsaDataEncryptPri, TpRsaDataEncryptUtil.rsaDataEncryptPub);
        String respCode = rdw.getRespCode();
        String respMsg = rdw.getRespMsg();
        if (respCode.equals("000000")) {
            Map<String, String> respData = rdw.getResponseData();
            return RespResult.success(respData.get("resp_code"), respData.get("resp_msg"));
        } else {
            return RespResult.fail(respCode, respMsg);
        }
    }
}
