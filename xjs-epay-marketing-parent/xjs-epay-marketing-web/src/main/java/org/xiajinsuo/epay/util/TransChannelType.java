package org.xiajinsuo.epay.util;

/**
 * Created by Administrator on 2017/2/21.
 * 支付类型
 */
public enum TransChannelType {
    WEIXINPAY_QR("WEIXINPAY_QR", "微信", "01"),
    ALIPAY_QR("ALIPAY_QR", "支付宝", "02"),
    SHORTCUTPAY("SHORTCUTPAY", "快捷支付", "03");

    TransChannelType(String value, String name, String code) {
        this.value = value;
        this.name = name;
        this.code = code;
    }

    private String value;
    private String name;
    private String code;

    public static String findCodeByChannelType(String transChannelType) {
        for (TransChannelType item : TransChannelType.values()) {
            if (transChannelType.equals(item.getValue())) {
                return item.getCode();
            }
        }
        return null;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
