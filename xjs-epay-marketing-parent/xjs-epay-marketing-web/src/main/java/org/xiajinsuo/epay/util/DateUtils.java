package org.xiajinsuo.epay.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Administrator on 2016/12/20.
 */
public class DateUtils {
    static final String FORMAT_DATETIME_DEFAULT = "yyyyMMddHHmmss";
    static final String FORMAT_DATE_DEFAULT = "yyyyMMdd";
    static final String FORMAT_MONTH_DEFAULT = "yyyyMM";

    public static String formatDateTime(Date date) {
        return new SimpleDateFormat(FORMAT_DATETIME_DEFAULT).format(date);
    }

    public static String formatDate(Date date) {
        return new SimpleDateFormat(FORMAT_DATE_DEFAULT).format(date);
    }

    public static String formatMonth(Date date) {
        return new SimpleDateFormat(FORMAT_MONTH_DEFAULT).format(date);
    }

    /**
     * 构建流水号 yyyyMMdd+size
     *
     * @param size
     * @return
     */
    public static String generateRundomDate(int size) {
        String time = formatDate(new Date());
        String nanoTime = System.nanoTime() + "";
        return String.format("%s%s", time, nanoTime.substring(nanoTime.length() - size));
    }

    /**
     * 获取当前月份的第一天 yyyyMMddHHmmss
     *
     * @return
     */
    public static String monthMaxDay() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        return formatDate(calendar.getTime()) + "240000";
    }

    /**
     * 获取当前月份的第一天 yyyyMMdd
     *
     * @return
     */
    public static String currentMonth(int days) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, days);
        return formatDate(calendar.getTime());
    }

    /**
     * 获取当前月份的第一天 yyyyMMddHHmmss
     *
     * @return
     */
    public static String monthMaxDay(int days) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, days);
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        return formatDate(calendar.getTime()) + "240000";
    }

    /**
     * 获取当前月份的最后一天 yyyyMMddHHmmss
     *
     * @return
     */
    public static String monthMinDay() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
        return formatDate(calendar.getTime()) + "000000";
    }
}
