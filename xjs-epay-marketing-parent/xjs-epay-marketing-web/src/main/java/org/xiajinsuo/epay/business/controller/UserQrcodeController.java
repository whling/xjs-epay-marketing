/**
 * Autogenerated by bestpay-maven-plugin
 * DO NOT EDIT DIRECTLY
 *
 * @Copyright 2016 www.bestpay.io Inc. All rights reserved.
 */
package org.xiajinsuo.epay.business.controller;

import io.bestpay.framework.base.Response;
import io.bestpay.framework.base.UserRequest;
import io.bestpay.framework.controller.HttpRequestTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.xiajinsuo.epay.business.TransChannel;
import org.xiajinsuo.epay.business.TransSubChannel;
import org.xiajinsuo.epay.business.UserQrcode;
import org.xiajinsuo.epay.cache.CacheUtils;
import org.xiajinsuo.epay.exception.NotAuthSuccessException;
import org.xiajinsuo.epay.exception.OtherErrorException;
import org.xiajinsuo.epay.security.AuthStatus;
import org.xiajinsuo.epay.security.User;
import org.xiajinsuo.epay.util.DateUtils;
import org.xiajinsuo.epay.util.LoginUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.Date;


@SuppressWarnings("all")
/** 用户收款二维码 RequestBody Controller */
@org.apache.avro.specific.AvroGenerated
@Controller
@RequestMapping("/userqrcode")
public class UserQrcodeController extends io.bestpay.framework.controller.RequestBodyAbstractController<UserQrcode, String> {
    Logger logger = LoggerFactory.getLogger(UserQrcodeController.class);
    @javax.annotation.Resource
    private org.xiajinsuo.epay.business.protocol.rpc.TransRpcProtocol transRpcProtocol;
    @javax.annotation.Resource
    private org.xiajinsuo.epay.business.protocol.rpc.TransSubChannelRpcProtocol transSubChannelRpcProtocol;
    @javax.annotation.Resource
    private org.xiajinsuo.epay.business.protocol.rpc.TransChannelRpcProtocol transChannelRpcProtocol;
    @javax.annotation.Resource
    private CacheUtils cacheUtils;
    @javax.annotation.Resource
    private org.xiajinsuo.epay.business.protocol.rpc.UserQrcodeRpcProtocol userQrcodeRpcProtocol;


    protected org.xiajinsuo.epay.business.protocol.rpc.UserQrcodeRpcProtocol getProtocol() {
        return this.userQrcodeRpcProtocol;
    }

    /**
     * 创建收款码
     *
     * @param loginUser     登录用户
     * @param userRequest
     * @param payOthersRate 自定义费率
     * @param transType     通道方式
     * @param transTypeCode 通道方式代码
     * @param host          主机地址
     * @param isFixed       是否是固定码
     * @return
     * @throws Exception
     */
    private String createUserQrcode(User loginUser, UserRequest userRequest, String payOthersRate, String transType, String transTypeCode, String host, boolean isFixed) throws Exception {
        // 用户输入的费率
        BigDecimal inputRate = null;
        try {
            inputRate = new BigDecimal(payOthersRate);
            if (inputRate == null || inputRate.compareTo(new BigDecimal("2")) > 0) {
                throw new OtherErrorException("请输入小于2%的费率值");
            }
        }catch (Exception e){
            throw new OtherErrorException("请输入小于2%的费率值");
        }

        double inputRateStr0 = inputRate.doubleValue();
        String inputRateStr = inputRateStr0 + "";
        if ("1.0".equals(inputRateStr)){
            inputRateStr = "1";
        }

        TransChannel transChannel = transChannelRpcProtocol.findByTransType(loginUser.getOrgCode(), transType, transTypeCode);
        TransSubChannel transSubChannel = transSubChannelRpcProtocol.findBySubTransType(transType, transChannel.getSubChannel());
        if (transChannel == null || transSubChannel == null) {
            throw new OtherErrorException("交易渠道不存在");
        }
        if (!transChannel.getEnable()) {
            throw new OtherErrorException("交易渠道已失效");
        }

        // 判断高签费率是否合法
        try {
            BigDecimal rateT0 = new BigDecimal(transChannel.getRateT0());
            if (inputRate.compareTo(rateT0) < 0) {
                throw new OtherErrorException("自定义费率必须高于渠道费率");
            }
        } catch (Exception e) {
            throw new OtherErrorException("费率参数异常");
        }

        UserQrcode dbQrcode = getProtocol().getByTypeAndUserId(loginUser.getId(), transType, isFixed, transTypeCode, inputRateStr);
        if (null == dbQrcode) {
            // 创建
            UserQrcode userQrcode = UserQrcode.newBuilder()
                    .setUserId(loginUser.getId())
                    .setPayOthersRate(inputRateStr)
                    .setQrcodeUrl(null)
                    .setQrcodeFixed(isFixed)
                    .setTransTypeId(transChannel.getId())
                    .setTransType(transChannel.getTransType())
                    .setTransTypeCode(transChannel.getTransTypeCode())
                    .build();
            dbQrcode = getProtocol().create(userQrcode, userRequest);

            // 更新二维码图片
            StringBuilder sbQrcodeUrl = new StringBuilder();
            sbQrcodeUrl.append(host);
            sbQrcodeUrl.append("/userqrcode/otherPay_step2.html?");
            sbQrcodeUrl.append("id=" + dbQrcode.getId());
            sbQrcodeUrl.append("$_");
            String qrcodeUrlImg = "http://pan.baidu.com/share/qrcode?w=300&h=300&url=" + sbQrcodeUrl.toString();
            boolean updateResult = getProtocol().updateRate(dbQrcode.getId(), inputRateStr, qrcodeUrlImg, DateUtils.formatDateTime(new Date()));
            if (!updateResult) {
                throw new OtherErrorException("创建二维码失败");
            }
        }

        return dbQrcode.getId();
    }

    /**
     * 创建用户付款码(非固定)
     *
     * @param otherPayRate  自定义费率
     * @param transType     支付方式类型
     * @param transTypeCode 支付方式代码
     * @return
     */
    @RequestMapping("/notfixed/create")
    @ResponseBody
    public Response<String> notfixedCreate(final String otherPayRate, final String transType, final String transTypeCode, final String host, final HttpServletRequest request) {

        return this.doRequest(new HttpRequestTemplate<String>() {

            @Override
            public String doRequest(UserRequest userRequest, HttpServletRequest req, HttpServletResponse resp) throws Exception {
                User loginUser = LoginUtil.getLoginUser(request);
                if (loginUser.getStatus() != AuthStatus.SUCCESS) {
                    throw new NotAuthSuccessException();
                }

                return createUserQrcode(loginUser, userRequest, otherPayRate, transType, transTypeCode, host, false);
            }
        });
    }
}
