package org.xiajinsuo.epay.util;

/**
 * 实名认证请求结果
 */
public class RespResult {
    private boolean success;
    private String error_code;
    private String error_msg;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getError_code() {
        return error_code;
    }

    public void setError_code(String error_code) {
        this.error_code = error_code;
    }

    public String getError_msg() {
        return error_msg;
    }

    public void setError_msg(String error_msg) {
        this.error_msg = error_msg;
    }

    public RespResult(boolean success, String error_code, String error_msg) {
        this.success = success;
        this.error_code = error_code;
        this.error_msg = error_msg;
    }

    public RespResult() {
    }

    public static RespResult success() {
        return new RespResult(true, "", "");
    }

    public static RespResult fail(String error_code, String error_msg) {
        return new RespResult(false, error_code, error_msg);
    }
}
