package org.xiajinsuo.epay.util.mypays.agent;

/**
 * 代付结果
 */
public class RespResult {
    private boolean success;
    private String respCode;
    private String respMsg;

    public boolean isSuccess() {
        return success;
    }

    private RespResult(boolean success, String respCode, String respMsg) {
        this.success = success;
        this.respCode = respCode;
        this.respMsg = respMsg;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getRespCode() {
        return respCode;
    }

    public void setRespCode(String respCode) {
        this.respCode = respCode;
    }

    public String getRespMsg() {
        return respMsg;
    }

    public void setRespMsg(String respMsg) {
        this.respMsg = respMsg;
    }

    public static RespResult success(String respCode, String respMsg) {
        return new RespResult(true, respCode, respMsg);

    }

    public static RespResult fail(String respCode, String respMsg) {
        return new RespResult(false, respCode, respMsg);
    }

    @Override
    public String toString() {
        return "RespResult{" +
                "success=" + success +
                ", respCode='" + respCode + '\'' +
                ", respMsg='" + respMsg + '\'' +
                '}';
    }
}
