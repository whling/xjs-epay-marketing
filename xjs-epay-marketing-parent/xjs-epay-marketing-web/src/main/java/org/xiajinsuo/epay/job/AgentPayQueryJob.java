package org.xiajinsuo.epay.job;

import com.dangdang.ddframe.job.api.ShardingContext;
import com.dangdang.ddframe.job.api.simple.SimpleJob;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.xiajinsuo.epay.business.ClearingStatus;
import org.xiajinsuo.epay.business.TransClearing;
import org.xiajinsuo.epay.util.DateUtils;
import org.xiajinsuo.epay.util.mypays.agent.AgentApiUtil;
import org.xiajinsuo.epay.util.mypays.agent.RespResult;

import java.util.Calendar;
import java.util.List;

/**
 * Created by Administrator on 2016/11/29.
 * 代付结果查询
 */
@Component
public class AgentPayQueryJob implements SimpleJob {
    private static Logger LOGGER = LoggerFactory.getLogger(AgentPayQueryJob.class);
    @javax.annotation.Resource
    private org.xiajinsuo.epay.business.protocol.rpc.TransClearingRpcProtocol transClearingRpcProtocol;
    /**
     * 当前状态-已经清算,状态未知
     */
    private final static ClearingStatus CURRENT_STATUS = ClearingStatus.CLEARING_END;

    /**
     * 代付结果查询
     *
     * @param trans
     * @throws Exception
     */
    private void execute(TransClearing trans) throws Exception {
        if (trans == null) {
            return;
        }
        String clientTransId = trans.getClientTransId();
        RespResult respPayResult = AgentApiUtil.qry(clientTransId);
        String errorCode = respPayResult.getRespCode();
        String errorMsg = respPayResult.getRespMsg();
        if (respPayResult.isSuccess()) {
            if ("PAY_SUCCESS".equalsIgnoreCase(errorCode)) {
                transClearingRpcProtocol.updateClearingStatus(clientTransId, ClearingStatus.CLEARING_SUCCESS, ClearingStatus.CLEARING_END,
                        StringUtils.isNotEmpty(errorCode) ? errorCode : "", StringUtils.isNotEmpty(errorMsg) ? errorMsg : "");
            } else if ("PAY_FAILURE".equalsIgnoreCase(errorCode)) {
                transClearingRpcProtocol.updateClearingStatus(clientTransId, ClearingStatus.CLEARING_FAILURE, ClearingStatus.CLEARING_END,
                        StringUtils.isNotEmpty(errorCode) ? errorCode : "", StringUtils.isNotEmpty(errorMsg) ? errorMsg : "");
            }
        } else {
            LOGGER.info("单笔代付查询失败,errorCode={},errorMsg={}", errorCode, errorMsg);
        }
    }


    /**
     * 5分钟之前的交易记录
     *
     * @return
     */
    private String generateTransTimeEnd() {
        Calendar today = Calendar.getInstance();
        today.add(Calendar.MINUTE, -5);
        return DateUtils.formatDateTime(today.getTime());
    }

    /**
     * 微信和支付宝二维码有效时间
     * 微信2小时
     *
     * @return
     */
    private String generateTransTimeStar() {
        Calendar today = Calendar.getInstance();
        today.add(Calendar.DAY_OF_MONTH, -7);
        return DateUtils.formatDateTime(today.getTime());
    }

    @Override
    public void execute(ShardingContext shardingContext) {
        String transTimeStart = generateTransTimeStar();
        String transTimeEnd = generateTransTimeEnd();
        LOGGER.info("查询代付结果开始,transTimeStart={},transTimeEnd={}", transTimeStart, transTimeEnd);
        try {
            //获取已经提交清算的记录
            List<TransClearing> transList = transClearingRpcProtocol.findByClearingStatus(CURRENT_STATUS, transTimeStart, transTimeEnd);
            if (CollectionUtils.isEmpty(transList)) {
                LOGGER.info("TransClearing is null");
                return;
            }
            for (int i = 0; i < transList.size(); i++) {
                TransClearing trans = transList.get(i);
                try {
                    execute(trans);
                    Thread.sleep(1000);
                } catch (Exception e) {
                    LOGGER.error("trans[{}],async execute query request fail,error=[{}]", trans, e);
                }
            }
        } catch (Exception e) {
            LOGGER.error("process fail,{}", e);
        }
        LOGGER.info("查询代付结果结束");
    }
}
