package org.xiajinsuo.epay.util;

/**
 * Created by Administrator on 2017/2/21.
 * 交易类型
 */
public enum TransTypeEnum {
    WEIXINPAY_QR("WEIXINPAY_QR", "微信"),
    ALIPAY_QR("ALIPAY_QR", "支付宝"),
    SHORTCUTPAY_MILIAN("SHORTCUTPAY_MILIAN", "米联"),
    SHORTCUTPAY_MILIAN_INTEGRAL("SHORTCUTPAY_MILIAN_INTEGRAL", "米联有积分"),
    SHORTCUTPAY_EFFERSONPAY("SHORTCUTPAY_EFFERSONPAY", "卡友"),
    SHORTCUTPAY_HANYIN("SHORTCUTPAY_HANYIN", "瀚银"),
    SHORTCUTPAY_HANYIN_NNDEBIT("SHORTCUTPAY_HANYIN_NNDEBIT", "瀚银商旅");

    TransTypeEnum(String value, String name) {
        this.value = value;
        this.name = name;
    }

    private String value;
    private String name;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
