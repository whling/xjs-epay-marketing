package org.xiajinsuo.epay.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by Administrator on 2016/11/14.
 */
@Component
public final class SysConfigUtils {
    //appcode
    private static String app_id;
    //二维码图片生成api
    private static String qr_img_api;
    private static String epay_mypays_cn;
    //客户端私钥密码
    private static String app_key_password;
    //服务器公钥密码
    private static String pub_app_key_password;
    //实名认证接口
    private static String mypays_auth_api_url;
    //支付接口
    private static String mypays_pay_api_url;
    //微信相关参数
    private static String weixin_api_code_url;
    private static String weixin_api_openid_url;
    private static String weixin_api_access_token_url;
    private static String weixin_api_jsapi_ticket_url;
    //服务器配置
    private static String ftp_user;
    private static String ftp_password;
    private static String ftp_path;
    private static String ftp_url;
    private static String ftp_secondPath;
    @Value("#{applicationCfg['sys.ftp.secondPath']}")
    public void setFtp_secondPath(String ftp_secondPath) {
        SysConfigUtils.ftp_secondPath = ftp_secondPath;
    }
    @Value("#{applicationCfg['sys.ftp.user']}")
    public void setFtp_user(String ftp_user) {
        SysConfigUtils.ftp_user = ftp_user;
    }
    @Value("#{applicationCfg['sys.ftp.password']}")
    public void setFtp_password(String ftp_password) {
        SysConfigUtils.ftp_password = ftp_password;
    }
    @Value("#{applicationCfg['sys.ftp.path']}")
    public void setFtp_path(String ftp_path) {
        SysConfigUtils.ftp_path = ftp_path;
    }
    @Value("#{applicationCfg['sys.ftp.url']}")
    public void setFtp_url(String ftp_url) {
        SysConfigUtils.ftp_url = ftp_url;
    }

    @Value("#{applicationCfg['sys.epay_mypays_cn']}")
    public void setEpay_mypays_cn(String epay_mypays_cn) {
        SysConfigUtils.epay_mypays_cn = epay_mypays_cn;
    }

    @Value("#{applicationCfg['weixin.api.jsapi_ticket_url']}")
    public void setWeixin_api_jsapi_ticket_url(String weixin_api_jsapi_ticket_url) {
        SysConfigUtils.weixin_api_jsapi_ticket_url = weixin_api_jsapi_ticket_url;
    }

    @Value("#{applicationCfg['weixin.api.access_token_url']}")
    public void setWeixin_api_access_token_url(String weixin_api_access_token_url) {
        SysConfigUtils.weixin_api_access_token_url = weixin_api_access_token_url;
    }


    @Value("#{applicationCfg['weixin.api.code_url']}")
    public void setWeixin_api_code_url(String weixin_api_code_url) {
        SysConfigUtils.weixin_api_code_url = weixin_api_code_url;
    }

    @Value("#{applicationCfg['weixin.api.openid_url']}")
    public void setWeixin_api_openid_url(String weixin_api_openid_url) {
        SysConfigUtils.weixin_api_openid_url = weixin_api_openid_url;
    }

    @Value("#{applicationCfg['sys.app_id']}")
    public void setApp_id(String app_id) {
        SysConfigUtils.app_id = app_id;
    }

    @Value("#{applicationCfg['sys.app_key_password']}")
    public void setApp_key_password(String app_key_password) {
        SysConfigUtils.app_key_password = app_key_password;
    }


    @Value("#{applicationCfg['sys.pub_app_key_password']}")
    public void setPub_app_key_password(String pub_app_key_password) {
        SysConfigUtils.pub_app_key_password = pub_app_key_password;
    }


    @Value("#{applicationCfg['sys.qr_img_api']}")
    public void setQr_img_api(String qr_img_api) {
        SysConfigUtils.qr_img_api = qr_img_api;
    }

    @Value("#{applicationCfg['sys.mypays_auth_api_url']}")
    public void setMypays_auth_api_url(String mypays_auth_api_url) {
        SysConfigUtils.mypays_auth_api_url = mypays_auth_api_url;
    }

    @Value("#{applicationCfg['sys.mypays_pay_api_url']}")
    public void setMypays_pay_api_url(String mypays_pay_api_url) {
        SysConfigUtils.mypays_pay_api_url = mypays_pay_api_url;
    }


    public static String getApp_id() {
        return app_id;
    }

    public static String getQr_img_api() {
        return qr_img_api;
    }

    public static String getApp_key_password() {
        return app_key_password;
    }

    public static String getPub_app_key_password() {
        return pub_app_key_password;
    }

    public static String getMypays_auth_api_url() {
        return mypays_auth_api_url;
    }

    public static String getMypays_pay_api_url() {
        return mypays_pay_api_url;
    }

    public static String getWeixin_api_code_url() {
        return weixin_api_code_url;
    }

    public static String getWeixin_api_openid_url() {
        return weixin_api_openid_url;
    }

    public static String getWeixin_api_access_token_url() {
        return weixin_api_access_token_url;
    }

    public static String getWeixin_api_jsapi_ticket_url() {
        return weixin_api_jsapi_ticket_url;
    }

    public static String getEpay_mypays_cn() {
        return epay_mypays_cn;
    }

    public static String getFtp_user() {
        return ftp_user;
    }

    public static String getFtp_password() {
        return ftp_password;
    }

    public static String getFtp_path() {
        return ftp_path;
    }

    public static String getFtp_url() {
        return ftp_url;
    }

    public static String getFtp_secondPath() {
        return ftp_secondPath;
    }
}
