package org.xiajinsuo.epay.service;

import io.bestpay.framework.base.CodeException;
import io.bestpay.framework.base.UserRequest;
import org.xiajinsuo.epay.business.*;
import org.xiajinsuo.epay.security.User;
import org.xiajinsuo.epay.util.ClearingTypeEnum;
import org.xiajinsuo.epay.util.RespPayResult;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Administrator on 2016/11/21.
 * 快捷支付
 */
public interface ShortcutpayServiceApi {
    /**
     * 是否支持支付通道
     *
     * @param channel    大类
     * @param subChannel 小类
     * @return
     */
    boolean support(String channel, String subChannel);

    /**
     * 执行具体支付渠道
     *
     * @param transChannel    大类
     * @param transSubChannel 小类
     * @param loginUser       登录用户
     * @param req
     * @param transAmt        交易金额（元）
     * @param cardId          付款卡
     * @param payOthers       是否是他人收款
     * @param payOthersRate   他人收款费率
     * @param memo            备注
     * @param invitationUser  二维码分享人
     * @return
     * @throws CodeException
     */
    RespPayResult execute(UserRequest userRequest,
                          TransChannel transChannel,
                          TransSubChannel transSubChannel,
                          User loginUser,
                          HttpServletRequest req,
                          String transAmt,
                          String cardId,
                          boolean payOthers,
                          String payOthersRate,
                          String memo,
                          User invitationUser
    ) throws CodeException;

    /**
     * 创建交易记录
     *
     * @param channel
     * @param sub_channel
     * @param loginUser
     * @param transAmt
     * @param clientTransId
     * @param transTime
     * @param card
     * @param rate
     * @param counterFee
     * @param operationFee
     * @param payAmount
     * @param codeUrl
     * @param payBankNo
     * @param transTypeCode
     * @param thirdpartMerCode
     * @return
     */
    default Trans generalTransRecord(String channel, String sub_channel, User loginUser, String transAmt, String clientTransId,
                                     String transTime, Card card, String rate, int counterFee, int operationFee, int payAmount,
                                     String codeUrl, String payBankNo, String transTypeCode, String thirdpartMerCode,
                                     boolean payOthers,
                                     String payOthersRate, String memo) {
        return Trans.newBuilder()
                .setClientTransId(clientTransId)
                .setTransTime(transTime)
                .setAmount(transAmt)
                .setPayStatus(PayStatus.PAY_SUBMIT)
                .setTransType(channel)
                .setSubTransType(sub_channel)
                .setUserId(loginUser.getId())
                .setCodeUrl(codeUrl)
                .setFamilyName(loginUser.getFamilyName())
                .setClearingType(ClearingTypeEnum.T0.getValue())
                .setRate(rate)
                .setCounterFee(counterFee + "")
                .setOperationFee(operationFee + "")
                .setPayAmount(payAmount + "")
                .setUsername(loginUser.getUsername())
                .setReferee(loginUser.getReferee())
                .setRefereeFamilyName(loginUser.getRefereeFamilyName())
                .setMobile(card.getMobile())
                .setIdcard(card.getIdcard())
                .setBankName(card.getBankName())
                .setBankNo(card.getBankNo())
                .setBranchCode(card.getBranchCode())
                .setBranchName(card.getBranchName())
                .setOrgCode(loginUser.getOrgCode())
                .setOrgName(loginUser.getOrgName())
                .setPayBankNo(payBankNo)
                .setAgent(loginUser.getAgent())
                .setAgentFamilyName(loginUser.getAgentFamilyName())
                .setTransTypeCode(transTypeCode)
                .setThirdpartMerCode(thirdpartMerCode)
                .setPayOthers(payOthers)
                .setPayOthersRate(payOthersRate)
                .setMemo(memo)
                .build();
    }
}
