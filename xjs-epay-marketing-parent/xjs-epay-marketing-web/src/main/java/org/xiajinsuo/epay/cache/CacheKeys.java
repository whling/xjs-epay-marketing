package org.xiajinsuo.epay.cache;

/**
 * Created by Administrator on 2016/11/21.
 */
public final class CacheKeys {
    /**
     * cardbin
     */
    public final static String CACHE_EPAY_MARKETING_BANK_CARD_BIN = "cache_epay_marketing_bank_card_bin";
    /**
     * citybankcode 联行号
     */
    public final static String CACHE_EPAY_MARKETING_CITY_BANK_CODE = "cache_epay_marketing_city_bank_code[%s]";
    /**
     * accessToken 微信api accessToken
     */
    public final static String CACHE_EPAY_MARKETING_ACCESS_TOKEN = "cache_epay_marketing_access_token[%s]";
    /**
     * jsapi_ticket 微信api jsapi_ticket
     */
    public final static String CACHE_EPAY_MARKETING_JSAPI_TICKET = "cache_epay_marketing_jsapi_ticket[%s]";
    /**
     * 交易渠道
     */
    public final static String CACHE_EPAY_MARKETING_TRANS_CHANNEL = "cache_epay_marketing_trans_channel[%s][%s][%s]";
    /**
     * 机构信息
     */
    public static final String CACHE_EPAY_MARKETING_ORG = "cache_epay_marketing_org[%s]";
}
