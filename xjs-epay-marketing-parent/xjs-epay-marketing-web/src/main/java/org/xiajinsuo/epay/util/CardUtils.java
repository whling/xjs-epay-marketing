package org.xiajinsuo.epay.util;

import org.apache.commons.lang.StringUtils;
import org.springframework.util.CollectionUtils;
import org.xiajinsuo.epay.business.Card;
import org.xiajinsuo.epay.sdk.RRParams;
import org.xiajinsuo.epay.sdk.ResponseDataWrapper;
import org.xiajinsuo.epay.util.mypays.TpRsaDataEncryptUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017\8\12 0012.
 */
public class CardUtils {
    /**
     * 判断银行卡是否已经存在
     *
     * @param bankNo
     * @param cards
     * @return
     */

    public static boolean existx(String bankNo, List<Card> cards) {
        if (CollectionUtils.isEmpty(cards)) {
            return false;
        }
        for (Card item : cards) {
            if (bankNo.equals(item.getBankNo())) {
                return true;
            }
        }
        return false;
    }

    public static Card existxCard(String bankNo, List<Card> cards) {
        if (CollectionUtils.isEmpty(cards)) {
            return null;
        }
        for (Card item : cards) {
            if (bankNo.equals(item.getBankNo())) {
                return item;
            }
        }
        return null;
    }

    public static Card existxCardWithMobile(String bankNo, String phone, List<Card> cards) {
        if (CollectionUtils.isEmpty(cards)) {
            return null;
        }
        for (Card item : cards) {
            if (bankNo.equals(item.getBankNo()) && phone.equals(item.getUsername())) {
                return item;
            }
        }
        return null;
    }

    /**
     * 四要素认证
     *
     * @param card
     * @return
     */
    public static RespResult realnameVerify(Card card) {
        Map<String, String> nameVerifyParam4 = new HashMap<String, String>();
        nameVerifyParam4.put("family_name", card.getFamilyName());
        nameVerifyParam4.put("id_card", card.getIdcard());
        nameVerifyParam4.put("bank_no", card.getBankNo());
        nameVerifyParam4.put("mobile", card.getMobile());
        RRParams requestData = RRParams.newBuilder()
                .setAppId(SysConfigUtils.getApp_id())
                .setClientTransId(System.currentTimeMillis() + "")
                .setTransTimestamp(System.currentTimeMillis())
                .setTransType("REALNAME_AUTH4")
                .build();
        ResponseDataWrapper rdw = org.xiajinsuo.epay.sdk.HttpUtils.post(SysConfigUtils.getMypays_auth_api_url(), requestData, nameVerifyParam4, TpRsaDataEncryptUtil.rsaDataEncryptPri, TpRsaDataEncryptUtil.rsaDataEncryptPub);
        if (rdw.getRespCode().equals("000000")) {
            return RespResult.success();
        } else {
            return RespResult.fail(rdw.getRespCode(), rdw.getRespMsg());
        }
    }

    /**
     * 替换字符串全部空格
     *
     * @param str
     * @return
     */
    public static String trim(String str) {
        return StringUtils.isEmpty(str) ? "" : str.replaceAll(" ", "");
    }
}
