package org.xiajinsuo.epay.vo;

public class FixedRateVo{
    private String otherPayRate;
    private String transType;
    private String transTypeCode;
    private String host;

    public String getOtherPayRate() {
        return otherPayRate;
    }

    public void setOtherPayRate(String otherPayRate) {
        this.otherPayRate = otherPayRate;
    }

    public String getTransType() {
        return transType;
    }

    public void setTransType(String transType) {
        this.transType = transType;
    }

    public String getTransTypeCode() {
        return transTypeCode;
    }

    public void setTransTypeCode(String transTypeCode) {
        this.transTypeCode = transTypeCode;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }
}