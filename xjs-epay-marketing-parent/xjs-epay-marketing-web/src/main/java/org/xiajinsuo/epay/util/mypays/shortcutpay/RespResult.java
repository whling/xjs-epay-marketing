package org.xiajinsuo.epay.util.mypays.shortcutpay;

/**
 * 代付结果
 */
public class RespResult {
    private boolean success;
    private String respCode;
    private String respMsg;
    private String data;

    public boolean isSuccess() {
        return success;
    }

    private RespResult(boolean success, String respCode, String respMsg, String data) {
        this.success = success;
        this.respCode = respCode;
        this.respMsg = respMsg;
        this.data = data;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getRespCode() {
        return respCode;
    }

    public void setRespCode(String respCode) {
        this.respCode = respCode;
    }

    public String getRespMsg() {
        return respMsg;
    }

    public void setRespMsg(String respMsg) {
        this.respMsg = respMsg;
    }

    public static RespResult success(String respCode, String respMsg, String data) {
        return new RespResult(true, respCode, respMsg, data);

    }

    public static RespResult fail(String respCode, String respMsg) {
        return new RespResult(false, respCode, respMsg, "");
    }

}
