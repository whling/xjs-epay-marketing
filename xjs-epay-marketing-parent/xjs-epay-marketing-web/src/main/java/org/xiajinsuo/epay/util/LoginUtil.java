package org.xiajinsuo.epay.util;

import com.alibaba.fastjson.JSON;
import io.bestpay.framework.base.CodeException;
import io.bestpay.framework.base.SpecificRecordUtils;
import io.bestpay.framework.controller.HttpRequestTemplate;
import io.bestpay.framework.request.UserHttpRequest;
import org.apache.commons.lang.StringUtils;
import org.xiajinsuo.epay.security.Organization;
import org.xiajinsuo.epay.security.User;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Administrator on 2016/10/10.
 */
public final class LoginUtil {
    private static final String LOGIN_USER = "LOGIN_USER";
    private static final String LOGIN_OPENID = "LOGIN_OPENID";
    private static final String LOGIN_USER_ORG = "LOGIN_USER_ORG";

    public static User getLoginUser(HttpServletRequest request) {
        Object attribute = request.getSession().getAttribute(LOGIN_USER);
        if (attribute != null) {
            try {
                User result = new User();
                SpecificRecordUtils.parse(result, JSON.parseObject(attribute.toString()));
                return result;
            } catch (CodeException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static void setLoginUser(HttpServletRequest request, User user) {
        request.getSession().setAttribute(HttpRequestTemplate.USER_ID_KEY, user.getId());
        request.getSession().setAttribute(LOGIN_USER, user.toString());
    }

    public static String getLoginOpenid(HttpServletRequest request) {
        final String openId = (String) request.getSession().getAttribute(LOGIN_OPENID);
        return StringUtils.isEmpty(openId) ? "" : openId;
    }

    public static void setLoginOpenid(HttpServletRequest request, String openid) {
        request.getSession().setAttribute(LOGIN_OPENID, openid);
    }

    public static void clearLoginUser(HttpServletRequest request) {
        request.getSession().removeAttribute(LOGIN_USER);
    }

    public static String getRemoteHost(HttpServletRequest request) {
        String ip = request.getHeader("x-forwarded-for");
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
        return ip.equals("0:0:0:0:0:0:0:1") ? "127.0.0.1" : ip;
    }

    public static void setLoginUserOrg(HttpServletRequest request, Organization organization) {
        request.getSession().setAttribute(LOGIN_USER_ORG, organization.toString());
    }

    public static Organization getLoginUserOrg(HttpServletRequest request) {
        Object attribute = request.getSession().getAttribute(LOGIN_USER_ORG);
        if (attribute != null) {
            try {
                Organization result = new Organization();
                SpecificRecordUtils.parse(result, JSON.parseObject(attribute.toString()));
                return result;
            } catch (CodeException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static UserHttpRequest getTempSysUserHttpRequest(){
        UserHttpRequest tempUser = new UserHttpRequest();
        tempUser.setPrincipal("sys_to_other");
        tempUser.setAuthorized(true);
        return tempUser;
    }
}
