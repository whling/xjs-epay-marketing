package org.xiajinsuo.epay.util.sftp;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xiajinsuo.epay.util.SysConfigUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.UUID;

/**
 * sftp 操作工具类
 */
public class SftpUtils {
    private final static Logger logger = LoggerFactory.getLogger(SftpUtils.class);

    /**
     * 文件上传
     *
     * @param file
     * @param fileDir 文件目录 epay/*
     * @param extName 文件扩展名
     * @return
     * @throws IOException
     */
    public static String upload(File file, String fileDir, String extName) {
        String ftp_user = SysConfigUtils.getFtp_user();
        String ftp_password = SysConfigUtils.getFtp_password();
        String ftp_path = SysConfigUtils.getFtp_path();
        String ftp_url = SysConfigUtils.getFtp_url();
        String baseDir = SysConfigUtils.getFtp_secondPath();
        FtpConf ftpConf = new FtpConf(ftp_url, ftp_user, ftp_password, ftp_path, 22);
        ChannelSftp channelSftp = null;
        String uuid = (UUID.randomUUID() + "").replaceAll("-", "");
        String dst = baseDir + "/" + fileDir + "/" + uuid + extName;
        try {
            channelSftp = SftpFactory.getInstance().getChannel(ftpConf, 10000);
        } catch (JSchException e) {
            logger.error("channelSftp create error", e);
        }
        try {
            channelSftp.mkdir(baseDir + "/" + fileDir);
        } catch (SftpException e) {
            logger.error("channelSftp mkdir error", e);
        }
        FileInputStream inputStream = null;
        try {
            inputStream = new FileInputStream(file);
            channelSftp.put(inputStream, dst);
            return "http://" + ftp_url + ftp_path + dst;
        } catch (Exception e) {
            logger.error("channelSftp put error", e);
        } finally {
            try {
                inputStream.close();
                SftpFactory.getInstance().closeChannel();
            } catch (Exception e) {
                logger.error("channel close error", e);
            }
        }
        return null;
    }
}
