/**
 * Autogenerated by Avro
 *
 * DO NOT EDIT DIRECTLY
 */

/**
 * @Copyright 2017 www.bestpay.io Inc. All rights reserved.
 */

package org.xiajinsuo.epay.security;
@SuppressWarnings("all")
@org.apache.avro.specific.AvroGenerated
public enum AuthStatus {
  TO_BE_CONFIRMED, SUCCESS, FAIL  ;
  public static final org.apache.avro.Schema SCHEMA$ = new org.apache.avro.Schema.Parser().parse("{\"type\":\"enum\",\"name\":\"AuthStatus\",\"namespace\":\"org.xiajinsuo.epay.security\",\"symbols\":[\"TO_BE_CONFIRMED\",\"SUCCESS\",\"FAIL\"]}");
  public static org.apache.avro.Schema getClassSchema() { return SCHEMA$; }
  /**   */
  public static AuthStatus valueOf(int ordinal) {
	  AuthStatus[] values = AuthStatus.values();
	  for (AuthStatus s : values) {
		  if (s.ordinal() == ordinal) {
			  return s;
		  }
	  }
	  throw new org.apache.avro.AvroRuntimeException("Bad ordinal");
  }  
}
