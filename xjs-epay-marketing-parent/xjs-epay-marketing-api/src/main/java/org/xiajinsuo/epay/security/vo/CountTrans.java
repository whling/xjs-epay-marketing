package org.xiajinsuo.epay.security.vo;

import java.io.Serializable;

/**
 * Created by tums on 2017/6/23.
 */
public class CountTrans implements Serializable {
    private int month_trans;
    private String month_trans_amt;
    private int today_trans;
    private String today_trans_amt;

    public CountTrans(int month_trans, String month_trans_amt, int today_trans, String today_trans_amt) {
        this.month_trans = month_trans;
        this.month_trans_amt = month_trans_amt;
        this.today_trans = today_trans;
        this.today_trans_amt = today_trans_amt;
    }

    public int getMonth_trans() {
        return month_trans;
    }

    public void setMonth_trans(int month_trans) {
        this.month_trans = month_trans;
    }

    public String getMonth_trans_amt() {
        return month_trans_amt;
    }

    public void setMonth_trans_amt(String month_trans_amt) {
        this.month_trans_amt = month_trans_amt;
    }

    public int getToday_trans() {
        return today_trans;
    }

    public void setToday_trans(int today_trans) {
        this.today_trans = today_trans;
    }

    public String getToday_trans_amt() {
        return today_trans_amt;
    }

    public void setToday_trans_amt(String today_trans_amt) {
        this.today_trans_amt = today_trans_amt;
    }

    public CountTrans() {
    }
}
