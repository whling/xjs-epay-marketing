/**
 * Autogenerated by bestpay-maven-plugin
 * YOU CAN EDIT DIRECTLY
 *
 * @Copyright 2017 www.bestpay.io Inc. All rights reserved.
 *
 */
package org.xiajinsuo.epay.business.protocol.rpc;

import java.io.Serializable;
import java.util.Map;
import javax.validation.constraints.Min;
import org.xiajinsuo.epay.business.BankCardBinExt;
import io.bestpay.framework.base.CodeException;
import io.bestpay.framework.base.UserRequest;

@SuppressWarnings("all")
/** 卡bin  RpcProtocol */
@org.apache.avro.specific.AvroGenerated
public interface BankCardBinExtRpcProtocol {
	/** 通过id删除 */
	public boolean delete(java.lang.String id, UserRequest userRequest) throws CodeException;

	/** 更新 */
	public BankCardBinExt update(BankCardBinExt e, UserRequest userRequest) throws CodeException;

	/** 分页查询 */
	public io.bestpay.framework.base.SpecificPage<BankCardBinExt> query(UserRequest userRequest, @Min(1L) Long currentPage, @Min(1L) Long pageSize) throws CodeException;

	/** 创建对像 */
	public BankCardBinExt create(BankCardBinExt e, UserRequest userRequest) throws CodeException;

	/** 通过id获取对像 */
	public BankCardBinExt get(java.lang.String id, UserRequest userRequest) throws CodeException;
}