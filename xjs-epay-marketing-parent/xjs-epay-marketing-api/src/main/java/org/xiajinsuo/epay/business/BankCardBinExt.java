/**
 * Autogenerated by Avro
 *
 * DO NOT EDIT DIRECTLY
 */

/**
 * @Copyright 2017 www.bestpay.io Inc. All rights reserved.
 */

package org.xiajinsuo.epay.business;

import org.apache.avro.specific.SpecificData;

import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@SuppressWarnings("all")
/** 卡bin */
@javax.xml.bind.annotation.XmlRootElement
@javax.xml.bind.annotation.XmlAccessorType(javax.xml.bind.annotation.XmlAccessType.FIELD)
@javax.xml.bind.annotation.XmlType(name="BankCardBinExt", namespace="org.xiajinsuo.epay.business")
@org.apache.avro.specific.AvroGenerated
public class BankCardBinExt extends io.bestpay.framework.base.SpecificRecordBase implements io.bestpay.framework.base.SpecificRecord {
  private static final long serialVersionUID = 6969514096307301497L;
  public static final org.apache.avro.Schema SCHEMA$ = new org.apache.avro.Schema.Parser().parse("{\"type\":\"record\",\"name\":\"BankCardBinExt\",\"namespace\":\"org.xiajinsuo.epay.business\",\"doc\":\"卡bin\",\"fields\":[{\"name\":\"id\",\"type\":{\"type\":\"string\",\"avro.java.string\":\"String\"},\"doc\":\"主键\",\"default\":\"\"},{\"name\":\"bank_code\",\"type\":{\"type\":\"string\",\"avro.java.string\":\"String\"},\"doc\":\"bank_code\",\"default\":\"\"},{\"name\":\"bank_name\",\"type\":{\"type\":\"string\",\"avro.java.string\":\"String\"},\"doc\":\"bank_name\",\"default\":\"\"},{\"name\":\"card_name\",\"type\":{\"type\":\"string\",\"avro.java.string\":\"String\"},\"doc\":\"card_name\",\"default\":\"\"},{\"name\":\"description\",\"type\":{\"type\":\"string\",\"avro.java.string\":\"String\"},\"doc\":\"description\",\"default\":\"\"},{\"name\":\"card_bin\",\"type\":{\"type\":\"string\",\"avro.java.string\":\"String\"},\"doc\":\"card_bin\",\"default\":\"\"},{\"name\":\"card_length\",\"type\":{\"type\":\"string\",\"avro.java.string\":\"String\"},\"doc\":\"card_length\",\"default\":\"\"},{\"name\":\"card_bin_length\",\"type\":{\"type\":\"string\",\"avro.java.string\":\"String\"},\"doc\":\"card_bin_length\",\"default\":\"\"},{\"name\":\"card_type\",\"type\":{\"type\":\"string\",\"avro.java.string\":\"String\"},\"doc\":\"银行卡类型\",\"default\":\"\"},{\"name\":\"card_type_name\",\"type\":{\"type\":\"string\",\"avro.java.string\":\"String\"},\"doc\":\"银行卡类型名称\",\"default\":\"\"},{\"name\":\"issu_id\",\"type\":{\"type\":\"string\",\"avro.java.string\":\"String\"},\"doc\":\"id\",\"default\":\"\"},{\"name\":\"issu_name\",\"type\":{\"type\":\"string\",\"avro.java.string\":\"String\"},\"doc\":\"名称\",\"default\":\"\"}],\"aliases\":[\"BankCardBinExt\"]}");
  public static org.apache.avro.Schema getClassSchema() { return SCHEMA$; }
  /** 主键 */
  @javax.xml.bind.annotation.XmlElement(name="id")
  private java.lang.String id;
  /** bank_code */
  @javax.xml.bind.annotation.XmlElement(name="bank_code")
  private java.lang.String bank_code;
  /** bank_name */
  @javax.xml.bind.annotation.XmlElement(name="bank_name")
  private java.lang.String bank_name;
  /** card_name */
  @javax.xml.bind.annotation.XmlElement(name="card_name")
  private java.lang.String card_name;
  /** description */
  @javax.xml.bind.annotation.XmlElement(name="description")
  private java.lang.String description;
  /** card_bin */
  @javax.xml.bind.annotation.XmlElement(name="card_bin")
  private java.lang.String card_bin;
  /** card_length */
  @javax.xml.bind.annotation.XmlElement(name="card_length")
  private java.lang.String card_length;
  /** card_bin_length */
  @javax.xml.bind.annotation.XmlElement(name="card_bin_length")
  private java.lang.String card_bin_length;
  /** 银行卡类型 */
  @javax.xml.bind.annotation.XmlElement(name="card_type")
  private java.lang.String card_type;
  /** 银行卡类型名称 */
  @javax.xml.bind.annotation.XmlElement(name="card_type_name")
  private java.lang.String card_type_name;
  /** id */
  @javax.xml.bind.annotation.XmlElement(name="issu_id")
  private java.lang.String issu_id;
  /** 名称 */
  @javax.xml.bind.annotation.XmlElement(name="issu_name")
  private java.lang.String issu_name;

  /**
   * Default constructor.  Note that this does not initialize fields
   * to their default values from the schema.  If that is desired then
   * one should use <code>newBuilder()</code>.
   */
  public BankCardBinExt() {}

  /**
   * All-args constructor.
   * @param id 主键
   * @param bank_code bank_code
   * @param bank_name bank_name
   * @param card_name card_name
   * @param description description
   * @param card_bin card_bin
   * @param card_length card_length
   * @param card_bin_length card_bin_length
   * @param card_type 银行卡类型
   * @param card_type_name 银行卡类型名称
   * @param issu_id id
   * @param issu_name 名称
   */
  public BankCardBinExt(java.lang.String id, java.lang.String bank_code, java.lang.String bank_name, java.lang.String card_name, java.lang.String description, java.lang.String card_bin, java.lang.String card_length, java.lang.String card_bin_length, java.lang.String card_type, java.lang.String card_type_name, java.lang.String issu_id, java.lang.String issu_name) {
    this.id = id;
    this.bank_code = bank_code;
    this.bank_name = bank_name;
    this.card_name = card_name;
    this.description = description;
    this.card_bin = card_bin;
    this.card_length = card_length;
    this.card_bin_length = card_bin_length;
    this.card_type = card_type;
    this.card_type_name = card_type_name;
    this.issu_id = issu_id;
    this.issu_name = issu_name;
  }

  @com.fasterxml.jackson.annotation.JsonIgnore
  @org.codehaus.jackson.annotate.JsonIgnore
  @javax.xml.bind.annotation.XmlTransient
  public org.apache.avro.Schema getSchema() { return SCHEMA$; }
  // Used by DatumWriter.  Applications should not call.
  public java.lang.Object get(int field$) {
    switch (field$) {
    case 0: return id;
    case 1: return bank_code;
    case 2: return bank_name;
    case 3: return card_name;
    case 4: return description;
    case 5: return card_bin;
    case 6: return card_length;
    case 7: return card_bin_length;
    case 8: return card_type;
    case 9: return card_type_name;
    case 10: return issu_id;
    case 11: return issu_name;
    default: throw new org.apache.avro.AvroRuntimeException("Bad index");
    }
  }

  // Used by DatumReader.  Applications should not call.
  @SuppressWarnings(value="unchecked")
  public void put(int field$, java.lang.Object value$) {
    switch (field$) {
    case 0: id = (java.lang.String)value$; break;
    case 1: bank_code = (java.lang.String)value$; break;
    case 2: bank_name = (java.lang.String)value$; break;
    case 3: card_name = (java.lang.String)value$; break;
    case 4: description = (java.lang.String)value$; break;
    case 5: card_bin = (java.lang.String)value$; break;
    case 6: card_length = (java.lang.String)value$; break;
    case 7: card_bin_length = (java.lang.String)value$; break;
    case 8: card_type = (java.lang.String)value$; break;
    case 9: card_type_name = (java.lang.String)value$; break;
    case 10: issu_id = (java.lang.String)value$; break;
    case 11: issu_name = (java.lang.String)value$; break;
    default: throw new org.apache.avro.AvroRuntimeException("Bad index");
    }
  }

  /**
   * Gets the value of the 'id' field.
   * @return 主键
   */
  @com.fasterxml.jackson.annotation.JsonGetter("id")
  @org.codehaus.jackson.annotate.JsonProperty("id")   
  @com.fasterxml.jackson.annotation.JsonInclude(Include.NON_NULL)
  @org.codehaus.jackson.map.annotate.JsonSerialize(include=Inclusion.NON_NULL)  
  @javax.xml.bind.annotation.XmlTransient
  public java.lang.String getId() {
    return id;
  }

  /**
   * Sets the value of the 'id' field.
   * 主键
   * @param value the value to set.
   */
  protected void setId(java.lang.String value) {
    this.id = value;
  }

  /**
   * Gets the value of the 'bank_code' field.
   * @return bank_code
   */
  @com.fasterxml.jackson.annotation.JsonGetter("bank_code")
  @org.codehaus.jackson.annotate.JsonProperty("bank_code")   
  @com.fasterxml.jackson.annotation.JsonInclude(Include.NON_NULL)
  @org.codehaus.jackson.map.annotate.JsonSerialize(include=Inclusion.NON_NULL)  
  @javax.xml.bind.annotation.XmlTransient
  public java.lang.String getBankCode() {
    return bank_code;
  }

  /**
   * Sets the value of the 'bank_code' field.
   * bank_code
   * @param value the value to set.
   */
  protected void setBankCode(java.lang.String value) {
    this.bank_code = value;
  }

  /**
   * Gets the value of the 'bank_name' field.
   * @return bank_name
   */
  @com.fasterxml.jackson.annotation.JsonGetter("bank_name")
  @org.codehaus.jackson.annotate.JsonProperty("bank_name")   
  @com.fasterxml.jackson.annotation.JsonInclude(Include.NON_NULL)
  @org.codehaus.jackson.map.annotate.JsonSerialize(include=Inclusion.NON_NULL)  
  @javax.xml.bind.annotation.XmlTransient
  public java.lang.String getBankName() {
    return bank_name;
  }

  /**
   * Sets the value of the 'bank_name' field.
   * bank_name
   * @param value the value to set.
   */
  protected void setBankName(java.lang.String value) {
    this.bank_name = value;
  }

  /**
   * Gets the value of the 'card_name' field.
   * @return card_name
   */
  @com.fasterxml.jackson.annotation.JsonGetter("card_name")
  @org.codehaus.jackson.annotate.JsonProperty("card_name")   
  @com.fasterxml.jackson.annotation.JsonInclude(Include.NON_NULL)
  @org.codehaus.jackson.map.annotate.JsonSerialize(include=Inclusion.NON_NULL)  
  @javax.xml.bind.annotation.XmlTransient
  public java.lang.String getCardName() {
    return card_name;
  }

  /**
   * Sets the value of the 'card_name' field.
   * card_name
   * @param value the value to set.
   */
  protected void setCardName(java.lang.String value) {
    this.card_name = value;
  }

  /**
   * Gets the value of the 'description' field.
   * @return description
   */
  @com.fasterxml.jackson.annotation.JsonGetter("description")
  @org.codehaus.jackson.annotate.JsonProperty("description")   
  @com.fasterxml.jackson.annotation.JsonInclude(Include.NON_NULL)
  @org.codehaus.jackson.map.annotate.JsonSerialize(include=Inclusion.NON_NULL)  
  @javax.xml.bind.annotation.XmlTransient
  public java.lang.String getDescription() {
    return description;
  }

  /**
   * Sets the value of the 'description' field.
   * description
   * @param value the value to set.
   */
  protected void setDescription(java.lang.String value) {
    this.description = value;
  }

  /**
   * Gets the value of the 'card_bin' field.
   * @return card_bin
   */
  @com.fasterxml.jackson.annotation.JsonGetter("card_bin")
  @org.codehaus.jackson.annotate.JsonProperty("card_bin")   
  @com.fasterxml.jackson.annotation.JsonInclude(Include.NON_NULL)
  @org.codehaus.jackson.map.annotate.JsonSerialize(include=Inclusion.NON_NULL)  
  @javax.xml.bind.annotation.XmlTransient
  public java.lang.String getCardBin() {
    return card_bin;
  }

  /**
   * Sets the value of the 'card_bin' field.
   * card_bin
   * @param value the value to set.
   */
  protected void setCardBin(java.lang.String value) {
    this.card_bin = value;
  }

  /**
   * Gets the value of the 'card_length' field.
   * @return card_length
   */
  @com.fasterxml.jackson.annotation.JsonGetter("card_length")
  @org.codehaus.jackson.annotate.JsonProperty("card_length")   
  @com.fasterxml.jackson.annotation.JsonInclude(Include.NON_NULL)
  @org.codehaus.jackson.map.annotate.JsonSerialize(include=Inclusion.NON_NULL)  
  @javax.xml.bind.annotation.XmlTransient
  public java.lang.String getCardLength() {
    return card_length;
  }

  /**
   * Sets the value of the 'card_length' field.
   * card_length
   * @param value the value to set.
   */
  protected void setCardLength(java.lang.String value) {
    this.card_length = value;
  }

  /**
   * Gets the value of the 'card_bin_length' field.
   * @return card_bin_length
   */
  @com.fasterxml.jackson.annotation.JsonGetter("card_bin_length")
  @org.codehaus.jackson.annotate.JsonProperty("card_bin_length")   
  @com.fasterxml.jackson.annotation.JsonInclude(Include.NON_NULL)
  @org.codehaus.jackson.map.annotate.JsonSerialize(include=Inclusion.NON_NULL)  
  @javax.xml.bind.annotation.XmlTransient
  public java.lang.String getCardBinLength() {
    return card_bin_length;
  }

  /**
   * Sets the value of the 'card_bin_length' field.
   * card_bin_length
   * @param value the value to set.
   */
  protected void setCardBinLength(java.lang.String value) {
    this.card_bin_length = value;
  }

  /**
   * Gets the value of the 'card_type' field.
   * @return 银行卡类型
   */
  @com.fasterxml.jackson.annotation.JsonGetter("card_type")
  @org.codehaus.jackson.annotate.JsonProperty("card_type")   
  @com.fasterxml.jackson.annotation.JsonInclude(Include.NON_NULL)
  @org.codehaus.jackson.map.annotate.JsonSerialize(include=Inclusion.NON_NULL)  
  @javax.xml.bind.annotation.XmlTransient
  public java.lang.String getCardType() {
    return card_type;
  }

  /**
   * Sets the value of the 'card_type' field.
   * 银行卡类型
   * @param value the value to set.
   */
  protected void setCardType(java.lang.String value) {
    this.card_type = value;
  }

  /**
   * Gets the value of the 'card_type_name' field.
   * @return 银行卡类型名称
   */
  @com.fasterxml.jackson.annotation.JsonGetter("card_type_name")
  @org.codehaus.jackson.annotate.JsonProperty("card_type_name")   
  @com.fasterxml.jackson.annotation.JsonInclude(Include.NON_NULL)
  @org.codehaus.jackson.map.annotate.JsonSerialize(include=Inclusion.NON_NULL)  
  @javax.xml.bind.annotation.XmlTransient
  public java.lang.String getCardTypeName() {
    return card_type_name;
  }

  /**
   * Sets the value of the 'card_type_name' field.
   * 银行卡类型名称
   * @param value the value to set.
   */
  protected void setCardTypeName(java.lang.String value) {
    this.card_type_name = value;
  }

  /**
   * Gets the value of the 'issu_id' field.
   * @return id
   */
  @com.fasterxml.jackson.annotation.JsonGetter("issu_id")
  @org.codehaus.jackson.annotate.JsonProperty("issu_id")   
  @com.fasterxml.jackson.annotation.JsonInclude(Include.NON_NULL)
  @org.codehaus.jackson.map.annotate.JsonSerialize(include=Inclusion.NON_NULL)  
  @javax.xml.bind.annotation.XmlTransient
  public java.lang.String getIssuId() {
    return issu_id;
  }

  /**
   * Sets the value of the 'issu_id' field.
   * id
   * @param value the value to set.
   */
  protected void setIssuId(java.lang.String value) {
    this.issu_id = value;
  }

  /**
   * Gets the value of the 'issu_name' field.
   * @return 名称
   */
  @com.fasterxml.jackson.annotation.JsonGetter("issu_name")
  @org.codehaus.jackson.annotate.JsonProperty("issu_name")   
  @com.fasterxml.jackson.annotation.JsonInclude(Include.NON_NULL)
  @org.codehaus.jackson.map.annotate.JsonSerialize(include=Inclusion.NON_NULL)  
  @javax.xml.bind.annotation.XmlTransient
  public java.lang.String getIssuName() {
    return issu_name;
  }

  /**
   * Sets the value of the 'issu_name' field.
   * 名称
   * @param value the value to set.
   */
  protected void setIssuName(java.lang.String value) {
    this.issu_name = value;
  }

  /**
   * Creates a new BankCardBinExt RecordBuilder.
   * @return A new BankCardBinExt RecordBuilder
   */
  public static org.xiajinsuo.epay.business.BankCardBinExt.Builder newBuilder() {
    return new org.xiajinsuo.epay.business.BankCardBinExt.Builder();
  }

  /**
   * Creates a new BankCardBinExt RecordBuilder by copying an existing Builder.
   * @param other The existing builder to copy.
   * @return A new BankCardBinExt RecordBuilder
   */
  public static org.xiajinsuo.epay.business.BankCardBinExt.Builder newBuilder(org.xiajinsuo.epay.business.BankCardBinExt.Builder other) {
    return new org.xiajinsuo.epay.business.BankCardBinExt.Builder(other);
  }

  /**
   * Creates a new BankCardBinExt RecordBuilder by copying an existing BankCardBinExt instance.
   * @param other The existing instance to copy.
   * @return A new BankCardBinExt RecordBuilder
   */
  public static org.xiajinsuo.epay.business.BankCardBinExt.Builder newBuilder(org.xiajinsuo.epay.business.BankCardBinExt other) {
    return new org.xiajinsuo.epay.business.BankCardBinExt.Builder(other);
  }

  /**
   * RecordBuilder for BankCardBinExt instances.
   */
  public static class Builder extends org.apache.avro.specific.SpecificRecordBuilderBase<BankCardBinExt>
    implements org.apache.avro.data.RecordBuilder<BankCardBinExt> {

    /** 主键 */
    private java.lang.String id;
    /** bank_code */
    private java.lang.String bank_code;
    /** bank_name */
    private java.lang.String bank_name;
    /** card_name */
    private java.lang.String card_name;
    /** description */
    private java.lang.String description;
    /** card_bin */
    private java.lang.String card_bin;
    /** card_length */
    private java.lang.String card_length;
    /** card_bin_length */
    private java.lang.String card_bin_length;
    /** 银行卡类型 */
    private java.lang.String card_type;
    /** 银行卡类型名称 */
    private java.lang.String card_type_name;
    /** id */
    private java.lang.String issu_id;
    /** 名称 */
    private java.lang.String issu_name;

    /** Creates a new Builder */
    private Builder() {
      super(SCHEMA$);
    }

    /**
     * Creates a Builder by copying an existing Builder.
     * @param other The existing Builder to copy.
     */
    private Builder(org.xiajinsuo.epay.business.BankCardBinExt.Builder other) {
      super(other);
      if (isValidValue(fields()[0], other.id)) {
        this.id = data().deepCopy(fields()[0].schema(), other.id);
        fieldSetFlags()[0] = true;
      }
      if (isValidValue(fields()[1], other.bank_code)) {
        this.bank_code = data().deepCopy(fields()[1].schema(), other.bank_code);
        fieldSetFlags()[1] = true;
      }
      if (isValidValue(fields()[2], other.bank_name)) {
        this.bank_name = data().deepCopy(fields()[2].schema(), other.bank_name);
        fieldSetFlags()[2] = true;
      }
      if (isValidValue(fields()[3], other.card_name)) {
        this.card_name = data().deepCopy(fields()[3].schema(), other.card_name);
        fieldSetFlags()[3] = true;
      }
      if (isValidValue(fields()[4], other.description)) {
        this.description = data().deepCopy(fields()[4].schema(), other.description);
        fieldSetFlags()[4] = true;
      }
      if (isValidValue(fields()[5], other.card_bin)) {
        this.card_bin = data().deepCopy(fields()[5].schema(), other.card_bin);
        fieldSetFlags()[5] = true;
      }
      if (isValidValue(fields()[6], other.card_length)) {
        this.card_length = data().deepCopy(fields()[6].schema(), other.card_length);
        fieldSetFlags()[6] = true;
      }
      if (isValidValue(fields()[7], other.card_bin_length)) {
        this.card_bin_length = data().deepCopy(fields()[7].schema(), other.card_bin_length);
        fieldSetFlags()[7] = true;
      }
      if (isValidValue(fields()[8], other.card_type)) {
        this.card_type = data().deepCopy(fields()[8].schema(), other.card_type);
        fieldSetFlags()[8] = true;
      }
      if (isValidValue(fields()[9], other.card_type_name)) {
        this.card_type_name = data().deepCopy(fields()[9].schema(), other.card_type_name);
        fieldSetFlags()[9] = true;
      }
      if (isValidValue(fields()[10], other.issu_id)) {
        this.issu_id = data().deepCopy(fields()[10].schema(), other.issu_id);
        fieldSetFlags()[10] = true;
      }
      if (isValidValue(fields()[11], other.issu_name)) {
        this.issu_name = data().deepCopy(fields()[11].schema(), other.issu_name);
        fieldSetFlags()[11] = true;
      }
    }

    /**
     * Creates a Builder by copying an existing BankCardBinExt instance
     * @param other The existing instance to copy.
     */
    private Builder(org.xiajinsuo.epay.business.BankCardBinExt other) {
            super(SCHEMA$);
      if (isValidValue(fields()[0], other.id)) {
        this.id = data().deepCopy(fields()[0].schema(), other.id);
        fieldSetFlags()[0] = true;
      }
      if (isValidValue(fields()[1], other.bank_code)) {
        this.bank_code = data().deepCopy(fields()[1].schema(), other.bank_code);
        fieldSetFlags()[1] = true;
      }
      if (isValidValue(fields()[2], other.bank_name)) {
        this.bank_name = data().deepCopy(fields()[2].schema(), other.bank_name);
        fieldSetFlags()[2] = true;
      }
      if (isValidValue(fields()[3], other.card_name)) {
        this.card_name = data().deepCopy(fields()[3].schema(), other.card_name);
        fieldSetFlags()[3] = true;
      }
      if (isValidValue(fields()[4], other.description)) {
        this.description = data().deepCopy(fields()[4].schema(), other.description);
        fieldSetFlags()[4] = true;
      }
      if (isValidValue(fields()[5], other.card_bin)) {
        this.card_bin = data().deepCopy(fields()[5].schema(), other.card_bin);
        fieldSetFlags()[5] = true;
      }
      if (isValidValue(fields()[6], other.card_length)) {
        this.card_length = data().deepCopy(fields()[6].schema(), other.card_length);
        fieldSetFlags()[6] = true;
      }
      if (isValidValue(fields()[7], other.card_bin_length)) {
        this.card_bin_length = data().deepCopy(fields()[7].schema(), other.card_bin_length);
        fieldSetFlags()[7] = true;
      }
      if (isValidValue(fields()[8], other.card_type)) {
        this.card_type = data().deepCopy(fields()[8].schema(), other.card_type);
        fieldSetFlags()[8] = true;
      }
      if (isValidValue(fields()[9], other.card_type_name)) {
        this.card_type_name = data().deepCopy(fields()[9].schema(), other.card_type_name);
        fieldSetFlags()[9] = true;
      }
      if (isValidValue(fields()[10], other.issu_id)) {
        this.issu_id = data().deepCopy(fields()[10].schema(), other.issu_id);
        fieldSetFlags()[10] = true;
      }
      if (isValidValue(fields()[11], other.issu_name)) {
        this.issu_name = data().deepCopy(fields()[11].schema(), other.issu_name);
        fieldSetFlags()[11] = true;
      }
    }

    /**
      * Gets the value of the 'id' field.
      * 主键
      * @return The value.
      */
    public java.lang.String getId() {
      return id;
    }

    /**
      * Sets the value of the 'id' field.
      * 主键
      * @param value The value of 'id'.
      * @return This builder.
      */
    public org.xiajinsuo.epay.business.BankCardBinExt.Builder setId(java.lang.String value) {
      validate(fields()[0], value);
      this.id = value;
      fieldSetFlags()[0] = true;
      return this;
    }

    /**
      * Checks whether the 'id' field has been set.
      * 主键
      * @return True if the 'id' field has been set, false otherwise.
      */
    public boolean hasId() {
      return fieldSetFlags()[0];
    }


    /**
      * Clears the value of the 'id' field.
      * 主键
      * @return This builder.
      */
    public org.xiajinsuo.epay.business.BankCardBinExt.Builder clearId() {
      id = null;
      fieldSetFlags()[0] = false;
      return this;
    }

    /**
      * Gets the value of the 'bank_code' field.
      * bank_code
      * @return The value.
      */
    public java.lang.String getBankCode() {
      return bank_code;
    }

    /**
      * Sets the value of the 'bank_code' field.
      * bank_code
      * @param value The value of 'bank_code'.
      * @return This builder.
      */
    public org.xiajinsuo.epay.business.BankCardBinExt.Builder setBankCode(java.lang.String value) {
      validate(fields()[1], value);
      this.bank_code = value;
      fieldSetFlags()[1] = true;
      return this;
    }

    /**
      * Checks whether the 'bank_code' field has been set.
      * bank_code
      * @return True if the 'bank_code' field has been set, false otherwise.
      */
    public boolean hasBankCode() {
      return fieldSetFlags()[1];
    }


    /**
      * Clears the value of the 'bank_code' field.
      * bank_code
      * @return This builder.
      */
    public org.xiajinsuo.epay.business.BankCardBinExt.Builder clearBankCode() {
      bank_code = null;
      fieldSetFlags()[1] = false;
      return this;
    }

    /**
      * Gets the value of the 'bank_name' field.
      * bank_name
      * @return The value.
      */
    public java.lang.String getBankName() {
      return bank_name;
    }

    /**
      * Sets the value of the 'bank_name' field.
      * bank_name
      * @param value The value of 'bank_name'.
      * @return This builder.
      */
    public org.xiajinsuo.epay.business.BankCardBinExt.Builder setBankName(java.lang.String value) {
      validate(fields()[2], value);
      this.bank_name = value;
      fieldSetFlags()[2] = true;
      return this;
    }

    /**
      * Checks whether the 'bank_name' field has been set.
      * bank_name
      * @return True if the 'bank_name' field has been set, false otherwise.
      */
    public boolean hasBankName() {
      return fieldSetFlags()[2];
    }


    /**
      * Clears the value of the 'bank_name' field.
      * bank_name
      * @return This builder.
      */
    public org.xiajinsuo.epay.business.BankCardBinExt.Builder clearBankName() {
      bank_name = null;
      fieldSetFlags()[2] = false;
      return this;
    }

    /**
      * Gets the value of the 'card_name' field.
      * card_name
      * @return The value.
      */
    public java.lang.String getCardName() {
      return card_name;
    }

    /**
      * Sets the value of the 'card_name' field.
      * card_name
      * @param value The value of 'card_name'.
      * @return This builder.
      */
    public org.xiajinsuo.epay.business.BankCardBinExt.Builder setCardName(java.lang.String value) {
      validate(fields()[3], value);
      this.card_name = value;
      fieldSetFlags()[3] = true;
      return this;
    }

    /**
      * Checks whether the 'card_name' field has been set.
      * card_name
      * @return True if the 'card_name' field has been set, false otherwise.
      */
    public boolean hasCardName() {
      return fieldSetFlags()[3];
    }


    /**
      * Clears the value of the 'card_name' field.
      * card_name
      * @return This builder.
      */
    public org.xiajinsuo.epay.business.BankCardBinExt.Builder clearCardName() {
      card_name = null;
      fieldSetFlags()[3] = false;
      return this;
    }

    /**
      * Gets the value of the 'description' field.
      * description
      * @return The value.
      */
    public java.lang.String getDescription() {
      return description;
    }

    /**
      * Sets the value of the 'description' field.
      * description
      * @param value The value of 'description'.
      * @return This builder.
      */
    public org.xiajinsuo.epay.business.BankCardBinExt.Builder setDescription(java.lang.String value) {
      validate(fields()[4], value);
      this.description = value;
      fieldSetFlags()[4] = true;
      return this;
    }

    /**
      * Checks whether the 'description' field has been set.
      * description
      * @return True if the 'description' field has been set, false otherwise.
      */
    public boolean hasDescription() {
      return fieldSetFlags()[4];
    }


    /**
      * Clears the value of the 'description' field.
      * description
      * @return This builder.
      */
    public org.xiajinsuo.epay.business.BankCardBinExt.Builder clearDescription() {
      description = null;
      fieldSetFlags()[4] = false;
      return this;
    }

    /**
      * Gets the value of the 'card_bin' field.
      * card_bin
      * @return The value.
      */
    public java.lang.String getCardBin() {
      return card_bin;
    }

    /**
      * Sets the value of the 'card_bin' field.
      * card_bin
      * @param value The value of 'card_bin'.
      * @return This builder.
      */
    public org.xiajinsuo.epay.business.BankCardBinExt.Builder setCardBin(java.lang.String value) {
      validate(fields()[5], value);
      this.card_bin = value;
      fieldSetFlags()[5] = true;
      return this;
    }

    /**
      * Checks whether the 'card_bin' field has been set.
      * card_bin
      * @return True if the 'card_bin' field has been set, false otherwise.
      */
    public boolean hasCardBin() {
      return fieldSetFlags()[5];
    }


    /**
      * Clears the value of the 'card_bin' field.
      * card_bin
      * @return This builder.
      */
    public org.xiajinsuo.epay.business.BankCardBinExt.Builder clearCardBin() {
      card_bin = null;
      fieldSetFlags()[5] = false;
      return this;
    }

    /**
      * Gets the value of the 'card_length' field.
      * card_length
      * @return The value.
      */
    public java.lang.String getCardLength() {
      return card_length;
    }

    /**
      * Sets the value of the 'card_length' field.
      * card_length
      * @param value The value of 'card_length'.
      * @return This builder.
      */
    public org.xiajinsuo.epay.business.BankCardBinExt.Builder setCardLength(java.lang.String value) {
      validate(fields()[6], value);
      this.card_length = value;
      fieldSetFlags()[6] = true;
      return this;
    }

    /**
      * Checks whether the 'card_length' field has been set.
      * card_length
      * @return True if the 'card_length' field has been set, false otherwise.
      */
    public boolean hasCardLength() {
      return fieldSetFlags()[6];
    }


    /**
      * Clears the value of the 'card_length' field.
      * card_length
      * @return This builder.
      */
    public org.xiajinsuo.epay.business.BankCardBinExt.Builder clearCardLength() {
      card_length = null;
      fieldSetFlags()[6] = false;
      return this;
    }

    /**
      * Gets the value of the 'card_bin_length' field.
      * card_bin_length
      * @return The value.
      */
    public java.lang.String getCardBinLength() {
      return card_bin_length;
    }

    /**
      * Sets the value of the 'card_bin_length' field.
      * card_bin_length
      * @param value The value of 'card_bin_length'.
      * @return This builder.
      */
    public org.xiajinsuo.epay.business.BankCardBinExt.Builder setCardBinLength(java.lang.String value) {
      validate(fields()[7], value);
      this.card_bin_length = value;
      fieldSetFlags()[7] = true;
      return this;
    }

    /**
      * Checks whether the 'card_bin_length' field has been set.
      * card_bin_length
      * @return True if the 'card_bin_length' field has been set, false otherwise.
      */
    public boolean hasCardBinLength() {
      return fieldSetFlags()[7];
    }


    /**
      * Clears the value of the 'card_bin_length' field.
      * card_bin_length
      * @return This builder.
      */
    public org.xiajinsuo.epay.business.BankCardBinExt.Builder clearCardBinLength() {
      card_bin_length = null;
      fieldSetFlags()[7] = false;
      return this;
    }

    /**
      * Gets the value of the 'card_type' field.
      * 银行卡类型
      * @return The value.
      */
    public java.lang.String getCardType() {
      return card_type;
    }

    /**
      * Sets the value of the 'card_type' field.
      * 银行卡类型
      * @param value The value of 'card_type'.
      * @return This builder.
      */
    public org.xiajinsuo.epay.business.BankCardBinExt.Builder setCardType(java.lang.String value) {
      validate(fields()[8], value);
      this.card_type = value;
      fieldSetFlags()[8] = true;
      return this;
    }

    /**
      * Checks whether the 'card_type' field has been set.
      * 银行卡类型
      * @return True if the 'card_type' field has been set, false otherwise.
      */
    public boolean hasCardType() {
      return fieldSetFlags()[8];
    }


    /**
      * Clears the value of the 'card_type' field.
      * 银行卡类型
      * @return This builder.
      */
    public org.xiajinsuo.epay.business.BankCardBinExt.Builder clearCardType() {
      card_type = null;
      fieldSetFlags()[8] = false;
      return this;
    }

    /**
      * Gets the value of the 'card_type_name' field.
      * 银行卡类型名称
      * @return The value.
      */
    public java.lang.String getCardTypeName() {
      return card_type_name;
    }

    /**
      * Sets the value of the 'card_type_name' field.
      * 银行卡类型名称
      * @param value The value of 'card_type_name'.
      * @return This builder.
      */
    public org.xiajinsuo.epay.business.BankCardBinExt.Builder setCardTypeName(java.lang.String value) {
      validate(fields()[9], value);
      this.card_type_name = value;
      fieldSetFlags()[9] = true;
      return this;
    }

    /**
      * Checks whether the 'card_type_name' field has been set.
      * 银行卡类型名称
      * @return True if the 'card_type_name' field has been set, false otherwise.
      */
    public boolean hasCardTypeName() {
      return fieldSetFlags()[9];
    }


    /**
      * Clears the value of the 'card_type_name' field.
      * 银行卡类型名称
      * @return This builder.
      */
    public org.xiajinsuo.epay.business.BankCardBinExt.Builder clearCardTypeName() {
      card_type_name = null;
      fieldSetFlags()[9] = false;
      return this;
    }

    /**
      * Gets the value of the 'issu_id' field.
      * id
      * @return The value.
      */
    public java.lang.String getIssuId() {
      return issu_id;
    }

    /**
      * Sets the value of the 'issu_id' field.
      * id
      * @param value The value of 'issu_id'.
      * @return This builder.
      */
    public org.xiajinsuo.epay.business.BankCardBinExt.Builder setIssuId(java.lang.String value) {
      validate(fields()[10], value);
      this.issu_id = value;
      fieldSetFlags()[10] = true;
      return this;
    }

    /**
      * Checks whether the 'issu_id' field has been set.
      * id
      * @return True if the 'issu_id' field has been set, false otherwise.
      */
    public boolean hasIssuId() {
      return fieldSetFlags()[10];
    }


    /**
      * Clears the value of the 'issu_id' field.
      * id
      * @return This builder.
      */
    public org.xiajinsuo.epay.business.BankCardBinExt.Builder clearIssuId() {
      issu_id = null;
      fieldSetFlags()[10] = false;
      return this;
    }

    /**
      * Gets the value of the 'issu_name' field.
      * 名称
      * @return The value.
      */
    public java.lang.String getIssuName() {
      return issu_name;
    }

    /**
      * Sets the value of the 'issu_name' field.
      * 名称
      * @param value The value of 'issu_name'.
      * @return This builder.
      */
    public org.xiajinsuo.epay.business.BankCardBinExt.Builder setIssuName(java.lang.String value) {
      validate(fields()[11], value);
      this.issu_name = value;
      fieldSetFlags()[11] = true;
      return this;
    }

    /**
      * Checks whether the 'issu_name' field has been set.
      * 名称
      * @return True if the 'issu_name' field has been set, false otherwise.
      */
    public boolean hasIssuName() {
      return fieldSetFlags()[11];
    }


    /**
      * Clears the value of the 'issu_name' field.
      * 名称
      * @return This builder.
      */
    public org.xiajinsuo.epay.business.BankCardBinExt.Builder clearIssuName() {
      issu_name = null;
      fieldSetFlags()[11] = false;
      return this;
    }

    @Override
    public BankCardBinExt build() {
      try {
        BankCardBinExt record = new BankCardBinExt();
        record.id = fieldSetFlags()[0] ? this.id : (java.lang.String) defaultValue(fields()[0]);
        record.bank_code = fieldSetFlags()[1] ? this.bank_code : (java.lang.String) defaultValue(fields()[1]);
        record.bank_name = fieldSetFlags()[2] ? this.bank_name : (java.lang.String) defaultValue(fields()[2]);
        record.card_name = fieldSetFlags()[3] ? this.card_name : (java.lang.String) defaultValue(fields()[3]);
        record.description = fieldSetFlags()[4] ? this.description : (java.lang.String) defaultValue(fields()[4]);
        record.card_bin = fieldSetFlags()[5] ? this.card_bin : (java.lang.String) defaultValue(fields()[5]);
        record.card_length = fieldSetFlags()[6] ? this.card_length : (java.lang.String) defaultValue(fields()[6]);
        record.card_bin_length = fieldSetFlags()[7] ? this.card_bin_length : (java.lang.String) defaultValue(fields()[7]);
        record.card_type = fieldSetFlags()[8] ? this.card_type : (java.lang.String) defaultValue(fields()[8]);
        record.card_type_name = fieldSetFlags()[9] ? this.card_type_name : (java.lang.String) defaultValue(fields()[9]);
        record.issu_id = fieldSetFlags()[10] ? this.issu_id : (java.lang.String) defaultValue(fields()[10]);
        record.issu_name = fieldSetFlags()[11] ? this.issu_name : (java.lang.String) defaultValue(fields()[11]);
        return record;
      } catch (Exception e) {
        throw new org.apache.avro.AvroRuntimeException(e);
      }
    }
  }

  private static final org.apache.avro.io.DatumWriter
    WRITER$ = new org.apache.avro.specific.SpecificDatumWriter(SCHEMA$);

  @Override public void writeExternal(java.io.ObjectOutput out)
    throws java.io.IOException {
    WRITER$.write(this, SpecificData.getEncoder(out));
  }

  private static final org.apache.avro.io.DatumReader
    READER$ = new org.apache.avro.specific.SpecificDatumReader(SCHEMA$);

  @Override public void readExternal(java.io.ObjectInput in)
    throws java.io.IOException {
    READER$.read(this, SpecificData.getDecoder(in));
  }

}
