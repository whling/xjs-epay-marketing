/**
 * Autogenerated by bestpay-maven-plugin
 * YOU CAN EDIT DIRECTLY
 *
 * @Copyright 2017 www.bestpay.io Inc. All rights reserved.
 */
package org.xiajinsuo.epay.business.protocol.rpc;

import io.bestpay.framework.base.CodeException;
import io.bestpay.framework.base.SpecificPage;
import io.bestpay.framework.base.UserRequest;
import org.xiajinsuo.epay.business.TransChannel;

import javax.validation.constraints.Min;
import java.util.List;

@SuppressWarnings("all")
/** 交易渠道  RpcProtocol */
@org.apache.avro.specific.AvroGenerated
public interface TransChannelRpcProtocol {
    /**
     * 通过id删除
     */
    public boolean delete(java.lang.String id, UserRequest userRequest) throws CodeException;

    /**
     * 更新
     */
    public TransChannel update(TransChannel e, UserRequest userRequest) throws CodeException;

    /**
     * 分页查询
     */
    public io.bestpay.framework.base.SpecificPage<TransChannel> query(UserRequest userRequest, @Min(1L) Long currentPage, @Min(1L) Long pageSize) throws CodeException;

    /**
     * 创建对像
     */
    public TransChannel create(TransChannel e, UserRequest userRequest) throws CodeException;

    /**
     * 通过id获取对像
     */
    public TransChannel get(java.lang.String id, UserRequest userRequest) throws CodeException;

    /**
     * 通过交易类型获取交易方式
     *
     * @param orgCode
     * @param transType
     * @return
     */
    TransChannel findByTransType(String orgCode, String transType, String code);

    /**
     * 获取支付类型列表
     *
     * @param orgCode
     * @param channel
     * @return
     */
    List<TransChannel> findBySubTransType(String orgCode, String channel);

    /**
     * 分页查询
     *
     * @param abstractUserRequest
     * @param orgStr
     * @param page
     * @param rows
     * @param transChannel
     * @return
     */
    SpecificPage<TransChannel> query(UserRequest userRequest, List<String> orgCodes, Long currentPage, Long pageSize, TransChannel transChannel) throws CodeException;

    /**
     * 删除禁用
     *
     * @param id
     * @param enable
     * @param updateTime
     * @param updateUser
     * @return
     */
    public boolean updateEnable(String id, Boolean enable, String updateTime, String updateUser);
}