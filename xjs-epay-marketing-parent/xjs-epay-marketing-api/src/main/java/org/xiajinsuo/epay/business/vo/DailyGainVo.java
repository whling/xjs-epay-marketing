package org.xiajinsuo.epay.business.vo;

import java.io.Serializable;

/**
 * Created by Administrator on 2017/4/12.
 * <p>
 * 分润明细
 */
public class DailyGainVo implements Serializable {
    private String day;
    private int totalAmount;
    private int totalBenefit;

    public DailyGainVo(String day, int totalAmount, int totalBenefit) {
        this.day = day;
        this.totalAmount = totalAmount;
        this.totalBenefit = totalBenefit;
    }

    public int getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(int totalAmount) {
        this.totalAmount = totalAmount;
    }

    public int getTotalBenefit() {
        return totalBenefit;
    }

    public void setTotalBenefit(int totalBenefit) {
        this.totalBenefit = totalBenefit;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }
}
