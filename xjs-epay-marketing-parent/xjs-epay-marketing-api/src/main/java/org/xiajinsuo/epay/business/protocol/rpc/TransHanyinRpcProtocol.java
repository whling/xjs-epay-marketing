/**
 * Autogenerated by bestpay-maven-plugin
 * YOU CAN EDIT DIRECTLY
 *
 * @Copyright 2017 www.bestpay.io Inc. All rights reserved.
 *
 */
package org.xiajinsuo.epay.business.protocol.rpc;

import java.io.Serializable;
import java.util.Map;
import javax.validation.constraints.Min;
import org.xiajinsuo.epay.business.TransHanyin;
import io.bestpay.framework.base.CodeException;
import io.bestpay.framework.base.UserRequest;

@SuppressWarnings("all")
/** 瀚银提现记录表  RpcProtocol */
@org.apache.avro.specific.AvroGenerated
public interface TransHanyinRpcProtocol {
	/** 通过id删除 */
	public boolean delete(java.lang.String id, UserRequest userRequest) throws CodeException;

	/** 更新 */
	public TransHanyin update(TransHanyin e, UserRequest userRequest) throws CodeException;

	/** 分页查询 */
	public io.bestpay.framework.base.SpecificPage<TransHanyin> query(UserRequest userRequest, @Min(1L) Long currentPage, @Min(1L) Long pageSize) throws CodeException;

	/** 创建对像 */
	public TransHanyin create(TransHanyin e, UserRequest userRequest) throws CodeException;

	/** 通过id获取对像 */
	public TransHanyin get(java.lang.String id, UserRequest userRequest) throws CodeException;
}