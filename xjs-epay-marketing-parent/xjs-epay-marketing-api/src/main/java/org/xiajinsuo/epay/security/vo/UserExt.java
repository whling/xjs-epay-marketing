package org.xiajinsuo.epay.security.vo;

import org.xiajinsuo.epay.security.User;

/**
 * Created by tums on 2017/6/23.
 */
public class UserExt extends User {
    /**
     * 实时分润
     */
    private String realRete;

    public String getRealRete() {
        return realRete;
    }

    public void setRealRete(String realRete) {
        this.realRete = realRete;
    }
}
